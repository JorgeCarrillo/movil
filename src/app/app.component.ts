import { UsuarioService } from './services/usuario.service';
import { Component } from '@angular/core';
// import { Router } from '@angular/router';

import { Platform, MenuController, NavController, } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { TranslateProvider } from './providers/translate/translate.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { Zoom } from '@ionic-native/zoom';
import { Pages } from './interfaces/pages';
import { AuthService } from './services/auth.service';
import { PropertyService } from './providers/property/property.service';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public appPages = [];
  public appPages1: Array<Pages>;
  public appPages2: Array<Pages>;
  nombreusuario: string;
  apellidousuario: string;
  tipousuario: string;
  picture: any;
  configuracion
  fecha = new Date()
  usuario
  constructor(
    private platform: Platform,
    private menu: MenuController,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateProvider,
    private translateService: TranslateService,
    private readonly _authService: AuthService,
    public usuarioService: UsuarioService,
    public service: PropertyService,
    private readonly _httpClient: HttpClient,
    public navCtrl: NavController,

    //public router: Router
  ) {
    this._authService.actualizarAulasZoom()
    this.initializeApp();

    this.appPages1 =
      [
        {
          title: 'Inicio',
          url: '/home-results',
          direct: 'root',
          icon: 'browsers'
        },

        {
          title: 'Mis Tutorías',
          url: '/invoices',
          direct: 'forward',
          icon: 'list-box'
        },

        {
          title: 'Favoritos',
          url: '/favorites',
          direct: 'forward',
          icon: 'heart'
        },

        {
          title: 'Mensajes',
          url: '/messages',
          direct: 'forward',
          icon: 'mail'
        },
        {
          title: 'Soporte',
          url: '/support',
          direct: 'forward',
          icon: 'help-buoy'
        },
        {
          title: 'Configuraciones',
          url: '/settings',
          direct: 'forward',
          icon: 'cog'
        },
      ];
    this.appPages2 =
      [
        {
          title: 'Inicio',
          url: '/home-results',
          direct: 'root',
          icon: 'browsers'

        },

        {
          title: 'Registrar',
          url: '/register',
          direct: 'root',
          icon: 'create'
        },

        {
          title: 'Principal',
          url: '/',
          direct: 'root',
          icon: 'photos'
        },

        {
          title: 'Soporte',
          url: '/support',
          direct: 'forward',
          icon: 'help-buoy'
        },


      ];
  }
  async verificarenlinea() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    if (this._authService.getIdUsuario() != null) {
      this.service.obtenerusuario(this._authService.getIdUsuario()).subscribe(res => {

        this.usuario = res
        this._authService.conexionApagada = this.usuario.estado_usuario_cuenta
      })
    }

  }
  async enLinea() {
    this.fecha = new Date()
    if (!this._authService.conexionApagada) {
      this._authService.conexionApagada = true
      this._authService.enLinea(new Date(this.fecha.setMinutes(this.fecha.getMinutes() + 30)).toISOString(), true).subscribe(res => {
      })
    } else {
      this._authService.enLinea(new Date(this.fecha.setMinutes(this.fecha.getMinutes() - 30)).toISOString(), false).subscribe(res => {
      })
      this._authService.conexionApagada = false
    }
    this.menu.close()
  }
  initializeApp() {
    this.obtenerdatos();
    this.configuraciones()
    this.verificarenlinea()
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 1000);
      // this.splashScreen.hide();
      // Set language of the app.
      this.translateService.setDefaultLang(environment.language);
      this.translateService.use(environment.language);
      this.translateService.getTranslation(environment.language).subscribe(translations => {
        this.translate.setTranslations(translations);
      });
    }).catch(() => {
      // Set language of the app.
      this.translateService.setDefaultLang(environment.language);
      this.translateService.use(environment.language);
      this.translateService.getTranslation(environment.language).subscribe(translations => {
        this.translate.setTranslations(translations);
      });
    });
  }

  obtenerdatos() {
    /*obtener nombres de usuario*/
    if (this._authService.getNombres() === null && this._authService.getApellidos() === null) {
      this.nombreusuario = 'BIENVENIDO',
        this.apellidousuario = ''
    } else {
      this.nombreusuario = this._authService.getNombres()
      this.apellidousuario = this._authService.getApellidos()
    }
    /*fin nombres de usuario*/
    /*obtener Rol de usuario*/
    if (this._authService.getRol() === null) {
      this.tipousuario = 'CLIENTE'
    } else {
      this.tipousuario = this._authService.getRol()
      this._authService.getfoto()
    }

    let idusuario
    if (this._authService.getIdUsuario() != null) {
      idusuario = this._authService.getIdUsuario()
    }

    /*obtener foto de usuario*/
    if (this.tipousuario != 'CLIENTE') {

      this.service.traerimg().subscribe(res => {
        this.picture = res
      })
    }
    return this.nombreusuario + this.apellidousuario + this.tipousuario
  }

  configuraciones() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'configuracion', header)
      .subscribe(configuracion => {
        //for (let a of data) {}
        this.configuracion = configuracion


      }, error => {

      });
    return this.configuracion

  }

  closeMenu() {
    this.menu.close();
  }

  // goToEditProgile() {
  //   this.router.navigateByUrl('/edit-profile');
  // }

  //logout() {
  //  this.router.navigateByUrl('/login');
  //}

  async logOut(): Promise<void> {
    try {
      this._authService.enLinea(new Date(this.fecha.setMinutes(this.fecha.getMinutes() - 5)).toISOString(), false).subscribe(res => {
      })
      this.navCtrl.navigateForward('/login');
      localStorage.getItem('currentUser');
      localStorage.removeItem('currentUser');
      localStorage.clear();
      //window.location.replace('/login');
    } catch (error) {
      console.log(error);
    }
  }
}