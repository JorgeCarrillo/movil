import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EnvioCorreoServiceService {
  constructor(private readonly _httpClient: HttpClient,
    private router: Router) {
  }

  async postEnvioCreacion(correo): Promise<any> {
    try {
      return await this._httpClient.post(environment.url + 'sendemail/registro', correo)
        .toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));
    } catch (e) {
      console.log(e);
    }
  }

  async postEnvioCreacionMoodle(correo): Promise<any> {
    try {
      return await this._httpClient.post(environment.url + 'sendemail/moodle', correo)
        .toPromise()
        .then(result => result)
        .catch(error => {
          alert(error.error.message);
        });
    } catch (e) {
      console.log(e);
    }
  }



  async postEnvioInscripcionCurso(correo): Promise<any> {
    try {
      this._httpClient.post(environment.url + 'sendemail/registrocurso', correo).toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));
    } catch (e) {
      console.log(e);
    }

  }

  async postEnvioInscripcionCursoMoodle(correo): Promise<any> {
    try {
      this._httpClient.post(environment.url + 'sendemail/cursoMoodle', correo).toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));
    } catch (e) {
      console.log(e);
    }

  }

  async postEnvioMeetingId(correo): Promise<any> {
    try {
      this._httpClient.post(environment.url + 'sendemail/meeting', correo).toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));
    } catch (e) {
      console.log(e);
    }

  }

}
