import { Injectable } from '@angular/core';
import { UserInterface } from "../interfaces/user.interface";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";
import { Router } from "@angular/router";
import { EnvioCorreoServiceService } from "./envio-correo-service.service";
import { CuentaInterface } from "../interfaces/cuenta.interface";
import { dashCaseToCamelCase } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _authService: AuthService,
    private readonly _correo: EnvioCorreoServiceService,
    private router: Router,
  ) {
  }
  datos
  foto

  async createUser(cuenta: CuentaInterface, correo): Promise<any> {
    try {
      return await this._httpClient.post(environment.url + 'usuario/registro', cuenta)
        .toPromise()
        .then(result => result)
        .catch(error => {
          if (error.error.message.includes('nueva cuenta catch')) {
            this._correo.postEnvioInscripcionCursoMoodle({
              to: correo.nombre_login,
              curso: correo.curso,
              url: 'http://www.amautaec.education:3000/#/pages/auth/login',
            });
          } else {
            alert("err" + error.error.message);
          }
        });
    } catch (e) {
      console.log(e);
    }

  }

  async getAllUser(): Promise<any> {
    try {
      return await this._httpClient.get(environment.url + 'usuario',
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));
    } catch (e) {
      console.log(e);
    }
  }


  async getAllTeachers(): Promise<any> {
    try {
      return await this._httpClient.get(environment.url + 'usuario/profesores',
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));
    } catch (e) {
      console.log(e);
    }
  }

  async getUserById(id: number): Promise<any> {

    try {
      return await this._httpClient.get(environment.url + 'usuario/id/' + id,
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));
    } catch (e) {
      console.log(e);
    }
  }

  getById(id) {
    this.datos = []
    this._httpClient.get(environment.url + 'usuario/id/' + id,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(usus => {
        let usu;
        usu = usus
        this.datos.push({
          cedula: usu.identificacion_usuario,
          fechana: usu.fecha_naci_usuario,
          telefono: usu.telefono_usuario,
          direccion: usu.direccion_usuario,
          genero: usu.sexo_usuario,
        })

      }, error => {

      });
    return this.datos

  }



  updatePerfil(id, data) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.put(environment.url + 'usuario/' + id, JSON.stringify(data), header)
      .subscribe(usuario => {

      }, error => {

      });

  }



  async activeUser(id, token, rol?): Promise<any> {
    try {
      let activarCuenta: boolean = false;
      let activarUsuario: boolean = false;
      switch (rol) {
        case 'PROFESOR':
          activarUsuario = true;
          break;
        case 'ESTUDIANTE':
          activarCuenta = true;
          activarUsuario = true;
          break;
        default:
          alert('no existe ningun rol para este usuario');
          break;
      }

      return await this._httpClient.put(environment.url + 'usuario/' + id, {
        estado_usuario: activarUsuario,
        estado_usuario_cuenta: activarCuenta
      }, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        })
      })
        .toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));

    } catch (e) {
      console.log(e);
    }
  }

  async updateUser(usuario: UserInterface, id: number): Promise<any> {
    try {
      return await this._httpClient.put(environment.url + 'usuario/' + id, usuario, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
        .toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));
    } catch (e) {
      console.log(e);
    }
  }


  uploadFileUser(file: any): Observable<any> {
    const urlService = environment.url + 'usuario/upload-image';
    const formData: FormData = new FormData();

    formData.append('filename', file);
    return this._httpClient.post<any>(urlService, formData, { headers: new HttpHeaders({ Authorization: 'Bearer ' + this._authService.getToken() }) });
  }


}
