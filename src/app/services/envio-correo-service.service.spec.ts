import { TestBed } from '@angular/core/testing';

import { EnvioCorreoServiceService } from './envio-correo-service.service';

describe('EnvioCorreoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnvioCorreoServiceService = TestBed.get(EnvioCorreoServiceService);
    expect(service).toBeTruthy();
  });
});
