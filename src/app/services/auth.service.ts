import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserInterface } from "../interfaces/user.interface";
import { sha256 } from 'js-sha256';

import * as CryptoJS from 'crypto-js';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user$: Observable<UserInterface>;
  public usuarioRecuperar;
  conexionApagada
  constructor(

    private router: Router,
    private readonly _httpClient: HttpClient,
    public navCtrl: NavController,


  ) {

  }

  /*async loginGoogle(): Promise<User> {
      try {
          const {user} = await this.afAuth.signInWithPopup(
              new auth.GoogleAuthProvider()
          );
          console.log(user);
          this.updateUserData(user);
          return user;
      } catch (error) {
          console.log(error);
      }
  }*/

  async resetPassword(email: string): Promise<any> {
    try {
      const res = await this._httpClient.post(environment.url + 'sendemail/recovery', {
        nombre_login: email
      })
        .toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message));

      if (res) {
        alert('Revise su correo para poder recuperar la contraseña');
        this.router.navigate(['/pages/auth/login']);
      }

    } catch (error) {
      console.log(error);
    }
  }


  async login(email: string, password: string): Promise<any> {
    try {
      let token = await this._httpClient.post(environment.url + 'autenticacion', {
        nombre_login: email,
        password_login: sha256(password)
      }).toPromise()
        .then(result => result)
        .catch(error => {
          if (error.error.message.includes('Por favor ingresa una contraseña')) {
            this.router.navigate(['/pages/auth/register']);
          }
          alert(error.error.message);
        });
      return token;
    } catch (error) {
      console.log(error);
    }
  }

  actualizarAulasZoom() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.getToken()
      })
    }
    this._httpClient.get(environment.url + 'cuenta', header)
      .subscribe(rescuenta => {
        let cuentas;
        let body;
        let usuariosZoom;
        cuentas = rescuenta;

        if (cuentas.length == 0) {
          this._httpClient.get(environment.url + 'zoom-api/usuarios', header)
            .subscribe(resusuarios => {
              usuariosZoom = resusuarios;
              body = {
                nombre_cuenta: usuariosZoom.users[0].email,
                password_cuenta: " ",
                api_secret: " ",
                token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkJYRDlGSVJnVGpxSkQwemdhNjlVRkEiLCJleHAiOjE2OTQ0MzkxODQsImlhdCI6MTU5NDQzMTc4NH0.X81J2br2JGmKBfJlojteSs2djbzJeSwhyyPYgs402gg'",
                numero_cuentas: usuariosZoom.users.length - 1

              }

              this._httpClient.post(environment.url + 'cuenta', body, header)
                .subscribe(respostcuenta => {
                  let cuenta;
                  cuenta = respostcuenta
                  let pares = Math.ceil((usuariosZoom.users.length - 1) / 2)
                  let impares = Math.floor((usuariosZoom.users.length - 1) / 2)
                  let texto = []
                  for (let i = 0; i <= pares; i++) {
                    texto.push("Par")
                  }
                  for (let i = 0; i <= impares; i++) {
                    texto.push("Impar")
                  }
                  for (let i = 1; i < usuariosZoom.users.length; i++) {

                    body = {
                      nombre_usuario: usuariosZoom.users[i].email,
                      contraseña: "",
                      id_cuenta: cuenta.identifiers[0].id_cuenta_zoom,
                      id_usuario_zoom: usuariosZoom.users[i].id,
                      disponibilidad_hora: texto[i]
                    }
                    this._httpClient.post(environment.url + 'subcuenta-zoom', body, header)
                      .subscribe(res_sub_cuenta => {

                      }, error => {
                      });
                  }

                }, error => {
                });

            }, error => {
            });
        }
      }, error => {
      });


  }

  crearTokenRecuperar(body) {
    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.getToken()
        })
      }

      return this._httpClient.post(environment.url + 'usuario/recuperarcontrasenia', body, header)
        .subscribe(restoken => {
          observer.next(restoken)
        }, error => {
          observer.next(error)
        });
    });
  }
  actualizarContrasenia(body) {

    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.getToken()
        })
      }
      this._httpClient.post(environment.url + 'usuario/cambiarcontrasenia', body, header)
        .subscribe(rescontra => {
          observer.next(rescontra)

        }, error => {
          observer.error(error)
        });
    });
  }

  enLinea(fecha, valid) {
    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.getToken()
        })
      }
      this._httpClient.get(environment.url + "usuario/enlinea/'" + fecha + "'/" + this.getIdUsuario() + "/" + valid, header)
        .subscribe(resEnLinea => {
          observer.next(resEnLinea)

        }, error => {
          observer.error(error)
        });
      observer.next()
    })


  }
  /*async register(email: string, password: string): Promise<User> {
      try {
          const {user} = await this.afAuth.createUserWithEmailAndPassword(
              email,
              password
          );
          await this.sendVerificationEmail();
          return user;
      } catch (error) {
          console.log(error);
      }
  }*/

  async logout(): Promise<void> {
    try {
      localStorage.getItem('currentUser');
      localStorage.removeItem('currentUser');
      localStorage.clear();
      window.location.replace('/#/pages/auth/login');
    } catch (error) {
      console.log(error);
    }
  }

  /*private updateUserData(user: User) {
      const userRef: AngularFirestoreDocument<User> = this.afs.doc(
          `users/${user.id_usuario}`
      );

      const data: User = {
          uid: user.uid,
          correo_usuario: user.correo_usuario,
          foto_usuario: user.foto_usuario,
          role: 'ADMIN',
          emailVerified: user.emailVerified
      };

      return userRef.set(data, {merge: true});
  }*/


  setCurrentUser(tokenUser, decodeToken, user): void {
    if (decodeToken.data.picture == null) {
      localStorage.setItem('currentUser', JSON.stringify({
        token: tokenUser,
        id_login: CryptoJS.AES.encrypt(decodeToken.data.id_login.toString(), environment.tokenKey).toString(),
        id_usuario: CryptoJS.AES.encrypt(decodeToken.data.id_usuario.toString(), environment.tokenKey).toString(),
        nombre_login: CryptoJS.AES.encrypt(decodeToken.data.nombre_login.toString(), environment.tokenKey).toString(),
        rol: CryptoJS.AES.encrypt(decodeToken.data.rol.toString(), environment.tokenKey).toString(),
        estado_usuario: CryptoJS.AES.encrypt(decodeToken.data.estado.toString(), environment.tokenKey).toString(),
        estado_usuario_cuenta: CryptoJS.AES.encrypt(decodeToken.data.estado_cuenta.toString(), environment.tokenKey).toString(),
        nombres_usuario: CryptoJS.AES.encrypt(decodeToken.data.nombres.toString(), environment.tokenKey).toString(),
        apellidos_usuario: CryptoJS.AES.encrypt(decodeToken.data.apellidos.toString(), environment.tokenKey).toString(),
        id_usuario_rol: CryptoJS.AES.encrypt(decodeToken.data.id_usuario_rol.toString(), environment.tokenKey).toString(),
        usuario: user
      }));
    } else {
      localStorage.setItem('currentUser', JSON.stringify({
        token: tokenUser,
        id_login: CryptoJS.AES.encrypt(decodeToken.data.id_login.toString(), environment.tokenKey).toString(),
        id_usuario: CryptoJS.AES.encrypt(decodeToken.data.id_usuario.toString(), environment.tokenKey).toString(),
        nombre_login: CryptoJS.AES.encrypt(decodeToken.data.nombre_login.toString(), environment.tokenKey).toString(),
        rol: CryptoJS.AES.encrypt(decodeToken.data.rol.toString(), environment.tokenKey).toString(),
        estado_usuario: CryptoJS.AES.encrypt(decodeToken.data.estado.toString(), environment.tokenKey).toString(),
        estado_usuario_cuenta: CryptoJS.AES.encrypt(decodeToken.data.estado_cuenta.toString(), environment.tokenKey).toString(),
        nombres_usuario: CryptoJS.AES.encrypt(decodeToken.data.nombres.toString(), environment.tokenKey).toString(),
        apellidos_usuario: CryptoJS.AES.encrypt(decodeToken.data.apellidos.toString(), environment.tokenKey).toString(),
        id_usuario_rol: CryptoJS.AES.encrypt(decodeToken.data.id_usuario_rol.toString(), environment.tokenKey).toString(),
        usuario: user
      }));
    }

  }

  getToken() {
    if (localStorage.getItem('currentUser')) {
      var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).token, environment.tokenKey);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return null;
    }

  }

  setUsuario(usuario) {

    const user = JSON.parse(localStorage.getItem('currentUser'));
    user.usuario = usuario;
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  getUsuario() {
    /*var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).usuario, environment.tokenKey);
    return bytes.toString(CryptoJS.enc.Utf8);*/
    return JSON.parse(localStorage.getItem('currentUser')).usuario;
  }

  getIdUsuarioRol() {
    if (localStorage.getItem('currentUser')) {
      var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).id_usuario_rol, environment.tokenKey);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return null;
    }

  }

  getIdUsuario() {
    if (localStorage.getItem('currentUser')) {
      var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).id_usuario, environment.tokenKey);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return null;
    }

  }
  setUser(user) {
    localStorage.removeItem("usuario")
    localStorage.setItem("usuario", JSON.stringify(user[0]))
  }
  getIdLogin() {
    if (localStorage.getItem('currentUser')) {
      var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).id_login, environment.tokenKey);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return null;
    }
  }

  getNombreLogin() {
    if (localStorage.getItem('currentUser')) {
      var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).nombre_login, environment.tokenKey);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return null;
    }

  }

  getRol() {
    if (localStorage.getItem('currentUser')) {
      var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).rol, environment.tokenKey);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return null;
    }

  }

  getfoto() {
    let foto
    this._httpClient.get(environment.url + "usuario/idrol/" + this.getIdUsuarioRol(),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.getToken()
        })
      })
      .subscribe(img => {
        this.setUser(img)
        let imgu = img

        if (img[0].foto_usuario != null && img[0].foto_usuario != "") foto = environment.urlfotos + img[0].foto_usuario
        else foto = "https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-vector-avatar-icon-png-image_695765.jpg"
        return foto
      }, error => {

      });


  }

  getEstado() {
    if (localStorage.getItem('currentUser')) {
      var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).estado_usuario, environment.tokenKey);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return null;
    }

  }

  getEstadoCuenta() {
    if (localStorage.getItem('currentUser')) {
      var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).estado_usuario_cuenta, environment.tokenKey);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return null;
    }

  }

  getNombres() {

    if (localStorage.getItem('usuario')) {

      return JSON.parse(localStorage.getItem('usuario')).nombres_usuario
    } else {
      return null;
    }

  }
  getFotouser() {
    if (localStorage.getItem('usuario')) {

      return environment.urlfotos + JSON.parse(localStorage.getItem('usuario')).foto_usuario
    } else {
      return null;
    }

  }
  getApellidos() {
    if (localStorage.getItem('usuario')) {

      return JSON.parse(localStorage.getItem('usuario')).apellidos_usuario
    } else {
      return null;
    }

  }


  isLoggedIn(): boolean {
    if (localStorage.getItem('currentUser')) {
      const user = localStorage.getItem('currentUser');
      return (user !== null) ? true : false;
    } else {
      return null;
    }

  }

  /* logOut(): void {
    if (localStorage.getItem('currentUser')) {
      localStorage.getItem('currentUser');
      localStorage.removeItem('currentUser');
    } else {
      return null;
    }
  }*/
  async logOut(): Promise<void> {
    try {
      this.navCtrl.navigateForward('/login');
      localStorage.getItem('currentUser');
      localStorage.removeItem('currentUser');
      localStorage.clear();
      //window.location.replace('/login');
    } catch (error) {
      console.log(error);
    }
  }

}
