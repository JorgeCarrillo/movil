import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { PropertyService } from '../../providers/property/property.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  lang: any;
  enableNotifications: any;
  paymentMethod: any;
  currency: any;
  enablePromo: any;
  enableHistory: any;

  languages: any = ['English', 'Portuguese', 'French'];
  paymentMethods: any = ['Paypal', 'Tarjeta de Credito'];
  currencies: any = ['USD', 'BRL', 'EUR'];
  nombreusuario: string;
  apellidousuario: string;
  tipousuario: string;
  email: string;
  picture: any;
  fecha = new Date()

  constructor(
    public navCtrl: NavController,
    private readonly _authService: AuthService,
    public service: PropertyService
  ) { }

  ngOnInit(
  ) {
    this.obtenerdatos()
  }

  obtenerdatos() {
    /*obtener nombres de usuario*/
    if (this._authService.getNombres() === null && this._authService.getApellidos() === null) {
      this.nombreusuario = 'BIENVENIDO',
        this.apellidousuario = 'USUARIO'
    } else {
      this.nombreusuario = this._authService.getNombres()
      this.apellidousuario = this._authService.getApellidos()
    }
    /*fin nombres de usuario*/
    /*obtener Rol de usuario*/
    if (this._authService.getRol() === null) {
      this.tipousuario = 'CLIENTE'
    } else {
      this.tipousuario = this._authService.getRol()
    }
    /*fin rol de usuario*/
    /*obtener correo de usuario*/
    if (this._authService.getNombreLogin() === null) {
      this.email = 'null'
    } else {
      this.email = this._authService.getNombreLogin()
    }

    this.service.traerimg().subscribe(res => {
      this.picture = res
    })
  }

  editProfile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  /*async logout(): Promise<void> {
    try {
      localStorage.getItem('currentUser');
      localStorage.removeItem('currentUser');
      localStorage.clear();
      //window.location.replace('/#/pages/auth/login');
      window.location.replace('/login');
    } catch (error) {
      console.log(error);
    }
  }*/
  async logout(): Promise<void> {
    try {
      this._authService.enLinea(new Date(this.fecha.setMinutes(this.fecha.getMinutes() - 5)).toISOString(), false).subscribe(res => {
      })
      this.navCtrl.navigateForward('/login');
      localStorage.getItem('currentUser');
      localStorage.removeItem('currentUser');
      localStorage.clear();
      //window.location.replace('/login');
    } catch (error) {
      console.log(error);
    }
  }

}
