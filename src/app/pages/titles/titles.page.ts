import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { AuthService } from '../../services/auth.service';
import { AlertController } from '@ionic/angular';
import {
  PropertyService,
  // BrokerService,
} from '../../providers';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';

@Component({
  selector: 'app-titles',
  templateUrl: './titles.page.html',
  styleUrls: ['./titles.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
        query(':enter', stagger('300ms', [animate('500ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class TitlesPage implements OnInit {
  nombre;
  nivel;
  titulos;
  properID: any;
  constructor(
    private propertyService: PropertyService,
    public route: ActivatedRoute,
    private readonly _authService: AuthService,
    private _httpClient: HttpClient,
    public navCtrl: NavController,
    public alertController: AlertController,
  ) {
    this.obtenerdatos()
    this.traertitulos(this.properID)
  }

  ngOnInit() {
  }

  async obtenerdatos() {

    if (this._authService.getIdUsuarioRol() === null) {
      this.properID = 'null'
    } else {
      this.properID = this._authService.getIdUsuarioRol()
    }

  }
  traertitulos(properID) {
    this.titulos = []
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'titulos-profesor/usuario/' + properID, header)
      .subscribe(restitulos => {
        this.titulos = restitulos;
      }, error => {
      });

  }


  async updateTitulos(data1) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    let alert = await this.alertController.create({
      header: 'Editar Titulo',
      message: 'Escriba su nuevo titulo para actualizar.',
      inputs: [
        { name: 'senescyt_titulos_profe', 'placeholder': data1.senescyt_titulos_profe, 'value': data1.senescyt_titulos_profe },
        { name: 'nivel_profesor', 'placeholder': data1.nivel_profesor, 'value': data1.nivel_profesor }
      ],
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Actualizar', handler: data => {
          this._httpClient.put(environment.url + 'titulos-profesor/' + data1.id_titulos_profe, data, header)
            .subscribe(restitulos => {
              this.traertitulos(data1.id_usuario_rol)
            }, error => {
            });
        }
      }
      ]
    });
    alert.present();
  }
  async deleteTitulo(i) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    let alert = await this.alertController.create({
      header: 'Eliminar Titulo',
      message: 'Esta seguro de eliminar este titulo:',
      inputs: [
        { name: 'senescyt_titulos_profe', 'disabled': true, 'placeholder': i.senescyt_titulos_profe, 'value': i.senescyt_titulos_profe },
        { name: 'nivel_profesor', 'disabled': true, 'placeholder': i.nivel_profesor, 'value': i.nivel_profesor }
      ],
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Aceptar', handler: data => {
          this._httpClient.delete(environment.url + 'titulos-profesor/' + i.id_titulos_profe, header)
            .subscribe(restitulos => {
              this.traertitulos(this.properID)
            }, error => {
            });
        }
      }
      ]
    });
    alert.present();

  }
  async enviartitulos() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    let aux
    aux = "TITULO ";
    let body = {
      senescyt_titulos_profe: aux + this.nombre,
      id_usuario_rol: +this.properID,
      nivel_profesor: this.nivel
    }
    if (body.senescyt_titulos_profe == "TITULO undefined" || body.nivel_profesor == "" || body.nivel_profesor == undefined) {
      let alert = await this.alertController.create({
        header: 'Alerta Titulo',
        message: 'Porfavor llena todos los campos.',
        buttons: [{ text: 'Cancelar', role: 'cancel' },
        {
          text: 'Aceptar', handler: data => {

          }
        }
        ]
      });
      alert.present();

    } else {
      let alert = await this.alertController.create({
        header: 'Agregar Titulo',
        message: 'Agregar este Titulo a tus estudios.',
        buttons: [{ text: 'Cancelar', role: 'cancel' },
        {
          text: 'Agregar', handler: data => {
            this._httpClient.post(environment.url + 'titulos-profesor/', body, header)
              .subscribe(restitulos => {
                this.traertitulos(body.id_usuario_rol)
                this.nivel = ""
                this.nombre = ""
                this.propertyService.faltaTitulos = false
                this.navCtrl.pop()

              }, error => {
              });
          }
        }
        ]
      });
      alert.present();

    }
  }

}
