import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TitlesPage } from './titles.page';

describe('TitlesPage', () => {
  let component: TitlesPage;
  let fixture: ComponentFixture<TitlesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TitlesPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TitlesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
