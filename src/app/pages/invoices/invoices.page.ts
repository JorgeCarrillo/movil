import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import {
  InvoicesService,
  PropertyService,
  TranslateProvider
} from '../../providers';

import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { AuthService } from '../../services/auth.service';
import { AlertController } from '@ionic/angular';
import { environment } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NavController, MenuController, LoadingController } from '@ionic/angular';
import { CancelarhoraPage } from '../modal/cancelarhora/cancelarhora.page'

import { Observable } from 'rxjs';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.page.html',
  styleUrls: ['./invoices.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
        query(':enter', stagger('300ms', [animate('500ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})

export class InvoicesPage {
  lastInvoices;
  ids_pedidos;
  id
  imputs = [];
  invoice = { hora_inicio: "", id_deta_dia_hora: "" };
  invoiceID;
  cargando = true
  idhoracancelar;
  idnuevahora;
  pedidosprof;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private invoicesService: InvoicesService,
    private readonly _authService: AuthService,
    public alertController: AlertController,
    private _httpClient: HttpClient,
    public navCtrl: NavController,
    public loadingctrl: LoadingController,
    public propertyservice: PropertyService,
    private modalCtrl: ModalController
  ) {


    this.invoicesService.traerPedidos().subscribe(async res => {
      if (this.invoicesService.nuevaTutoria) {
        let alert = await this.alertController.create({
          header: 'Alerta',
          message: 'Recuerde pagar su tutoria en los siguientes 5 minutos',

          buttons: [
            {
              text: 'Aceptar', handler: data => {

              }
            }
          ]
        });
        alert.present();
        this.invoicesService.nuevaTutoria = false
      }
      this.lastInvoices = res
      this.cargando = false
      this.verfica()
    })

    this.invoicesService.traerPedidosprof().subscribe(res => {
      this.pedidosprof = res
    })


  }
  traerLinkZoom(idpedido) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'reunion/idpedido/' + idpedido, header)
      .subscribe(link => {
        const linkz: any = link
        window.open(linkz[0].enlaceEstudiante, '_system', 'location=yes');

      }, error => {
      });
  }
  irPerfilProfesor(id) {
    this.router.navigate(["property-detail/" + id])
  }
  async eliminarHora(a) {
    // 
    let alert = await this.alertController.create({
      header: "Eliminar tutoria",
      message: "Seguro de eliminar esta tutoria ",
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Aceptar', handler: data => {
          this.invoicesService.Eliminarhora({
            id_pedido: a.id, fecha_inicio: a.date, estado: a.paid, id_deta_dia_hora: a.id_hora, id_asig_user_per: a.id_asig_user_per, id_detalle_pedido: a.id_detalle_pedido
          })

          this.navCtrl.navigateRoot('/home-results');
        }
      }
      ]
    });

    alert.present();
  }
  getInvoices() {
    this.invoicesService.getInvoices()
      .then(data => {
        this.lastInvoices = data;
      });
  }

  async goCheckout(invoice) {
    if (!invoice.paid) {
      let navigationExtras: NavigationExtras = {
        state: {
          invoice: invoice
        }
      };

      this.router.navigate(['checkout'], navigationExtras);
    };

  }

  async cancelar(a) {
    this.idhoracancelar = a.id_hora
    //this.createInputs(a.id_asig_user_per)
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    let alert = await this.alertController.create({
      header: 'Cancelar Clase',
      message: 'Esta seguro de cancelar esta clase.',
      inputs: [
        { name: 'condicion_anula', 'placeholder': "Ingrese Motivo", 'value': name },
        { name: 'condicion_cambia', 'placeholder': "Ingrese porque Cambia", 'value': name }
      ],
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Aceptar', handler: async data => {

          let body = {
            condicion_anula: data.condicion_anula,
            condicion_cambia: data.condicion_cambia,
            id_pedido: a.id
          }
          if (body.condicion_anula == "" || body.condicion_cambia == "") {
            let alert = await this.alertController.create({
              header: 'Alerta',
              message: 'Porfavor llene todos los datos.',

              buttons: [{ text: 'Cancelar', role: 'cancel' },
              {
                text: 'Aceptar', handler: data => {

                }
              }
              ]
            });
            alert.present();
          } else {
            this.propertyservice.traerDisponibilidad(a.id_asig_user_per).subscribe(async res => {
              const modal = await this.modalCtrl.create({
                component: CancelarhoraPage,
                cssClass: 'my-custom-class',
                componentProps: {
                  "horas": res,
                }
              });
              modal.onDidDismiss().then(async (data) => {
                this.invoice.hora_inicio = data.data.horainicio;
                this.invoice.id_deta_dia_hora = data.data.idhora;
                this.invoiceID = a.id
                this.idnuevahora = data.data.idhora
                this.checkoutInvoice()
                if (data.data.idhora != false) {
                  alert.present()


                  let loading = await this.loadingctrl.create({
                    message: "Cargando..."
                  });

                  loading.present();
                  this._httpClient.get(environment.url + 'asig-user-per/id/' + a.id_asig_user_per, header)
                    .subscribe(async resusurol => {
                      let usuariorol: any = resusurol
                      this._httpClient.get(environment.url + 'usuario/idrol/' + usuariorol.id_usuario_rol, header)
                        .subscribe(async resusu => {
                          let usuario: any = resusu
                          let split = a.date.split("T")
                          let hora = split[1].split(":")
                          hora = new Date(new Date().setHours(hora[0] - 5)).getHours() + ":" + hora[1]
                          let data = {
                            fecha: split[0],
                            hora: hora,
                            correo_profesor: usuario[0].correo_usuario,
                            correo_estudiante: this._authService.getUsuario().correo_usuario
                          }

                          this._httpClient.post(environment.url + 'usuario/cancelarclase', data, header)
                            .subscribe(async rescancelacioncorreo => {
                              if (rescancelacioncorreo) {
                                this._httpClient.post(environment.url + 'cancelacion', body, header)
                                  .subscribe(async rescancelacion => {
                                    this.alert("Cancelación", "Clase cancelada correctamente.")
                                    loading.dismiss()
                                    this.navCtrl.navigateRoot('/home-results');


                                  }, error => {
                                  });
                              } else {
                                this.alert("Cancelación", "No se pudo cancelar la clase.")
                                loading.dismiss()

                              }
                            }, error => {
                            });
                        }, error => {
                        });
                    }, error => {
                    });
                }



              })
              modal.present()
            })


          }
        }
      }]
    });
    alert.present();

  }
  async alert(header, mensaje) {
    let alert = await this.alertController.create({
      header: header,
      message: mensaje,
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Aceptar', handler: data => {

        }
      }
      ]
    });

    alert.present();
  }
  verfica() {
    this.ids_pedidos = []
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'cancelacion', header)
      .subscribe(restitulos => {
        let aux
        aux = restitulos
        for (let a of this.lastInvoices) {
          a.cancelada = false

          if (new Date(new Date(a.date).setHours(new Date(a.date).getHours() - 5)) < new Date()) {

            a.cancelada = true
          }
          for (let b of aux) {
            if (b.id_pedido == a.id) {
              a.cancelada = true
            }
          }
        }

      }, error => {
      });

  }
  async createInputs(id) {
    let myArray = [1, 1, 1, 1, 1, 1];
    const theNewInputs = [];
    this.propertyservice.traerDisponibilidad(id).subscribe(res => {
      let arr: any = res
      this.imputs = []

      for (let b of arr) {


        this.imputs.push(
          {
            type: 'radio',
            label: new Date(b.hora_inicio).toLocaleString(),
            value: b,
            checked: false,

          }
        );

      }
      return theNewInputs;

    })

  }

  traerDispononibilidad(id) {
    return new Observable(observer => {


      this.propertyservice.traerDisponibilidad(id).subscribe(res => {
        observer.next(res)
      })
    })
  }
  async checkoutInvoice() {
    let loading = await this.loadingctrl.create({
      message: "Cargando..."
    });
    loading.present()
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }



    const loader = await this.loadingctrl.create({
      duration: 2000
    });

    this._httpClient.get(environment.url + "reunion/fecha_inicio/'" + this.invoice.hora_inicio + "'", header)
      .subscribe(resresuniones => {
        let disponibilidad;
        let idsdisponbles = [];
        let reuniones;
        reuniones = resresuniones
        let aulas;
        let aulasdisponibles = [];
        if (new Date(this.invoice.hora_inicio).getHours() % 2 == 0) {
          disponibilidad = "Par";
        }
        else {
          disponibilidad = "Impar";
        }
        this._httpClient.get(environment.url + "subcuenta-zoom/disponibilidad/'" + disponibilidad + "'", header)
          .subscribe(async resaulas => {
            aulas = resaulas

            if (aulas.length > reuniones.length) {
              for (let a of aulas) {
                if (reuniones.length > 0) {
                  for (let b of reuniones) {
                    if (a.id_subcuenta != b.id_subcuenta) {
                      aulasdisponibles.push(a.id_subcuenta)
                      idsdisponbles.push(a.id_usuario_zoom)
                    }
                  }
                } else {
                  aulasdisponibles.push(a.id_subcuenta)
                  idsdisponbles.push(a.id_usuario_zoom)
                }
              }
              let options = {
                "idusuario": idsdisponbles[0],
                'topic': "Tutoria",
                'type': 2,
                'start_time': this.invoice.hora_inicio,
                'duration': 60,
                'timezone': 'America/Bogota',
                'settings': {
                  'host_video': false,
                  'participant_video': true,
                  'join_before_host': true,
                  'mute_upon_entry': true,
                  'use_pmi': true,
                  'approval_type': 1,
                  'waiting_room': true,
                  'auto_recording': 'cloud'
                }
              }
              this._httpClient.post(environment.url + "zoom-api/meeting", options, header)
                .subscribe(resmeet => {
                  let meet;
                  meet = resmeet;

                  let body = {
                    id_reunion: meet.id.toString(),
                    enlace: meet.start_url,
                    enlaceEstudiante: meet.join_url,
                    password: meet.password,
                    fecha_inicio: this.invoice.hora_inicio,
                    fecha_fin: new Date(new Date(this.invoice.hora_inicio).setHours(new Date(this.invoice.hora_inicio).getHours() + 1)).toISOString(),
                    hora_inicio: new Date(this.invoice.hora_inicio).getHours().toString(),
                    hora_fin: new Date(new Date(this.invoice.hora_inicio).setHours(new Date(this.invoice.hora_inicio).getHours() + 1)).getHours().toString(),
                    estado: true,
                    id_subcuenta: aulasdisponibles[0],
                    id_pedido: +this.invoiceID

                  }


                  this.invoicesService.cambiarFechaReunion(this.invoice.hora_inicio, this.invoiceID, this.idnuevahora)
                  this.invoicesService.crearreunion(body, meet.join_url, this.invoice.id_deta_dia_hora)
                  this.invoicesService.updateestadohora(this.idhoracancelar, 1)
                  this.invoicesService.updateestadohora(this.idnuevahora, 3)

                  loading.dismiss()
                  this.navCtrl.navigateForward('home-results');
                }, error => {
                });
            } else {
              let alert = await this.alertController.create({
                header: 'Agregar Aulas',
                cssClass: 'alertpedido',
                message: 'Pago no procesado no se cuenta con aulas disponibles porfavor seleccione otra hora.',
                //inputs: this.data,
                buttons: [
                  {
                    text: 'Aceptar',
                    handler: () => {

                    }
                  }
                ]
              });
              alert.present();

            }
          }, error => {
          });
      }, error => {
      });



  }
}
