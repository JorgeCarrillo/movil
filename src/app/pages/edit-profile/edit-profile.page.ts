import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  LoadingController,
  ToastController,
  PopoverController,
  ModalController,
  ActionSheetController,
  Platform
} from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { TranslateProvider } from '../../providers';
import { PropertyService } from '../../providers';
import { AuthService } from '../../services/auth.service';
import { UsuarioService } from '../../services/usuario.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { Storage } from '@ionic/storage';
import { Observable, of } from 'rxjs';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
const STORAGE_KEY = 'my_images';
const baseUrl = environment.url + "usuario/upload-image";
const MAX_FILE_SIZE = 5 * 1024 * 1024;
const ALLOWED_MIME_TYPE = "video/mp4";
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
        query(':enter', stagger('300ms', [animate('500ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})

export class EditProfilePage {
  pais;
  viewMode = 'perfil';
  idPaisSelec;
  idCiudadSelec;
  idProvinciaSelec;
  nombreciudad
  video
  ciudad
  estado
  nombre
  apellido
  foto
  fechana
  cedula
  nombreImagen;
  nombreVideo;
  verVideo = false;
  telefono
  direccion
  ciudadnombre
  genero
  tipousuario: string;
  email: string;
  picture: string;
  datos;
  detaprof
  properID: any;
  urlvideo
  auxfecha: any;
  minFecha: string = (new Date().getFullYear() - 100).toString();
  maxFecha: string = (new Date().getFullYear()).toString();
  detalles = {
    "preciofijo": "",
    "descri_deta_profe": "",
    "facebook": "",
    "twitter": "",
    "youtube": "",
    "instagram": "",
    "preferencia_ense": "",
    "experiencia": "",
    "id_usuario_rol": +this._authService.getIdUsuarioRol()
  }

  id_deta_profe
  imgURL: any;
  public message: string;
  imagenEstudiante: File;


  pathFotoProf: string;


  aux: any;


  private base64Image: string;
  cargando = true
  images = [];
  imagensubida = false;
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private translate: TranslateProvider,
    private readonly _authService: AuthService,
    private _usuarioService: UsuarioService,
    public router: Router,
    public route: ActivatedRoute,
    private propertyService: PropertyService,
    private _httpClient: HttpClient,
    private actionSheetController: ActionSheetController,
    public camara: Camera,
    private filePath: FilePath,
    private file: File,
    private plt: Platform,
    private storage: Storage,
    private ref: ChangeDetectorRef,
    public webview: WebView,
    private transfer: FileTransfer,
    private theInAppBrowser: InAppBrowser,
  ) {
    this.properID = this._authService.getIdUsuarioRol()
    this.obetenerpais().subscribe(res => {
      this.obetenerciudad().subscribe(res => {
        this.obetenerProvincia().subscribe(res => {
          this.traerDatos().subscribe(res => {
            if (this.tipousuario != "PROFESOR") this.cargando = false
            else this.traerDetallesProfesor().subscribe(res => {
              this.cargando = false
            })
          })
        })
      })
    })

  }

  //fotografias
  async selectImage() {
    const actionSheet = await this.actionSheetController.create(
      {
        header: "Seleccione el Recurso Para Cargar la Imagen",
        buttons: [{
          text: 'Cargar desde Galería',
          handler: () => {
            this.takePicture(this.camara.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tomar Foto',
          handler:
            () => {
              this.takePicture(this.camara.PictureSourceType.CAMERA);
            }
        }
          ,
        {
          text: 'Cancelar',
          role:
            'cancel'
        }
        ]
      })
      ;
    await actionSheet.present();
  }
  takePicture(sourceType: PictureSourceType) {
    var options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camara.getPicture(options).then(imagePath => {

      if (this.plt.is('android') && sourceType === this.camara.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    });

  }
  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.updateStoredImages(newFileName);
    }, error => {
      this.presentToast('Error al guardar el archivo.');
    });
  }
  createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  updateStoredImages(name) {
    this.storage.get(STORAGE_KEY).then(images => {
      let arr = JSON.parse(images);
      if (!arr) {
        let newImages = [name];
        this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
      } else {
        arr.push(name);
        this.storage.set(STORAGE_KEY, JSON.stringify(arr));
      }

      let filePath = this.file.dataDirectory + name;
      let resPath = this.pathForImage(filePath);

      let newEntry = {
        name: name,
        path: resPath,
        filePath: filePath
      };

      this.images = [newEntry, ...this.images];
      this.ref.detectChanges(); // trigger change detection cycle
      this.imagensubida = true
      this._authService.getfoto()
    });
  }
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  startUpload(imgEntry) {
    this.nombreImagen = imgEntry.name
    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry => {
        (<FileEntry>entry).file(file => this.readFile(file))
      })
      .catch(err => {
        this.presentToast('Error al leer el archivo.');
      });
  }
  readFile(file: any) {

    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      formData.append('filename', imgBlob, file.name);
      this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }
  async uploadImageData(formData: FormData) {
    const loading = await this.loadingCtrl.create({
      //content: "Uploading image...",
    });
    await loading.present();

    this._httpClient.post(environment.url + "usuario/upload-image", formData, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + this._authService.getToken() }) })
      .pipe(
        finalize(() => {
          loading.dismiss();
        })
      )
      .subscribe(res => {

        this.pathFotoProf = Object.values(res)[1];
        this.presentToast("Imagen ingresada con éxito")
        this._httpClient.get(environment.url + "usuario/actualizarfoto/'" + this.nombreImagen + "'/" + this._authService.getIdUsuario(), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + this._authService.getToken() }) })
          .pipe(
            finalize(() => {
              loading.dismiss();
            })
          )
          .subscribe(res => {
            localStorage.setItem('fotoUser', JSON.stringify({
              fotousuario: environment.urlfotos + this.nombreImagen
            }));
            this.navCtrl.navigateRoot("/home-results")

          });

      });
  }

  ///////////////////////////////////////////// inicio video


  selectedVideo: string; //= "https://res.cloudinary.com/demo/video/upload/w_640,h_640,c_pad/dog.mp4";
  uploadedVideo: string;

  isUploading: boolean = false;
  uploadPercent: number = 0;
  videoFileUpload: FileTransferObject;
  loading;
  async selectVideos() {
    const actionSheet = await this.actionSheetController.create(
      {
        header: "Seleccione el Recurso Para Cargar el Video",
        buttons: [{
          text: 'Cargar desde Galería',
          handler: () => {
            // this.takePicture(this.camara.PictureSourceType.PHOTOLIBRARY);
            this.selectVideo()
          }
        },
        /* {
           text: 'Tomar Video',
           handler:
             () => {
               this.takePicture(this.camara.PictureSourceType.CAMERA);
             }
         }
           ,*/
        {
          text: 'Cancelar',
          role:
            'cancel'
        }
        ]
      })
      ;
    await actionSheet.present();
  }
  async showLoader() {

    this.loading = await this.loadingCtrl.create({
      //content: "Uploading image...",
    });
    await this.loading.present();
  }

  dismissLoader() {
    this.loading.dismiss();
  }




  cancelSelection() {
    this.selectedVideo = null;
    this.uploadedVideo = null;
  }

  selectVideo() {
    const options: CameraOptions = {
      mediaType: this.camara.MediaType.VIDEO,
      sourceType: this.camara.PictureSourceType.PHOTOLIBRARY
    }

    this.camara.getPicture(options)
      .then(async (videoUrl) => {
        if (videoUrl) {
          // this.showLoader();
          const loading = await this.loadingCtrl.create({
            //content: "Uploading image...",
          });
          loading.present()
          //await this.loading.present();
          this.uploadedVideo = null;

          var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
          var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);

          dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;

          try {
            var dirUrl = await this.file.resolveDirectoryUrl(dirpath);
            var retrievedFile = await this.file.getFile(dirUrl, filename, {});


          } catch (err) {
            //this.dismissLoader();
            loading.dismiss();
            return this.presentToast("Algo salio mal.");
          }

          retrievedFile.file(data => {
            //this.dismissLoader();
            loading.dismiss();
            if (data.size > MAX_FILE_SIZE) return this.presentToast("No puede cargar mas de 5m");
            if (data.type !== ALLOWED_MIME_TYPE) return this.presentToast("Tipo de archivo incorrecto");

            this.selectedVideo = retrievedFile.nativeURL;

          });
        }
      },
        (err) => {
          console.log(err);
        });
  }
  createFileNameVideo() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".mp4";
    return newFileName;
  }
  uploadVideo() {
    this.createFileNameVideo()
    var url = baseUrl;
    //var filename = this.selectedVideo.substr(this.selectedVideo.lastIndexOf('/') + 1);
    var filename = this.createFileNameVideo();
    this.nombreVideo = filename
    var options: FileUploadOptions = {
      fileName: filename,
      fileKey: "filename",
      mimeType: "video/mp4"
    }

    this.videoFileUpload = this.transfer.create();

    this.isUploading = true;

    this.videoFileUpload.upload(this.selectedVideo, url, options)
      .then((data) => {
        this.isUploading = false;
        this.uploadPercent = 0;
        return JSON.parse(data.response);
      })
      .then((data) => {
        this.uploadedVideo = data.url;


        this._httpClient.get(environment.url + "usuario/actualizarvideo/'" + this.nombreVideo + "'/" + this._authService.getIdUsuarioRol(), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + this._authService.getToken() }) })
          .pipe(
            finalize(() => {
              //loading.dismiss();
            })
          )
          .subscribe(res => {
            this.navCtrl.navigateRoot("/home-results")

          });
        this.presentToast("video subido.");
      })
      .catch((err) => {
        this.isUploading = false;
        this.uploadPercent = 0;
        this.presentToast("Error al subir video.");
      });

    this.videoFileUpload.onProgress((data) => {
      this.uploadPercent = Math.round((data.loaded / data.total) * 100);
    });

  }

  cancelUpload() {
    this.videoFileUpload.abort();
    this.uploadPercent = 0;
  }


  ////fin video
  onChangeano(fecha) {
    this.auxfecha = fecha;

  }
  obetenerpais() {
    return new Observable(observer => {


      this.pais = []
      let auxpais: any
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.get(environment.url + 'pais', header)
        .subscribe(respais => {
          //this.genero = this.datos.usuario.sexo_usuario
          auxpais = respais
          for (let i = 0; i < auxpais.length; i++) {
            this.pais.push({
              id: auxpais[i].id_pais,
              nombre: auxpais[i].nombre_pais,
            })
          }
          observer.next(this.pais)
        }, error => {
        });
    })
  }

  obetenerciudad() {
    return new Observable(observer => {


      this.ciudad = []
      let auxciudad: any
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.get(environment.url + 'ciudad', header)
        .subscribe(resciudad => {
          //this.genero = this.datos.usuario.sexo_usuario
          auxciudad = resciudad
          for (let i = 0; i < auxciudad.length; i++) {
            this.ciudad.push({
              id: auxciudad[i].id_ciudad,
              nombre: auxciudad[i].nombre_ciudad,
              id_estado: auxciudad[i].id_estado,
            })
          }
          observer.next(this.ciudad)
        }, error => {
        });
    })
  }

  obetenerProvincia() {
    return new Observable(observer => {


      this.estado = []
      let auxestado: any
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.get(environment.url + 'estado', header)
        .subscribe(resestado => {
          //this.genero = this.datos.usuario.sexo_usuario
          auxestado = resestado
          for (let i = 0; i < auxestado.length; i++) {
            this.estado.push({
              id: auxestado[i].id_estado,
              nombre: auxestado[i].nombre_estado,
              id_pais: auxestado[i].id_pais,
            })
          }
          observer.next(this.estado)
        }, error => {
        });
    })
  }

  traerDatos() {
    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.get(environment.url + 'usuario-rol/id/' + this.properID, header)
        .subscribe(resdatos => {
          this.datos = resdatos;
          if (this.datos.usuario.foto_usuario != null) {
            this.picture = environment.urlfotos + this.datos.usuario.foto_usuario
          } else {
            this.picture = environment.urlfotos + "avatar.png"
          }


          this.cedula = this.datos.usuario.identificacion_usuario
          this.nombre = this.datos.usuario.nombres_usuario
          this.apellido = this.datos.usuario.apellidos_usuario
          this.fechana = this.datos.usuario.fecha_naci_usuario
          this.telefono = this.datos.usuario.telefono_usuario
          this.direccion = this.datos.usuario.direccion_usuario
          this.tipousuario = this.datos.rol.nombre_rol
          this.genero = this.datos.usuario.sexo_usuario
          if (this.datos.usuario.ciudad != null) {

            this.idPaisSelec = this.datos.usuario.ciudad.estado.id_pais
            this.idProvinciaSelec = this.datos.usuario.ciudad.id_estado
            this.idCiudadSelec = this.datos.usuario.ciudad.id_ciudad
            this.pais.nombre = this.datos.usuario.ciudad.estado.pais.nombre_pais
            this.estado.nombre = this.datos.usuario.ciudad.estado.nombre_estado
            this.ciudad.nombre = this.datos.usuario.ciudad.nombre_ciudad
          }
          observer.next(true)
        }, error => {
        });

    })

  }
  traerDetallesProfesor() {
    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.get(environment.url + 'detalles_profesor/usuario/' + this._authService.getIdUsuarioRol(), header)
        .subscribe(resdatos => {
          if (resdatos != null || resdatos != undefined) {
            let detallesprof
            this.detaprof = resdatos;
            detallesprof = resdatos;
            if (detallesprof.video_presentacion != null || detallesprof.video_presentacion != undefined) {
              this.urlvideo = environment.urlfotos + detallesprof.video_presentacion
              this.verVideo = true;
            } else {
              this.verVideo = false;
            }
            this.detalles.descri_deta_profe = detallesprof.descri_deta_profe
            this.detalles.facebook = detallesprof.facebook
            this.detalles.twitter = detallesprof.twitter
            this.detalles.youtube = detallesprof.youtube
            this.detalles.preciofijo = detallesprof.preciofijo
            this.detalles.preferencia_ense = detallesprof.preferencia_ense
            this.detalles.instagram = detallesprof.instagram
            this.detalles.experiencia = detallesprof.experiencia
            this.id_deta_profe = detallesprof.id_deta_profe

            observer.next(this.detalles)
          } else { observer.next(this.detalles) }
        }, error => {
        });
    })
  }

  sendDataDetaProf() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    if (this.detaprof == null) {
      this._httpClient.post(environment.url + 'detalles_profesor', this.detalles, header)
        .subscribe(resdatos => {
          this.navCtrl.navigateRoot('/home-results');
        }, error => {
        });

    } else {
      for (let a in this.detalles) {

      }
      this._httpClient.put(environment.url + 'detalles_profesor/' + this.id_deta_profe, this.detalles, header)
        .subscribe(resdatos => {
          if (this.detalles.preciofijo == ""
            || this.detalles.preciofijo == null
            || this.detalles.descri_deta_profe == null
            || this.detalles.descri_deta_profe == ""
            || this.detalles.experiencia == null
            || this.detalles.experiencia == ""
            || this.detalles.preferencia_ense == null
            || this.detalles.preferencia_ense == ""

          ) {
            this.propertyService.faltaCOnfiguracion = true

          } else {
            this.propertyService.faltaCOnfiguracion = false

          }

          this._authService.getfoto()
          this.navCtrl.navigateRoot('/home-results');

        }, error => {
        });
    }
  }

  onChange() {
    this.idCiudadSelec = 0;
    this.idProvinciaSelec = 0;
  }
  nombreCiudad() {


    for (let a of this.ciudad) {
      if (a.id == this.idCiudadSelec) {
        this.ciudadnombre = a.nombre
      }
    }
  }
  async sendData() {
    this.nombreCiudad()
    // send booking info
    let datosCompletos = true;
    let data = {
      "nombres_usuario": this.nombre,
      "apellidos_usuario": this.apellido,
      "identificacion_usuario": this.cedula,
      "fecha_naci_usuario": this.fechana,
      "telefono_usuario": this.telefono,
      "direccion_usuario": this.direccion,
      "ciudad_nombre": this.ciudadnombre,
      "id_ciudad": this.idCiudadSelec,
      "foto_usuario": this.picture,
      "id_usuario": +this._authService.getIdUsuario(),
      "sexo_usuario": this.genero
    }
    for (let a in data) {
      if (data[a] == null) {
        this.presentToast("Todos los datos son requeridos")
        datosCompletos = false;
        break;

      }
    }
    if (datosCompletos) {


      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.post(environment.url + 'usuario/actualizar', data, header)
        .subscribe(resactualizacion => {

          this._authService.getfoto()
          this.navCtrl.pop()
        }, error => {
        });
    }

  }

  editarensena(ensena) {

  }
  actualizarensena() {
  }

  cambiar(perfil) {
    this.viewMode = perfil

  }
  async presentToast(mensaje) {
    let toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom'
    });



    toast.present();
  }


  openViseo() {
    let options: InAppBrowserOptions = {
      location: 'yes',//Or 'no' 
      hidden: 'no', //Or  'yes'
      clearcache: 'yes',
      clearsessioncache: 'yes',
      zoom: 'yes',//Android only ,shows browser zoom controls 
      hardwareback: 'yes',
      mediaPlaybackRequiresUserAction: 'no',
      shouldPauseOnSuspend: 'no', //Android only 
      closebuttoncaption: 'Close', //iOS only
      disallowoverscroll: 'no', //iOS only 
      toolbar: 'yes', //iOS only 
      enableViewportScale: 'no', //iOS only 
      allowInlineMediaPlayback: 'no',//iOS only 
      presentationstyle: 'pagesheet',//iOS only 
      fullscreen: 'yes',//Windows only    
    };
    let target = "_blank";
    //let target = "_system";
    //let target = "_self";
    //this.urlvideo

    this.theInAppBrowser.create(this.urlvideo, target, options);

  }
}
