import { Component } from '@angular/core';
import { NavController, ActionSheetController, ModalController, ToastController, LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { ImagePage } from './../modal/image/image.page';
import * as moment from 'moment';
import {
  InvoicesService,
  PropertyService,
  // BrokerService,
} from '../../providers';

import {
  trigger,
  style,
  animate,
  transition,
  query,

  stagger
} from '@angular/animations';
import { AuthService } from '../../services/auth.service';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';

registerLocaleData(localeES);
import { environment } from "../../../environments/environment";

@Component({
  selector: 'app-property-detail',
  templateUrl: './property-detail.page.html',
  styleUrls: ['./property-detail.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('300ms', [animate('600ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class PropertyDetailPage {
  viewTitle;
  propertyID: any;
  propertyIDRol: any;
  property: any;
  mayor = "16";
  menor = "7";
  idUsuarioLogueado;
  auxrating;
  click = 0;
  propertyopts: String = 'description';
  estado1: boolean;
  estado2: boolean;
  estado3: boolean;
  estado4: boolean;
  estado5: boolean;
  calificar = false;
  tipousuario;
  comentariosAll = false;
  calendario = false
  mismousuario = false;
  comentarios = [];
  event;
  usuariovalido = true
  comentario;
  array = true
  cargar = true
  items: any = [];
  datos: Array<{ id: number, title: string, startTime: Date, endTime: string, allDay: boolean, encabezado: string, anio: string, mes: string, fecha: string, horas: string, tema: string, descripcion: string, nombreCliente: string, idCliente: number, tipo: string }> = [];
  eventSource
  obj
  final
  newevent
  isToday: boolean;
  enlinea = false;
  itemHeight: number = 0;
  verVideo = false;
  urlvideo
  horas = []
  calendar = {
    mode: 'week',
    menor: "7",
    mayor: "9",

    currentDate: new Date(),
    dateFormatter: {
      formatMonthViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getFullYear();
      },
      formatWeekViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getFullYear();
      },
      formatDayViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
      },
    },
  };
  constructor(
    public asCtrl: ActionSheetController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public route: ActivatedRoute,
    public alertController: AlertController,
    public router: Router,
    private propertyService: PropertyService,
    public loadingCtrl: LoadingController,
    public _authService: AuthService,
    public invoiceService: InvoicesService
  ) {

    this.horas.push(new Date(new Date(new Date(new Date(new Date().setMinutes(new Date().getMinutes() - new Date().getMinutes())).setHours(new Date().getHours() + 1)).setSeconds(new Date().getSeconds() - new Date().getSeconds())).setMilliseconds(new Date().getMilliseconds() - new Date().getMilliseconds())).toISOString())
    this.horas.push(new Date(new Date(new Date(new Date(new Date().setMinutes(new Date().getMinutes() - new Date().getMinutes())).setHours(new Date().getHours() + 2)).setSeconds(new Date().getSeconds() - new Date().getSeconds())).setMilliseconds(new Date().getMilliseconds() - new Date().getMilliseconds())).toISOString())


    if (this._authService.getRol() === null) {
      this.tipousuario = 'CLIENTE'
    } else {
      this.tipousuario = this._authService.getRol()
    }
    this.idUsuarioLogueado = _authService.getIdUsuario()
    this.propertyID = this.route.snapshot.paramMap.get('id');

    this.presentLoadingDefault();

    this.property = this.propertyService.getItem(this.propertyID) ?
      this.propertyService.getItem(this.propertyID) :
      this.propertyService.getProperties()[0];
    this.propertyIDRol = this.property.bedrooms.id_usuario_rol;
    if (this.property.video_presentacion) {
      this.verVideo = true
      this.urlvideo = environment.urlfotos + this.property.video_presentacion
    }

    this.enlinea = this.property.period
    if (this.propertyID == this.idUsuarioLogueado) this.mismousuario = true


    this.estado1 = false;
    this.estado2 = true;
    this.estado3 = false;
    this.estado4 = false;
    this.estado5 = false;

  }

  abrircomentar() {
    if (this.usuariovalido) {
      if (this.calificar) this.calificar = false
      else this.calificar = true
    }
  }

  allcomentarios(all) {
    this.comentariosAll = all
  }

  async enviarcomentario() {
    let data = {
      fecha_valoracion: new Date(),
      comentario: this.comentario,
      valoracion: +this.auxrating,
      id_usuario_rol: +this._authService.getIdUsuarioRol(),
      id_usuario_rol_profesor: +this.propertyIDRol
    }

    let alert = await this.alertController.create({
      header: 'Agregar comentario',
      cssClass: 'alertpedido',
      //inputs: this.data,
      buttons: [
        {
          text: 'No',
          role: 'No',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.calificar = false
            this.propertyService.enviarcomentario(data).subscribe(res => {
              this.presentLoadingDefault();
            });



          }
        }
      ]
    });
    alert.present();

  }


  async disponible(datahora) {
    if (this.usuariovalido) {
      if (this.idUsuarioLogueado != this.propertyID && this.tipousuario != "CLIENTE") {
        this.calendario = false
        let alert = await this.alertController.create({
          header: 'Reservar hora',
          cssClass: 'alertpedido',
          message: '¿reservar hora?',
          inputs: this.createInputs(),
          buttons: [
            {
              text: 'No',
              role: 'No',
              handler: () => {
                this.calendario = true
              }
            },
            {
              text: 'Si',
              handler: (data) => {
                this.propertyService.pedido(datahora.event.startTime, +this._authService.getIdUsuarioRol(), data.asig_user_per, datahora.event.id, this.propertyID, data.nombre + "/" + data.nombre_materia + "/" + data.precio + "/" + this.property.id, data, this.property.id).subscribe(res => {
                  if (res) {
                    this.navCtrl.navigateForward('/invoices')
                    this.presentLoadingDefault()
                    this.invoiceService.nuevaTutoria = true

                  }
                })

              }
            }
          ]
        });
        alert.present();

      }
    }
  }
  createInputs() {
    let myArray = [1, 1, 1, 1, 1, 1];
    const theNewInputs = [];

    for (let b of this.property.ensena) {


      theNewInputs.push(
        {
          type: 'radio',
          label: b.categoria + "/ " + b.niveles,
          value: b,
          checked: false
        }
      );

    }
    return theNewInputs;
  }


  async presentLoadingDefault() {
    let loading = await this.loadingCtrl.create({
      message: "Cargando..."
    });

    loading.present();
    this.propertyService.getDetaHorario(this.propertyID).subscribe(res => {
      this.propertyService.traercomentarios(this.propertyIDRol).subscribe(comentarios => {
        let comentario: any = comentarios


        this.eventSource = res
        let horas = [];
        let splice
        let g = []
        for (let a of this.eventSource) {

          splice = new Date(a.startTime).toString().split(" ")

          splice = splice[4].split(":")

          horas.push(new Date(new Date().setHours(((splice[0])))).getHours())
          splice = new Date(a.endTime).toString().split(" ")
          splice = splice[4].split(":")
          horas.push(new Date(new Date().setHours(((splice[0])))).getHours())


        }

        this.calendar.menor = Math.min(...horas).toString()
        this.calendar.mayor = (Math.max(...horas) + 1).toString()
        if (this.eventSource.length > 0) {
          this.calendario = true
        }



        if (comentarios >= 3) {
          this.comentarios.push(comentario[0])
          this.comentarios.push(comentario[1])
          this.comentarios.push(comentario[2])
          this.comentarios.reverse()
        }
        else {
          this.comentarios = comentario.reverse()
        }
        this.cargar = false
        loading.dismiss()
      })
    })
  }
  onEventSelected() {

  }
  logcon(eve) {

  }
  logRatingChange(rating) {
    this.auxrating = rating;

  }
  ionViewWillEnter() {

  }
  cambiarEstado1() {

    if (this.estado1 === true) {
      this.estado1 = false
    } else {
      this.estado1 = true
    }
  }
  cambiarEstado2() {

    if (this.estado2 === true) {
      this.estado2 = false
    } else {
      this.estado2 = true
    }
  }
  cambiarEstado3() {
    if (this.estado3 === true) {
      this.estado3 = false
    } else {
      this.estado3 = true
    }
  }
  cambiarEstado4() {
    if (this.estado4 === true) {
      this.estado4 = false
    } else {
      this.estado4 = true
    }
  }

  cambiarEstado5() {
    if (this.estado5 === true) {
      this.estado5 = false
    } else {
      this.estado5 = true
    }
  }
  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
  today() {
    this.calendar.currentDate = new Date();
  }
  changeMode(mode) {
    this.calendar.mode = mode;
  }



  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }


  favorite(property) {
    this.propertyService.favorite(property)
      .then(async res => {
        const toast = await this.toastCtrl.create({
          showCloseButton: true,
          message: 'Añadido a favoritos',
          duration: 2000,
          position: 'bottom'
        });

        toast.present();
      });
  }

  async share() {
    const actionSheet = await this.asCtrl.create({
      header: 'Redes Sociales:',
      buttons: [{
        text: 'Facebook',
        role: 'facebook',

        icon: 'logo-facebook',

        handler: () => {
          if (this.property.facebook) window.open(this.property.facebook)
          else this.presentAlert("El usuario no a cuenta con esta red social")

        }
      }, {
        text: 'Twitter',
        icon: 'logo-twitter',
        handler: () => {
          if (this.property.twitter) window.open(this.property.twitter)
          else this.presentAlert("El usuario no a cuenta con esta red social")
        }
      }, {

        text: 'Youtube',
        icon: 'logo-youtube',
        handler: () => {
          if (this.property.youtube) window.open(this.property.youtube)
          else this.presentAlert("El usuario no a cuenta con esta red social")
        }
      }, {
        text: 'Instagram',
        icon: 'logo-instagram',
        handler: () => {
          if (this.property.instagram) window.open(this.property.instagram)
          else this.presentAlert("El usuario no a cuenta con esta red social")
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',

        handler: () => {
        }
      }]
    });
    await actionSheet.present();
  }

  range(n: Array<any>) {
    return new Array(n);
  }
  async presentAlert(header) {
    const alert = await this.alertController.create({

      subHeader: header,

      buttons: ['OK']
    });
    alert.present()
  }

  avgRating() {
    let average = 0;

    this.property.reviews.forEach((val: any, key: any) => {
      average += val.rating;
    });

    return average / this.property.reviews.length;
  }

  // async openCart() {
  //   const modal = await this.modalCtrl.create({
  //     component: CartPage
  //   });
  //   return await modal.present();
  // }
  propertybroker: any;

  expandItem(item) {

    this.items.map((listItem) => {

      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
      } else {
        listItem.expanded = false;
      }

      return listItem;

    });

  }


}
