import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { NavController, MenuController, LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { AuthService } from '../../services/auth.service';
import {
  InvoicesService,
} from '../../providers';
import { of } from 'rxjs';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})

export class CheckoutPage implements OnInit {
  invoiceID: any;
  invoice: any;
  titular;
  numero;
  mes;
  detalle_pedido
  auxmes: any;
  bodypago
  ano;
  cvv;
  auxano: any;
  paymethods = 'creditcard';
  orderNumber: number = Math.floor(Math.random() * 10000);
  categorias
  targetas = [];
  constructor(
    public alertController: AlertController,
    private _authService: AuthService,
    private _httpClient: HttpClient,
    private route: ActivatedRoute,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,

    private readonly router: Router,
    private invoicesService: InvoicesService

  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.invoiceID = this.router.getCurrentNavigation().extras.state.invoice;
        this.invoice = this.router.getCurrentNavigation().extras.state.invoice;
        this.invoiceID = this.invoiceID.id
      }
    });

    this.invoicesService.traerTargetas().subscribe(res => {
      let re: any = res
      this.targetas = re

    })
  }

  ngOnInit() {

  }


  onChange(mes) {
    this.auxmes = mes;

  }
  onChangeTargeta() {
    for (let a of this.targetas) {
      if (this.numero == a.numero_tarjeta) {

        this.mes = a.mes
        this.auxmes = a.mes
        this.auxano = a.ano
        this.ano = a.ano
        this.cvv = a.cvv
        this.titular = a.titular_tarjeta
      }
    }
  }

  onChangeano(ano) {
    this.auxano = ano;

  }

  async checkoutInvoice() {

    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    let data = {
      titular_tarjeta: this.titular,
      numero_tarjeta: +this.numero,
      mes: +this.auxmes,
      ano: +this.auxano,
      cvv: +this.cvv,
      id_usuario_rol: +this._authService.getIdUsuarioRol(),
    }



    let bodypago = {
      metodo_pago: "tarjeta de credito",
      sub_total: this.invoice.value,
      impuesto: this.invoice.value,
      total: this.invoice.value,
    }
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });




    this._httpClient.get(environment.url + "reunion/fecha_inicio/'" + this.invoice.date + "'", header)
      .subscribe(resresuniones => {
        let disponibilidad;
        let idsdisponbles = [];
        let reuniones;
        reuniones = resresuniones
        let aulas;
        let aulasdisponibles = [];
        if (new Date(this.invoice.date).getHours() % 2 == 0) {
          disponibilidad = "Par";
        }
        else {
          disponibilidad = "Impar";
        }
        this._httpClient.get(environment.url + "subcuenta-zoom/disponibilidad/'" + disponibilidad + "'", header)
          .subscribe(async resaulas => {
            aulas = resaulas

            if (aulas.length > reuniones.length) {
              for (let a of aulas) {
                if (reuniones.length > 0) {
                  for (let b of reuniones) {
                    if (a.id_subcuenta === b.id_subcuenta) { }
                    else {
                      aulasdisponibles.push(a.id_subcuenta)
                      idsdisponbles.push(a.id_usuario_zoom)
                    }
                  }
                } else {
                  aulasdisponibles.push(a.id_subcuenta)
                  idsdisponbles.push(a.id_usuario_zoom)
                }
              }
              let options = {
                "idusuario": idsdisponbles[0],
                'topic': "Tutoria",
                'type': 2,
                'start_time': this.invoice.date,
                'duration': 60,
                'timezone': 'America/Bogota',
                'settings': {
                  'host_video': false,
                  'participant_video': true,
                  'join_before_host': true,
                  'mute_upon_entry': true,
                  'use_pmi': true,
                  'approval_type': 1,
                  'waiting_room': true,
                  'auto_recording': 'cloud'
                }
              }
              this._httpClient.post(environment.url + "zoom-api/meeting", options, header)
                .subscribe(resmeet => {
                  let meet;
                  meet = resmeet;

                  let body = {
                    id_reunion: meet.id.toString(),
                    enlace: meet.start_url,
                    enlaceEstudiante: meet.join_url,
                    password: meet.password,
                    fecha_inicio: this.invoice.date,
                    fecha_fin: new Date(new Date(this.invoice.date).setHours(new Date(this.invoice.date).getHours() + 1)).toISOString(),
                    hora_inicio: new Date(this.invoice.date).getHours().toString(),
                    hora_fin: new Date(new Date(this.invoice.date).setHours(new Date(this.invoice.date).getHours() + 1)).getHours().toString(),
                    estado: true,
                    id_subcuenta: aulasdisponibles[0],
                    id_pedido: +this.invoiceID

                  }
                  let nuevaTargeta = true
                  this.invoicesService.crearreunion(body, meet.join_url, this.invoice.id_hora)
                  if (this.targetas.length > 0) {
                    for (let a of this.targetas) {
                      if (a.aux == this.auxmes && a.ano == this.auxano && this.cvv == a.cvv && this.titular == a.titular_tarjeta && this.numero == a.numero_tarjeta) {
                        nuevaTargeta = false;
                        break;
                      } else {
                        nuevaTargeta = true
                      }

                    }
                  }
                  this.invoicesService.realizarPago(this.invoiceID, true);
                  if (nuevaTargeta) {
                    this.invoicesService.guardartarjeta(data);
                  } else {

                  }
                  this.invoicesService.GuardarPago(bodypago, this.invoice.id);
                  this.invoicesService.updateestadohora(this.invoice.id_hora, 3)
                  this.navCtrl.navigateForward('home-results');
                }, error => {
                });
            } else {
              let alert = await this.alertController.create({
                header: 'Agregar Aulas',
                cssClass: 'alertpedido',
                message: 'Pago no procesado no se cuenta con aulas disponibles porfavor seleccione otra hora.',
                //inputs: this.data,
                buttons: [
                  {
                    text: 'Aceptar',
                    handler: () => {

                    }
                  }
                ]
              });
              alert.present();

            }
          }, error => {
          });
      }, error => {
      });



  }
  targeta() {
    let data = {
      titular_tarjeta: this.titular,
      numero_tarjeta: +this.numero,
      mes: +this.auxmes,
      ano: +this.auxano,
      cvv: +this.cvv,
      id_usuario_rol: +this._authService.getIdUsuarioRol(),
    }
    this.invoicesService.guardartarjeta(data);
  }


}
