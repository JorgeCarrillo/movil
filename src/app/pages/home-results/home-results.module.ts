import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { NgxPaginationModule } from 'ngx-pagination';
// Pipes
import { PipesModule } from '../../pipes/pipes.module';

import { HomeResultsPage } from './home-results.page';
import {StarRatingModule} from "ionic4-star-rating";

const routes: Routes = [
  {
    path: '',
    component: HomeResultsPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        IonicModule,
        PipesModule,
        RouterModule.forChild(routes),
        TranslateModule.forChild(),
        StarRatingModule,
    ],
  declarations: [HomeResultsPage]
})
export class HomeResultsPageModule {}
