import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import {
  NavController,
  AlertController,
  MenuController,
  LoadingController,
  ToastController,
  PopoverController,
  ModalController,
  Platform,
  ActionSheetController
} from '@ionic/angular';

import { PropertyService } from '../../providers';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { NotificationsComponent } from './../../components/notifications/notifications.component';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";

import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { async } from '@angular/core/testing';
import { AuthService } from '../../services/auth.service';
import { UsuarioService } from '../../services/usuario.service';
//
import { finalize } from 'rxjs/operators';
import { Storage } from '@ionic/storage';

import { File, FileEntry } from '@ionic-native/File/ngx';

import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
        query(':enter', stagger('200ms', [animate('400ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})

export class HomeResultsPage {

  properties;
  searchKey = '';
  label = '';
  yourLocation = '463 Beacon Street Guest House';
  rate = 3;
  cargar = true;
  tipoUsuario;
  usuario
  tienehoras
  todosdetalles
  fotoData
  collection = []
  validacion = false;
  url = ""
  hora = new Date()



  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public service: PropertyService,
    private router: Router,
    private _authService: AuthService,
    public alertController: AlertController,
    private readonly _httpClient: HttpClient,
    public camara: Camera,
    public webview: WebView,
    public usuarioservice: UsuarioService,
    private transfer: FileTransfer,

    private file: File,
    private actionSheetController: ActionSheetController,
    private storage: Storage,
    private plt: Platform,
    private ref: ChangeDetectorRef,
    private filePath: FilePath,
    private theInAppBrowser: InAppBrowser,

  ) {
    for (let i = 1; i <= 100; i++) {
      this.collection.push(`item ${i}`);
    }
    if (this._authService.getRol() === null) {
      this.tipoUsuario = 'CLIENTE'
    } else {
      this.tipoUsuario = this._authService.getRol()
    }

    if (this.tipoUsuario === 'PROFESOR') {
      /*this.service.validarHorasDisponibles(this._authService.getIdUsuario()).subscribe(res => {
        if (res) {

          //this.alert("Disponibilidad", "No cuentas con horas disponibles, recuerda agregar nuevas disponabilidades.")
          //this.navCtrl.navigateForward('/availability-page');
        } else {
          this.alert("Disponibilidad", "No cuentas con horas disponibles, recuerda agregar nuevas disponabilidades.")
        }
      })*/

    }
  }




  ionViewDidEnter() {
    this.validacion = false
    this.hora = new Date()
    this.menuCtrl.enable(true);

    this.tienehora()
    this.verdetalles()

    this.rate = 3;
    this.presentLoadingDefault()
    this.actualizar()
  }
  async actualizar() {
    let usuario
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    if (this._authService.getIdUsuario() != null) {
      this.service.obtenerusuario(this._authService.getIdUsuario()).subscribe(res => {
        usuario = res

        if (usuario.estado_usuario_cuenta === true && new Date(usuario.ultima_conexion) >= new Date()) {
          console.log("relax");
          this._authService.enLinea(new Date(this.hora.setMinutes(this.hora.getMinutes() + 5)).toISOString(), true).subscribe(res => {
          })
        } else if (usuario.estado_usuario_cuenta === true && new Date(usuario.ultima_conexion) < new Date()) {
          this._authService.enLinea(new Date(this.hora.setMinutes(this.hora.getMinutes() - 5)).toISOString(), false).subscribe(res => {
          })
        }
        return usuario
      })
    }
  }



  async presentLoadingDefault() {
    let loading = await this.loadingCtrl.create({
      message: "Cargando..."
    });

    loading.present();

    this.service.findAll().subscribe(res => {

      this.properties = res
      loading.dismiss();
      this.comparaciones()
      this.cargar = false

    })
  }

  async comparaciones() {
    if (!this.validacion) {
      this.validacion = true
      if (this.tipoUsuario == "PROFESOR") {
        this.usuario = this.service.getItem(this._authService.getIdUsuario())


        if (this.usuario.ensena == (0) || this.usuario.ensena == []) {
          this.alert("Alerta", "Agregue sus materias para continuar.")
          this.service.faltaMaterias = true
          this.navCtrl.navigateForward('/teaches');
        }
        else if (this.usuario.estudios.length == 0) {
          this.alert("Alerta", "Agregue sus titulos para continuar.")
          this.service.faltaTitulos = true
          this.navCtrl.navigateForward('/titles');
        }


        else if (this.tienehoras == true) {
          this.alert("Alerta", "Agregue su disponibilidad para continuar.")
          this.service.faltaDisponibilidad = true
          this.navCtrl.navigateForward('/availability-page');
        }

        else if (this.usuario.price == null || this.usuario.price == ""
          || this.todosdetalles.descri_deta_profe == null
          || this.todosdetalles.descri_deta_profe == ""
          || this.todosdetalles.experiencia == null
          || this.todosdetalles.experiencia == ""
          || this.todosdetalles.preferencia_ense == null
          || this.todosdetalles.preferencia_ense == ""
        ) {
          this.alert("Alerta", "Agregue sus detalles en editar perfil para continuar.")
          this.service.faltaCOnfiguracion = true
          this.navCtrl.navigateRoot('/edit-profile');
        }

      }


    }
  }

  async tienehora() {

    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    if (this._authService.getIdUsuario() != null) {
      this.service.validarHorasDisponibles(this._authService.getIdUsuario()).subscribe(res => {
        this.tienehoras = res
      })
      return this.tienehoras
    }

  }

  async verdetalles() {

    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    if (this._authService.getIdUsuario() != null) {
      this._httpClient.get(environment.url + 'detalles_profesor/usuario/' + this._authService.getIdUsuarioRol(), header)
        .subscribe(detallesprof => {
          this.todosdetalles = detallesprof


        }, error => {

        });

    }
    return this.todosdetalles
  }



  async alert(header, mensaje) {
    let alert = await this.alertController.create({
      header: header,
      message: mensaje,
      buttons: [
        {
          text: 'Aceptar', handler: data => {

          }
        }
      ]
    });

    alert.present();
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  onInput(event) {
    this.service.findByName(this.searchKey)
      .then(data => {
        this.properties = data;
      })
      .catch(error => alert(JSON.stringify(error)));
  }

  logRatingChange(rating) {
    // do your stuff
  }



  onCancel(event) {
    this.menuCtrl.enable(true);

    this.tienehora()
    this.verdetalles()

    this.rate = 3;

    this.presentLoadingDefault();
  }



  async openPropertyListPage(label?: any) {
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });

    loader.present();
    loader.onWillDismiss().then(() => {
      let navigationExtras: NavigationExtras = {
        state: {
          cat: '',
          label: label
        }
      };
      this.router.navigate(['property-list'], navigationExtras);
    });
  }

  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Change Location',
      message: 'Type your Address to change list in that area.',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter your new Location',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Change',
          handler: async (data) => {
            this.yourLocation = data.location;
            const toast = await this.toastCtrl.create({
              message: 'Location was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  async searchFilter() {
    let todonull = true;
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    modal.onDidDismiss().then(async (data) => {
      if (data.data == "reiniciar") {
        this.presentLoadingDefault()
      } else {
        this.service.newfilter()
        for (let a in data.data) {
          if (data.data[a] != null) {

            if (a == "estrella") this.properties = this.service.findByEstrella(data.data.estrella)
            else if (a == "pais") this.properties = this.service.findByPais(data.data.pais)
            else if (a == "idioma") this.properties = this.service.findByIdioma(data.data.idioma)
            else if (a == "precmin") this.properties = this.service.findByPrecio(data.data.precmin, data.data.preciomax)
            else if (a == "categoria") this.properties = this.service.findByCategoria(data.data.categoria)
            else if (a == "filtrohorario") {
              this.service.findByHorario(data.data.filtrohorario).subscribe(data => {
                let dat;
                dat = data
                this.properties = dat
              })
            }
            todonull = false
          }
        }
      }

    })
    return await modal.present();
  }

  async notifications() {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }
}



