import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController, ToastController } from '@ionic/angular';
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { LoginInterface } from "../../interfaces/login.interface";
import { UserInterface } from "../../interfaces/user.interface";
import { UsuarioService } from "../../services/usuario.service";
import { Router } from "@angular/router";
import { EnvioCorreoServiceService } from "../../services/envio-correo-service.service";
import { sha256 } from 'js-sha256';
import { RolesRegister } from "../../app.constant";
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public onRegisterForm: FormGroup;
  private _unsubscribeAll: Subject<any>;
  rolesRegister: any[];

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private readonly _usuarioService: UsuarioService,
    private readonly router: Router,
    private _correo: EnvioCorreoServiceService,
    public toastCtrl: ToastController,
  ) {
    this._unsubscribeAll = new Subject();
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.rolesRegister = RolesRegister;

    this.onRegisterForm = this.formBuilder.group({
      'name': [null, Validators.compose([
        Validators.required
      ])],
      'apellido': [null, Validators.compose([
        Validators.required
      ])],
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ]),
      ],
      'passwordConfirm': [null, Validators.compose([
        Validators.required
      ])],
      'rol': [null, Validators.compose([
        Validators.required
      ])]
    });

    this.onRegisterForm.get('password').valueChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        this.onRegisterForm.get('passwordConfirm').updateValueAndValidity();
      });
  }

  async signUp() {
    this.createCount()

  }

  // // //
  goToLogin() {
    this.navCtrl.navigateRoot('/login');
  }

  async createCount() {

    const loginNew: LoginInterface = {
      nombre_login: this.onRegisterForm.value.email,
      password_login: sha256(this.onRegisterForm.value.password),
      fecha_ingre: new Date(),
      id_usuario: 0
    };

    const userNew: UserInterface = {
      nombres_usuario: this.onRegisterForm.value.name,
      apellidos_usuario: this.onRegisterForm.value.apellido,
      correo_usuario: this.onRegisterForm.value.email,
      estado_usuario: false,
      estado_usuario_cuenta: false
    };

    try {
      const res = await this._usuarioService.createUser({
        login: loginNew,
        user: userNew,
        nombre_rol: this.onRegisterForm.value.rol
      }, {});
      if (res) {
        try {
          const resPost = await this._correo.postEnvioCreacion({
            nombre_login: loginNew.nombre_login,
            id_usuario: res.id_usuario,
            nombre_rol: this.onRegisterForm.value.rol
          });

          if (resPost) {
            //await this.router.navigate(['/pages/auth/mail-confirm', {email: loginNew.nombre_login}]);
            const loader = await this.loadingCtrl.create({
              duration: 2000
            });

            loader.present();
            loader.onWillDismiss().then(async () => {
              this.navCtrl.navigateRoot('/login');
              const toast = await this.toastCtrl.create({
                showCloseButton: true,
                message: 'Usuario registrado con exito, porfavor active su cuenta.',
                duration: 2000,
                position: 'bottom'
              });

              toast.present();
            });
          }
        }
        catch (e) {
          alert('No se ha enviado ningun correo');
        }

      }
    }
    catch (e) {
      alert('El usuario no ha podido crearse');
    }

  }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if (!control.parent || !control) {
    return null;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if (!password || !passwordConfirm) {
    return null;
  }

  if (passwordConfirm.value === '') {
    return null;
  }

  if (password.value === passwordConfirm.value) {
    return null;
  }

  return { passwordsNotMatching: true };

};
