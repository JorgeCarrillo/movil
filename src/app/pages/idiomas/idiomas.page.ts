import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../services/auth.service';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import {
  PropertyService,
  // BrokerService,
} from '../../providers';

@Component({
  selector: 'app-idiomas',
  templateUrl: './idiomas.page.html',
  styleUrls: ['./idiomas.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
        query(':enter', stagger('300ms', [animate('500ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class IdiomasPage implements OnInit {
  idiomas
  idIdiomaSelec
  properID: any;
  property: any;
  public onRegisterForm: FormGroup;
  rolesRegister: any[];
  constructor(
    private _authService: AuthService,
    private _httpClient: HttpClient,
    private route: ActivatedRoute,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private readonly router: Router,
    public alertController: AlertController,
    private propertyService: PropertyService,) {

    this.property = this.propertyService.getItem(this._authService.getIdUsuario())
    this.traerIdiomas()
    this.obtenerdatos()

  }

  ngOnInit() {
  }
  async obtenerdatos() {

    if (this._authService.getIdUsuarioRol() === null) {
      this.properID = 'null'
    } else {
      this.properID = this._authService.getIdUsuarioRol()
    }
  }

  traerIdiomas() {
    this.idiomas = []
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'idioma/filtro', header)
      .subscribe(residioma => {
        this.idiomas = residioma;
      }, error => {
      });

  }

  async Agregaridioma() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    let alert = await this.alertController.create({
      header: 'Agregar idioma',
      message: 'Ingrese el nuevo Idioma.',
      inputs: [
        { name: 'idioma', 'placeholder': "Idioma", 'value': name },
        { name: 'nivel', 'placeholder': "Nivel idioma", 'value': name }
      ],
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Agregar', handler: data => {

          let dataidioma
          dataidioma = data.idioma
          dataidioma = dataidioma.toLowerCase()
          dataidioma = dataidioma[0].toUpperCase() + dataidioma.slice(1);
          let datanivel
          datanivel = data.nivel
          datanivel = datanivel.toLowerCase()
          datanivel = datanivel[0].toUpperCase() + datanivel.slice(1);

          let body = {

            idioma: dataidioma,
            nivel: datanivel,
            preferencia: true,
            id_usuario: parseInt(this._authService.getIdUsuario(), 10)
          }

          this._httpClient.post(environment.url + 'idioma', body, header)
            .subscribe(async rescat => {
              this.traerIdiomas()
              let alert1 = await this.alertController.create({
                header: 'Agregación Idioma',
                cssClass: 'alertpedido',
                message: 'Idioma agregado exitosamente.',
                //inputs: this.data,
                buttons: [
                  {
                    text: 'Aceptar',
                    handler: () => {
                      this.navCtrl.navigateForward('/home-results');
                    }
                  }
                ]
              });
              alert1.present();
            }, error => {
            });
        }
      }
      ]
    });
    alert.present();

  }
  async EnviaridiomaSelect() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    if (this.idIdiomaSelec == undefined) {
      let alert = await this.alertController.create({
        header: 'Alerta Registro',
        message: 'Porfavor seleccione un idioma para agregar.',
        buttons: [{ text: 'Cancelar', role: 'cancel' },
        {
          text: 'Aceptar', handler: data => {
          }
        }
        ]
      });
      alert.present();

    } else {

      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      let auxidio
      let auxnivel
      let str = this.idIdiomaSelec;
      let res = str.split(" ");
      auxidio = res[0]
      auxnivel = res[1]

      let alert = await this.alertController.create({
        header: 'Agregar Idioma',
        message: 'Desea Agregar este idioma: ' + auxidio + ' - ' + auxnivel,
        buttons: [{ text: 'Cancelar', role: 'cancel' },
        {
          text: 'Agregar', handler: data => {
            let body = {
              idioma: auxidio,
              nivel: auxnivel,
              preferencia: true,
              id_usuario: parseInt(this._authService.getIdUsuario(), 10)
            }

            this._httpClient.post(environment.url + 'idioma', body, header)
              .subscribe(async rescat => {
                let alert1 = await this.alertController.create({
                  header: 'Agregación Idioma',
                  cssClass: 'alertpedido',
                  message: 'Idioma agregado exitosamente.',
                  //inputs: this.data,
                  buttons: [
                    {
                      text: 'Aceptar',
                      handler: () => {
                        this.navCtrl.pop()
                      }
                    }
                  ]
                });
                alert1.present();
              }, error => {
              });
          }
        }
        ]
      });
      alert.present()
    }
  }
  async deleteIdioma(i) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    let alert = await this.alertController.create({
      header: 'Eliminar Titulo',
      message: 'Esta seguro de eliminar este titulo:',
      inputs: [
        { name: 'senescyt_titulos_profe', 'disabled': true, 'placeholder': i.nombre, 'value': i.nombre },
        { name: 'nivel_profesor', 'disabled': true, 'placeholder': i.nivel, 'value': i.nivel }
      ],
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Aceptar', handler: data => {
          this._httpClient.delete(environment.url + 'idioma/' + i.id_idioma, header)
            .subscribe(async residomas => {
              let alert1 = await this.alertController.create({
                header: 'Eliminación Idioma',
                cssClass: 'alertpedido',
                message: 'Idioma eliminado exitosamente.',
                //inputs: this.data,
                buttons: [
                  {
                    text: 'Aceptar',
                    handler: () => {
                      this.navCtrl.pop()
                    }
                  }
                ]
              });
              alert1.present();

            }, error => {
            });
        }
      }
      ]
    });
    alert.present();

  }
  async updateIdioma(data1) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    let alert = await this.alertController.create({
      header: 'Editar Titulo',
      message: 'Escriba su nuevo titulo para actualizar.',
      inputs: [
        { name: 'idioma', 'placeholder': data1.nombre, 'value': data1.nombre },
        { name: 'nivel', 'placeholder': data1.nivel, 'value': data1.nivel }
      ],
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Actualizar', handler: data => {
          let dataidioma
          dataidioma = data.idioma
          dataidioma = dataidioma.toLowerCase()
          dataidioma = dataidioma[0].toUpperCase() + dataidioma.slice(1);
          let datanivel
          datanivel = data.nivel
          datanivel = datanivel.toLowerCase()
          datanivel = datanivel[0].toUpperCase() + datanivel.slice(1);
          let body = {
            idioma: dataidioma,
            nivel: datanivel,
          }
          this._httpClient.put(environment.url + 'idioma/' + data1.id_idioma, body, header)
            .subscribe(async residioma => {
              this.traerIdiomas()
              let alert1 = await this.alertController.create({
                header: 'Actualización Idioma',
                cssClass: 'alertpedido',
                message: 'Idioma actualizado exitosamente.',
                //inputs: this.data,
                buttons: [
                  {
                    text: 'Aceptar',
                    handler: () => {
                      this.navCtrl.navigateForward('/home-results');
                    }
                  }
                ]
              });
              alert1.present();
            }, error => {
            });
        }
      }
      ]
    });
    alert.present();
  }
}
