
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { EnvioCorreoServiceService } from "../../services/envio-correo-service.service";
import { environment } from "../../../environments/environment";
import {
  BrokerService, PropertyService
} from '../../providers';
import { AuthService } from '../../services/auth.service';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-broker-chat',
  templateUrl: './broker-chat.page.html',
  styleUrls: ['./broker-chat.page.scss'],
})


export class BrokerChatPage implements OnInit {

  @ViewChild("scrollElement", { static: true }) content: IonContent;
  brokerID: any;
  broker: any;
  cargando = true;
  propierties;
  conversation: any = [];
  idUsuario;
  input = '';
  usuariosRol;
  usuarioemisorid;
  usuarioreceptorid;
  mensajes;
  mensajesrecibidos;
  mensaje;
  mensajerecibido;
  fotoreceptor;
  fotoemisor;

  constructor(
    private brokerService: BrokerService,
    private route: ActivatedRoute,
    private router: Router,
    private service: PropertyService,
    private autservice: AuthService,
    private readonly _authService: AuthService,
    private readonly _httpClient: HttpClient,

  ) {
    this.route.queryParams.subscribe(params => {
      this.router.getCurrentNavigation().extras.state.broker
    });
    this.brokerID = this.router.getCurrentNavigation().extras.state.broker



  }

  ngOnInit() {

  }
  traermensajes() {
    this.mensajesEnviados().subscribe(res => {
      this.scrollToBottom()
      this.traermensajes()

    })
  }

  ionViewDidEnter() {
    this.idUsuario = this.autservice.getIdUsuario()
    this.propierties = this.service.properties

    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'usuario-rol', header)
      .subscribe(res => {
        this.usuariosRol = res
        for (let a of this.usuariosRol) {
          if (a.usuario.id_usuario == this.brokerID) this.usuarioreceptorid = a.id_usuario_rol
          if (a.usuario.id_usuario == this.idUsuario) this.usuarioemisorid = a.id_usuario_rol
        }
        this._httpClient.get(environment.url + 'usuario/idrol/' + this.usuarioreceptorid)
          .subscribe(resusuario => {
            if (resusuario[0].foto_usuario) this.fotoreceptor = environment.url + "public/usuario/" + resusuario[0].foto_usuario
            else this.fotoreceptor = "https://definicion.de/wp-content/uploads/2019/06/perfildeusuario.jpg"
            this.broker = {
              name: resusuario[0].nombres_usuario + " " + resusuario[0].apellidos_usuario
            }
          }, error => {
          });
        this._httpClient.get(environment.url + 'usuario/idrol/' + this.usuarioemisorid)
          .subscribe(resusuario => {
            if (resusuario[0].foto_usuario) {
              this.fotoemisor = environment.url + "public/usuario/" + resusuario[0].foto_usuario
            } else {
              this.fotoemisor = "https://definicion.de/wp-content/uploads/2019/06/perfildeusuario.jpg"
            }
          }, error => {
          });
        this.traermensajes()

      }, error => {

      });

    this.idUsuario = this.autservice.getIdUsuario()
    this.propierties = this.service.properties
    for (let a of this.propierties) {
      if (a.id == this.brokerID) {
        this.broker = a
        this.conversation = [
          { text: 'Hola como vas?', sender: 0, image: this.broker.picture, fecha: new Date(), id: 0 }
        ]
        this.conversation.shift()
      }
    }
    setTimeout(() => {
      this.scrollToBottom()
    }, 10)
  }

  send() {
    let fecha = new Date()
    let body
    let mensaje;
    if (this.input !== '') {
      body = {
        mensaje: this.input,
        hora: fecha.getHours() + ":" + fecha.getMinutes(),
        fecha_mensaje: fecha

      }
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.post(environment.url + 'mensaje', JSON.stringify(body), header)
        .subscribe(mensaj => {
          mensaje = mensaj
          body = {
            "id_emisor": this.usuarioemisorid,
            "id_receptor": this.usuarioreceptorid,
            "id_mensaje": mensaje.identifiers[0].id_mensaje

          }
          this._httpClient.post(environment.url + 'emisor_receptor', JSON.stringify(body), header)
            .subscribe(emisor_receptor => {
              this.conversation.push({ text: this.input, sender: 1, image: this.fotoemisor, fecha: fecha, id: 1 });

              this.input = '';
            }, error => {

            });
        }, error => {

        });

      setTimeout(() => {
        this.scrollToBottom()
      }, 100)
    }
  }


  scrollToBottom() {



    let content = document.getElementById("chat-container");
    let parent = document.getElementById("chat-parent");
    let scrollOptions = {
      left: 0,
      top: 1000
    }
    content.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
  }
  mensajesEnviados() {
    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.get(environment.url + 'emisor_receptor/emisor/' + this.usuarioemisorid + "/" + this.usuarioreceptorid, header)
        .subscribe(resmen => {
          this.mensajes = resmen
          this.conversation = []
          for (let b of this.mensajes) {


            let hora;
            hora = new Date(b.fecha_mensaje).toLocaleString()
            hora = hora.split(" ")
            hora = hora[1].split(":")
            hora = hora[0] + ":" + hora[1]
            if (b.id_emisor == this.usuarioemisorid) this.conversation.push({ text: b.mensaje, sender: 1, image: this.fotoemisor, fecha: b.fecha_mensaje, id: b.id_mensaje, hora: hora });
            else this.conversation.push({ text: b.mensaje, sender: 0, image: this.fotoreceptor, fecha: b.fecha_mensaje, id: b.id_mensaje, hora: hora });

          }

          setTimeout(() => {
            observer.next(this.mensajes)
          }, 1000)

        }, error => {
        });

    })
  }

}







