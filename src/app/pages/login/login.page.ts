import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateProvider } from '../../providers';
import { LoginInterface } from "../../interfaces/login.interface";
import { AuthInterface } from '../../interfaces/auth.interface';
import { UserInterface } from "../../interfaces/user.interface";
import { environment } from "../../../environments/environment";
import { AuthService } from "../../services/auth.service";
import { Router, RouterStateSnapshot } from "@angular/router";
import { UsuarioService } from "../../services/usuario.service";
import * as CryptoJS from 'crypto-js';
import { JwtHelperService } from '@auth0/angular-jwt';
import { EnvioCorreoServiceService } from "../../services/envio-correo-service.service";
import { AppComponent } from '../../app.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public onLoginForm: FormGroup;
  private intentos: any;
  fecha = new Date()

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private translate: TranslateProvider,
    private formBuilder: FormBuilder,
    private authSvc: AuthService,
    private router: Router,
    private _jwtHelper: JwtHelperService,
    private readonly _usuarioService: UsuarioService,
    private _correo: EnvioCorreoServiceService,
    private appcomponet: AppComponent,
  ) { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      'nombre_login': [null, Validators.compose([
        Validators.required
      ])],
      'password_login': [null, Validators.compose([
        Validators.required
      ])]
    });
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    const root = snapshot.root;
    const queryParams = root.queryParams;
    if (queryParams.id && queryParams.autorizathion) {
      this.activarUsuario(queryParams);
    }
  }

  async forgotPass() {
    const alert = await this.alertCtrl.create({
      header: this.translate.get('app.pages.login.label.forgot'),
      message: this.translate.get('app.pages.login.text.forgot'),
      inputs: [
        {
          name: 'usuario',
          type: 'email',
          placeholder: this.translate.get('app.label.email')
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Confirm',
          handler: async (data) => {
            const loader = await this.loadingCtrl.create({
              duration: 2000
            });

            this.authSvc.usuarioRecuperar = data.usuario
            this.authSvc.crearTokenRecuperar(JSON.stringify(data)).subscribe(res => {
              loader.present();
              if (res) {
                loader.present();
                loader.onWillDismiss().then(async l => {
                  const toast = await this.toastCtrl.create({
                    showCloseButton: true,
                    message: "Correo enviado correctamente",
                    duration: 3000,
                    position: 'bottom'
                  });

                  toast.present();
                  this.router.navigate(["/recover-password"])
                });
              } else {
                loader.present();
                loader.onWillDismiss().then(async l => {
                  const toast = await this.toastCtrl.create({
                    showCloseButton: true,
                    message: "No se encontró el correo",
                    duration: 3000,
                    position: 'bottom'
                  });

                  toast.present();

                });




              }
            })


          }
        }
      ]
    });

    await alert.present();
  }

  // // //
  goToRegister() {
    this.navCtrl.navigateRoot('/register');
  }

  async goToHome() {
    const login: LoginInterface = this.onLoginForm.value;
    try {

      const token: AuthInterface = await this.authSvc.login(login.nombre_login, login.password_login);
      if (token && token.accessToken) {

        const auxToken = Object.values(token)[0];
        let user: UserInterface;

        if (Object.values(token)[1]) {
          user = Object.values(token)[1];
        }

        const decodeToken = this._jwtHelper.decodeToken(auxToken);
        this.authSvc.setCurrentUser(CryptoJS.AES.encrypt(auxToken, environment.tokenKey).toString(), decodeToken, user);
        environment.nombreUsuario = decodeToken.data.nombre_login;
        this.appcomponet.obtenerdatos();
        this.authSvc.enLinea(new Date(this.fecha.setMinutes(this.fecha.getMinutes() - 5)).toISOString(), false).subscribe(res => {
        })



        if (this.authSvc.getRol() === 'ESTUDIANTE') {
          this.authSvc.getfoto()
          if (JSON.parse(this.authSvc.getEstadoCuenta())) {
            this.navCtrl.navigateRoot('/home-results');
          } else {
            this.navCtrl.navigateRoot('/home-results');
          }

        }
        if (this.authSvc.getRol() === 'PROFESOR') {
          this.authSvc.getfoto()
          if (JSON.parse(this.authSvc.getEstadoCuenta())) {
            this.navCtrl.navigateRoot('/home-results');
          } else {
            this.navCtrl.navigateRoot('/home-results');
          }
        }


      }

    } catch (error) {
      this.intentos += this.intentos + 1;
      console.log(error);
    }

  }


  async activarUsuario(params) {
    const decodeToken = this._jwtHelper.decodeToken(params.autorizathion);
    const res = await this._usuarioService.activeUser(params.id, params.autorizathion, decodeToken.data.nombre_rol);
    if (res) {
      this.router.navigate(['/login']);
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        message: 'Inicie sesión para activar su cuenta.',
        duration: 2000,
        position: 'bottom'
      });

      toast.present();
    } else {
      const resPost = await this._correo.postEnvioCreacion({
        nombre_login: decodeToken.data.nombre_login,
        id_usuario: Number(params.id),
        nombre_rol: decodeToken.data.nombre_rol
      });

      if (resPost) {
        alert('El enlace a expirado, se ha enviado nuevamente un correo de activación, recuerde que tiene 5 min para activar su cuenta');
        await this.router.navigate(['/pages/auth/mail-confirm', { email: decodeToken.data.nombre_login }]);
      }
    }
  }

  private checkUserIsVerified(user: UserInterface) {
    /*if (user && user.emailVerified) {
        this.router.navigate(['/apps/academy/courses']);
    } else if (user) {
        this.router.navigate(['/verification-email']);
    } else {
        this.router.navigate(['/pages/auth/register']);
    }*/
  }

}
