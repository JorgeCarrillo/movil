import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CancelarhoraPage } from './cancelarhora.page';

describe('CancelarhoraPage', () => {
  let component: CancelarhoraPage;
  let fixture: ComponentFixture<CancelarhoraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelarhoraPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CancelarhoraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
