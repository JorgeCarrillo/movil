import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
@Component({
  selector: 'app-cancelarhora',
  templateUrl: './cancelarhora.page.html',
  styleUrls: ['./cancelarhora.page.scss'],
})
export class CancelarhoraPage implements OnInit {
  eventSource
  obj
  final
  event
  idrol;
  horas = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
  newevent
  horainicio = null;
  horafin = null;
  fechainicio = null;
  fechafin = null;
  calendario = true
  isToday: boolean;
  cargar = true
  array = false
  menor = false
  viewTitle;
  itemHeight: number = 0;
  calendar = {
    mode: 'week',
    menor: "0",
    mayor: "24",
    currentDate: new Date(),
    dateFormatter: {
      formatMonthViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getFullYear();
      },
      formatWeekViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getFullYear();
      },
      formatDayViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
      },
    },
  };
  constructor(navparams: NavParams, public modalCtrl: ModalController) {
    this.eventSource = navparams.get('horas')
  }

  ngOnInit() {
  }
  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  disponible(ev) {
    this.modalCtrl.dismiss({
      'idhora': ev.event.id,
      'horainicio': ev.event.startTime
    });

  }
  onEventSelected() {

  }
  closeModal() {
    this.modalCtrl.dismiss();
  }

}
