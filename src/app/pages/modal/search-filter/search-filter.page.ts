import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { CategoriasService } from "../../../services/categorias.service";
import { PropertyService } from '../../../providers';
//import { PropertyService } from '../../providers';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.page.html',
  styleUrls: ['./search-filter.page.scss'],
})
export class SearchFilterPage implements OnInit {
  organizeby: any;
  proptype: any;
  wantslabel: any;
  objects: any;
  categorias: Array<any>;
  searchKey = '';
  searchKei: Array<any>;
  auxrating: any;
  auxidioma: any;
  jsonfiltros = {
    categoria: null,
    horario: null,
    pais: null,
    idioma: null,
    estrella: null,
    precmin: null,
    preciomax: null,
    filtrohorario: null
  }
  auxminprice: any;
  auxmaxprice: any;
  maxprice;
  properties;
  idiomas: Array<any>;

  public radiusmiles = 1;
  public minmaxprice = {
    upper: 100,
    lower: 0
  };
  auxcategoria: number;
  pais: Array<any>;

  public selectObjectById(list: any[], id: string, property: string) {
    var item = list.find(item => item._id === id);
    var prop = eval('this.' + property);
    prop = property;
  }

  constructor(
    private modalCtrl: ModalController,
    public completeTestService: CategoriasService,
    public service: PropertyService,
    public navctrl: NavController
  ) {
  }
  ngOnInit() {
  }

  ionViewWillEnter() {
    this.findAll();
    this.allidiomas();
    this.allpais();
    this.allcategorias();
  }

  ionViewDidEnter() {
    this.service.maxPrecio().subscribe(res => {
      let enteromax
      enteromax = parseInt(res[0].preciofijo) + 10
      this.maxprice = enteromax

      this.minmaxprice = {
        upper: this.maxprice,
        lower: 0
      };

    })
  }

  closeModal() {
    this.modalCtrl.dismiss(this.jsonfiltros);
  }


  findAll() {
    this.service.findAll().subscribe(res => {
      this.properties = res

    })
  }



  logRatingChange(rating) {
    this.auxrating = rating;
    this.jsonfiltros.estrella = this.auxrating
  }

  logPriceChange(event) {
    this.auxminprice = this.minmaxprice.lower
    this.auxmaxprice = this.minmaxprice.upper
    this.jsonfiltros.preciomax = this.auxmaxprice
    this.jsonfiltros.precmin = this.auxminprice
  }


  allidiomas() {
    this.idiomas = []
    this.service.traerIdiomas()
      .then(data => {
        this.idiomas = data;
      }).catch(error => alert(error));
  }

  allpais() {
    this.pais = []
    this.service.AllPais()
      .then(datapais => {
        this.pais = datapais;
      }).catch(error => alert(error));
  }

  allcategorias() {
    this.categorias = []
    this.service.AllCategorias()
      .then(datacategorias => {
        this.categorias = datacategorias;
      }).catch(error => alert(error));
  }
  reiniciar() {

    for (let a in this.jsonfiltros) {
      this.jsonfiltros[a] = null
    }
    this.modalCtrl.dismiss("reiniciar")
  }

}
