import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AvailabilityPagePageRoutingModule } from './availability-page-routing.module';
import { NgCalendarModule } from "ionic2-calendar";
import { AvailabilityPagePage } from './availability-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,

    NgCalendarModule,
    AvailabilityPagePageRoutingModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-EC' }
  ],
  declarations: [AvailabilityPagePage]
})
export class AvailabilityPagePageModule { }
