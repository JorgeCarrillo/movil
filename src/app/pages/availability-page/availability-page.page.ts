import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController, ModalController, ToastController, LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ImagePage } from './../modal/image/image.page';
import * as moment from 'moment';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';
registerLocaleData(localeES);
import {
  PropertyService,
  // BrokerService,
} from '../../providers';

import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-availability-page',
  templateUrl: './availability-page.page.html',
  styleUrls: ['./availability-page.page.scss'],
})
export class AvailabilityPagePage implements OnInit {

  eventSource
  obj
  final
  event
  idrol;
  horas = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
  newevent
  horainicio = null;
  horafin = null;
  fechainicio = null;
  fechafin = null;
  calendario = true
  isToday: boolean;
  cargar = true
  array = false
  menor = false
  itemHeight: number = 0;
  calendar = {
    mode: 'week',
    menor: "0",
    mayor: "24",
    currentDate: new Date(),
    dateFormatter: {
      formatMonthViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getFullYear();
      },
      formatWeekViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getFullYear();
      },
      formatDayViewTitle: function (date: Date) {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ];
        return monthNames[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
      },
    },
  };
  constructor(
    public asCtrl: ActionSheetController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public route: ActivatedRoute,
    public alertController: AlertController,
    public router: Router,
    private propertyService: PropertyService,
    private authservice: AuthService,
    public loadingCtrl: LoadingController) {
    this.idrol = authservice.getIdUsuarioRol()
    this.presentLoadingDefault()
    //this.generarDisponibilidad()
  }
  async presentLoadingDefault() {
    let loading = await this.loadingCtrl.create({
      message: "Cargando..."
    });
    loading.present();
    this.propertyService.getDetaHorario(+this.authservice.getIdUsuario()).subscribe(res => {
      this.eventSource = res;
      this.cargar = false
      loading.dismiss()

      this.calendario = true;
    })

  }
  MOstrarDatos() {
  }
  async onTimeSelected(ev) {





    if (ev.selectedTime) {
      if (ev.events.length == 0) this.array = false;
      else this.array = true

      this.event = ev.events
      this.obj = new Date(ev.selectedTime)
      if (this.obj < new Date()) this.menor = true
      else this.menor = false


      this.final = new Date(ev.selectedTime.setHours(ev.selectedTime.getHours() + 1))

      this.newevent = {
        hora_inicio: this.obj.toISOString(), hora_fin: this.final.toISOString(), estado_reserva: 1, id_usuario_rol: +this.idrol
      }


    }
    else if (ev.isTrusted && this.array && !this.menor) {


      let dia = new Date(this.obj).getDate();
      this.calendario = false
      let alert = await this.alertController.create({
        header: 'Borrar hora',
        message: '¿Estas seguro de eliminar esta hora?',
        buttons: [
          {
            text: 'No',
            role: 'No',
            handler: () => {
              this.calendario = true
            }
          },
          {
            text: 'Si',
            handler: () => {
              this.propertyService.eliminarhora(this.event[0].id, +this.idrol)
              this.presentLoadingDefault()


            }
          }
        ]
      });
      alert.present();

    };
    if (ev.isTrusted && !this.array && !this.menor) {


      let dia = new Date(this.obj).getDate();
      this.calendario = false
      let alert = await this.alertController.create({
        header: 'Nueva hora',
        message: '¿Seguro de agregar una nueva hora el dia ' + dia + ' de las ' + new Date(this.obj).getHours() + ' a las ' + (new Date(this.obj).getHours() + 1) + '?',
        buttons: [
          {
            text: 'No',
            role: 'No',
            handler: () => {
              this.calendario = true
            }
          },
          {
            text: 'Si',
            handler: () => {
              this.propertyService.setreserva(this.newevent, this.authservice.getIdUsuario())
              this.presentLoadingDefault()


            }
          }
        ]
      });
      alert.present();

    };



  }

  onEventSelected() {

  }
  ngOnInit() {
  }
  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }
  onViewTitleChanged(title) {

  }
  today() {
    this.calendar.currentDate = new Date();
  }
  changeMode(mode) {
    this.calendar.mode = mode;
  }
  generarDisponibilidad() {
    let event = []
    if (this.fechainicio == null) this.crearAlert("Cuiado!!", "Fecha inicial requerida");
    else if (this.horainicio == null) this.crearAlert("Cuiado!!", "Hora inicial requerida");
    else if (this.horafin == null) this.crearAlert("Cuiado!!", "Hora final requerida");
    else if (this.fechafin == null) this.crearAlert("Cuiado!!", "Fecha final requerida");
    else if (this.horafin < this.horainicio) this.crearAlert("Cuiado!!", "Para programar nuevas disponibilidades  la hora final debe ser mayor");
    else if (this.horafin == this.horainicio) this.crearAlert("Cuiado!!", "Para programar nuevas disponibilidades la hora final debe ser diferente a la inicial");
    else if (new Date(this.fechafin) < new Date(this.fechainicio)) this.crearAlert("Cuiado!!", "Para programar nuevas disponibilidades la fecha final debe ser mayor a la inicial");
    else {
      let splitfechainicio = this.fechainicio.split("-");
      let splitfechafin = this.fechafin.split("-");
      let fechainicio = new Date(new Date(this.fechainicio).setHours(this.horainicio))
      fechainicio = new Date(fechainicio.setDate(splitfechainicio[2]))
      fechainicio = new Date(fechainicio.setFullYear(splitfechainicio[0]))
      let fechafin = new Date(new Date(this.fechafin).setHours(this.horainicio))
      fechafin = new Date(fechafin.setDate(splitfechafin[2]))

      fechafin = new Date(fechafin.setFullYear(splitfechafin[0]))
      let fechaHoraFin = new Date(new Date(fechainicio).setHours(this.horafin))

      for (fechainicio; fechainicio <= fechafin; fechainicio = new Date(fechainicio.setDate(fechainicio.getDate() + 1))) {
        for (fechainicio; fechainicio <= fechaHoraFin; fechainicio = new Date(fechainicio.setHours(fechainicio.getHours() + 1))) {
          let horaini = new Date(fechainicio.setSeconds(fechainicio.getSeconds() - fechainicio.getSeconds()))
          event.push({
            hora_inicio: horaini.toISOString(), hora_fin: new Date(horaini.setHours(horaini.getHours() + 1)).toISOString(), estado_reserva: 1, id_usuario_rol: +this.idrol
          })
          this.propertyService.setreservas(this.newevent)


        }

        fechaHoraFin = new Date(fechaHoraFin.setDate(fechaHoraFin.getDate() + 1))
        fechainicio = new Date(fechainicio.setHours(this.horainicio))
      }
      this.propertyService.setreservas(event).subscribe(res => {
        this.presentLoadingDefault()

      })
    }
  }


  async crearAlert(header, mensaje) {
    let alert = await this.alertController.create({
      header: header,
      message: mensaje,
      buttons: [
        {
          text: 'OK',
          role: 'No',
          handler: () => {

          }
        }
      ]
    });
    alert.present();

  };


}
