import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TeachesPage } from './teaches.page';

describe('TeachesPage', () => {
  let component: TeachesPage;
  let fixture: ComponentFixture<TeachesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeachesPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TeachesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
