import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { AlertController } from '@ionic/angular';
import { AuthService } from '../../services/auth.service';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import {
  PropertyService,
  // BrokerService,
} from '../../providers';

@Component({
  selector: 'app-teaches',
  templateUrl: './teaches.page.html',
  styleUrls: ['./teaches.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
        query(':enter', stagger('300ms', [animate('500ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class TeachesPage implements OnInit {
  categorias;
  materias;
  idCategoriaSelec;
  idMateriaSelec;
  ultimopersonalizado
  idpersonalizado
  ultimodetallemateria
  iddetallemateria
  idservicioper
  idpermate
  ultimoservicioper
  properID: any;
  property: any;
  public onRegisterForm: FormGroup;
  rolesRegister: any[];
  actualizar = false
  constructor(
    private _authService: AuthService,
    private _httpClient: HttpClient,
    private route: ActivatedRoute,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private readonly router: Router,
    public alertController: AlertController,
    private propertyService: PropertyService,) {

    this.property = this.propertyService.getItem(this._authService.getIdUsuario())
    this.traerCategorias()
    this.traerMaterias()
    this.obtenerdatos()
    this.propiedades()

  }

  ngOnInit() {
  }

  async propiedades() {



  }
  async obtenerdatos() {

    if (this._authService.getIdUsuarioRol() === null) {
      this.properID = 'null'
    } else {
      this.properID = this._authService.getIdUsuarioRol()
    }
  }


  traerMaterias() {
    this.materias = []
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'materia/', header)
      .subscribe(resmaterias => {
        this.materias = resmaterias;
      }, error => {
      });

  }
  traerCategorias() {
    this.categorias = []
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'categoria/', header)
      .subscribe(rescategoria => {
        this.categorias = rescategoria;
      }, error => {
      });

  }

  async Agregarcategoria() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    let alert = await this.alertController.create({
      header: 'Agregar Categoria',
      message: 'Ingrese la nueva Categoria.',
      inputs: [
        { name: 'nombre_categoria', 'placeholder': "Nombre Categoria", 'value': name }
      ],
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Agregar', handler: data => {
          // data.id_categoria=11;
          this._httpClient.post(environment.url + 'categoria/', data, header)
            .subscribe(rescat => {
              this.traerCategorias()
            }, error => {
            });
        }
      }
      ]
    });
    alert.present();
  }

  async Agregarmateria(idcategoria) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    let alert = await this.alertController.create({
      header: 'Agregar Materia',
      message: 'Ingrese la nueva Materia.',
      inputs: [
        { name: 'nombre_materia', 'placeholder': "Nombre Materia", 'value': name }
      ],
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Agregar', handler: data => {
          let body = {
            nombre_materia: data.nombre_materia,
            id_categoria: idcategoria,
          }
          this._httpClient.post(environment.url + 'materia/', body, header)
            .subscribe(rescat => {
              this.traerMaterias()
            }, error => {
            });
        }
      }
      ]
    });
    alert.present();

  }

  async enviarensena() {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    if (this.idMateriaSelec == undefined) {
      let alert = await this.alertController.create({
        header: 'Alerta Registro',
        message: 'Porfavor llene todos los campos.',
        buttons: [{ text: 'Cancelar', role: 'cancel' },
        {
          text: 'Aceptar', handler: data => {
          }
        }
        ]
      });
      alert.present();

    } else {
      let alert = await this.alertController.create({
        header: 'Agregar Materia',
        message: 'Desea Agregar esta materia a sus clases.',
        buttons: [{ text: 'Cancelar', role: 'cancel' },
        {
          text: 'Agregar', handler: data => {
            //todo
            let body = {
              fecha_creacion: new Date().toISOString(),
              demo: true,
              duracion_demo: new Date(new Date().setFullYear(new Date().getFullYear() + 5)).toISOString(),
            }

            //registro personalizado
            this._httpClient.post(environment.url + 'personalizado/', body, header)
              .subscribe(respersonalizado => {

                this.idpersonalizado = respersonalizado;
                this.ultimopersonalizado = this.idpersonalizado.identifiers[0].id_tutoria;

                let bodydeta = {
                  id_tutoria: this.ultimopersonalizado,
                  id_materia: this.idMateriaSelec
                }
                //registro deta_dia_hora    
                this._httpClient.post(environment.url + 'detalle_materia', bodydeta, header)
                  .subscribe(resdetalleMateria => {
                    this.iddetallemateria = resdetalleMateria;
                    this.ultimodetallemateria = this.iddetallemateria.identifiers[0].id_deta_per_mate;
                  }, error => {
                  });

                let bodyperserv = {
                  id_tutoria: this.ultimopersonalizado,
                  id_servicio: 1
                }
                //registro serper   
                this._httpClient.post(environment.url + 'asig-servicio-per', bodyperserv, header)
                  .subscribe(resservicioper => {
                    this.idservicioper = resservicioper;
                    this.ultimoservicioper = this.idservicioper.identifiers[0].id_asig_per_serv;
                    let bodyuserper = {
                      id_usuario_rol: +this.properID,
                      id_asig_per_serv: this.ultimoservicioper
                    }
                    //registro serper   

                    this._httpClient.post(environment.url + 'asig-user-per', bodyuserper, header)
                      .subscribe(resuserperr => {
                        this.propertyService.faltaMaterias = false

                      }, error => {
                      });
                  }, error => {
                  });


              }, error => {
              });

            this.navCtrl.pop()
            // this.navCtrl.navigateRoot('/login');

          }
        }
        ]
      });
      alert.present()



    }
  }
  editarensena(ensena) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    for (let a of this.categorias) {
      if (a.nombre_categoria == ensena.categoria) {
        this.idCategoriaSelec = a.id_categoria
      }
    }
    this.idMateriaSelec = ensena.id_materia

    this.actualizar = true
    this._httpClient.get(environment.url + 'detalle_materia/idtuto/' + ensena.id_tutoria, header)
      .subscribe(resmateria => {
        //location.reload();
        let auxmate
        auxmate = resmateria
        // this./detalle_materia/idtuto/1
        this.idpermate = auxmate.id_deta_per_mate

      }, error => {
      });

  }
  async actualizarensena(ensena) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    let bodydeta = {
      id_materia: this.idMateriaSelec
    }
    let alert = await this.alertController.create({
      header: 'Actualizar Materia',
      message: 'Esta seguro de actualizar esta materia.',
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      {
        text: 'Actualizar', handler: data => {
          this._httpClient.put(environment.url + 'detalle_materia/' + this.idpermate, bodydeta, header)
            .subscribe(resmat => {
              //location.reload();
              this.navCtrl.pop()
            }, error => {
            });
        }
      }
      ]
    });
    alert.present();

  }



}
