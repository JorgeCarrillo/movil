import { File } from '@ionic-native/File/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { AgmCoreModule } from '@agm/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AppComponent } from './app.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
// Services/Providers
import { TranslateProvider, PropertyService } from './providers';

// Modal Pages
import { ImagePageModule } from './pages/modal/image/image.module';
import { CancelarhoraPageModule } from './pages/modal/cancelarhora/cancelarhora.module';
import { SearchFilterPageModule } from './pages/modal/search-filter/search-filter.module';

// Environment
import { environment } from '../environments/environment';

// Components
import { NotificationsComponent } from './components/notifications/notifications.component';

// Pipes
import { PipesModule } from './pipes/pipes.module';
import { AuthService } from "./services/auth.service";
import { LoginService } from "./services/login.service";
import { EnvioCorreoServiceService } from "./services/envio-correo-service.service";
import { JwtModule } from "@auth0/angular-jwt";
import * as CryptoJS from 'crypto-js';
import { NgCalendarModule } from "ionic2-calendar";
import localeDe from '@angular/common/locales/de';
import { registerLocaleData } from "@angular/common";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx'
import { CancelarhoraPage } from './pages/modal/cancelarhora/cancelarhora.page';
registerLocaleData(localeDe);

export function tokenGetter() {
  if (localStorage.getItem('currentUser')) {
    var bytes = CryptoJS.AES.decrypt(JSON.parse(localStorage.getItem('currentUser')).token, environment.tokenKey);
    return bytes.toString(CryptoJS.enc.Utf8);
  }

}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent, NotificationsComponent],
  imports: [
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    }),
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(environment.config),
    AppRoutingModule,
    HttpClientModule,
    ImagePageModule,
    CancelarhoraPageModule,
    SearchFilterPageModule,
    NgCalendarModule,
    NgxPaginationModule,
    IonicStorageModule.forRoot({
      name: '__ionproperty2',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBnTnX1cVqp8AbMAL6TNL50WV8pKPI6t7Q'
    }),
    PipesModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  entryComponents: [NotificationsComponent,CancelarhoraPage],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    WebView,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    FileTransfer,
    TranslateProvider,
    PropertyService,
    AuthService,
    InAppBrowser,
    LoginService,
    EnvioCorreoServiceService,
    File,
    FilePath,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
