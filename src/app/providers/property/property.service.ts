import { CiudadInterface } from './../../interfaces/ciudad.interface';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../../services/auth.service";
import { EnvioCorreoServiceService } from "../../services/envio-correo-service.service";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Observable } from 'rxjs';
import { async } from '@angular/core/testing';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {
  categoriasall: any[];
  inicio = 7;
  fin = 19;
  constructor(private readonly _httpClient: HttpClient,
    private readonly _authService: AuthService,
    private readonly _correo: EnvioCorreoServiceService,
    private router: Router) {
    //this.traerDatos()
    this.traerCiudadPais(1)

  }
  comentarios = []

  favoriteCounter = 0;
  favorites: Array<any> = [];
  properties = [];
  titulos = [];
  idiomas = [];
  pais = [];
  ciudadpais = []
  fotousuario: string;
  filtro = [];
  idiomasall = [];
  idiomasfiltro = [];
  paisall = [];
  eventsources = []
  lara = "hola"
  faltaTitulos = false;
  faltaMaterias = false;
  faltaDisponibilidad = false;
  faltaCOnfiguracion = false;
  appPages =
    [
      {
        title: 'Inicio',
        url: '/home-results',
        direct: 'root',
        icon: 'browsers',
        falta: true
      },
      {
        title: 'Idiomas',
        url: '/idiomas',
        direct: 'forward',
        icon: 'add',
        falta: true
      },
      {
        title: 'Títulos',
        url: '/titles',
        direct: 'forward',
        icon: 'school',
        falta: this.faltaTitulos
      },

      {
        title: 'Materias',
        url: '/teaches',
        direct: 'forward',
        icon: 'book',
        falta: this.faltaMaterias
      },

      {
        title: 'Disponibilidad',
        url: '/availability-page',
        direct: 'forward',
        icon: 'calendar',
        falta: this.faltaDisponibilidad
      },

      {
        title: 'Mis Tutorías',
        url: '/invoices',
        direct: 'forward',
        icon: 'list-box',
        falta: false

      },


      {
        title: 'Mensajes',
        url: '/messages',
        direct: 'forward',
        icon: 'mail',
        falta: false
      },


      {
        title: 'Soporte',
        url: '/support',
        direct: 'forward',
        icon: 'help-buoy',
        falta: false
      },
      {
        title: 'Configuraciones',
        url: '/settings',
        direct: 'forward',
        icon: 'cog',
        falta: this.faltaCOnfiguracion
      }
    ];

  findAll() {
    return new Observable(observer => {
      this.traerDatos().subscribe(aux => {
        if (aux) {
          this.traeridiomas(aux).subscribe(res => {
            this.traerCiudadPais(aux).subscribe(res => {
              this.traerDetallesprofesor(aux).subscribe(res => {
                this.obtenerdetalles().subscribe(res => {
                  this.obtenerHorario(this.properties).subscribe(res => {
                    //this.validarprofesores().subscribe(res => {
                    observer.next(res)
                    // })
                  })
                })
              })
            })
          })
        } else {
          observer.next(this.properties)
        }
      })

    })
  }
  returnimg() {
    this.traerimg().subscribe(res => {
      return res

    })
  }
  obtenerHorario(propierties) {

    return new Observable(observer => {
      var prop = []
      for (let a of this.properties) {

        this._httpClient.get(environment.url + 'usuario-rol/usuarioid/' + a.id,
          {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          })
          .subscribe(idrol => {
            this._httpClient.get(environment.url + "detalle_horario/hora/'" + new Date().toISOString() + "'/" + idrol[0].id_usuario_rol,
              {
                headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Authorization': 'Bearer ' + this._authService.getToken()
                })
              }).subscribe(horario => {
                var horari: any = horario
                if (horari.length > 0) {
                  a.horas = true
                  if (a.ensena != (0) && a.ensena != [] && a.price != null && a.price != '' && a.horas) {
                    prop.push(a)
                  }
                }
                else {
                  a.horas = false
                }

              }, error => {

              });
          }, error => {

          });
      }
      observer.next(prop)
    })
  }

  validarprofesores() {
    return new Observable(observer => {
      var prop = []
      let a
      for (let property of this.properties) {

        if (property.ensena != (0) && property.ensena != [] && property.price != null && property.price != '') {

          prop.push(property)
        }
      }
      observer.next(prop)
    })
  }
  obtenerusuario(id) {
    return new Observable(observer => {

      this._httpClient.get(environment.url + 'usuario/id/' + id,
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(usuario => {
          observer.next(usuario)
        }, error => {

        });
    })
  }
  validarHorasDisponibles(id) {
    return new Observable(observer => {

      this._httpClient.get(environment.url + 'usuario-rol/usuarioid/' + id,
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(idrol => {
          this._httpClient.get(environment.url + "detalle_horario/hora/'" + new Date().toISOString() + "'/" + idrol[0].id_usuario_rol,
            {
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
              })
            }).subscribe(horario => {
              var horari: any = horario

              if (horari.length < 1) observer.next(true)
              else observer.next(false)

            }, error => {

            });
        }, error => {

        });


    })
  }
  traerimg() {
    return new Observable(observer => {


      this._httpClient.get(environment.url + "usuario/idrol/" + this._authService.getIdUsuarioRol(),
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(img => {

          let imgu = img

          if (img[0].foto_usuario != null || img[0].foto_usuario != "") this.fotousuario = environment.urlfotos + img[0].foto_usuario
          else this.fotousuario = environment.urlfotos + "avatar.png"
          observer.next(this.fotousuario)
        }, error => {

        });
    })
  }
  maxPrecio() {
    return new Observable(observer => {


      this._httpClient.get(environment.url + "usuario/maxprecio",
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(precio => {
          observer.next(precio)
        }, error => {

        });
    })
  }
  getDetaHorario(id) {
    return new Observable(observer => {
      this.eventsources = []
      this._httpClient.get(environment.url + 'usuario-rol/usuarioid/' + id,
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(idrol => {
          this._httpClient.get(environment.url + 'detalle_horario/validarhora/' + idrol[0].id_usuario_rol,
            {
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
              })
            })
            .subscribe(valida => {
              let validar;
              validar = valida
              for (let a of validar) {
                if (new Date(a.fecha_creacion) < new Date(new Date().setHours(new Date().getHours())) && a.estado == false) {
                  this._httpClient.get(environment.url + 'detalle_horario/updateestado/' + a.id_deta_dia_hora + "/" + 1,
                    {
                      headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    })
                    .subscribe(update => {
                    }, error => {

                    });
                }
              }
              setTimeout(() => {
                this.getdataHorario(idrol[0].id_usuario_rol).subscribe(res => {
                  observer.next(res)
                })
              }, 1000);

            }, error => {

            });

        }, error => {

        });
    });

  }
  getHorasCancelar(idrol) {
    return new Observable(observer => {
      this.eventsources = []

      this._httpClient.get(environment.url + 'detalle_horario/validarhora/' + idrol,
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(valida => {
          let validar;
          validar = valida
          for (let a of validar) {
            if (new Date(a.fecha_creacion) < new Date(new Date().setHours(new Date().getHours())) && a.estado == false) {
              this._httpClient.get(environment.url + 'detalle_horario/updateestado/' + a.id_deta_dia_hora + "/" + 1,
                {
                  headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                  })
                })
                .subscribe(update => {
                }, error => {

                });
            }
          }
          setTimeout(() => {
            this.getdataHorario(idrol).subscribe(res => {
              observer.next(res)
            })
          }, 1000);

        }, error => {

        });

    });
  }
  getProperties() {
    return this.properties;
  }
  getdataHorario(idrol) {
    return new Observable(observer => {
      this._httpClient.get(environment.url + 'detalle_horario/idrol/' + idrol,
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(dat => {
          let data;
          data = dat
          for (let a of data) {
            if (new Date(a.hora_inicio) < new Date()) {
              this.eventsources.push({
                title: "",
                startTime: a.hora_inicio,
                endTime: a.hora_fin,
                reservado: 3,
                id: a.id_deta_dia_hora


              })
            } else {
              this.eventsources.push({
                title: "",
                startTime: a.hora_inicio,
                endTime: a.hora_fin,
                reservado: a.estado_reserva,
                id: a.id_deta_dia_hora


              })
            }
          }
          observer.next(this.eventsources)
        }, error => {

        });
    })
  }

  setreserva(even, idrol) {
    this.eventsources.push({
      title: "",
      startTime: even.hora_inicio,
      endTime: even.hora_fin,
      reservado: even.estado_reserva
    })
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }

    this._httpClient.post(environment.url + 'detalle_horario', JSON.stringify(even), header)
      .subscribe(emisor_receptor => {

      }, error => {

      });

  }
  setreservas(event) {
    return new Observable(observer => {
      for (let even of event) {
        this.eventsources.push({
          title: "",
          startTime: even.hora_inicio,
          endTime: even.hora_fin,
          reservado: even.estado_reserva
        })
        let header = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        }

        this._httpClient.post(environment.url + 'detalle_horario', JSON.stringify(even), header)
          .subscribe(emisor_receptor => {

          }, error => {

          });
      }
      observer.next(true)
    });

  }
  enviarcomentario(data) {
    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.post(environment.url + 'comentario', JSON.stringify(data), header)
        .subscribe(comentario => {
          observer.next(comentario)
        }, error => {

        });
    })
  }
  traerDisponibilidad(asiguserper) {
    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.get(environment.url + 'asig-user-per/id/' + asiguserper, header)
        .subscribe(async resusurol => {
          let usuariorol: any = resusurol
          this.getHorasCancelar(usuariorol.id_usuario_rol).subscribe(res => {
            observer.next(res)
          })
        }, error => {

        });
    })
  }
  traercomentarios(idprof) {
    return new Observable(observer => {

      this.comentarios = []
      this._httpClient.get(environment.url + 'comentario/idprofesor/' + idprof,
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(comen => {
          let comentario;
          comentario = comen
          for (let a of comentario) {
            this._httpClient.get(environment.url + "usuario/idrol/" + a.id_usuario_rol,
              {
                headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Authorization': 'Bearer ' + this._authService.getToken()
                })
              })
              .subscribe(id => {

                let idusu = id
                let split = a.fecha_valoracion.split("T")
                let b = {
                  valoracion: a.valoracion,
                  nombre: idusu[0].nombres_usuario + " " + idusu[0].apellidos_usuario,
                  foto: environment.urlfotos + idusu[0].foto_usuario,
                  fecha: split[0],
                  comentario: a.comentario
                }
                this.comentarios.push(b)

              }, error => {

              });

          }

          observer.next(this.comentarios)


        }, error => {

        });
    })
  }


  pedido(fecha, idrol, id_asig_user, idhora, idprofesor, nro_pedido, detalles, idprof) {
    return new Observable(observer => {

      let data;
      data = {
        fecha_inicio: new Date(fecha).toISOString(),
        estado: false,
        id_usuario_rol: idrol,
        id_asig_user_per: id_asig_user,
        detalle_pedido: nro_pedido,
        id_deta_dia_hora: idhora,
        id_usuario_profesor: idprof
      }
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }

      this._httpClient.post(environment.url + 'pedido', JSON.stringify(data), header)
        .subscribe(pedido => {
          let pedidos;
          pedidos = pedido
          data = {
            descripcion: "",
            cantidad: 1,
            codigo: "kakak",
            valor_unitario: +detalles.precio,
            valor_total: +detalles.precio * 1,
            id_pedido: +pedidos.identifiers[0].id_pedido,
            fecha_creacion: new Date(new Date().setMinutes(new Date().getMinutes() + 5)).toISOString()

          }
          this._httpClient.post(environment.url + 'detalle_pedido', JSON.stringify(data), header)
            .subscribe(detalle_pedido => {
              this._httpClient.get(environment.url + 'detalle_horario/updateestado/' + idhora + "/" + 2,
                {
                  headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                  })
                })
                .subscribe(update => {
                  observer.next(true)
                  this.getDetaHorario(idprofesor)

                }, error => {

                });
            }, error => {

            });
        }, error => {

        });
    })
  }
  eliminarhora(ideliminar, idrol) {
    this._httpClient.get(environment.url + 'detalle_horario/delete/' + ideliminar,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(idro => {
        this.getDetaHorario(idrol)
      }, error => {

      });
  }


  findById(id) {
    return Promise.resolve(this.properties[id - 1]);
  }

  getItem(id) {
    for (let i = 0; i < this.properties.length; i++) {
      if (this.properties[i].id === parseInt(id)) {
        return this.properties[i];
      }
    }
    return null;
  }


  findByName(searchKey: string) {
    const key: string = searchKey.toUpperCase();
    return Promise.resolve(this.properties.filter((property: any) =>
      (property.title + ' ' + property.address + ' ' + property.city + ' ' + property.description).toUpperCase().indexOf(key) > -1));
  }

  findByIdioma(searchKey: string) {
    const key: string = searchKey.toUpperCase();
    let filtro;
    let filter = []

    if (this.filtro == null) {
      return filter;

    }
    if (this.filtro.length > 0) {
      filtro = this.filtro
    } else {
      filtro = this.properties
    }
    for (let b of filtro) {
      for (let a of b.idiomas) {
        let aux = a.nombre + ' ' + a.nivel
        if (aux.toUpperCase().indexOf(key) > -1) {
          filter.push(b)
        }
      }
    }
    if (filter.length == 0) {
      this.filtro = null;
    } else {
      this.filtro = filter
    }
    return filter
  }
  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  findByPais(searchKey: string) {
    const key: string = searchKey.toUpperCase();
    // let b=this.prot
    let filtro;
    let filter = []
    if (this.filtro == null) {
      return filter;

    }
    if (this.filtro.length > 0) {
      filtro = this.filtro
    } else {
      filtro = this.properties
    }
    for (let b of filtro) {
      let aux = b.city.split(",")
      if (aux[0].toUpperCase().indexOf(key) > -1) {
        filter.push(b)
      }
    }
    if (filter.length == 0) {
      this.filtro = null;
    } else {
      this.filtro = filter
    }

    return filter
  }
  newfilter() {
    this.filtro = []
  }

  findByCategoria(searchKey: string) {
    let categorias: string = searchKey.toUpperCase();
    // let b=this.prot
    let filtro;
    let filter = []
    if (this.filtro == null) {
      return filter;

    }
    if (this.filtro.length > 0) {
      filtro = this.filtro
    } else {
      filtro = this.properties
    }
    for (let b of filtro) {
      for (let a of b.ensena) {
        if (a.categoria.toUpperCase().indexOf(categorias) > -1) {
          let yaHay = false
          for (let c of filter) {
            if (c.id == b.id) yaHay = true
          }
          if (!yaHay) {
            filter.push(b)
          }

        }
      }

    }

    if (filter.length == 0) {
      this.filtro = null;
    } else {
      this.filtro = filter
    }

    return filter
  }
  findByHorario(id) {
    let menor
    let mayor
    if (id == 1) {
      menor = 8
      mayor = 14
    }
    else if (id == 2) {
      menor = 14
      mayor = 20
    }
    else if (id == 3) {
      menor = 20
      mayor = 8
    }
    else if (id == 4) {
      menor = "Sat"
      mayor = "Sun"
    }
    let split
    let ids = []
    let filter = []
    let filtro = [];
    return new Observable(observer => {
      if (this.filtro == null) {
        observer.next(filter)

      } else {
        if (this.filtro.length > 0) {
          filtro = this.filtro
        } else {
          filtro = this.properties
        }
      }
      this._httpClient.get(environment.url + "usuario/filtrarhorario/'" + new Date().toISOString() + "'",
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(horas => {
          let horasfil;
          horasfil = horas

          for (let a of horasfil) {

            split = new Date(a.hora_inicio).toString().split(" ")
            if (id != 4) {
              split = split[4].split(":")
              split = split[0]

              if (+split <= mayor && +split >= menor) {
                ids.push(a.id_usuario)
              }
            }
            else if (id == 4) {

              split = split[0]


              if (split == mayor || split == menor) {
                ids.push(a.id_usuario)


              }
            }
          }


          for (let b of filtro) {
            for (let c of ids.filter(this.onlyUnique)) {

              if (b.id == c) {

                filter.push(b)
              }
            }
          }


          observer.next(filter)
        }, error => {

        })
    });
  }

  findByEstrella(estrellas) {
    const key = +estrellas;
    let filtro;
    let filter = []
    if (this.filtro == null) {
      return filter;

    }
    if (this.filtro.length > 0) {
      filtro = this.filtro
    } else {
      filtro = this.properties
    }
    for (let b of filtro) {
      if (Math.ceil(b.estrellas) >= key) {
        filter.push(b)
      }
    }
    if (filter.length == 0) {
      this.filtro = null;
    } else {
      this.filtro = filter
    }

    return filter
  }

  findByPrecio(preciomin, preciomax) {
    const max = +preciomax;
    const min = +preciomin;
    // let b=this.prot
    let filtro;
    let filter = []
    if (this.filtro == null) {
      return filter;

    }
    if (this.filtro.length > 0) {
      filtro = this.filtro
    } else {
      filtro = this.properties
    }
    for (let b of filtro) {
      if (b.price >= min && b.price <= max) {
        filter.push(b)
      }
    }
    if (filter.length == 0) {
      this.filtro = null;
    } else {
      this.filtro = filter
    }

    return filter
  }



  getFavorites() {
    return Promise.resolve(this.favorites);
  }

  favorite(property) {
    this.favoriteCounter = this.favoriteCounter + 1;
    this.favorites.push({ id: this.favoriteCounter, property: property });
    return Promise.resolve();
  }

  unfavorite(favorite) {
    const index = this.favorites.indexOf(favorite);
    if (index > -1) {
      this.favorites.splice(index, 1);
    }
    return Promise.resolve();
  }


  traerDatos() {
    return new Observable(observer => {
      this.properties = []
      this.titulos = []

      let aux: any

      let auxtitulos: any
      let auxidiomas: any
      this._httpClient.get(environment.url + 'usuario/profesores',
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        }
      )
        .subscribe(data => {
          aux = data
          var hayDatos = false
          if (aux.length == 0) {
            aux = false
          } else {


            for (let i = 0; i < aux.length; i++) {
              this._httpClient.get(environment.url + 'titulos-profesor/usuario/' + aux[i].id_usuario_rol,
                {
                  headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                  })
                })
                .subscribe(dat => {
                  auxtitulos = dat;
                  this.titulos = []

                  for (let a of auxtitulos) {
                    this.titulos.push({
                      tipo: a.nivel_profesor,
                      descripcion: a.senescyt_titulos_profe,
                    })
                  }
                  let foto
                  if (aux[i].foto_usuario === null || aux[i].foto_usuario === "") {
                    foto = environment.urlfotos + "avatar.png"
                  } else {
                    foto = environment.urlfotos + aux[i].foto_usuario
                  }


                  let Ciudad
                  if (aux[i].id_ciudad === null) {
                    Ciudad = ''
                  } else {
                    Ciudad = aux[i].id_ciudad
                  }
                  let estado = false
                  if (new Date(aux[i].ultima_conexion) >= new Date()) {
                    estado = true
                  }
                  this.properties.push({
                    id: aux[i].id_usuario,
                    id_ciudad: Ciudad,
                    address: aux[i].direccion_usuario,
                    city: "",
                    state: "MA",
                    zip: aux[i].id_usuario,
                    price: "",
                    title: aux[i].nombres_usuario + " " + aux[i].apellidos_usuario,
                    estrellas: aux[i].estrellas.promedio,
                    bedrooms: aux[i],
                    bathrooms: 3,
                    long: -78.1223300,
                    lat: 0.3517100,
                    //picture: environment.url + 'public/users/img/profesores/' + aux[i].foto_usuario,
                    picture: foto,
                    thumbnail: environment.url + 'public/usuario/' + aux[i].foto_usuario,
                    ensena: "",
                    images: [
                      "assets/img/properties/house01.jpg",
                      "assets/img/properties/house03.jpg",
                      "assets/img/properties/house06.jpg",
                      "assets/img/properties/house08.jpg"
                    ],
                    tags: "suburban",
                    description: "",
                    label: aux[i].estado_usuario,
                    estado_usuario_linea: aux[i].estado_usuario_cuenta,
                    estudios: this.titulos,
                    idiomas: this.idiomas,
                    period: estado,
                    facebook: "",
                    instagram: "",
                    twitter: "",
                    youtube: "",
                    square: 152,
                    broker: {
                      id: 1,
                      name: aux[i].nombres_usuario + " " + aux[i].apellidos_usuario,
                      title: "Matemáticas, Idiomas",
                      picture: environment.url + 'public/usuario/' + aux[i].foto_usuario,
                    },
                    phone: aux[i].telefono_usuario,
                    mobilePhone: aux[i].telefono_usuario,
                    email: aux[i].correo_usuario,
                  })



                }, error => {

                });


            }
          }
          observer.next(aux)
        }, error => {

        });

    })
  }
  async Allidiomas() {
    this.idiomasall = []
    let auxidiomas;
    {
      this._httpClient.get(environment.url + 'idioma',
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(dataidiomas => {
          auxidiomas = dataidiomas;
          for (let j of auxidiomas) {
            this.idiomasall.push({
              nombre: j.idioma,
              nivel: j.nivel,
            })
          }

        }, error => {

        });

      return this.idiomasall;
    }
  }
  async traerIdiomas() {
    this.idiomasfiltro = []
    let auxtraer;
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'idioma/filtro', header)
      .subscribe(residioma => {

        auxtraer = residioma
        for (let j of auxtraer) {
          this.idiomasfiltro.push({
            nombre: j.idioma,
            nivel: j.nivel,
          })
        }

      }, error => {
      });
    return this.idiomasfiltro;
  }

  async AllPais() {
    this.paisall = []
    let auxpais;
    {
      this._httpClient.get(environment.url + 'pais',
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(datapais => {
          auxpais = datapais;
          for (let j of auxpais) {
            //idiomas = j.idioma
            this.paisall.push({
              //id: j.id_idioma,
              nombre: j.nombre_pais,
            })
          }

        }, error => {

        });
      return this.paisall;
    }
  }


  async AllCategorias() {
    this.categoriasall = []
    let auxcategorias;
    {
      this._httpClient.get(environment.url + 'categoria',
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(datacategorias => {
          auxcategorias = datacategorias;
          for (let j of auxcategorias) {
            //idiomas = j.idioma
            this.categoriasall.push({
              //id: j.id_idioma,
              nombre: j.nombre_categoria,
            })
          }

        }, error => {

        });
      return this.categoriasall;
    }
  }


  traerCiudadPais(aux) {
    return new Observable(observer => {

      let paisciudad = [];
      for (let i of this.properties) {
        if (i.id_ciudad) {
          this._httpClient.get(environment.url + 'ciudad/id/' + i.id_ciudad,
            {
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
              })
            })
            .subscribe(datapaiss => {
              let datapais: any
              datapais = datapaiss


              paisciudad.push({
                id: i.id,
                ciudadpais: datapais.estado.pais.nombre_pais + ", " + datapais.nombre_ciudad,
              })


              for (let a of this.properties) {
                for (let b of paisciudad) {
                  if (a.id == b.id) {
                    a.city = b.ciudadpais
                  }
                }
              }
              observer.next(this.properties)
            }, error => {

            });
        } else observer.next(this.properties)

      }
    })
  }

  traerDetallesprofesor(aux) {
    return new Observable(observer => {
      let detalle;
      let usuario_rol;
      this._httpClient.get(environment.url + 'detalles_profesor',
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(detalles => {
          detalle = detalles
          this._httpClient.get(environment.url + 'usuario-rol',
            {
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
              })
            })
            .subscribe(usuario_rols => {
              usuario_rol = usuario_rols
              for (let a of this.properties) {
                for (let b of usuario_rol) {
                  if (a.id == b.usuario.id_usuario) {
                    for (let c of detalle) {
                      if (c.id_usuario_rol == b.id_usuario_rol) {
                        a.description = c.descri_deta_profe
                        a.experiencia = c.experiencia
                        a.preferencia_ense = c.preferencia_ense
                        a.price = c.preciofijo
                        a.facebook = c.facebook
                        a.twitter = c.twitter
                        a.youtube = c.youtube
                        a.instagram = c.instagram
                        a.video_presentacion = c.video_presentacion
                      }



                    }
                  }
                }
              }
              observer.next(this.properties)
            }, error => {

            });
        }, error => {

        });

    })
  }


  traeridiomas(aux) {
    return new Observable(observer => {
      let auxidiomas;
      let idiomas = [];
      for (let i of aux) {
        this._httpClient.get(environment.url + 'idioma/usuario/' + i.id_usuario,
          {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          })
          .subscribe(dataidiomas => {
            auxidiomas = dataidiomas;
            if (auxidiomas.length > 0) {
              for (let j of auxidiomas) {

                idiomas.push({
                  id: i.id_usuario,
                  nombre: j.idioma,
                  nivel: j.nivel,
                  id_idioma: j.id_idioma,
                })
              }

              for (let a of this.properties) {
                let idioma = []
                for (let b of idiomas) {
                  if (a.id == b.id) {
                    idioma.push(b)
                    a.idiomas = idioma
                  }
                }
              }
            }
            observer.next(this.properties)
          }, error => {

          });

      }
    })
  }

  obtenerdetalles() {
    return new Observable(observer => {
      let ensena;
      let categorias;
      let detallemateria;
      let asig_servicio_per;
      let asig_user_per;
      let usuario_rol;
      this._httpClient.get(environment.url + 'categoria',
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(categoria => {

          categorias = categoria;
          this._httpClient.get(environment.url + 'detalle_materia',
            {
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
              })
            })
            .subscribe(detallematerias => {

              detallemateria = detallematerias;

              this._httpClient.get(environment.url + 'asig-servicio-per',
                {
                  headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                  })
                })
                .subscribe(asig_servicio_pers => {

                  asig_servicio_per = asig_servicio_pers;
                  this._httpClient.get(environment.url + 'asig-user-per',
                    {
                      headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    })
                    .subscribe(asig_user_pers => {

                      asig_user_per = asig_user_pers;
                      this._httpClient.get(environment.url + 'usuario-rol',
                        {
                          headers: new HttpHeaders({
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + this._authService.getToken()
                          })
                        })
                        .subscribe(usuario_rols => {

                          usuario_rol = usuario_rols;

                          for (let a of this.properties) {
                            ensena = []
                            for (let f of usuario_rol) {
                              if (f.usuario.id_usuario == a.id) {

                                for (let b of asig_user_per) {
                                  if (f.id_usuario_rol == b.id_usuario_rol) {
                                    b.id_usuario_rol = 0;
                                    for (let c of asig_servicio_per) {
                                      if (b.id_asig_per_serv == c.id_asig_per_serv) {
                                        for (let d of detallemateria) {
                                          if (c.id_tutoria == d.id_tutoria) {
                                            for (let e of categorias) {
                                              if (d.materia.id_categoria == e.id_categoria) {
                                                ensena.push({
                                                  asig_user_per: b.id_asig_user_per,
                                                  id_tutoria: d.id_tutoria,
                                                  categoria: e.nombre_categoria,
                                                  niveles: d.materia.nombre_materia + " $" + a.price,
                                                  id_materia: d.materia.id_materia,
                                                  nombre_materia: d.materia.nombre_materia,
                                                  id_usuario_rol: f.id_usuario_rol,
                                                  precio: a.price,
                                                  nombre: a.title
                                                })

                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            a.ensena = ensena
                          }
                          observer.next(this.properties)



                        }, error => {

                        });
                    }, error => {

                    });

                }, error => {

                });
            }, error => {

            });

        }, error => {

        });

    }
    )

  }





  async traerpais() {
    this.pais = []
    let aux: any
    this._httpClient.get(environment.url + 'pais',
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
    )
      .subscribe(data => {
        aux = data
        for (let i = 0; i < aux.length; i++) {
          this.pais.push({
            id: aux[i].id_pais,
            nombre_pais: aux[i].nombre_pais,
          })
        }

      }, error => {

      });
  }

  async obtenertodopais() {
    await this.traerpais()
    return Promise.resolve(this.pais);
  }

  getPais() {
    return this.pais;
  }

  findByIdPais(id) {
    return Promise.resolve(this.pais[id - 1]);
  }

  getItemPais(id) {
    for (let i = 0; i < this.pais.length; i++) {
      if (this.pais[i].id === parseInt(id)) {
        return this.pais[i];
      }
    }
    return null;
  }

}

