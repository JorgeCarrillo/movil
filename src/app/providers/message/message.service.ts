import { Injectable } from '@angular/core';
import messages from './mock-messages';
import { environment } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from '../../services/auth.service';
@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messageCounter: number = 0;
  messages: Array<any> = messages;

  constructor(private readonly _httpClient: HttpClient,
    private readonly _authService: AuthService,) { }

  findById(id) {
    return Promise.resolve(this.messages[id - 1]);
  }

  getMessages(idrol) {
    let idusuarios;
    messages.length = 0
    let ids = [];
    let mensajes = []
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'emisor_receptor/receptor/' + idrol, header)
      .subscribe(resmen => {
        idusuarios = resmen;
        for (let a of idusuarios) {
          if (a.id_emisor == idrol) ids.push(a.id_receptor)
          else if (a.id_receptor == idrol) ids.push(a.id_emisor)
        }
        ids = ids.filter(this.onlyUnique)
        for (let b = 0; b < ids.length; b++) {
          this._httpClient.get(environment.url + 'usuario/idrol/' + ids[b])
            .subscribe(resusuario => {
              this._httpClient.get(environment.url + 'emisor_receptor/ultimomensaje/' + idrol + "/" + ids[b])
                .subscribe(resultimomensaje => {
                  let foto;
                  if (resusuario[0].foto_usuario != null) foto = environment.urlfotos + resusuario[0].foto_usuario
                  else foto = "assets/img/avatar.png"

                  messages.push({
                    picture: foto,
                    id: resusuario[0].id_usuario,
                    title: resusuario[0].nombres_usuario + " " + resusuario[0].apellidos_usuario,
                    date: resultimomensaje[0].fecha_mensaje,
                    senderId: 2,
                    senderName: resultimomensaje[0].mensaje,
                    email: "caroline@ionbooking.com",
                    message: resultimomensaje[0].mensaje,
                    read: true
                  })
                  this.messageCounter = this.messageCounter + 1;
                }, error => {
                });
            }, error => {
            });
        }


      }, error => {
      });
    return this.messages
  }
  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  // message(message) {
  //   this.messageCounter = this.messageCounter + 1;
  //   this.messages.push({id: this.messageCounter, message: message});
  //   return Promise.resolve();
  // }

  getItem(id) {
    for (var i = 0; i < this.messages.length; i++) {
      if (this.messages[i].id === parseInt(id)) {
        return this.messages[i];
      }
    }
    return null;
  }

  delMessage(message) {
    this.messages.splice(this.messages.indexOf(message), 1);
  }
}
