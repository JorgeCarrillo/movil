import { PropertyDetailPageModule } from './../../pages/property-detail/property-detail.module';
import { Injectable } from '@angular/core';
import invoices from './mock-invoices';;
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../../services/auth.service";
import { EnvioCorreoServiceService } from "../../services/envio-correo-service.service";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Observable } from 'rxjs';
import { ObserveOnOperator } from 'rxjs/internal/operators/observeOn';

@Injectable({
  providedIn: 'root'
})

export class InvoicesService {
  nuevaTutoria = false
  invoiceCounter: number = 0;
  invoices: Array<any> = invoices;
  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _authService: AuthService,
    private readonly _correo: EnvioCorreoServiceService,
    private router: Router
  ) {

  }
  findAll() {
    return this.invoices;
  }

  findById(id) {
    return Promise.resolve(this.invoices[id - 1]);
  }

  getItem(id) {
    for (let i = 0; i < this.invoices.length; i++) {
      if (this.invoices[i].id === parseInt(id)) {
        return this.invoices[i];
      }
    }
    return null;
  }
  updateestadohora(idhora, estado) {
    this._httpClient.get(environment.url + 'detalle_horario/updateestado/' + idhora + "/" + estado,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(update => {


      }, error => {

      });
  }
  traerPedidos() {
    return new Observable(observer => {
      this._httpClient.get(environment.url + 'pedido/idrol/' + this._authService.getIdUsuarioRol(),
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(pedidos => {
          invoices.length = 0
          let pedido;
          pedido = pedidos
          for (let a of pedido) {
            //salto en el tiempo
            //if (new Date(a.fecha_creacion)> new Date(new Date().setHours(new Date().getHours()+1)) || a.estado==true) {
            if (new Date(a.fecha_creacion) > new Date() || a.estado == true) {

              let splice = a.detalle_pedido.split("/")
              invoices.push({
                id: a.id_pedido,//
                title: splice[0],
                date: a.fecha_inicio,//
                senderName: splice[1],
                value: splice[2],
                paid: a.estado,
                id_hora: a.id_deta_dia_hora,
                id_asig_user_per: a.id_asig_user_per,
                id_detalle_pedido: a.id_detalle_pedido,
                id_profesor: splice[3]
              })


            }
            else {
              this.Eliminarhora(a)
            }

          }
          observer.next(invoices)
        }, error => {

        });
    })
  }
  traerPedidosprof() {
    return new Observable(observer => {
      var invoicesprof = []
      this._httpClient.get(environment.url + "pedido/pedidosbyprof/'" + new Date(new Date(new Date(new Date(new Date().setMinutes(new Date().getMinutes() - new Date().getMinutes())).setHours(new Date().getHours())).setSeconds(new Date().getSeconds() - new Date().getSeconds())).setMilliseconds(new Date().getMilliseconds() - new Date().getMilliseconds())).toISOString() + "'/" + this._authService.getIdUsuario(),
        {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this._authService.getToken()
          })
        })
        .subscribe(pedidos => {

          let pedido;
          pedido = pedidos
          for (let a of pedido) {

            this._httpClient.get(environment.url + "usuario/idrol/" + a.id_usuario_rol,
              {
                headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Authorization': 'Bearer ' + this._authService.getToken()
                })
              })
              .subscribe(usu => {
                var usuario: any = usu
                if (a.estado == true) {
                  let splice = a.detalle_pedido.split("/")
                  invoicesprof.push({
                    id: a.id_pedido,//
                    title: usuario[0].nombres_usuario + " " + usuario[0].apellidos_usuario,
                    date: a.fecha_inicio,//
                    senderName: splice[1],
                    value: splice[2],
                    paid: a.estado,
                    id_hora: a.id_deta_dia_hora,
                    id_asig_user_per: a.id_asig_user_per,
                    id_detalle_pedido: a.id_detalle_pedido,
                    id_profesor: splice[3]
                  })


                }

              }, error => {

              });
          }

          observer.next(invoicesprof)
        }, error => {

        });
    })
  }
  getInvoices() {
    return Promise.resolve(this.invoices);
  }
  Eliminarhora(a) {
    this._httpClient.get(environment.url + 'detalle_horario/updateestado/' + a.id_deta_dia_hora + "/" + 1,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(update => {
      }, error => {

      });
    this._httpClient.delete(environment.url + 'detalle_pedido/' + a.id_detalle_pedido,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(delet => {
      }, error => {

      });
    this._httpClient.delete(environment.url + 'pedido/' + a.id_pedido,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(delet => {
      }, error => {

      });
  }

  realizarPago(id, estado) {
    this._httpClient.get(environment.url + 'pedido/updateestadopedido/' + estado + "/" + id,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(pedidos => {

      }, error => {

      });
  }

  GuardarPago(bodypago, id) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.get(environment.url + 'detalle_pedido/idPedido/' + id, header)
      .subscribe(respedido => {
        let auxpedido
        auxpedido = respedido

        let body2 = {
          metodo_pago: bodypago.metodo_pago,
          sub_total: bodypago.sub_total,
          impuesto: bodypago.impuesto,
          total: bodypago.total,
          id_detalle_pedido: auxpedido[0].id_detalle_pedido
        }
        this._httpClient.post(environment.url + 'pago', body2, header)
          .subscribe(pago => {

          }, error => {

          });
      }, error => {
      });

  }


  cambiarFechaReunion(fecha, idpedido, idhora) {
    this._httpClient.get(environment.url + 'reunion/cancelarReunion/' + idpedido + "/'Cancelada'",
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(pedidos => {
        if (pedidos) {
          this._httpClient.get(environment.url + 'pedido/cancelarReunion/' + idpedido + "/'" + fecha + "'/" + idhora,
            {
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
              })
            })
            .subscribe(pedidos => {



            }, error => {

            });
        }
      }, error => {

      });
  }

  crearreunion(body, url, idhora) {
    this._httpClient.post(environment.url + 'reunion', body,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      })
      .subscribe(resreunion => {
        this._httpClient.get(environment.url + 'detalle_horario/id/' + idhora,
          {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          })
          .subscribe(resdetallehorario => {
            let usuario;
            usuario = resdetallehorario

            if (resreunion) {
              let bodyemail = [];
              bodyemail.push({
                correo: this._authService.getUsuario().correo_usuario,
                fecha: new Date(body.fecha_inicio).toLocaleDateString(),
                hora: new Date(body.fecha_inicio).getHours() + ":" + new Date(body.fecha_inicio).getMinutes(),
                link: url,
              });
              let split = body.fecha_inicio.split("T")
              let hora = split[1].split(":")

              hora = new Date(new Date().setHours(hora[0] - 5)).getHours() + ":" + hora[1]
              bodyemail.push({
                correo: usuario.usuario.usuario.correo_usuario,
                fecha: split[0],
                hora: hora,
                link: body.enlace,
              });
              for (let a of bodyemail) {
                this._httpClient.post(environment.url + 'sendemail/meeting', a,
                  {
                    headers: new HttpHeaders({
                      'Content-Type': 'application/json',
                      'Authorization': 'Bearer ' + this._authService.getToken()
                    })
                  })
                  .subscribe(resemail => {

                  }, error => {

                  })
              }
            }

          }, error => {

          });
      }, error => {

      });

  }

  guardartarjeta(data) {
    let header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._authService.getToken()
      })
    }
    this._httpClient.post(environment.url + 'tarjeta_pago', JSON.stringify(data), header)
      .subscribe(tarjeta_pago => {

      }, error => {

      });
  }
  traerTargetas() {
    return new Observable(observer => {
      let header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
      this._httpClient.get(environment.url + 'tarjeta_pago/idrol/' + this._authService.getIdUsuarioRol(), header)
        .subscribe(tarjeta_pago => {
          observer.next(tarjeta_pago)
        }, error => {

        });

    })
  }

}
