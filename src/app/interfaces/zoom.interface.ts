export interface ZoomInterface {

    id_zoom?: number,
    topic?: string,
    duration?: string,
    codigo_zoom?: string,
    enlace_zoom?: string,
    start_time: Date | string,
    password?: string,
    usuario_cuenta?: string,
    meeting_id?: string,
    estado_zoom?: string,
    capacitacion?: string

}