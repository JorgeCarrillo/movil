import {UserInterface} from './user.interface';

export interface AuthInterface {

    accessToken?: string,
    usuario?: UserInterface,
    loginurl?: string

}