import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/walkthrough/walkthrough.module').then(m => m.WalkthroughPageModule) },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule) },
  { path: 'register', loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule) },
  { path: 'about', loadChildren: () => import('./pages/about/about.module').then(m => m.AboutPageModule) },
  { path: 'support', loadChildren: () => import('./pages/support/support.module').then(m => m.SupportPageModule) },
  { path: 'settings', loadChildren: () => import('./pages/settings/settings.module').then(m => m.SettingsPageModule) },
  { path: 'edit-profile', loadChildren: () => import('./pages/edit-profile/edit-profile.module').then(m => m.EditProfilePageModule) },
  { path: 'messages', loadChildren: () => import('./pages/messages/messages.module').then(m => m.MessagesPageModule) },
  { path: 'message/:id', loadChildren: () => import('./pages/message/message.module').then(m => m.MessagePageModule) },
  { path: 'home-results', loadChildren: () => import('./pages/home-results/home-results.module').then(m => m.HomeResultsPageModule) },
  { path: 'search-filter', loadChildren: () => import('./pages/modal/search-filter/search-filter.module').then(m => m.SearchFilterPageModule) },
  { path: 'cancelarhora', loadChildren: () => import('./pages/modal/cancelarhora/cancelarhora.module').then(m => m.CancelarhoraPageModule) },
  { path: 'nearby', loadChildren: () => import('./pages/nearby/nearby.module').then(m => m.NearbyPageModule) },
  { path: 'bycategory', loadChildren: () => import('./pages/bycategory/bycategory.module').then(m => m.BycategoryPageModule) },
  { path: 'property-detail/:id', loadChildren: () => import('./pages/property-detail/property-detail.module').then(m => m.PropertyDetailPageModule) },
  { path: 'broker-list', loadChildren: () => import('./pages/broker-list/broker-list.module').then(m => m.BrokerListPageModule) },
  { path: 'broker-detail/:id', loadChildren: () => import('./pages/broker-detail/broker-detail.module').then(m => m.BrokerDetailPageModule) },
  { path: 'broker-chat', loadChildren: () => import('./pages/broker-chat/broker-chat.module').then(m => m.BrokerChatPageModule) },
  { path: 'checkout', loadChildren: () => import('./pages/checkout/checkout.module').then(m => m.CheckoutPageModule) },
  { path: 'favorites', loadChildren: () => import('./pages/favorites/favorites.module').then(m => m.FavoritesPageModule) },
  { path: 'invoices', loadChildren: () => import('./pages/invoices/invoices.module').then(m => m.InvoicesPageModule) },
  { path: 'teaches', loadChildren: () => import('./pages/teaches/teaches.module').then(m => m.TeachesPageModule) },
  { path: 'titles', loadChildren: () => import('./pages/titles/titles.module').then(m => m.TitlesPageModule) },
  { path: 'idiomas', loadChildren: () => import('./pages/idiomas/idiomas.module').then(m => m.IdiomasPageModule) },
  { path: 'activar', loadChildren: () => import('./pages/activar/activar.module').then(m => m.ActivarPageModule) },
  { path: 'recover-password', loadChildren: () => import('./pages/recover-password/recover-password.module').then(m => m.RecoverPasswordPageModule) },
  { path: 'availability-page', loadChildren: () => import('./pages/availability-page/availability-page.module').then(m => m.AvailabilityPagePageModule) },
  { path: '**', redirectTo: '/home-results' },
  {
    path: 'availability',
    loadChildren: () => import('./availability/availability.module').then(m => m.AvailabilityPageModule)
  },
  {
    path: 'availability-page',
    loadChildren: () => import('./pages/availability-page/availability-page.module').then(m => m.AvailabilityPagePageModule)
  },
  {
    path: 'teaches',
    loadChildren: () => import('./pages/teaches/teaches.module').then(m => m.TeachesPageModule)
  },
  {
    path: 'recover-password',
    loadChildren: () => import('./pages/recover-password/recover-password.module').then(m => m.RecoverPasswordPageModule)
  },
  {
    path: 'titles',
    loadChildren: () => import('./pages/titles/titles.module').then(m => m.TitlesPageModule)
  },
  {
    path: 'activar',
    loadChildren: () => import('./pages/activar/activar.module').then(m => m.ActivarPageModule)
  },











];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
