(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-broker-detail-broker-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-detail/broker-detail.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-detail/broker-detail.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      {{ 'app.pages.broker.title.header' | translate }}\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"profile\">\r\n  <div>\r\n    <ion-card class=\"ion-no-margin dotted\">\r\n      <ion-card-content class=\"bg-profile\">\r\n\r\n\r\n        <h2 class=\"ion-margin-bottom\">\r\n          <img [src]=\"datos.img\" onerror=\"this.src='./assets/img/avatar.jpeg';\">\r\n\r\n        </h2>\r\n      </ion-card-content>\r\n    </ion-card>\r\n\r\n    <ion-card class=\"ion-no-margin bg-white dotted\">\r\n      <ion-list class=\"ion-no-padding ion-no-margin\">\r\n        <ion-item tappable href=\"tel: {{datos.telefono}}\">\r\n          <ion-label>\r\n            <h2 class=\"fw500\">\r\n              <ion-text color=\"primary\">{{datos.telefono}}</ion-text>\r\n            </h2>\r\n          </ion-label>\r\n          <ion-icon color=\"dark\" name=\"call\" slot=\"start\"></ion-icon>\r\n        </ion-item>\r\n\r\n        <ion-item tappable href=\"tel: {{datos.telefono}}\">\r\n          <ion-label>\r\n            <h2 class=\"fw500\">\r\n              <ion-text color=\"primary\">{{datos.nombre}}</ion-text>\r\n            </h2>\r\n          </ion-label>\r\n          <ion-icon color=\"dark\" name=\"phone-portrait\" slot=\"start\"></ion-icon>\r\n        </ion-item>\r\n\r\n        <ion-item tappable href=\"mailto:{{datos.correo}}\">\r\n          <ion-label>\r\n            <h2 class=\"fw500\">\r\n              <ion-text color=\"primary\"> {{datos.correo}}</ion-text>\r\n            </h2>\r\n          </ion-label>\r\n          <ion-icon color=\"dark\" name=\"mail\" slot=\"start\"></ion-icon>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-card>\r\n\r\n    <ion-button size=\"large\" expand=\"full\" color=\"primary\" tappable (click)=\"scheduleVisit(brokerID)\"\r\n      class=\"ion-no-margin\">\r\n      {{ 'app.pages.schedule.title.header' | translate }}\r\n      <ion-icon color=\"light\" name=\"clock\" slot=\"start\"></ion-icon>\r\n    </ion-button>\r\n\r\n    <ion-button size=\"large\" expand=\"full\" color=\"secondary\" tappable (click)=\"brokerChat(brokerID)\"\r\n      class=\"ion-no-margin\">\r\n      {{ 'app.pages.chat.title.header' | translate }}\r\n      <ion-icon name=\"chatboxes\" slot=\"start\"></ion-icon>\r\n    </ion-button>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/broker-detail/broker-detail.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/broker-detail/broker-detail.module.ts ***!
  \*************************************************************/
/*! exports provided: BrokerDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrokerDetailPageModule", function() { return BrokerDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _broker_detail_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./broker-detail.page */ "./src/app/pages/broker-detail/broker-detail.page.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm2015/agm-core.js");









const routes = [
    {
        path: '',
        component: _broker_detail_page__WEBPACK_IMPORTED_MODULE_7__["BrokerDetailPage"]
    }
];
let BrokerDetailPageModule = class BrokerDetailPageModule {
};
BrokerDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(),
            _agm_core__WEBPACK_IMPORTED_MODULE_8__["AgmCoreModule"].forRoot({
                apiKey: 'AIzaSyD9BxeSvt3u--Oj-_GD-qG2nPr1uODrR0Y'
            })
        ],
        declarations: [_broker_detail_page__WEBPACK_IMPORTED_MODULE_7__["BrokerDetailPage"]]
    })
], BrokerDetailPageModule);



/***/ }),

/***/ "./src/app/pages/broker-detail/broker-detail.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/broker-detail/broker-detail.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light)) ;\n}\n:host ion-item {\n  --border-color: var(--ion-color-medium);\n  --border-style: dotted;\n}\n.profile ion-card {\n  width: 100%;\n  border-radius: 0;\n  background-color: #fff;\n}\n.profile ion-card ion-card-content {\n  padding: 32px;\n  color: #fff;\n  text-align: center;\n}\n.profile ion-card ion-card-content img {\n  width: 128px;\n  height: 128px;\n  border-radius: 50%;\n  border: solid 4px #fff;\n  display: inline;\n  box-shadow: 0 0 28px rgba(255, 255, 255, 0.65);\n}\n.profile ion-card ion-card-content h1 {\n  margin-top: 0.5rem;\n}\n.profile ion-item ion-input {\n  border-bottom: 1px solid var(--ion-color-tertiary);\n}\n.profile ion-buttom button {\n  margin: 0;\n}\n.dotted {\n  border-top: 1px dotted var(--ion-color-secondary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnJva2VyLWRldGFpbC9DOlxceGFtcHBcXGh0ZG9jc1xcc29sdXRpdm9cXGFtYXV0YW1vdmlsL3NyY1xcYXBwXFxwYWdlc1xcYnJva2VyLWRldGFpbFxcYnJva2VyLWRldGFpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Jyb2tlci1kZXRhaWwvYnJva2VyLWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDRSx3RkFBQTtBQ0FOO0FER0k7RUFDSSx1Q0FBQTtFQUNBLHNCQUFBO0FDRFI7QURNSTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0FDSE47QURJTTtFQUNFLGFBQUE7RUFFQSxXQUFBO0VBQ0Esa0JBQUE7QUNIUjtBREtRO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLDhDQUFBO0FDSFY7QURNUTtFQUNFLGtCQUFBO0FDSlY7QURXTTtFQUNFLGtEQUFBO0FDVFI7QURjTTtFQUNFLFNBQUE7QUNaUjtBRGlCQTtFQUNJLGlEQUFBO0FDZEoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9icm9rZXItZGV0YWlsL2Jyb2tlci1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKSwgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSlcclxuICAgIH1cclxuXHJcbiAgICBpb24taXRlbSB7XHJcbiAgICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xyXG4gICAgICAgIC0tYm9yZGVyLXN0eWxlOiBkb3R0ZWQ7XHJcbiAgICB9XHJcbn1cclxuICBcclxuLnByb2ZpbGUge1xyXG4gICAgaW9uLWNhcmQge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgICAgaW9uLWNhcmQtY29udGVudCB7XHJcbiAgICAgICAgcGFkZGluZzogMzJweDtcclxuXHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIFxyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogMTI4cHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDEyOHB4O1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgYm9yZGVyOiBzb2xpZCA0cHggI2ZmZjtcclxuICAgICAgICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAyOHB4IHJnYmEoMjU1LDI1NSwyNTUsIC42NSk7XHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIGgxIHtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IC41cmVtO1xyXG4gICAgICAgIH1cclxuICBcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIFxyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICBpb24taW5wdXQge1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgXHJcbiAgICBpb24tYnV0dG9tIHtcclxuICAgICAgYnV0dG9uIHtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmRvdHRlZCB7XHJcbiAgICBib3JkZXItdG9wOiAxcHggZG90dGVkIHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG59IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKSwgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSkgO1xufVxuOmhvc3QgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gIC0tYm9yZGVyLXN0eWxlOiBkb3R0ZWQ7XG59XG5cbi5wcm9maWxlIGlvbi1jYXJkIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG59XG4ucHJvZmlsZSBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMzJweDtcbiAgY29sb3I6ICNmZmY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5wcm9maWxlIGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgaW1nIHtcbiAgd2lkdGg6IDEyOHB4O1xuICBoZWlnaHQ6IDEyOHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogc29saWQgNHB4ICNmZmY7XG4gIGRpc3BsYXk6IGlubGluZTtcbiAgYm94LXNoYWRvdzogMCAwIDI4cHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjY1KTtcbn1cbi5wcm9maWxlIGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgaDEge1xuICBtYXJnaW4tdG9wOiAwLjVyZW07XG59XG4ucHJvZmlsZSBpb24taXRlbSBpb24taW5wdXQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5KTtcbn1cbi5wcm9maWxlIGlvbi1idXR0b20gYnV0dG9uIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4uZG90dGVkIHtcbiAgYm9yZGVyLXRvcDogMXB4IGRvdHRlZCB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/broker-detail/broker-detail.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/broker-detail/broker-detail.page.ts ***!
  \***********************************************************/
/*! exports provided: BrokerDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrokerDetailPage", function() { return BrokerDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _modal_image_image_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../modal/image/image.page */ "./src/app/pages/modal/image/image.page.ts");
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");







let BrokerDetailPage = class BrokerDetailPage {
    constructor(navCtrl, toastCtrl, modalCtrl, route, router, brokerService, service) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.route = route;
        this.router = router;
        this.brokerService = brokerService;
        this.service = service;
        this.items = [];
        this.itemHeight = 0;
        this.datos = {
            img: "",
            telefono: "",
            correo: "",
            nombre: ""
        };
        this.brokerID = this.route.snapshot.paramMap.get('id');
        this.broker = this.brokerService.getItem(this.brokerID) ?
            this.brokerService.getItem(this.brokerID) :
            this.brokerService.getbrokers()[0];
    }
    ionViewWillEnter() {
        this.propierties = this.service.properties;
        for (let a of this.propierties) {
            if (a.id == this.brokerID) {
                this.datos.telefono = a.phone;
                this.datos.img = a.picture;
                this.datos.correo = a.email;
                this.datos.nombre = a.title;
            }
        }
    }
    scheduleVisit(broker) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let navigationExtras = {
                state: {
                    broker: broker,
                }
            };
            this.router.navigate(['property-detail/' + broker], navigationExtras);
        });
    }
    ;
    presentImage(image) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _modal_image_image_page__WEBPACK_IMPORTED_MODULE_4__["ImagePage"],
                componentProps: { value: image }
            });
            return yield modal.present();
        });
    }
    brokerChat(broker) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let navigationExtras = {
                state: {
                    broker: broker
                }
            };
            this.router.navigate(['broker-chat'], navigationExtras);
        });
    }
    expandItem(item) {
        this.items.map((listItem) => {
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
            }
            else {
                listItem.expanded = false;
            }
            return listItem;
        });
    }
};
BrokerDetailPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_5__["BrokerService"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_5__["PropertyService"] }
];
BrokerDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-broker-detail',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./broker-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-detail/broker-detail.page.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["trigger"])('staggerIn', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["transition"])('* => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["animate"])('600ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
                ])
            ])
        ],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./broker-detail.page.scss */ "./src/app/pages/broker-detail/broker-detail.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _providers__WEBPACK_IMPORTED_MODULE_5__["BrokerService"],
        _providers__WEBPACK_IMPORTED_MODULE_5__["PropertyService"]])
], BrokerDetailPage);



/***/ })

}]);
//# sourceMappingURL=pages-broker-detail-broker-detail-module-es2015.js.map