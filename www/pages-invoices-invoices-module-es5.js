function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-invoices-invoices-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invoices/invoices.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invoices/invoices.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesInvoicesInvoicesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>{{ 'app.pages.invoices.title.header' | translate }}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"!cargando\">\r\n  <ion-card *ngIf=\"lastInvoices.length === 0 && pedidosprof.length === 0\" color=\"primary\" class=\"ion-margin-top\">\r\n    <ion-card-content>\r\n      <p class=\"ion-text-center text-white\">No tienes tutorías.</p>\r\n    </ion-card-content>\r\n  </ion-card>\r\n  <ion-list class=\"ion-no-padding\" [@staggerIn]=\"lastInvoices\">\r\n\r\n    <ion-item class=\"bg-white\" lines=\"none\" *ngFor=\"let invoice of lastInvoices\" tappable\r\n     >\r\n      \r\n      <ion-icon slot=\"start\" name=\"list-box\" color=\"primary\"></ion-icon>\r\n      <ion-label>\r\n        <h2 (click)=\"irPerfilProfesor(invoice.id_profesor)\">\r\n          <ion-text color=\"dark\"><strong>{{invoice.title}}</strong></ion-text>\r\n        </h2>\r\n        <div><ion-badge style=\"font-size: 15px;\"  color=\"white\">{{invoice.senderName}}</ion-badge></div>\r\n        <div><ion-badge style=\"font-size: 15px;\" color=\"white\">{{invoice.date | date: 'dd/MM/yyyy'}}</ion-badge></div>\r\n        \r\n        \r\n        <ion-badge *ngIf=\"!invoice.paid\" color=\"warning\">Pendiente</ion-badge>\r\n        <ion-badge *ngIf=\"invoice.paid\" color=\"success\">Pagado</ion-badge>\r\n        \r\n      </ion-label>\r\n  \r\n      <ion-label>\r\n        <ion-badge float-right slot=\"end\">USD {{invoice.value }}.00</ion-badge><br><br>\r\n        <ion-button *ngIf=\"invoice.paid && !invoice.cancelada\" float-right slot=\"end\" size=\"small\" color=\"danger\" class=\"nombre_materia\" (click)=\"cancelar(invoice)\" >Cancelar</ion-button>\r\n      <ion-button *ngIf=\"invoice.paid\" float-right slot=\"end\" size=\"small\" color=\"primary\" class=\"nombre_materia\" (click)=\"traerLinkZoom(invoice.id)\" >Zoom</ion-button>\r\n      <ion-button *ngIf=\"!invoice.paid\" float-right slot=\"end\" size=\"small\" style=\"background-color:green; color:white\" (click)=\"goCheckout(invoice)\" >Pagar</ion-button>\r\n      <ion-button *ngIf=\"!invoice.paid\" float-right slot=\"end\" size=\"small\" color=\"danger\" class=\"nombre_materia\" (click)=\"eliminarHora(invoice)\" >Eliminar</ion-button>\r\n    </ion-label>\r\n    </ion-item> \r\n\r\n  </ion-list>\r\n<br>\r\n  <ion-button *ngIf=\"pedidosprof.length != 0\" class=\"ion-no-margin\" expand=\"full\" size=\"medium\" color=\"dark\">\r\n    Tutorías faltantes por dar como Profesor.\r\n  </ion-button>\r\n  <ion-list *ngIf=\"pedidosprof.length != 0\" class=\"ion-no-padding\" [@staggerIn]=\"pedidosprof\">\r\n\r\n    <ion-item class=\"bg-white\" lines=\"none\" *ngFor=\"let invoice of pedidosprof\" tappable\r\n     >\r\n      \r\n      <ion-icon slot=\"start\" name=\"list-box\" color=\"primary\"></ion-icon>\r\n      <ion-label>\r\n        <h2>\r\n          <ion-text color=\"dark\"><strong>{{invoice.title}}</strong></ion-text>\r\n        </h2>\r\n        <p class=\"text-12x\">\r\n          <ion-text >{{invoice.date | date: 'dd/MM/yyyy'}}</ion-text>\r\n        </p>\r\n        \r\n      </ion-label>\r\n    </ion-item> \r\n\r\n  </ion-list>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/invoices/invoices.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/invoices/invoices.module.ts ***!
    \***************************************************/

  /*! exports provided: InvoicesPageModule */

  /***/
  function srcAppPagesInvoicesInvoicesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InvoicesPageModule", function () {
      return InvoicesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _invoices_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./invoices.page */
    "./src/app/pages/invoices/invoices.page.ts");

    var routes = [{
      path: '',
      component: _invoices_page__WEBPACK_IMPORTED_MODULE_7__["InvoicesPage"]
    }];

    var InvoicesPageModule = function InvoicesPageModule() {
      _classCallCheck(this, InvoicesPageModule);
    };

    InvoicesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(), _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_invoices_page__WEBPACK_IMPORTED_MODULE_7__["InvoicesPage"]]
    })], InvoicesPageModule);
    /***/
  },

  /***/
  "./src/app/pages/invoices/invoices.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/pages/invoices/invoices.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesInvoicesInvoicesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host ion-content {\n  --background: var(--ion-color-light);\n}\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium);\n}\n:host .nombre_materia {\n  margin-left: 5vh;\n}\n:host .bg-white {\n  border: solid 0.3px #6c75f1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaW52b2ljZXMvQzpcXHhhbXBwXFxodGRvY3NcXHNvbHV0aXZvXFxhbWF1dGFtb3ZpbC9zcmNcXGFwcFxccGFnZXNcXGludm9pY2VzXFxpbnZvaWNlcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2ludm9pY2VzL2ludm9pY2VzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNFLG9DQUFBO0FDQU47QURHSTtFQUNJLGdCQUFBO0VBQ0EsaURBQUE7QUNEUjtBREdJO0VBQ0osZ0JBQUE7QUNEQTtBREdJO0VBQ0UsMkJBQUE7QUNETiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ludm9pY2VzL2ludm9pY2VzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICB9XHJcbiAgICAubm9tYnJlX21hdGVyaWF7XHJcbm1hcmdpbi1sZWZ0OiA1dmg7XHJcbiAgICB9XHJcbiAgICAuYmctd2hpdGV7XHJcbiAgICAgIGJvcmRlcjogc29saWQgMC4zcHggcmdiKDEwOCwgMTE3LCAyNDEpO1xyXG4gICAgfVxyXG59IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG59XG46aG9zdCBpb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG59XG46aG9zdCAubm9tYnJlX21hdGVyaWEge1xuICBtYXJnaW4tbGVmdDogNXZoO1xufVxuOmhvc3QgLmJnLXdoaXRlIHtcbiAgYm9yZGVyOiBzb2xpZCAwLjNweCAjNmM3NWYxO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/invoices/invoices.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/invoices/invoices.page.ts ***!
    \*************************************************/

  /*! exports provided: InvoicesPage */

  /***/
  function srcAppPagesInvoicesInvoicesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InvoicesPage", function () {
      return InvoicesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers */
    "./src/app/providers/index.ts");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _modal_cancelarhora_cancelarhora_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../modal/cancelarhora/cancelarhora.page */
    "./src/app/pages/modal/cancelarhora/cancelarhora.page.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var InvoicesPage = /*#__PURE__*/function () {
      function InvoicesPage(route, router, invoicesService, _authService, alertController, _httpClient, navCtrl, loadingctrl, propertyservice, modalCtrl) {
        var _this = this;

        _classCallCheck(this, InvoicesPage);

        this.route = route;
        this.router = router;
        this.invoicesService = invoicesService;
        this._authService = _authService;
        this.alertController = alertController;
        this._httpClient = _httpClient;
        this.navCtrl = navCtrl;
        this.loadingctrl = loadingctrl;
        this.propertyservice = propertyservice;
        this.modalCtrl = modalCtrl;
        this.imputs = [];
        this.invoice = {
          hora_inicio: "",
          id_deta_dia_hora: ""
        };
        this.cargando = true;
        this.invoicesService.traerPedidos().subscribe(function (res) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var alert;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!this.invoicesService.nuevaTutoria) {
                      _context.next = 6;
                      break;
                    }

                    _context.next = 3;
                    return this.alertController.create({
                      header: 'Alerta',
                      message: 'Recuerde pagar su tutoria en los siguientes 5 minutos',
                      buttons: [{
                        text: 'Aceptar',
                        handler: function handler(data) {}
                      }]
                    });

                  case 3:
                    alert = _context.sent;
                    alert.present();
                    this.invoicesService.nuevaTutoria = false;

                  case 6:
                    this.lastInvoices = res;
                    this.cargando = false;
                    this.verfica();

                  case 9:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        });
        this.invoicesService.traerPedidosprof().subscribe(function (res) {
          _this.pedidosprof = res;
        });
      }

      _createClass(InvoicesPage, [{
        key: "traerLinkZoom",
        value: function traerLinkZoom(idpedido) {
          var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          };

          this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'reunion/idpedido/' + idpedido, header).subscribe(function (link) {
            var linkz = link;
            window.open(linkz[0].enlaceEstudiante, '_system', 'location=yes');
          }, function (error) {});
        }
      }, {
        key: "irPerfilProfesor",
        value: function irPerfilProfesor(id) {
          this.router.navigate(["property-detail/" + id]);
        }
      }, {
        key: "eliminarHora",
        value: function eliminarHora(a) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this2 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.alertController.create({
                      header: "Eliminar tutoria",
                      message: "Seguro de eliminar esta tutoria ",
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Aceptar',
                        handler: function handler(data) {
                          _this2.invoicesService.Eliminarhora({
                            id_pedido: a.id,
                            fecha_inicio: a.date,
                            estado: a.paid,
                            id_deta_dia_hora: a.id_hora,
                            id_asig_user_per: a.id_asig_user_per,
                            id_detalle_pedido: a.id_detalle_pedido
                          });

                          _this2.navCtrl.navigateRoot('/home-results');
                        }
                      }]
                    });

                  case 2:
                    alert = _context2.sent;
                    alert.present();

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "getInvoices",
        value: function getInvoices() {
          var _this3 = this;

          this.invoicesService.getInvoices().then(function (data) {
            _this3.lastInvoices = data;
          });
        }
      }, {
        key: "goCheckout",
        value: function goCheckout(invoice) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var navigationExtras;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    if (!invoice.paid) {
                      navigationExtras = {
                        state: {
                          invoice: invoice
                        }
                      };
                      this.router.navigate(['checkout'], navigationExtras);
                    }

                    ;

                  case 2:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "cancelar",
        value: function cancelar(a) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
            var _this4 = this;

            var header, alert;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    this.idhoracancelar = a.id_hora; //this.createInputs(a.id_asig_user_per)

                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };
                    _context11.next = 4;
                    return this.alertController.create({
                      header: 'Cancelar Clase',
                      message: 'Esta seguro de cancelar esta clase.',
                      inputs: [{
                        name: 'condicion_anula',
                        'placeholder': "Ingrese Motivo",
                        'value': name
                      }, {
                        name: 'condicion_cambia',
                        'placeholder': "Ingrese porque Cambia",
                        'value': name
                      }],
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Aceptar',
                        handler: function handler(data) {
                          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
                            var _this5 = this;

                            var body, _alert;

                            return regeneratorRuntime.wrap(function _callee10$(_context10) {
                              while (1) {
                                switch (_context10.prev = _context10.next) {
                                  case 0:
                                    body = {
                                      condicion_anula: data.condicion_anula,
                                      condicion_cambia: data.condicion_cambia,
                                      id_pedido: a.id
                                    };

                                    if (!(body.condicion_anula == "" || body.condicion_cambia == "")) {
                                      _context10.next = 8;
                                      break;
                                    }

                                    _context10.next = 4;
                                    return this.alertController.create({
                                      header: 'Alerta',
                                      message: 'Porfavor llene todos los datos.',
                                      buttons: [{
                                        text: 'Cancelar',
                                        role: 'cancel'
                                      }, {
                                        text: 'Aceptar',
                                        handler: function handler(data) {}
                                      }]
                                    });

                                  case 4:
                                    _alert = _context10.sent;

                                    _alert.present();

                                    _context10.next = 9;
                                    break;

                                  case 8:
                                    this.propertyservice.traerDisponibilidad(a.id_asig_user_per).subscribe(function (res) {
                                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
                                        var _this6 = this;

                                        var modal;
                                        return regeneratorRuntime.wrap(function _callee9$(_context9) {
                                          while (1) {
                                            switch (_context9.prev = _context9.next) {
                                              case 0:
                                                _context9.next = 2;
                                                return this.modalCtrl.create({
                                                  component: _modal_cancelarhora_cancelarhora_page__WEBPACK_IMPORTED_MODULE_9__["CancelarhoraPage"],
                                                  cssClass: 'my-custom-class',
                                                  componentProps: {
                                                    "horas": res
                                                  }
                                                });

                                              case 2:
                                                modal = _context9.sent;
                                                modal.onDidDismiss().then(function (data) {
                                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this6, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                                                    var _this7 = this;

                                                    var loading;
                                                    return regeneratorRuntime.wrap(function _callee8$(_context8) {
                                                      while (1) {
                                                        switch (_context8.prev = _context8.next) {
                                                          case 0:
                                                            this.invoice.hora_inicio = data.data.horainicio;
                                                            this.invoice.id_deta_dia_hora = data.data.idhora;
                                                            this.invoiceID = a.id;
                                                            this.idnuevahora = data.data.idhora;
                                                            this.checkoutInvoice();

                                                            if (!(data.data.idhora != false)) {
                                                              _context8.next = 12;
                                                              break;
                                                            }

                                                            alert.present();
                                                            _context8.next = 9;
                                                            return this.loadingctrl.create({
                                                              message: "Cargando..."
                                                            });

                                                          case 9:
                                                            loading = _context8.sent;
                                                            loading.present();

                                                            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'asig-user-per/id/' + a.id_asig_user_per, header).subscribe(function (resusurol) {
                                                              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this7, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                                                                var _this8 = this;

                                                                var usuariorol;
                                                                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                                                                  while (1) {
                                                                    switch (_context7.prev = _context7.next) {
                                                                      case 0:
                                                                        usuariorol = resusurol;

                                                                        this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'usuario/idrol/' + usuariorol.id_usuario_rol, header).subscribe(function (resusu) {
                                                                          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                                                                            var _this9 = this;

                                                                            var usuario, split, hora, data;
                                                                            return regeneratorRuntime.wrap(function _callee6$(_context6) {
                                                                              while (1) {
                                                                                switch (_context6.prev = _context6.next) {
                                                                                  case 0:
                                                                                    usuario = resusu;
                                                                                    split = a.date.split("T");
                                                                                    hora = split[1].split(":");
                                                                                    hora = new Date(new Date().setHours(hora[0] - 5)).getHours() + ":" + hora[1];
                                                                                    data = {
                                                                                      fecha: split[0],
                                                                                      hora: hora,
                                                                                      correo_profesor: usuario[0].correo_usuario,
                                                                                      correo_estudiante: this._authService.getUsuario().correo_usuario
                                                                                    };

                                                                                    this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'usuario/cancelarclase', data, header).subscribe(function (rescancelacioncorreo) {
                                                                                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this9, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                                                                                        var _this10 = this;

                                                                                        return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                                                                          while (1) {
                                                                                            switch (_context5.prev = _context5.next) {
                                                                                              case 0:
                                                                                                if (rescancelacioncorreo) {
                                                                                                  this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'cancelacion', body, header).subscribe(function (rescancelacion) {
                                                                                                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this10, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                                                                                                      return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                                                                                        while (1) {
                                                                                                          switch (_context4.prev = _context4.next) {
                                                                                                            case 0:
                                                                                                              this.alert("Cancelación", "Clase cancelada correctamente.");
                                                                                                              loading.dismiss();
                                                                                                              this.navCtrl.navigateRoot('/home-results');

                                                                                                            case 3:
                                                                                                            case "end":
                                                                                                              return _context4.stop();
                                                                                                          }
                                                                                                        }
                                                                                                      }, _callee4, this);
                                                                                                    }));
                                                                                                  }, function (error) {});
                                                                                                } else {
                                                                                                  this.alert("Cancelación", "No se pudo cancelar la clase.");
                                                                                                  loading.dismiss();
                                                                                                }

                                                                                              case 1:
                                                                                              case "end":
                                                                                                return _context5.stop();
                                                                                            }
                                                                                          }
                                                                                        }, _callee5, this);
                                                                                      }));
                                                                                    }, function (error) {});

                                                                                  case 6:
                                                                                  case "end":
                                                                                    return _context6.stop();
                                                                                }
                                                                              }
                                                                            }, _callee6, this);
                                                                          }));
                                                                        }, function (error) {});

                                                                      case 2:
                                                                      case "end":
                                                                        return _context7.stop();
                                                                    }
                                                                  }
                                                                }, _callee7, this);
                                                              }));
                                                            }, function (error) {});

                                                          case 12:
                                                          case "end":
                                                            return _context8.stop();
                                                        }
                                                      }
                                                    }, _callee8, this);
                                                  }));
                                                });
                                                modal.present();

                                              case 5:
                                              case "end":
                                                return _context9.stop();
                                            }
                                          }
                                        }, _callee9, this);
                                      }));
                                    });

                                  case 9:
                                  case "end":
                                    return _context10.stop();
                                }
                              }
                            }, _callee10, this);
                          }));
                        }
                      }]
                    });

                  case 4:
                    alert = _context11.sent;
                    alert.present();

                  case 6:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));
        }
      }, {
        key: "alert",
        value: function alert(header, mensaje) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var alert;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    _context12.next = 2;
                    return this.alertController.create({
                      header: header,
                      message: mensaje,
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Aceptar',
                        handler: function handler(data) {}
                      }]
                    });

                  case 2:
                    alert = _context12.sent;
                    alert.present();

                  case 4:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }, {
        key: "verfica",
        value: function verfica() {
          var _this11 = this;

          this.ids_pedidos = [];
          var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          };

          this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'cancelacion', header).subscribe(function (restitulos) {
            var aux;
            aux = restitulos;

            var _iterator = _createForOfIteratorHelper(_this11.lastInvoices),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var a = _step.value;
                a.cancelada = false;

                if (new Date(new Date(a.date).setHours(new Date(a.date).getHours() - 5)) < new Date()) {
                  a.cancelada = true;
                }

                var _iterator2 = _createForOfIteratorHelper(aux),
                    _step2;

                try {
                  for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                    var b = _step2.value;

                    if (b.id_pedido == a.id) {
                      a.cancelada = true;
                    }
                  }
                } catch (err) {
                  _iterator2.e(err);
                } finally {
                  _iterator2.f();
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
          }, function (error) {});
        }
      }, {
        key: "createInputs",
        value: function createInputs(id) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
            var _this12 = this;

            var myArray, theNewInputs;
            return regeneratorRuntime.wrap(function _callee13$(_context13) {
              while (1) {
                switch (_context13.prev = _context13.next) {
                  case 0:
                    myArray = [1, 1, 1, 1, 1, 1];
                    theNewInputs = [];
                    this.propertyservice.traerDisponibilidad(id).subscribe(function (res) {
                      var arr = res;
                      _this12.imputs = [];

                      var _iterator3 = _createForOfIteratorHelper(arr),
                          _step3;

                      try {
                        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                          var b = _step3.value;

                          _this12.imputs.push({
                            type: 'radio',
                            label: new Date(b.hora_inicio).toLocaleString(),
                            value: b,
                            checked: false
                          });
                        }
                      } catch (err) {
                        _iterator3.e(err);
                      } finally {
                        _iterator3.f();
                      }

                      return theNewInputs;
                    });

                  case 3:
                  case "end":
                    return _context13.stop();
                }
              }
            }, _callee13, this);
          }));
        }
      }, {
        key: "traerDispononibilidad",
        value: function traerDispononibilidad(id) {
          var _this13 = this;

          return new rxjs__WEBPACK_IMPORTED_MODULE_10__["Observable"](function (observer) {
            _this13.propertyservice.traerDisponibilidad(id).subscribe(function (res) {
              observer.next(res);
            });
          });
        }
      }, {
        key: "checkoutInvoice",
        value: function checkoutInvoice() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
            var _this14 = this;

            var loading, header, loader;
            return regeneratorRuntime.wrap(function _callee15$(_context15) {
              while (1) {
                switch (_context15.prev = _context15.next) {
                  case 0:
                    _context15.next = 2;
                    return this.loadingctrl.create({
                      message: "Cargando..."
                    });

                  case 2:
                    loading = _context15.sent;
                    loading.present();
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };
                    _context15.next = 7;
                    return this.loadingctrl.create({
                      duration: 2000
                    });

                  case 7:
                    loader = _context15.sent;

                    this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + "reunion/fecha_inicio/'" + this.invoice.hora_inicio + "'", header).subscribe(function (resresuniones) {
                      var disponibilidad;
                      var idsdisponbles = [];
                      var reuniones;
                      reuniones = resresuniones;
                      var aulas;
                      var aulasdisponibles = [];

                      if (new Date(_this14.invoice.hora_inicio).getHours() % 2 == 0) {
                        disponibilidad = "Par";
                      } else {
                        disponibilidad = "Impar";
                      }

                      _this14._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + "subcuenta-zoom/disponibilidad/'" + disponibilidad + "'", header).subscribe(function (resaulas) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this14, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
                          var _this15 = this;

                          var _iterator4, _step4, a, _iterator5, _step5, b, options, alert;

                          return regeneratorRuntime.wrap(function _callee14$(_context14) {
                            while (1) {
                              switch (_context14.prev = _context14.next) {
                                case 0:
                                  aulas = resaulas;

                                  if (!(aulas.length > reuniones.length)) {
                                    _context14.next = 8;
                                    break;
                                  }

                                  _iterator4 = _createForOfIteratorHelper(aulas);

                                  try {
                                    for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
                                      a = _step4.value;

                                      if (reuniones.length > 0) {
                                        _iterator5 = _createForOfIteratorHelper(reuniones);

                                        try {
                                          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
                                            b = _step5.value;

                                            if (a.id_subcuenta != b.id_subcuenta) {
                                              aulasdisponibles.push(a.id_subcuenta);
                                              idsdisponbles.push(a.id_usuario_zoom);
                                            }
                                          }
                                        } catch (err) {
                                          _iterator5.e(err);
                                        } finally {
                                          _iterator5.f();
                                        }
                                      } else {
                                        aulasdisponibles.push(a.id_subcuenta);
                                        idsdisponbles.push(a.id_usuario_zoom);
                                      }
                                    }
                                  } catch (err) {
                                    _iterator4.e(err);
                                  } finally {
                                    _iterator4.f();
                                  }

                                  options = {
                                    "idusuario": idsdisponbles[0],
                                    'topic': "Tutoria",
                                    'type': 2,
                                    'start_time': this.invoice.hora_inicio,
                                    'duration': 60,
                                    'timezone': 'America/Bogota',
                                    'settings': {
                                      'host_video': false,
                                      'participant_video': true,
                                      'join_before_host': true,
                                      'mute_upon_entry': true,
                                      'use_pmi': true,
                                      'approval_type': 1,
                                      'waiting_room': true,
                                      'auto_recording': 'cloud'
                                    }
                                  };

                                  this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + "zoom-api/meeting", options, header).subscribe(function (resmeet) {
                                    var meet;
                                    meet = resmeet;
                                    var body = {
                                      id_reunion: meet.id.toString(),
                                      enlace: meet.start_url,
                                      enlaceEstudiante: meet.join_url,
                                      password: meet.password,
                                      fecha_inicio: _this15.invoice.hora_inicio,
                                      fecha_fin: new Date(new Date(_this15.invoice.hora_inicio).setHours(new Date(_this15.invoice.hora_inicio).getHours() + 1)).toISOString(),
                                      hora_inicio: new Date(_this15.invoice.hora_inicio).getHours().toString(),
                                      hora_fin: new Date(new Date(_this15.invoice.hora_inicio).setHours(new Date(_this15.invoice.hora_inicio).getHours() + 1)).getHours().toString(),
                                      estado: true,
                                      id_subcuenta: aulasdisponibles[0],
                                      id_pedido: +_this15.invoiceID
                                    };

                                    _this15.invoicesService.cambiarFechaReunion(_this15.invoice.hora_inicio, _this15.invoiceID, _this15.idnuevahora);

                                    _this15.invoicesService.crearreunion(body, meet.join_url, _this15.invoice.id_deta_dia_hora);

                                    _this15.invoicesService.updateestadohora(_this15.idhoracancelar, 1);

                                    _this15.invoicesService.updateestadohora(_this15.idnuevahora, 3);

                                    loading.dismiss();

                                    _this15.navCtrl.navigateForward('home-results');
                                  }, function (error) {});

                                  _context14.next = 12;
                                  break;

                                case 8:
                                  _context14.next = 10;
                                  return this.alertController.create({
                                    header: 'Agregar Aulas',
                                    cssClass: 'alertpedido',
                                    message: 'Pago no procesado no se cuenta con aulas disponibles porfavor seleccione otra hora.',
                                    //inputs: this.data,
                                    buttons: [{
                                      text: 'Aceptar',
                                      handler: function handler() {}
                                    }]
                                  });

                                case 10:
                                  alert = _context14.sent;
                                  alert.present();

                                case 12:
                                case "end":
                                  return _context14.stop();
                              }
                            }
                          }, _callee14, this);
                        }));
                      }, function (error) {});
                    }, function (error) {});

                  case 9:
                  case "end":
                    return _context15.stop();
                }
              }
            }, _callee15, this);
          }));
        }
      }]);

      return InvoicesPage;
    }();

    InvoicesPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers__WEBPACK_IMPORTED_MODULE_4__["InvoicesService"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }];
    };

    InvoicesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-invoices',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./invoices.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invoices/invoices.page.html"))["default"],
      animations: [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["trigger"])('staggerIn', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["transition"])('* => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({
        opacity: 0,
        transform: "translate3d(100px,0,0)"
      }), {
        optional: true
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["animate"])('500ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({
        opacity: 1,
        transform: "translate3d(0,0,0)"
      }))]), {
        optional: true
      })])])],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./invoices.page.scss */
      "./src/app/pages/invoices/invoices.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers__WEBPACK_IMPORTED_MODULE_4__["InvoicesService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]])], InvoicesPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-invoices-invoices-module-es5.js.map