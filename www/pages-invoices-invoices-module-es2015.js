(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-invoices-invoices-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invoices/invoices.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invoices/invoices.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>{{ 'app.pages.invoices.title.header' | translate }}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"!cargando\">\r\n  <ion-card *ngIf=\"lastInvoices.length === 0 && pedidosprof.length === 0\" color=\"primary\" class=\"ion-margin-top\">\r\n    <ion-card-content>\r\n      <p class=\"ion-text-center text-white\">No tienes tutorías.</p>\r\n    </ion-card-content>\r\n  </ion-card>\r\n  <ion-list class=\"ion-no-padding\" [@staggerIn]=\"lastInvoices\">\r\n\r\n    <ion-item class=\"bg-white\" lines=\"none\" *ngFor=\"let invoice of lastInvoices\" tappable\r\n     >\r\n      \r\n      <ion-icon slot=\"start\" name=\"list-box\" color=\"primary\"></ion-icon>\r\n      <ion-label>\r\n        <h2 (click)=\"irPerfilProfesor(invoice.id_profesor)\">\r\n          <ion-text color=\"dark\"><strong>{{invoice.title}}</strong></ion-text>\r\n        </h2>\r\n        <div><ion-badge style=\"font-size: 15px;\"  color=\"white\">{{invoice.senderName}}</ion-badge></div>\r\n        <div><ion-badge style=\"font-size: 15px;\" color=\"white\">{{invoice.date | date: 'dd/MM/yyyy'}}</ion-badge></div>\r\n        \r\n        \r\n        <ion-badge *ngIf=\"!invoice.paid\" color=\"warning\">Pendiente</ion-badge>\r\n        <ion-badge *ngIf=\"invoice.paid\" color=\"success\">Pagado</ion-badge>\r\n        \r\n      </ion-label>\r\n  \r\n      <ion-label>\r\n        <ion-badge float-right slot=\"end\">USD {{invoice.value }}.00</ion-badge><br><br>\r\n        <ion-button *ngIf=\"invoice.paid && !invoice.cancelada\" float-right slot=\"end\" size=\"small\" color=\"danger\" class=\"nombre_materia\" (click)=\"cancelar(invoice)\" >Cancelar</ion-button>\r\n      <ion-button *ngIf=\"invoice.paid\" float-right slot=\"end\" size=\"small\" color=\"primary\" class=\"nombre_materia\" (click)=\"traerLinkZoom(invoice.id)\" >Zoom</ion-button>\r\n      <ion-button *ngIf=\"!invoice.paid\" float-right slot=\"end\" size=\"small\" style=\"background-color:green; color:white\" (click)=\"goCheckout(invoice)\" >Pagar</ion-button>\r\n      <ion-button *ngIf=\"!invoice.paid\" float-right slot=\"end\" size=\"small\" color=\"danger\" class=\"nombre_materia\" (click)=\"eliminarHora(invoice)\" >Eliminar</ion-button>\r\n    </ion-label>\r\n    </ion-item> \r\n\r\n  </ion-list>\r\n<br>\r\n  <ion-button *ngIf=\"pedidosprof.length != 0\" class=\"ion-no-margin\" expand=\"full\" size=\"medium\" color=\"dark\">\r\n    Tutorías faltantes por dar como Profesor.\r\n  </ion-button>\r\n  <ion-list *ngIf=\"pedidosprof.length != 0\" class=\"ion-no-padding\" [@staggerIn]=\"pedidosprof\">\r\n\r\n    <ion-item class=\"bg-white\" lines=\"none\" *ngFor=\"let invoice of pedidosprof\" tappable\r\n     >\r\n      \r\n      <ion-icon slot=\"start\" name=\"list-box\" color=\"primary\"></ion-icon>\r\n      <ion-label>\r\n        <h2>\r\n          <ion-text color=\"dark\"><strong>{{invoice.title}}</strong></ion-text>\r\n        </h2>\r\n        <p class=\"text-12x\">\r\n          <ion-text >{{invoice.date | date: 'dd/MM/yyyy'}}</ion-text>\r\n        </p>\r\n        \r\n      </ion-label>\r\n    </ion-item> \r\n\r\n  </ion-list>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/invoices/invoices.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/invoices/invoices.module.ts ***!
  \***************************************************/
/*! exports provided: InvoicesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesPageModule", function() { return InvoicesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _invoices_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./invoices.page */ "./src/app/pages/invoices/invoices.page.ts");








const routes = [
    {
        path: '',
        component: _invoices_page__WEBPACK_IMPORTED_MODULE_7__["InvoicesPage"]
    }
];
let InvoicesPageModule = class InvoicesPageModule {
};
InvoicesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(),
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_invoices_page__WEBPACK_IMPORTED_MODULE_7__["InvoicesPage"]]
    })
], InvoicesPageModule);



/***/ }),

/***/ "./src/app/pages/invoices/invoices.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/invoices/invoices.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: var(--ion-color-light);\n}\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium);\n}\n:host .nombre_materia {\n  margin-left: 5vh;\n}\n:host .bg-white {\n  border: solid 0.3px #6c75f1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaW52b2ljZXMvQzpcXHhhbXBwXFxodGRvY3NcXHNvbHV0aXZvXFxhbWF1dGFtb3ZpbC9zcmNcXGFwcFxccGFnZXNcXGludm9pY2VzXFxpbnZvaWNlcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2ludm9pY2VzL2ludm9pY2VzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNFLG9DQUFBO0FDQU47QURHSTtFQUNJLGdCQUFBO0VBQ0EsaURBQUE7QUNEUjtBREdJO0VBQ0osZ0JBQUE7QUNEQTtBREdJO0VBQ0UsMkJBQUE7QUNETiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ludm9pY2VzL2ludm9pY2VzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICB9XHJcbiAgICAubm9tYnJlX21hdGVyaWF7XHJcbm1hcmdpbi1sZWZ0OiA1dmg7XHJcbiAgICB9XHJcbiAgICAuYmctd2hpdGV7XHJcbiAgICAgIGJvcmRlcjogc29saWQgMC4zcHggcmdiKDEwOCwgMTE3LCAyNDEpO1xyXG4gICAgfVxyXG59IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG59XG46aG9zdCBpb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG59XG46aG9zdCAubm9tYnJlX21hdGVyaWEge1xuICBtYXJnaW4tbGVmdDogNXZoO1xufVxuOmhvc3QgLmJnLXdoaXRlIHtcbiAgYm9yZGVyOiBzb2xpZCAwLjNweCAjNmM3NWYxO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/invoices/invoices.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/invoices/invoices.page.ts ***!
  \*************************************************/
/*! exports provided: InvoicesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesPage", function() { return InvoicesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _modal_cancelarhora_cancelarhora_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../modal/cancelarhora/cancelarhora.page */ "./src/app/pages/modal/cancelarhora/cancelarhora.page.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");













let InvoicesPage = class InvoicesPage {
    constructor(route, router, invoicesService, _authService, alertController, _httpClient, navCtrl, loadingctrl, propertyservice, modalCtrl) {
        this.route = route;
        this.router = router;
        this.invoicesService = invoicesService;
        this._authService = _authService;
        this.alertController = alertController;
        this._httpClient = _httpClient;
        this.navCtrl = navCtrl;
        this.loadingctrl = loadingctrl;
        this.propertyservice = propertyservice;
        this.modalCtrl = modalCtrl;
        this.imputs = [];
        this.invoice = { hora_inicio: "", id_deta_dia_hora: "" };
        this.cargando = true;
        this.invoicesService.traerPedidos().subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.invoicesService.nuevaTutoria) {
                let alert = yield this.alertController.create({
                    header: 'Alerta',
                    message: 'Recuerde pagar su tutoria en los siguientes 5 minutos',
                    buttons: [
                        {
                            text: 'Aceptar', handler: data => {
                            }
                        }
                    ]
                });
                alert.present();
                this.invoicesService.nuevaTutoria = false;
            }
            this.lastInvoices = res;
            this.cargando = false;
            this.verfica();
        }));
        this.invoicesService.traerPedidosprof().subscribe(res => {
            this.pedidosprof = res;
        });
    }
    traerLinkZoom(idpedido) {
        let header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
            })
        };
        this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'reunion/idpedido/' + idpedido, header)
            .subscribe(link => {
            const linkz = link;
            window.open(linkz[0].enlaceEstudiante, '_system', 'location=yes');
        }, error => {
        });
    }
    irPerfilProfesor(id) {
        this.router.navigate(["property-detail/" + id]);
    }
    eliminarHora(a) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // 
            let alert = yield this.alertController.create({
                header: "Eliminar tutoria",
                message: "Seguro de eliminar esta tutoria ",
                buttons: [{ text: 'Cancelar', role: 'cancel' },
                    {
                        text: 'Aceptar', handler: data => {
                            this.invoicesService.Eliminarhora({
                                id_pedido: a.id, fecha_inicio: a.date, estado: a.paid, id_deta_dia_hora: a.id_hora, id_asig_user_per: a.id_asig_user_per, id_detalle_pedido: a.id_detalle_pedido
                            });
                            this.navCtrl.navigateRoot('/home-results');
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    getInvoices() {
        this.invoicesService.getInvoices()
            .then(data => {
            this.lastInvoices = data;
        });
    }
    goCheckout(invoice) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (!invoice.paid) {
                let navigationExtras = {
                    state: {
                        invoice: invoice
                    }
                };
                this.router.navigate(['checkout'], navigationExtras);
            }
            ;
        });
    }
    cancelar(a) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.idhoracancelar = a.id_hora;
            //this.createInputs(a.id_asig_user_per)
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            let alert = yield this.alertController.create({
                header: 'Cancelar Clase',
                message: 'Esta seguro de cancelar esta clase.',
                inputs: [
                    { name: 'condicion_anula', 'placeholder': "Ingrese Motivo", 'value': name },
                    { name: 'condicion_cambia', 'placeholder': "Ingrese porque Cambia", 'value': name }
                ],
                buttons: [{ text: 'Cancelar', role: 'cancel' },
                    {
                        text: 'Aceptar', handler: (data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                            let body = {
                                condicion_anula: data.condicion_anula,
                                condicion_cambia: data.condicion_cambia,
                                id_pedido: a.id
                            };
                            if (body.condicion_anula == "" || body.condicion_cambia == "") {
                                let alert = yield this.alertController.create({
                                    header: 'Alerta',
                                    message: 'Porfavor llene todos los datos.',
                                    buttons: [{ text: 'Cancelar', role: 'cancel' },
                                        {
                                            text: 'Aceptar', handler: data => {
                                            }
                                        }
                                    ]
                                });
                                alert.present();
                            }
                            else {
                                this.propertyservice.traerDisponibilidad(a.id_asig_user_per).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                    const modal = yield this.modalCtrl.create({
                                        component: _modal_cancelarhora_cancelarhora_page__WEBPACK_IMPORTED_MODULE_9__["CancelarhoraPage"],
                                        cssClass: 'my-custom-class',
                                        componentProps: {
                                            "horas": res,
                                        }
                                    });
                                    modal.onDidDismiss().then((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                        this.invoice.hora_inicio = data.data.horainicio;
                                        this.invoice.id_deta_dia_hora = data.data.idhora;
                                        this.invoiceID = a.id;
                                        this.idnuevahora = data.data.idhora;
                                        this.checkoutInvoice();
                                        if (data.data.idhora != false) {
                                            alert.present();
                                            let loading = yield this.loadingctrl.create({
                                                message: "Cargando..."
                                            });
                                            loading.present();
                                            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'asig-user-per/id/' + a.id_asig_user_per, header)
                                                .subscribe((resusurol) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                                let usuariorol = resusurol;
                                                this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'usuario/idrol/' + usuariorol.id_usuario_rol, header)
                                                    .subscribe((resusu) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                                    let usuario = resusu;
                                                    let split = a.date.split("T");
                                                    let hora = split[1].split(":");
                                                    hora = new Date(new Date().setHours(hora[0] - 5)).getHours() + ":" + hora[1];
                                                    let data = {
                                                        fecha: split[0],
                                                        hora: hora,
                                                        correo_profesor: usuario[0].correo_usuario,
                                                        correo_estudiante: this._authService.getUsuario().correo_usuario
                                                    };
                                                    this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'usuario/cancelarclase', data, header)
                                                        .subscribe((rescancelacioncorreo) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                                        if (rescancelacioncorreo) {
                                                            this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'cancelacion', body, header)
                                                                .subscribe((rescancelacion) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                                                this.alert("Cancelación", "Clase cancelada correctamente.");
                                                                loading.dismiss();
                                                                this.navCtrl.navigateRoot('/home-results');
                                                            }), error => {
                                                            });
                                                        }
                                                        else {
                                                            this.alert("Cancelación", "No se pudo cancelar la clase.");
                                                            loading.dismiss();
                                                        }
                                                    }), error => {
                                                    });
                                                }), error => {
                                                });
                                            }), error => {
                                            });
                                        }
                                    }));
                                    modal.present();
                                }));
                            }
                        })
                    }]
            });
            alert.present();
        });
    }
    alert(header, mensaje) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let alert = yield this.alertController.create({
                header: header,
                message: mensaje,
                buttons: [{ text: 'Cancelar', role: 'cancel' },
                    {
                        text: 'Aceptar', handler: data => {
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    verfica() {
        this.ids_pedidos = [];
        let header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
            })
        };
        this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + 'cancelacion', header)
            .subscribe(restitulos => {
            let aux;
            aux = restitulos;
            for (let a of this.lastInvoices) {
                a.cancelada = false;
                if (new Date(new Date(a.date).setHours(new Date(a.date).getHours() - 5)) < new Date()) {
                    a.cancelada = true;
                }
                for (let b of aux) {
                    if (b.id_pedido == a.id) {
                        a.cancelada = true;
                    }
                }
            }
        }, error => {
        });
    }
    createInputs(id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let myArray = [1, 1, 1, 1, 1, 1];
            const theNewInputs = [];
            this.propertyservice.traerDisponibilidad(id).subscribe(res => {
                let arr = res;
                this.imputs = [];
                for (let b of arr) {
                    this.imputs.push({
                        type: 'radio',
                        label: new Date(b.hora_inicio).toLocaleString(),
                        value: b,
                        checked: false,
                    });
                }
                return theNewInputs;
            });
        });
    }
    traerDispononibilidad(id) {
        return new rxjs__WEBPACK_IMPORTED_MODULE_10__["Observable"](observer => {
            this.propertyservice.traerDisponibilidad(id).subscribe(res => {
                observer.next(res);
            });
        });
    }
    checkoutInvoice() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let loading = yield this.loadingctrl.create({
                message: "Cargando..."
            });
            loading.present();
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            const loader = yield this.loadingctrl.create({
                duration: 2000
            });
            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + "reunion/fecha_inicio/'" + this.invoice.hora_inicio + "'", header)
                .subscribe(resresuniones => {
                let disponibilidad;
                let idsdisponbles = [];
                let reuniones;
                reuniones = resresuniones;
                let aulas;
                let aulasdisponibles = [];
                if (new Date(this.invoice.hora_inicio).getHours() % 2 == 0) {
                    disponibilidad = "Par";
                }
                else {
                    disponibilidad = "Impar";
                }
                this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + "subcuenta-zoom/disponibilidad/'" + disponibilidad + "'", header)
                    .subscribe((resaulas) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    aulas = resaulas;
                    if (aulas.length > reuniones.length) {
                        for (let a of aulas) {
                            if (reuniones.length > 0) {
                                for (let b of reuniones) {
                                    if (a.id_subcuenta != b.id_subcuenta) {
                                        aulasdisponibles.push(a.id_subcuenta);
                                        idsdisponbles.push(a.id_usuario_zoom);
                                    }
                                }
                            }
                            else {
                                aulasdisponibles.push(a.id_subcuenta);
                                idsdisponbles.push(a.id_usuario_zoom);
                            }
                        }
                        let options = {
                            "idusuario": idsdisponbles[0],
                            'topic': "Tutoria",
                            'type': 2,
                            'start_time': this.invoice.hora_inicio,
                            'duration': 60,
                            'timezone': 'America/Bogota',
                            'settings': {
                                'host_video': false,
                                'participant_video': true,
                                'join_before_host': true,
                                'mute_upon_entry': true,
                                'use_pmi': true,
                                'approval_type': 1,
                                'waiting_room': true,
                                'auto_recording': 'cloud'
                            }
                        };
                        this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].url + "zoom-api/meeting", options, header)
                            .subscribe(resmeet => {
                            let meet;
                            meet = resmeet;
                            let body = {
                                id_reunion: meet.id.toString(),
                                enlace: meet.start_url,
                                enlaceEstudiante: meet.join_url,
                                password: meet.password,
                                fecha_inicio: this.invoice.hora_inicio,
                                fecha_fin: new Date(new Date(this.invoice.hora_inicio).setHours(new Date(this.invoice.hora_inicio).getHours() + 1)).toISOString(),
                                hora_inicio: new Date(this.invoice.hora_inicio).getHours().toString(),
                                hora_fin: new Date(new Date(this.invoice.hora_inicio).setHours(new Date(this.invoice.hora_inicio).getHours() + 1)).getHours().toString(),
                                estado: true,
                                id_subcuenta: aulasdisponibles[0],
                                id_pedido: +this.invoiceID
                            };
                            this.invoicesService.cambiarFechaReunion(this.invoice.hora_inicio, this.invoiceID, this.idnuevahora);
                            this.invoicesService.crearreunion(body, meet.join_url, this.invoice.id_deta_dia_hora);
                            this.invoicesService.updateestadohora(this.idhoracancelar, 1);
                            this.invoicesService.updateestadohora(this.idnuevahora, 3);
                            loading.dismiss();
                            this.navCtrl.navigateForward('home-results');
                        }, error => {
                        });
                    }
                    else {
                        let alert = yield this.alertController.create({
                            header: 'Agregar Aulas',
                            cssClass: 'alertpedido',
                            message: 'Pago no procesado no se cuenta con aulas disponibles porfavor seleccione otra hora.',
                            //inputs: this.data,
                            buttons: [
                                {
                                    text: 'Aceptar',
                                    handler: () => {
                                    }
                                }
                            ]
                        });
                        alert.present();
                    }
                }), error => {
                });
            }, error => {
            });
        });
    }
};
InvoicesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_4__["InvoicesService"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] }
];
InvoicesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-invoices',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./invoices.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/invoices/invoices.page.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["trigger"])('staggerIn', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["transition"])('* => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["animate"])('500ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
                ])
            ])
        ],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./invoices.page.scss */ "./src/app/pages/invoices/invoices.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _providers__WEBPACK_IMPORTED_MODULE_4__["InvoicesService"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]])
], InvoicesPage);



/***/ })

}]);
//# sourceMappingURL=pages-invoices-invoices-module-es2015.js.map