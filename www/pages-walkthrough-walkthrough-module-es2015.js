(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-walkthrough-walkthrough-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\r\n\r\n  <ion-slides pager=\"true\" dir=\"ltr\" options=\"slideOpts\">\r\n    <!-- <ion-slide *ngFor=\"let slide of slideList\" class=\"bg-profile\">\r\n      <div class=\"ion-padding\">\r\n        <img [src]=\"slide.image\" alt=\"ionProperty icons\" class=\"ion-margin-bottom slide-image animated fadeInDown slow\">\r\n        <h3 class=\"slide-title text-white animated fadeIn\" [innerHTML]=\"slide.title\"></h3>\r\n        <ion-text color=\"light\">\r\n          <p [innerHTML]=\"slide.description\" class=\"animated fadeIn\"></p>\r\n        </ion-text>\r\n\r\n        <ion-button icon-left expand=\"full\" shape=\"round\" color=\"secondary\" class=\"ion-margin-top\"\r\n          (click)=\"onSlideNext()\">\r\n          Siguiente\r\n          <ion-icon name=\"arrow-round-forward\"></ion-icon>\r\n        </ion-button>\r\n      </div>\r\n\r\n    </ion-slide>-->\r\n    <ion-slide class=\"bg-profile\">\r\n      <div class=\"w100 ion-padding\">\r\n        \r\n        <img src=\"assets/img/ionProperty-ico.png\" alt=\"Amauta\" class=\"logos slide-image animated fadeInDown slow\">\r\n        <h2 class=\"slide-title text-white ion-margin-bottom\">Listo para comenzar tu Tutoría?</h2>\r\n\r\n        <hr>\r\n\r\n        <ion-button expand=\"full\" shape=\"round\" color=\"secondary\" class=\"ion-margin-top\" (click)=\"openLoginPage()\">\r\n         <label style=\"color:#005b85;\"> Ingresar / Registrarse</label>\r\n        </ion-button>\r\n        <ion-button expand=\"full\" shape=\"round\" color=\"dark\" class=\"ion-margin-top\" (click)=\"openHomeLocation()\">\r\n          <!-- routerLink=\"/home\" -->\r\n          Inicio\r\n        </ion-button>\r\n\r\n       <!-- <a class=\"ion-home color-primary item\" href=\"#\" onclick=\"window.open('https://zoom.us/s/7038824581?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJkWFlhaUotQlEyZXBPeG1IYmlPdnFRIiwiaXNzIjoid2ViIiwic3R5IjoxLCJ3Y2QiOiJhdzEiLCJjbHQiOjAsInN0ayI6InF6V3BzWEZWMjc4UFBSODNCZ3lMSWR3WWQxdWpjYzJ5Zzc5VmxwUmZvOGsuQUcuVFVFb0xQYUJWRHRDVzdodk1wWmFSNzU5TG16cXNBcHhHYUF4Z1ZUdHU2N1Rvb3RpS0RGc0dQSnlnd3ZqeEI5SHpnWTh4WGp6QnJCMnozcy5weElweDRLZVV0TEV3UzRaTFBIbDF3LkZoUWwzWHVUTjRNVFAydHUiLCJleHAiOjE2MDcxOTA3MTMsImlhdCI6MTYwNzE4MzUxMywiYWlkIjoiUDBfWXZ2d1BSRk9aNTAxZ0o4dnNzUSIsImNpZCI6IiJ9.Aq7JZtzY4SOFYAstOxE51ZCU4GP12Bkbhwil8BkbCKA', '_system', 'location=yes'); return false;\"> Dashboard</a>-->\r\n      </div>\r\n\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/walkthrough/walkthrough.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/walkthrough/walkthrough.module.ts ***!
  \*********************************************************/
/*! exports provided: WalkthroughPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalkthroughPageModule", function() { return WalkthroughPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _walkthrough_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./walkthrough.page */ "./src/app/pages/walkthrough/walkthrough.page.ts");








const routes = [
    {
        path: '',
        component: _walkthrough_page__WEBPACK_IMPORTED_MODULE_7__["WalkthroughPage"]
    }
];
let WalkthroughPageModule = class WalkthroughPageModule {
};
WalkthroughPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild()
        ],
        declarations: [_walkthrough_page__WEBPACK_IMPORTED_MODULE_7__["WalkthroughPage"]]
    })
], WalkthroughPageModule);



/***/ }),

/***/ "./src/app/pages/walkthrough/walkthrough.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/walkthrough/walkthrough.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-slides {\n  height: 100%;\n}\n\n.toolbar-background {\n  background: var(--ion-color-primary) !important;\n  border-color: transparent;\n}\n\n.slide-zoom {\n  height: 99vh;\n}\n\n.slide-title {\n  margin-top: 0;\n}\n\n.slide-image {\n  margin: 0 0 18px;\n}\n\n.logos {\n  width: 25vh;\n  height: 25vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvd2Fsa3Rocm91Z2gvQzpcXHhhbXBwXFxodGRvY3NcXHNvbHV0aXZvXFxhbWF1dGFtb3ZpbC9zcmNcXGFwcFxccGFnZXNcXHdhbGt0aHJvdWdoXFx3YWxrdGhyb3VnaC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3dhbGt0aHJvdWdoL3dhbGt0aHJvdWdoLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7QUNDSjs7QURFRTtFQUNFLCtDQUFBO0VBQ0EseUJBQUE7QUNDSjs7QURFRTtFQUNFLFlBQUE7QUNDSjs7QURFRTtFQUNFLGFBQUE7QUNDSjs7QURFRTtFQUdFLGdCQUFBO0FDREo7O0FESUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQ0RGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvd2Fsa3Rocm91Z2gvd2Fsa3Rocm91Z2gucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXNsaWRlcyB7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAudG9vbGJhci1iYWNrZ3JvdW5kIHtcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICB9XHJcblxyXG4gIC5zbGlkZS16b29tIHtcclxuICAgIGhlaWdodDogOTl2aDtcclxuICB9XHJcblxyXG4gIC5zbGlkZS10aXRsZSB7XHJcbiAgICBtYXJnaW4tdG9wOiAwO1xyXG4gIH1cclxuXHJcbiAgLnNsaWRlLWltYWdlIHtcclxuICAgIC8vIG1heC1oZWlnaHQ6IDUwJTtcclxuICAgIC8vbWF4LXdpZHRoOiAxMjhweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luOiAwIDAgMThweDtcclxuICB9XHJcblxyXG4ubG9nb3N7XHJcbiAgd2lkdGg6IDI1dmg7XHJcbiAgaGVpZ2h0OiAyNXZoO1xyXG59IiwiaW9uLXNsaWRlcyB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLnRvb2xiYXItYmFja2dyb3VuZCB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xufVxuXG4uc2xpZGUtem9vbSB7XG4gIGhlaWdodDogOTl2aDtcbn1cblxuLnNsaWRlLXRpdGxlIHtcbiAgbWFyZ2luLXRvcDogMDtcbn1cblxuLnNsaWRlLWltYWdlIHtcbiAgbWFyZ2luOiAwIDAgMThweDtcbn1cblxuLmxvZ29zIHtcbiAgd2lkdGg6IDI1dmg7XG4gIGhlaWdodDogMjV2aDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/walkthrough/walkthrough.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/walkthrough/walkthrough.page.ts ***!
  \*******************************************************/
/*! exports provided: WalkthroughPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalkthroughPage", function() { return WalkthroughPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




let WalkthroughPage = class WalkthroughPage {
    constructor(navCtrl, menuCtrl, router) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.router = router;
        this.showSkip = true;
        this.slideOpts = {
            effect: 'flip',
            speed: 1000
        };
        this.dir = 'ltr';
        this.slideList = [
            {
                title: 'Que es <strong><span class="text-tertiary">AMAUTA</span> </strong>?',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque maximus, dui accumsan cursus lacinia, nisl risus.',
                image: 'assets/img/house01.png',
            },
            {
                title: 'Por que es mejor <strong><span class="text-tertiary">AMAUTA</span> </strong>?',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque maximus, dui accumsan cursus lacinia, nisl risus.',
                image: 'assets/img/business01.png',
            },
            {
                title: '<strong>Estas en el lugar perfecto!</strong>',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque maximus, dui accumsan cursus lacinia, nisl risus.',
                image: 'assets/img/rent01.png',
            }
        ];
        this.menuCtrl.enable(false);
    }
    ionViewWillEnter() {
    }
    ngOnInit() {
    }
    onSlideNext() {
        this.slides.slideNext(1000, false);
    }
    onSlidePrev() {
        this.slides.slidePrev(300);
    }
    // onLastSlide() {
    // 	this.slides.slideTo(3, 300)
    // }
    openHomeLocation() {
        this.navCtrl.navigateRoot('/home-results');
        // this.router.navigateByUrl('/tabs/(home:home)');
    }
    openLoginPage() {
        this.navCtrl.navigateForward('/login');
    }
};
WalkthroughPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"], { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
], WalkthroughPage.prototype, "slides", void 0);
WalkthroughPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-walkthrough',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./walkthrough.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./walkthrough.page.scss */ "./src/app/pages/walkthrough/walkthrough.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], WalkthroughPage);



/***/ })

}]);
//# sourceMappingURL=pages-walkthrough-walkthrough-module-es2015.js.map