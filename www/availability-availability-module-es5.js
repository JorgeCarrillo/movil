function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["availability-availability-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/availability/availability.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/availability/availability.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAvailabilityAvailabilityPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>availability</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/availability/availability-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/availability/availability-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: AvailabilityPageRoutingModule */

  /***/
  function srcAppAvailabilityAvailabilityRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AvailabilityPageRoutingModule", function () {
      return AvailabilityPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _availability_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./availability.page */
    "./src/app/availability/availability.page.ts");

    var routes = [{
      path: '',
      component: _availability_page__WEBPACK_IMPORTED_MODULE_3__["AvailabilityPage"]
    }];

    var AvailabilityPageRoutingModule = function AvailabilityPageRoutingModule() {
      _classCallCheck(this, AvailabilityPageRoutingModule);
    };

    AvailabilityPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AvailabilityPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/availability/availability.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/availability/availability.module.ts ***!
    \*****************************************************/

  /*! exports provided: AvailabilityPageModule */

  /***/
  function srcAppAvailabilityAvailabilityModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AvailabilityPageModule", function () {
      return AvailabilityPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _availability_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./availability-routing.module */
    "./src/app/availability/availability-routing.module.ts");
    /* harmony import */


    var _availability_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./availability.page */
    "./src/app/availability/availability.page.ts");

    var AvailabilityPageModule = function AvailabilityPageModule() {
      _classCallCheck(this, AvailabilityPageModule);
    };

    AvailabilityPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _availability_routing_module__WEBPACK_IMPORTED_MODULE_5__["AvailabilityPageRoutingModule"]],
      declarations: [_availability_page__WEBPACK_IMPORTED_MODULE_6__["AvailabilityPage"]]
    })], AvailabilityPageModule);
    /***/
  },

  /***/
  "./src/app/availability/availability.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/availability/availability.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAvailabilityAvailabilityPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F2YWlsYWJpbGl0eS9hdmFpbGFiaWxpdHkucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/availability/availability.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/availability/availability.page.ts ***!
    \***************************************************/

  /*! exports provided: AvailabilityPage */

  /***/
  function srcAppAvailabilityAvailabilityPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AvailabilityPage", function () {
      return AvailabilityPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var AvailabilityPage = /*#__PURE__*/function () {
      function AvailabilityPage() {
        _classCallCheck(this, AvailabilityPage);
      }

      _createClass(AvailabilityPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AvailabilityPage;
    }();

    AvailabilityPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-availability',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./availability.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/availability/availability.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./availability.page.scss */
      "./src/app/availability/availability.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], AvailabilityPage);
    /***/
  }
}]);
//# sourceMappingURL=availability-availability-module-es5.js.map