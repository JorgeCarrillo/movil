(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-edit-profile-edit-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Editar</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"!cargando\" class=\"profile\">\r\n  <div class=\"ion-no-margin\" *ngIf=\"viewMode === 'perfil'\">\r\n  <ion-card class=\"ion-no-margin\">\r\n    <ion-card-content class=\"bg-profile\" >\r\n      <img *ngIf=\"imagensubida\" style=\"border-radius: 50%;\"  [src]=\"images[0].path\" onerror=\"this.src='./assets/img/avatar.png';\">\r\n      <img *ngIf=\"!imagensubida\" style=\"border-radius: 50%;\"  [src]=\"picture\" >\r\n      <h1 class=\"fw500\">{{nombre}} {{apellido}}</h1>\r\n      <!--<h1 class=\"fw500\">Prueba andres</h1>-->\r\n      <h2 color=\"light\" class=\"ion-margin-bottom\">{{tipousuario}}</h2>\r\n      <ion-button *ngIf=\"!imagensubida\" icon-left color=\"secondary\" (click)=\"selectImage()\">\r\n        <ion-icon name=\"photos\"></ion-icon>\r\n        Editar imagen\r\n      </ion-button>\r\n      <ion-button *ngIf=\"imagensubida\"  icon-left color=\"secondary\" (click)=\"startUpload(images[0])\">\r\n        <ion-icon name=\"photos\"></ion-icon>\r\n        Subir imagen\r\n      </ion-button>\r\n    </ion-card-content>\r\n\r\n    <ion-grid fixed class=\"ion-no-padding\">\r\n      <ion-row>\r\n        <ion-col size=\"12\" class=\"ion-padding\">\r\n\r\n          <ion-list class=\"ion-margin-bottom\">\r\n            <ion-list-header color=\"light\">\r\n              <ion-label class=\"fw700\">{{ 'app.label.userdata' | translate }}</ion-label>\r\n            </ion-list-header>\r\n\r\n            <ion-item>\r\n              <ion-label color=\"dark\" position=\"stacked\">{{ 'app.label.fullname' | translate }}:</ion-label>\r\n              <ion-input inputmode=\"text\" placeholder=\"\" [(ngModel)]=\"nombre\" value=\"{{nombre}}\">\r\n              </ion-input>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-label color=\"dark\" position=\"stacked\">{{ 'app.label.fulllastname' | translate }}:</ion-label>\r\n              <ion-input inputmode=\"text\" placeholder=\"\" [(ngModel)]=\"apellido\" value=\"{{apellido}}\">\r\n              </ion-input>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-label color=\"dark\" position=\"stacked\">Identificación:</ion-label>\r\n              <ion-input inputmode=\"text\" placeholder=\"\" [(ngModel)]=\"cedula\" value=\"{{cedula}}\"></ion-input>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-label color=\"dark\" position=\"stacked\">Fecha Nacimiento:</ion-label>\r\n              <ion-datetime\r\n              displayFormat=\"DD/MM/YYYY\"\r\n              pikerFormat=\"YYY MM DD\"\r\n              cancelText=\"Cancelar\"\r\n              doneText=\"OK\"\r\n              [(ngModel)]=\"fechana\" \r\n              #A (ionChange)=\"onChangeano(A.value)\"\r\n              value=\"{{fechana}}\"\r\n              min={{minFecha}}\r\n              max={{maxFecha}}>{{fechana}</ion-datetime>\r\n            </ion-item>\r\n            <ion-item >\r\n              <ion-label color=\"dark\" position=\"stacked\">N° teléfono:</ion-label>\r\n              <ion-input inputmode=\"text\" placeholder=\"\" [(ngModel)]=\"telefono\" value=\"{{telefono}}\"></ion-input>\r\n            </ion-item>\r\n\r\n            <ion-item >\r\n              <ion-label color=\"dark\" position=\"stacked\">Dirección:</ion-label>\r\n              <ion-input inputmode=\"text\" placeholder=\"\" [(ngModel)]=\"direccion\" value=\"{{direccion}}\"></ion-input>\r\n            </ion-item>\r\n            <ion-item>\r\n              <ion-label color=\"dark\">Género</ion-label>\r\n              <ion-select [(ngModel)]=\"genero\">\r\n                <ion-select-option value=\"Femenino\">Femenino\r\n                </ion-select-option>\r\n                <ion-select-option  value=\"Masculino\">Masculino\r\n                </ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-label color=\"dark\">{{ 'app.label.country' | translate }}</ion-label>\r\n              <ion-select (ionChange)=\"onChange()\"  [(ngModel)]=\"idPaisSelec\" >\r\n                <ion-select-option  *ngFor=\"let pais of pais\"   [value]=\"pais.id\">{{ pais.nombre }}\r\n                </ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-label color=\"dark\">Provincia</ion-label>\r\n              <ion-select  [(ngModel)]=\"idProvinciaSelec\" >\r\n                <div *ngFor=\"let estado of estado\">\r\n                <ion-select-option    *ngIf=\"idPaisSelec==estado.id_pais\" [value]=\"estado.id\">{{ estado.nombre }}\r\n                </ion-select-option>\r\n                </div>\r\n              </ion-select>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-label color=\"dark\">Ciudad</ion-label>\r\n              <ion-select (ionChange)=\"nombreCiudad()\" [(ngModel)]=\"idCiudadSelec\" >\r\n                <div *ngFor=\"let ciudad of ciudad\">\r\n                <ion-select-option  *ngIf=\"idProvinciaSelec==ciudad.id_estado\"  [value]=\"ciudad.id\" >{{ ciudad.nombre }}\r\n                </ion-select-option>\r\n                </div>\r\n              </ion-select>\r\n            </ion-item>\r\n          </ion-list>\r\n\r\n         </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n\r\n    <ion-button size=\"large\" expand=\"full\" color=\"dark\" (click)=\"sendData()\" class=\"ion-no-margin\">\r\n      {{ 'app.label.edit' | translate }}</ion-button>\r\n  </ion-card>\r\n</div>\r\n\r\n<div style=\"\r\nbackground-color: white;\" *ngIf=\"viewMode === 'detalles'\">\r\n<br>\r\n<ion-grid fixed class=\"ion-no-padding\">\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"ion-padding\">\r\n      <ion-list class=\"ion-margin-bottom\">\r\n        <ion-list-header color=\"light\">\r\n          <ion-label class=\"fw700\">Detalles Profesor</ion-label>\r\n        </ion-list-header>\r\n<ion-item>\r\n  <ion-label color=\"dark\" position=\"stacked\">Costo clase por hora:</ion-label>\r\n  <ion-input inputmode=\"text\" placeholder=\"Ingrese el costo de sus clases por hora.\" [(ngModel)]=\"detalles.preciofijo\" >\r\n  </ion-input>\r\n</ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label color=\"dark\" position=\"stacked\">Descripción:</ion-label>\r\n    <ion-input inputmode=\"text\" placeholder=\"Ingrese su descripción como profesor.\" [(ngModel)]=\"detalles.descri_deta_profe\">\r\n    </ion-input>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label color=\"dark\" position=\"stacked\">Experiencia:</ion-label>\r\n    <ion-input inputmode=\"text\" placeholder=\"Ingrese su experiencia como profesor.\" [(ngModel)]=\"detalles.experiencia\" >\r\n    </ion-input>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label color=\"dark\" position=\"stacked\">Preferencias de Enseñar:</ion-label>\r\n    <ion-input inputmode=\"text\" placeholder=\"Ingrese sus preferencias.\" [(ngModel)]=\"detalles.preferencia_ense\">\r\n    </ion-input>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label color=\"dark\" position=\"stacked\">Facebook:</ion-label>\r\n    <ion-input inputmode=\"text\" placeholder=\"Ingrese su enlace.\" [(ngModel)]=\"detalles.facebook\" >\r\n    </ion-input>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label color=\"dark\" position=\"stacked\">Twitter:</ion-label>\r\n    <ion-input inputmode=\"text\" placeholder=\"Ingrese su enlace.\" [(ngModel)]=\"detalles.twitter\" >\r\n    </ion-input>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label color=\"dark\" position=\"stacked\">Youtube:</ion-label>\r\n    <ion-input inputmode=\"text\" placeholder=\"Ingrese su enlace.\" [(ngModel)]=\"detalles.youtube\" >\r\n    </ion-input>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label color=\"dark\" position=\"stacked\">Instagram:</ion-label>\r\n    <ion-input inputmode=\"text\" placeholder=\"Ingrese su enlace.\" [(ngModel)]=\"detalles.instagram\" >\r\n    </ion-input>\r\n  </ion-item>\r\n\r\n  <ion-list-header color=\"light\">\r\n    <ion-label class=\"fw700\">Video de Presentación</ion-label>\r\n  </ion-list-header>\r\n\r\n   <div class=\"video-selection\" >\r\n    <div class=\"card-background-page\" *ngIf=\"!selectedVideo && verVideo\" style=\"text-align: center;\">\r\n      <ion-list-header color=\"back\">\r\n        <ion-label class=\"fw700\">Usted ya cuenta con un video de presentación.</ion-label>\r\n      </ion-list-header>\r\n      <div>\r\n        <video [src]=\"urlvideo\" poster=\"../../../assets/img/play.png\" controls style=\"width: 100%;height: 50vh;\"></video>\r\n      </div>\r\n      <br>\r\n      <div class=\"card-title\" >Si desea cambiar de presentación Cargue el nuevo video.</div>\r\n      <div class=\"card-subtitle\">Peso máximo 5mb y formato .mp4</div>\r\n      <ion-button icon-left color=\"secondary\" (click)=\"selectVideos()\">\r\n        <ion-icon name=\"photos\"></ion-icon>\r\n        Cambiar Video\r\n      </ion-button>\r\n    \r\n    </div>\r\n    \r\n  </div>\r\n\r\n\r\n\r\n  <div class=\"video-selection\" *ngIf=\"!selectedVideo && !verVideo\">\r\n    <div class=\"card-background-page\">\r\n      <ion-card style=\"text-align: center;\">\r\n        <img src=\"../../../assets/img/video.png\">\r\n        <div class=\"card-title\" >Por favor suba su video de presentación.</div>\r\n        <div class=\"card-subtitle\">Peso máximo 5mb y formato .mp4</div>\r\n        <ion-button icon-left color=\"secondary\" (click)=\"selectVideos()\">\r\n          <ion-icon name=\"photos\"></ion-icon>\r\n          Subir Video\r\n        </ion-button>\r\n      </ion-card>\r\n    \r\n    </div>\r\n  </div>\r\n\r\n\r\n\r\n\r\n  <div class=\"video-section\" *ngIf=\"selectedVideo\">\r\n    <div class=\"card-background-page\">\r\n\r\n      <ion-card style=\"text-align: center;\">\r\n        <video controls [src]=\"selectedVideo\"></video>\r\n        <div class=\"button-options\" *ngIf=\"!uploadedVideo && !isUploading\">\r\n          <ion-button icon-left color=\"danger\" (click)=\"cancelSelection()\">\r\n            <ion-icon name=\"close-circle\"></ion-icon>\r\n            Cancelar\r\n          </ion-button>\r\n          <ion-button icon-left color=\"success\" (click)=\"uploadVideo()\">\r\n            <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n            Subir\r\n          </ion-button>\r\n        </div>\r\n     \r\n        <div *ngIf=\"isUploading\">\r\n          <div class=\"uploading\">\r\n            <p><ion-spinner name=\"bubbles\"></ion-spinner></p>\r\n            <p>Subida - {{ uploadPercent }}% Completo</p>\r\n          </div>\r\n          <div class=\"button-options\">\r\n            <ion-button icon-left color=\"danger\" (click)=\"cancelUpload()\">\r\n              <ion-icon name=\"close-circle\"></ion-icon>\r\n              Cancelar\r\n            </ion-button>\r\n          </div>\r\n        </div>\r\n    \r\n        <div class=\"button-options\" *ngIf=\"uploadedVideo\">\r\n          <ion-button icon-left color=\"success\" (click)=\"cancelSelection()\">\r\n            <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n            Comenzar de nuevo\r\n          </ion-button>\r\n        </div>\r\n      </ion-card>\r\n    \r\n    </div>\r\n   \r\n\r\n  </div>\r\n\r\n\r\n\r\n      </ion-list>\r\n</ion-col>\r\n</ion-row>\r\n</ion-grid>\r\n<ion-button size=\"large\" expand=\"full\" color=\"dark\" (click)=\"sendDataDetaProf()\" class=\"ion-no-margin\">Guardar</ion-button>\r\n</div>\r\n\r\n<div class=\"ion-no-margin\" *ngIf=\"viewMode === 'detalles2'\">\r\n\r\n\r\n</div>\r\n\r\n\r\n\r\n</ion-content>\r\n\r\n<ion-footer class=\"bg-dark\" *ngIf=\"tipousuario === 'PROFESOR'\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-grid class=\"ion-no-padding\">\r\n      <ion-row style=\"\r\n      justify-content: center;\">\r\n        <ion-col size=\"4\" class=\"ion-no-padding\">\r\n          <ion-button  (click) = \"cambiar('perfil')\" size=\"small\" expand=\"full\" fill=\"clear\" color=\"medium\">\r\n            DATOS\r\n            <ion-icon slot=\"start\" name=\"list\"></ion-icon>\r\n          </ion-button>\r\n        </ion-col>\r\n        <ion-col size=\"4\" class=\"ion-no-padding\" >\r\n          <ion-button (click) = \"cambiar('detalles')\"  size=\"small\" expand=\"full\" fill=\"clear\" color=\"medium\">\r\n            DETALLES\r\n            <ion-icon slot=\"start\" name=\"list-box\"></ion-icon>\r\n          </ion-button>\r\n        </ion-col>\r\n</ion-row>\r\n</ion-grid>\r\n</ion-toolbar>\r\n</ion-footer>\r\n\r\n\r\n");

/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.module.ts ***!
  \***********************************************************/
/*! exports provided: EditProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function() { return EditProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./edit-profile.page */ "./src/app/pages/edit-profile/edit-profile.page.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm2015/agm-core.js");









const routes = [
    {
        path: '',
        component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_7__["EditProfilePage"]
    }
];
let EditProfilePageModule = class EditProfilePageModule {
};
EditProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forChild(),
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _agm_core__WEBPACK_IMPORTED_MODULE_8__["AgmCoreModule"].forRoot({
                apiKey: 'AIzaSyD9BxeSvt3u--Oj-_GD-qG2nPr1uODrR0Y'
            })
        ],
        declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_7__["EditProfilePage"]]
    })
], EditProfilePageModule);



/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light)) ;\n}\n\n.bg-profile {\n  background-image: linear-gradient(180deg, #005b85 0%, #005b85 100%);\n}\n\n.profile ion-card {\n  width: 100%;\n  border-radius: 0;\n  background-color: #fff;\n}\n\n.profile ion-card ion-card-content {\n  padding: 32px;\n  background-color: var(--ion-color-primary);\n  color: #fff;\n  text-align: center;\n}\n\n.profile ion-card ion-card-content img {\n  height: 128px;\n  width: 128px;\n  border-radius: 50%;\n  border: solid 4px #fff;\n  display: inline;\n  box-shadow: 0 0 28px rgba(255, 255, 255, 0.65);\n}\n\n.profile ion-card ion-card-content h1 {\n  margin-top: 0.5rem;\n}\n\n.profile ion-item ion-input {\n  border-bottom: 1px solid var(--ion-color-tertiary);\n}\n\n.profile ion-buttom button {\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZWRpdC1wcm9maWxlL0M6XFx4YW1wcFxcaHRkb2NzXFxzb2x1dGl2b1xcYW1hdXRhbW92aWwvc3JjXFxhcHBcXHBhZ2VzXFxlZGl0LXByb2ZpbGVcXGVkaXQtcHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2VkaXQtcHJvZmlsZS9lZGl0LXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0Usd0ZBQUE7QUNBSjs7QURJQTtFQUNFLG1FQUFBO0FDREY7O0FES0U7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQ0ZKOztBREdJO0VBQ0UsYUFBQTtFQUNBLDBDQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDRE47O0FESU07RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsOENBQUE7QUNGUjs7QURLTTtFQUVFLGtCQUFBO0FDSlI7O0FEaUJJO0VBQ0Usa0RBQUE7QUNmTjs7QURvQkk7RUFDRSxTQUFBO0FDbEJOIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZWRpdC1wcm9maWxlL2VkaXQtcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSksIHZhcigtLWlvbi1jb2xvci1saWdodCkpXHJcbiAgfVxyXG59XHJcblxyXG4uYmctcHJvZmlsZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgIzAwNWI4NSAwJSwgIzAwNWI4NSAxMDAlKTtcclxuICAvLzA1NDU5N1xyXG59XHJcbi5wcm9maWxlIHtcclxuICBpb24tY2FyZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgaW9uLWNhcmQtY29udGVudCB7XHJcbiAgICAgIHBhZGRpbmc6IDMycHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgLy8gcGFkZGluZy1ib3R0b206IDI4cHg7XHJcblxyXG4gICAgICBpbWcge1xyXG4gICAgICAgIGhlaWdodDogMTI4cHg7XHJcbiAgICAgICAgd2lkdGg6IDEyOHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBib3JkZXI6IHNvbGlkIDRweCAjZmZmO1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMjhweCByZ2JhKDI1NSwyNTUsMjU1LCAuNjUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBoMSB7XHJcbiAgICAgICAgLy8gZm9udC1zaXplOiAyLjVyZW07XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLjVyZW07XHJcbiAgICAgICAgLy8gY29sb3I6ICNmZmY7XHJcbiAgICAgIH1cclxuXHJcbiAgICAvLyAgIGgzIHtcclxuICAgIC8vICAgICBmb250LXNpemU6IDEuOHJlbTtcclxuICAgIC8vICAgICBjb2xvcjogI2ZmZjtcclxuICAgIC8vICAgfVxyXG5cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlvbi1pdGVtIHtcclxuICAgIGlvbi1pbnB1dCB7XHJcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaW9uLWJ1dHRvbSB7XHJcbiAgICBidXR0b24ge1xyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsIjpob3N0IGlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSksIHZhcigtLWlvbi1jb2xvci1saWdodCkpIDtcbn1cblxuLmJnLXByb2ZpbGUge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCAjMDA1Yjg1IDAlLCAjMDA1Yjg1IDEwMCUpO1xufVxuXG4ucHJvZmlsZSBpb24tY2FyZCB7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xufVxuLnByb2ZpbGUgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDMycHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgY29sb3I6ICNmZmY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5wcm9maWxlIGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgaW1nIHtcbiAgaGVpZ2h0OiAxMjhweDtcbiAgd2lkdGg6IDEyOHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogc29saWQgNHB4ICNmZmY7XG4gIGRpc3BsYXk6IGlubGluZTtcbiAgYm94LXNoYWRvdzogMCAwIDI4cHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjY1KTtcbn1cbi5wcm9maWxlIGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgaDEge1xuICBtYXJnaW4tdG9wOiAwLjVyZW07XG59XG4ucHJvZmlsZSBpb24taXRlbSBpb24taW5wdXQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5KTtcbn1cbi5wcm9maWxlIGlvbi1idXR0b20gYnV0dG9uIHtcbiAgbWFyZ2luOiAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.page.ts ***!
  \*********************************************************/
/*! exports provided: EditProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePage", function() { return EditProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_usuario_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/usuario.service */ "./src/app/services/usuario.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");




















const STORAGE_KEY = 'my_images';
const baseUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + "usuario/upload-image";
const MAX_FILE_SIZE = 5 * 1024 * 1024;
const ALLOWED_MIME_TYPE = "video/mp4";
let EditProfilePage = class EditProfilePage {
    constructor(navCtrl, loadingCtrl, toastCtrl, translate, _authService, _usuarioService, router, route, propertyService, _httpClient, actionSheetController, camara, filePath, file, plt, storage, ref, webview, transfer, theInAppBrowser) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this._authService = _authService;
        this._usuarioService = _usuarioService;
        this.router = router;
        this.route = route;
        this.propertyService = propertyService;
        this._httpClient = _httpClient;
        this.actionSheetController = actionSheetController;
        this.camara = camara;
        this.filePath = filePath;
        this.file = file;
        this.plt = plt;
        this.storage = storage;
        this.ref = ref;
        this.webview = webview;
        this.transfer = transfer;
        this.theInAppBrowser = theInAppBrowser;
        this.viewMode = 'perfil';
        this.verVideo = false;
        this.minFecha = (new Date().getFullYear() - 100).toString();
        this.maxFecha = (new Date().getFullYear()).toString();
        this.detalles = {
            "preciofijo": "",
            "descri_deta_profe": "",
            "facebook": "",
            "twitter": "",
            "youtube": "",
            "instagram": "",
            "preferencia_ense": "",
            "experiencia": "",
            "id_usuario_rol": +this._authService.getIdUsuarioRol()
        };
        this.cargando = true;
        this.images = [];
        this.imagensubida = false;
        this.isUploading = false;
        this.uploadPercent = 0;
        this.properID = this._authService.getIdUsuarioRol();
        this.obetenerpais().subscribe(res => {
            this.obetenerciudad().subscribe(res => {
                this.obetenerProvincia().subscribe(res => {
                    this.traerDatos().subscribe(res => {
                        if (this.tipousuario != "PROFESOR")
                            this.cargando = false;
                        else
                            this.traerDetallesProfesor().subscribe(res => {
                                this.cargando = false;
                            });
                    });
                });
            });
        });
    }
    //fotografias
    selectImage() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetController.create({
                header: "Seleccione el Recurso Para Cargar la Imagen",
                buttons: [{
                        text: 'Cargar desde Galería',
                        handler: () => {
                            this.takePicture(this.camara.PictureSourceType.PHOTOLIBRARY);
                        }
                    },
                    {
                        text: 'Tomar Foto',
                        handler: () => {
                            this.takePicture(this.camara.PictureSourceType.CAMERA);
                        }
                    },
                    {
                        text: 'Cancelar',
                        role: 'cancel'
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    takePicture(sourceType) {
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camara.getPicture(options).then(imagePath => {
            if (this.plt.is('android') && sourceType === this.camara.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                    let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            }
        });
    }
    copyFileToLocalDir(namePath, currentName, newFileName) {
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
            this.updateStoredImages(newFileName);
        }, error => {
            this.presentToast('Error al guardar el archivo.');
        });
    }
    createFileName() {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    }
    updateStoredImages(name) {
        this.storage.get(STORAGE_KEY).then(images => {
            let arr = JSON.parse(images);
            if (!arr) {
                let newImages = [name];
                this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
            }
            else {
                arr.push(name);
                this.storage.set(STORAGE_KEY, JSON.stringify(arr));
            }
            let filePath = this.file.dataDirectory + name;
            let resPath = this.pathForImage(filePath);
            let newEntry = {
                name: name,
                path: resPath,
                filePath: filePath
            };
            this.images = [newEntry, ...this.images];
            this.ref.detectChanges(); // trigger change detection cycle
            this.imagensubida = true;
            this._authService.getfoto();
        });
    }
    pathForImage(img) {
        if (img === null) {
            return '';
        }
        else {
            let converted = this.webview.convertFileSrc(img);
            return converted;
        }
    }
    startUpload(imgEntry) {
        this.nombreImagen = imgEntry.name;
        this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
            .then(entry => {
            entry.file(file => this.readFile(file));
        })
            .catch(err => {
            this.presentToast('Error al leer el archivo.');
        });
    }
    readFile(file) {
        const reader = new FileReader();
        reader.onloadend = () => {
            const formData = new FormData();
            const imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('filename', imgBlob, file.name);
            this.uploadImageData(formData);
        };
        reader.readAsArrayBuffer(file);
    }
    uploadImageData(formData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
            //content: "Uploading image...",
            });
            yield loading.present();
            this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + "usuario/upload-image", formData, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({ 'Authorization': 'Bearer ' + this._authService.getToken() }) })
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(() => {
                loading.dismiss();
            }))
                .subscribe(res => {
                this.pathFotoProf = Object.values(res)[1];
                this.presentToast("Imagen ingresada con éxito");
                this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + "usuario/actualizarfoto/'" + this.nombreImagen + "'/" + this._authService.getIdUsuario(), { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({ 'Authorization': 'Bearer ' + this._authService.getToken() }) })
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(() => {
                    loading.dismiss();
                }))
                    .subscribe(res => {
                    localStorage.setItem('fotoUser', JSON.stringify({
                        fotousuario: _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].urlfotos + this.nombreImagen
                    }));
                    this.navCtrl.navigateRoot("/home-results");
                });
            });
        });
    }
    selectVideos() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetController.create({
                header: "Seleccione el Recurso Para Cargar el Video",
                buttons: [{
                        text: 'Cargar desde Galería',
                        handler: () => {
                            // this.takePicture(this.camara.PictureSourceType.PHOTOLIBRARY);
                            this.selectVideo();
                        }
                    },
                    /* {
                       text: 'Tomar Video',
                       handler:
                         () => {
                           this.takePicture(this.camara.PictureSourceType.CAMERA);
                         }
                     }
                       ,*/
                    {
                        text: 'Cancelar',
                        role: 'cancel'
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    showLoader() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading = yield this.loadingCtrl.create({
            //content: "Uploading image...",
            });
            yield this.loading.present();
        });
    }
    dismissLoader() {
        this.loading.dismiss();
    }
    cancelSelection() {
        this.selectedVideo = null;
        this.uploadedVideo = null;
    }
    selectVideo() {
        const options = {
            mediaType: this.camara.MediaType.VIDEO,
            sourceType: this.camara.PictureSourceType.PHOTOLIBRARY
        };
        this.camara.getPicture(options)
            .then((videoUrl) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (videoUrl) {
                // this.showLoader();
                const loading = yield this.loadingCtrl.create({
                //content: "Uploading image...",
                });
                loading.present();
                //await this.loading.present();
                this.uploadedVideo = null;
                var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
                var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);
                dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
                try {
                    var dirUrl = yield this.file.resolveDirectoryUrl(dirpath);
                    var retrievedFile = yield this.file.getFile(dirUrl, filename, {});
                }
                catch (err) {
                    //this.dismissLoader();
                    loading.dismiss();
                    return this.presentToast("Algo salio mal.");
                }
                retrievedFile.file(data => {
                    //this.dismissLoader();
                    loading.dismiss();
                    if (data.size > MAX_FILE_SIZE)
                        return this.presentToast("No puede cargar mas de 5m");
                    if (data.type !== ALLOWED_MIME_TYPE)
                        return this.presentToast("Tipo de archivo incorrecto");
                    this.selectedVideo = retrievedFile.nativeURL;
                });
            }
        }), (err) => {
            console.log(err);
        });
    }
    createFileNameVideo() {
        var d = new Date(), n = d.getTime(), newFileName = n + ".mp4";
        return newFileName;
    }
    uploadVideo() {
        this.createFileNameVideo();
        var url = baseUrl;
        //var filename = this.selectedVideo.substr(this.selectedVideo.lastIndexOf('/') + 1);
        var filename = this.createFileNameVideo();
        this.nombreVideo = filename;
        var options = {
            fileName: filename,
            fileKey: "filename",
            mimeType: "video/mp4"
        };
        this.videoFileUpload = this.transfer.create();
        this.isUploading = true;
        this.videoFileUpload.upload(this.selectedVideo, url, options)
            .then((data) => {
            this.isUploading = false;
            this.uploadPercent = 0;
            return JSON.parse(data.response);
        })
            .then((data) => {
            this.uploadedVideo = data.url;
            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + "usuario/actualizarvideo/'" + this.nombreVideo + "'/" + this._authService.getIdUsuarioRol(), { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({ 'Authorization': 'Bearer ' + this._authService.getToken() }) })
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(() => {
                //loading.dismiss();
            }))
                .subscribe(res => {
                this.navCtrl.navigateRoot("/home-results");
            });
            this.presentToast("video subido.");
        })
            .catch((err) => {
            this.isUploading = false;
            this.uploadPercent = 0;
            this.presentToast("Error al subir video.");
        });
        this.videoFileUpload.onProgress((data) => {
            this.uploadPercent = Math.round((data.loaded / data.total) * 100);
        });
    }
    cancelUpload() {
        this.videoFileUpload.abort();
        this.uploadPercent = 0;
    }
    ////fin video
    onChangeano(fecha) {
        this.auxfecha = fecha;
    }
    obetenerpais() {
        return new rxjs__WEBPACK_IMPORTED_MODULE_15__["Observable"](observer => {
            this.pais = [];
            let auxpais;
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + 'pais', header)
                .subscribe(respais => {
                //this.genero = this.datos.usuario.sexo_usuario
                auxpais = respais;
                for (let i = 0; i < auxpais.length; i++) {
                    this.pais.push({
                        id: auxpais[i].id_pais,
                        nombre: auxpais[i].nombre_pais,
                    });
                }
                observer.next(this.pais);
            }, error => {
            });
        });
    }
    obetenerciudad() {
        return new rxjs__WEBPACK_IMPORTED_MODULE_15__["Observable"](observer => {
            this.ciudad = [];
            let auxciudad;
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + 'ciudad', header)
                .subscribe(resciudad => {
                //this.genero = this.datos.usuario.sexo_usuario
                auxciudad = resciudad;
                for (let i = 0; i < auxciudad.length; i++) {
                    this.ciudad.push({
                        id: auxciudad[i].id_ciudad,
                        nombre: auxciudad[i].nombre_ciudad,
                        id_estado: auxciudad[i].id_estado,
                    });
                }
                observer.next(this.ciudad);
            }, error => {
            });
        });
    }
    obetenerProvincia() {
        return new rxjs__WEBPACK_IMPORTED_MODULE_15__["Observable"](observer => {
            this.estado = [];
            let auxestado;
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + 'estado', header)
                .subscribe(resestado => {
                //this.genero = this.datos.usuario.sexo_usuario
                auxestado = resestado;
                for (let i = 0; i < auxestado.length; i++) {
                    this.estado.push({
                        id: auxestado[i].id_estado,
                        nombre: auxestado[i].nombre_estado,
                        id_pais: auxestado[i].id_pais,
                    });
                }
                observer.next(this.estado);
            }, error => {
            });
        });
    }
    traerDatos() {
        return new rxjs__WEBPACK_IMPORTED_MODULE_15__["Observable"](observer => {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + 'usuario-rol/id/' + this.properID, header)
                .subscribe(resdatos => {
                this.datos = resdatos;
                if (this.datos.usuario.foto_usuario != null) {
                    this.picture = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].urlfotos + this.datos.usuario.foto_usuario;
                }
                else {
                    this.picture = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].urlfotos + "avatar.png";
                }
                this.cedula = this.datos.usuario.identificacion_usuario;
                this.nombre = this.datos.usuario.nombres_usuario;
                this.apellido = this.datos.usuario.apellidos_usuario;
                this.fechana = this.datos.usuario.fecha_naci_usuario;
                this.telefono = this.datos.usuario.telefono_usuario;
                this.direccion = this.datos.usuario.direccion_usuario;
                this.tipousuario = this.datos.rol.nombre_rol;
                this.genero = this.datos.usuario.sexo_usuario;
                if (this.datos.usuario.ciudad != null) {
                    this.idPaisSelec = this.datos.usuario.ciudad.estado.id_pais;
                    this.idProvinciaSelec = this.datos.usuario.ciudad.id_estado;
                    this.idCiudadSelec = this.datos.usuario.ciudad.id_ciudad;
                    this.pais.nombre = this.datos.usuario.ciudad.estado.pais.nombre_pais;
                    this.estado.nombre = this.datos.usuario.ciudad.estado.nombre_estado;
                    this.ciudad.nombre = this.datos.usuario.ciudad.nombre_ciudad;
                }
                observer.next(true);
            }, error => {
            });
        });
    }
    traerDetallesProfesor() {
        return new rxjs__WEBPACK_IMPORTED_MODULE_15__["Observable"](observer => {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + 'detalles_profesor/usuario/' + this._authService.getIdUsuarioRol(), header)
                .subscribe(resdatos => {
                if (resdatos != null || resdatos != undefined) {
                    let detallesprof;
                    this.detaprof = resdatos;
                    detallesprof = resdatos;
                    if (detallesprof.video_presentacion != null || detallesprof.video_presentacion != undefined) {
                        this.urlvideo = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].urlfotos + detallesprof.video_presentacion;
                        this.verVideo = true;
                    }
                    else {
                        this.verVideo = false;
                    }
                    this.detalles.descri_deta_profe = detallesprof.descri_deta_profe;
                    this.detalles.facebook = detallesprof.facebook;
                    this.detalles.twitter = detallesprof.twitter;
                    this.detalles.youtube = detallesprof.youtube;
                    this.detalles.preciofijo = detallesprof.preciofijo;
                    this.detalles.preferencia_ense = detallesprof.preferencia_ense;
                    this.detalles.instagram = detallesprof.instagram;
                    this.detalles.experiencia = detallesprof.experiencia;
                    this.id_deta_profe = detallesprof.id_deta_profe;
                    observer.next(this.detalles);
                }
                else {
                    observer.next(this.detalles);
                }
            }, error => {
            });
        });
    }
    sendDataDetaProf() {
        let header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
            })
        };
        if (this.detaprof == null) {
            this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + 'detalles_profesor', this.detalles, header)
                .subscribe(resdatos => {
                this.navCtrl.navigateRoot('/home-results');
            }, error => {
            });
        }
        else {
            for (let a in this.detalles) {
            }
            this._httpClient.put(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + 'detalles_profesor/' + this.id_deta_profe, this.detalles, header)
                .subscribe(resdatos => {
                if (this.detalles.preciofijo == ""
                    || this.detalles.preciofijo == null
                    || this.detalles.descri_deta_profe == null
                    || this.detalles.descri_deta_profe == ""
                    || this.detalles.experiencia == null
                    || this.detalles.experiencia == ""
                    || this.detalles.preferencia_ense == null
                    || this.detalles.preferencia_ense == "") {
                    this.propertyService.faltaCOnfiguracion = true;
                }
                else {
                    this.propertyService.faltaCOnfiguracion = false;
                }
                this._authService.getfoto();
                this.navCtrl.navigateRoot('/home-results');
            }, error => {
            });
        }
    }
    onChange() {
        this.idCiudadSelec = 0;
        this.idProvinciaSelec = 0;
    }
    nombreCiudad() {
        for (let a of this.ciudad) {
            if (a.id == this.idCiudadSelec) {
                this.ciudadnombre = a.nombre;
            }
        }
    }
    sendData() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.nombreCiudad();
            // send booking info
            let datosCompletos = true;
            let data = {
                "nombres_usuario": this.nombre,
                "apellidos_usuario": this.apellido,
                "identificacion_usuario": this.cedula,
                "fecha_naci_usuario": this.fechana,
                "telefono_usuario": this.telefono,
                "direccion_usuario": this.direccion,
                "ciudad_nombre": this.ciudadnombre,
                "id_ciudad": this.idCiudadSelec,
                "foto_usuario": this.picture,
                "id_usuario": +this._authService.getIdUsuario(),
                "sexo_usuario": this.genero
            };
            for (let a in data) {
                if (data[a] == null) {
                    this.presentToast("Todos los datos son requeridos");
                    datosCompletos = false;
                    break;
                }
            }
            if (datosCompletos) {
                let header = {
                    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                    })
                };
                this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].url + 'usuario/actualizar', data, header)
                    .subscribe(resactualizacion => {
                    this._authService.getfoto();
                    this.navCtrl.pop();
                }, error => {
                });
            }
        });
    }
    editarensena(ensena) {
    }
    actualizarensena() {
    }
    cambiar(perfil) {
        this.viewMode = perfil;
    }
    presentToast(mensaje) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let toast = yield this.toastCtrl.create({
                message: mensaje,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    openViseo() {
        let options = {
            location: 'yes',
            hidden: 'no',
            clearcache: 'yes',
            clearsessioncache: 'yes',
            zoom: 'yes',
            hardwareback: 'yes',
            mediaPlaybackRequiresUserAction: 'no',
            shouldPauseOnSuspend: 'no',
            closebuttoncaption: 'Close',
            disallowoverscroll: 'no',
            toolbar: 'yes',
            enableViewportScale: 'no',
            allowInlineMediaPlayback: 'no',
            presentationstyle: 'pagesheet',
            fullscreen: 'yes',
        };
        let target = "_blank";
        //let target = "_system";
        //let target = "_self";
        //this.urlvideo
        this.theInAppBrowser.create(this.urlvideo, target, options);
    }
};
EditProfilePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_4__["TranslateProvider"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _services_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__["Camera"] },
    { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_11__["FilePath"] },
    { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_16__["File"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_14__["Storage"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_17__["WebView"] },
    { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_12__["FileTransfer"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__["InAppBrowser"] }
];
EditProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-profile',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./edit-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_13__["trigger"])('staggerIn', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_13__["transition"])('* => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_13__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_13__["style"])({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_13__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_13__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_13__["animate"])('500ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_13__["style"])({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
                ])
            ])
        ],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./edit-profile.page.scss */ "./src/app/pages/edit-profile/edit-profile.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _providers__WEBPACK_IMPORTED_MODULE_4__["TranslateProvider"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
        _services_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
        _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__["Camera"],
        _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_11__["FilePath"],
        _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_16__["File"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_14__["Storage"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
        _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_17__["WebView"],
        _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_12__["FileTransfer"],
        _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__["InAppBrowser"]])
], EditProfilePage);



/***/ })

}]);
//# sourceMappingURL=pages-edit-profile-edit-profile-module-es2015.js.map