function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-broker-chat-broker-chat-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-chat/broker-chat.page.html":
  /*!***********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-chat/broker-chat.page.html ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesBrokerChatBrokerChatPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      {{ 'app.pages.chat.title.header' | translate }}\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content   class=\"ion-padding-vertical\" fullscreen>\r\n  <div id=\"chat-parent\">\r\n    <div #chat id=\"chat-container\">\r\n      <ion-row *ngFor=\"let conv of conversation; let i = index;\" class=\"ion-padding\">\r\n        <ion-col class=\"no-padding chat-row\" [ngClass]=\"{ 'reverse': conv.sender}\">\r\n          <div class=\"avatar-parent\">\r\n            <div class=\"avatar-border\"></div>\r\n            <img [src]=\"conv.image\" class=\"avatar\">\r\n          </div>\r\n          <div [ngClass]=\"{ 'name-row-parent-right': conv.sender,'name-row-parent-left': !conv.sender}\">\r\n            <div class=\"ion-no-padding\" [ngClass]=\"{ 'name-row-right': conv.sender,'name-row-left': !conv.sender}\">\r\n              {{conv.sender ? 'TU': broker.name}}\r\n              <div [ngClass]=\"{ 'timer-right': conv.sender,'timer-left': !conv.sender}\">{{conv.hora}}\r\n                <ion-icon *ngIf=\"conv.sender && conv.sent\" name=\"checkmark\" [color]=\"conv.read ? 'd3green': 'light'\"\r\n                  class=\"first-tick\"></ion-icon>\r\n                <ion-icon *ngIf=\"conv.sender && conv.delivered\" name=\"checkmark\"\r\n                  [color]=\"conv.read ? 'd3green': 'light'\" class=\"second-tick\"></ion-icon>\r\n              </div>\r\n              <div [ngClass]=\"{ 'reverse-arc-right': conv.sender,'reverse-arc-left': !conv.sender}\">\r\n                <div [ngClass]=\"{ 'reverse-arc-color-right': conv.sender,'reverse-arc-color-left': !conv.sender}\"></div>\r\n              </div>\r\n            </div>\r\n            <div class=\"ion-no-padding\"\r\n              [ngClass]=\"{ 'message-row-right': conv.sender,'message-row-left': !conv.sender}\">\r\n              <ion-text class=\"ion-no-margin text\">{{conv.text}}</ion-text>\r\n            </div>\r\n          </div>\r\n\r\n        </ion-col>\r\n      </ion-row>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n<ion-footer translucent>\r\n  <ion-row class=\"input-position\">\r\n    <ion-col size=\"9\" class=\"ion-no-padding\">\r\n      <ion-input class=\"ion-padding-start input restrict\" type=\"text\" placeholder=\"Escribe un mensaje\"\r\n        [(ngModel)]=\"input\">\r\n      </ion-input>\r\n    </ion-col>\r\n    <ion-col size=\"3\" class=\"ion-no-padding\">\r\n      <ion-button size=\"large\" expand=\"full\" class=\"ion-no-margin send-button\" color=\"primary\" (click)=\"send()\">\r\n        <ion-text color=\"light\">ENVIAR</ion-text>\r\n      </ion-button>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/broker-chat/broker-chat.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/broker-chat/broker-chat.module.ts ***!
    \*********************************************************/

  /*! exports provided: BrokerChatPageModule */

  /***/
  function srcAppPagesBrokerChatBrokerChatModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BrokerChatPageModule", function () {
      return BrokerChatPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _broker_chat_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./broker-chat.page */
    "./src/app/pages/broker-chat/broker-chat.page.ts");

    var routes = [{
      path: '',
      component: _broker_chat_page__WEBPACK_IMPORTED_MODULE_7__["BrokerChatPage"]
    }];

    var BrokerChatPageModule = function BrokerChatPageModule() {
      _classCallCheck(this, BrokerChatPageModule);
    };

    BrokerChatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild()],
      declarations: [_broker_chat_page__WEBPACK_IMPORTED_MODULE_7__["BrokerChatPage"]]
    })], BrokerChatPageModule);
    /***/
  },

  /***/
  "./src/app/pages/broker-chat/broker-chat.page.scss":
  /*!*********************************************************!*\
    !*** ./src/app/pages/broker-chat/broker-chat.page.scss ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesBrokerChatBrokerChatPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host ion-content {\n  --background: var(--ion-color-dark);\n}\n\n.left {\n  float: left;\n}\n\n.avatar {\n  width: calc(16vw - 4px);\n  height: calc(16vw - 4px);\n  border-radius: calc(8vw - 2px);\n  z-index: 2;\n}\n\n.restrict {\n  --height: 33px;\n}\n\n.input {\n  background-color: #fff;\n  height: 56px;\n}\n\n.animate {\n  -webkit-animation-name: fadeIn;\n          animation-name: fadeIn;\n  -webkit-animation-duration: 1s;\n          animation-duration: 1s;\n  transition: 0.1s;\n}\n\n.right {\n  float: right;\n}\n\n.text {\n  font-size: 16px;\n}\n\n.card {\n  margin-bottom: 9px;\n  max-width: 80%;\n  padding: 12px;\n  border-radius: 6px;\n  border: 0.5px solid #d2d2d2;\n  background-color: white;\n}\n\n.item-inner {\n  height: 33px;\n}\n\n@-webkit-keyframes fadeIn {\n  0% {\n    transform: translateY(15px);\n    opacity: 0;\n  }\n  100% {\n    transform: translateY(0px);\n    opacity: 1;\n  }\n}\n\n@keyframes fadeIn {\n  0% {\n    transform: translateY(15px);\n    opacity: 0;\n  }\n  100% {\n    transform: translateY(0px);\n    opacity: 1;\n  }\n}\n\n.input-position {\n  background: linear-gradient(-135deg, var(--ion-color-dark), var(--ion-color-primary));\n}\n\n.custom-footer-row {\n  height: 65px;\n}\n\n.chat-row {\n  display: flex;\n  flex-direction: row;\n}\n\n.reverse {\n  flex-direction: row-reverse;\n}\n\n.name-row-left {\n  background: black;\n  padding: 5px 10px 5px 9vw;\n  margin-left: -8vw;\n  z-index: 1;\n  color: white;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  border-top-right-radius: 16px;\n  font-size: 12px;\n  position: relative;\n  height: 6vw;\n}\n\n.name-row-right {\n  background: white;\n  padding: 5px 9vw 5px 10px;\n  margin-right: -8vw;\n  z-index: 1;\n  color: black;\n  text-align: right;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  border-top-left-radius: 16px;\n  font-size: 12px;\n  position: relative;\n  height: 6vw;\n}\n\n.name-row-parent-left {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-start;\n}\n\n.name-row-parent-right {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-end;\n}\n\n.message-row-left {\n  background: var(--ion-color-secondary);\n  padding: 7px 15px 8px 11vw;\n  margin-left: -8vw;\n  min-height: 10vw;\n  display: flex;\n  align-items: center;\n  border-top-right-radius: 5vw;\n  border-bottom-right-radius: 5vw;\n  color: black;\n  position: relative;\n}\n\n.message-row-right {\n  background: var(--ion-color-primary);\n  padding: 7px 15vw 8px 11px;\n  margin-right: -8vw;\n  min-height: 10vw;\n  display: flex;\n  align-items: center;\n  border-top-left-radius: 5vw;\n  border-bottom-left-radius: 5vw;\n  color: white;\n  position: relative;\n}\n\n.avatar-parent {\n  z-index: 2;\n  border-radius: 8vw;\n  min-width: 16vw;\n  height: 16vw;\n  position: relative;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.avatar-border {\n  position: absolute;\n  width: 16vw;\n  height: 16vw;\n  top: 0;\n  left: 0;\n  border: 1vw solid white;\n  border-radius: 50%;\n  box-shadow: none;\n  outline: none;\n}\n\n.reverse-arc-left {\n  width: 6vw;\n  height: 4vw;\n  position: absolute;\n  background: white;\n  left: 5.8vw;\n  bottom: -4vw;\n  display: flex;\n  justify-content: flex-end;\n  border-bottom-right-radius: 4vw;\n}\n\n.reverse-arc-color-left {\n  width: 4vw;\n  height: 4vw;\n  background: var(--ion-color-secondary);\n  border-top-left-radius: 4vw;\n  border-bottom-right-radius: 2vw;\n}\n\n.reverse-arc-right {\n  width: 6vw;\n  height: 4vw;\n  position: absolute;\n  background: white;\n  right: 5.8vw;\n  bottom: -4vw;\n  display: flex;\n  justify-content: flex-start;\n  border-bottom-left-radius: 4vw;\n}\n\n#chat-parent {\n  scroll-behavior: smooth;\n}\n\n.reverse-arc-color-right {\n  width: 4vw;\n  height: 4vw;\n  background: var(--ion-color-primary);\n  border-top-right-radius: 4vw;\n  border-bottom-left-radius: 3vw;\n}\n\n.timer-left {\n  position: absolute;\n  right: -50px;\n  color: rgba(255, 255, 255, 0.7);\n  font-size: 12px;\n  top: 4px;\n}\n\n.timer-right {\n  position: absolute;\n  left: -50px;\n  color: rgba(255, 255, 255, 0.7);\n  font-size: 12px;\n  top: 4px;\n}\n\n.title-block {\n  display: flex;\n  justify-content: center;\n  align-content: center;\n}\n\n.title-icon {\n  font-size: 32px;\n}\n\n.title-text {\n  padding-left: 5px;\n  align-self: center;\n}\n\n.first-tick {\n  font-size: 25px;\n  position: absolute;\n  left: -24px;\n  top: -4px;\n}\n\n.second-tick {\n  font-size: 25px;\n  position: absolute;\n  left: -28px;\n  top: -4px;\n}\n\n.custom-footer {\n  height: 65px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnJva2VyLWNoYXQvQzpcXHhhbXBwXFxodGRvY3NcXHNvbHV0aXZvXFxhbWF1dGFtb3ZpbC9zcmNcXGFwcFxccGFnZXNcXGJyb2tlci1jaGF0XFxicm9rZXItY2hhdC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Jyb2tlci1jaGF0L2Jyb2tlci1jaGF0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNFLG1DQUFBO0FDQU47O0FESUE7RUFDSSxXQUFBO0FDREo7O0FESUE7RUFDSSx1QkFBQTtFQUNBLHdCQUFBO0VBQ0EsOEJBQUE7RUFDQSxVQUFBO0FDREo7O0FESUE7RUFDSSxjQUFBO0FDREo7O0FESUE7RUFDSSxzQkFBQTtFQUNBLFlBQUE7QUNESjs7QURJQTtFQUNJLDhCQUFBO1VBQUEsc0JBQUE7RUFDQSw4QkFBQTtVQUFBLHNCQUFBO0VBQ0EsZ0JBQUE7QUNESjs7QURJQTtFQUNJLFlBQUE7QUNESjs7QURJQTtFQUNJLGVBQUE7QUNESjs7QURJQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0VBQ0EsdUJBQUE7QUNESjs7QURJQTtFQUNJLFlBQUE7QUNESjs7QURJQTtFQUNJO0lBQ0ksMkJBQUE7SUFDQSxVQUFBO0VDRE47RURHRTtJQUNJLDBCQUFBO0lBQ0EsVUFBQTtFQ0ROO0FBQ0Y7O0FEUEE7RUFDSTtJQUNJLDJCQUFBO0lBQ0EsVUFBQTtFQ0ROO0VER0U7SUFDSSwwQkFBQTtJQUNBLFVBQUE7RUNETjtBQUNGOztBRElBO0VBQ0kscUZBQUE7QUNGSjs7QURLQTtFQUNJLFlBQUE7QUNGSjs7QURLQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQ0ZKOztBREtBO0VBQ0ksMkJBQUE7QUNGSjs7QURLQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDRko7O0FES0E7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDRko7O0FES0E7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtBQ0ZKOztBREtBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7QUNGSjs7QURLQTtFQUNJLHNDQUFBO0VBQ0EsMEJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSwrQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0ZKOztBREtBO0VBQ0ksb0NBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDRko7O0FES0E7RUFFSSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNISjs7QURLQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QUNGSjs7QURJQTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsK0JBQUE7QUNESjs7QURHQTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0Esc0NBQUE7RUFDQSwyQkFBQTtFQUNBLCtCQUFBO0FDQUo7O0FERUE7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSwyQkFBQTtFQUNBLDhCQUFBO0FDQ0o7O0FEQ0E7RUFDSSx1QkFBQTtBQ0VKOztBREFBO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSxvQ0FBQTtFQUNBLDRCQUFBO0VBQ0EsOEJBQUE7QUNHSjs7QUREQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0VBQ0EsZUFBQTtFQUNBLFFBQUE7QUNJSjs7QURGQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLCtCQUFBO0VBQ0EsZUFBQTtFQUNBLFFBQUE7QUNLSjs7QURIQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0FDTUo7O0FESkE7RUFDSSxlQUFBO0FDT0o7O0FETEE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDUUo7O0FETkE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtBQ1NKOztBRFBBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUNVSjs7QURSQTtFQUNJLFlBQUE7QUNXSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Jyb2tlci1jaGF0L2Jyb2tlci1jaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5sZWZ0IHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4uYXZhdGFyIHtcclxuICAgIHdpZHRoOiBjYWxjKDE2dncgLSA0cHgpO1xyXG4gICAgaGVpZ2h0OiBjYWxjKDE2dncgLSA0cHgpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogY2FsYyg4dncgLSAycHgpO1xyXG4gICAgei1pbmRleDogMjtcclxufVxyXG5cclxuLnJlc3RyaWN0IHtcclxuICAgIC0taGVpZ2h0OiAzM3B4O1xyXG59XHJcblxyXG4uaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojZmZmO1xyXG4gICAgaGVpZ2h0OiA1NnB4O1xyXG59XHJcblxyXG4uYW5pbWF0ZSB7XHJcbiAgICBhbmltYXRpb24tbmFtZTogZmFkZUluO1xyXG4gICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAxcztcclxuICAgIHRyYW5zaXRpb246IC4xcztcclxufVxyXG5cclxuLnJpZ2h0e1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4udGV4dCB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbn1cclxuXHJcbi5jYXJkIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDlweDtcclxuICAgIG1heC13aWR0aDogODAlO1xyXG4gICAgcGFkZGluZzogMTJweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICAgIGJvcmRlcjogLjVweCBzb2xpZCByZ2IoMjEwLCAyMTAsIDIxMCk7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLml0ZW0taW5uZXIge1xyXG4gICAgaGVpZ2h0OiAzM3B4O1xyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGZhZGVJbiB7XHJcbiAgICAwJXtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMTVweCk7XHJcbiAgICAgICAgb3BhY2l0eTogMDtcclxuICAgIH1cclxuICAgIDEwMCV7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDBweCk7XHJcbiAgICAgICAgb3BhY2l0eTogMTtcclxuICAgIH1cclxufVxyXG5cclxuLmlucHV0LXBvc2l0aW9ue1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC0xMzVkZWcsIHZhcigtLWlvbi1jb2xvci1kYXJrKSwgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpKVxyXG59XHJcblxyXG4uY3VzdG9tLWZvb3Rlci1yb3d7XHJcbiAgICBoZWlnaHQ6IDY1cHg7XHJcbn1cclxuXHJcbi5jaGF0LXJvd3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcblxyXG4ucmV2ZXJzZXtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcclxufVxyXG5cclxuLm5hbWUtcm93LWxlZnR7XHJcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcclxuICAgIHBhZGRpbmc6IDVweCAxMHB4IDVweCA5dnc7XHJcbiAgICBtYXJnaW4tbGVmdDogLTh2dztcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTZweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGhlaWdodDogNnZ3O1xyXG59XHJcblxyXG4ubmFtZS1yb3ctcmlnaHR7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDVweCA5dncgNXB4IDEwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IC04dnc7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxNnB4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiA2dnc7XHJcbn1cclxuXHJcbi5uYW1lLXJvdy1wYXJlbnQtbGVmdHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbn1cclxuXHJcbi5uYW1lLXJvdy1wYXJlbnQtcmlnaHR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxufVxyXG5cclxuLm1lc3NhZ2Utcm93LWxlZnR7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgIHBhZGRpbmc6IDdweCAxNXB4IDhweCAxMXZ3O1xyXG4gICAgbWFyZ2luLWxlZnQ6IC04dnc7XHJcbiAgICBtaW4taGVpZ2h0OiAxMHZ3O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNXZ3O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDV2dztcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLm1lc3NhZ2Utcm93LXJpZ2h0e1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgcGFkZGluZzogN3B4IDE1dncgOHB4IDExcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IC04dnc7XHJcbiAgICBtaW4taGVpZ2h0OiAxMHZ3O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1dnc7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA1dnc7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5hdmF0YXItcGFyZW50e1xyXG4gICAgLy8gYm9yZGVyOiAxdncgc29saWQgd2hpdGU7XHJcbiAgICB6LWluZGV4OiAyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHZ3O1xyXG4gICAgbWluLXdpZHRoOiAxNnZ3O1xyXG4gICAgaGVpZ2h0OiAxNnZ3O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uYXZhdGFyLWJvcmRlcntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiAxNnZ3O1xyXG4gICAgaGVpZ2h0OiAxNnZ3O1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGJvcmRlcjogMXZ3IHNvbGlkIHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuLnJldmVyc2UtYXJjLWxlZnR7XHJcbiAgICB3aWR0aDogNnZ3O1xyXG4gICAgaGVpZ2h0OiA0dnc7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGxlZnQ6IDUuOHZ3O1xyXG4gICAgYm90dG9tOiAtNHZ3O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNHZ3O1xyXG59XHJcbi5yZXZlcnNlLWFyYy1jb2xvci1sZWZ0e1xyXG4gICAgd2lkdGg6IDR2dztcclxuICAgIGhlaWdodDogNHZ3O1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA0dnc7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMnZ3O1xyXG59XHJcbi5yZXZlcnNlLWFyYy1yaWdodHtcclxuICAgIHdpZHRoOiA2dnc7XHJcbiAgICBoZWlnaHQ6IDR2dztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgcmlnaHQ6IDUuOHZ3O1xyXG4gICAgYm90dG9tOiAtNHZ3O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDR2dztcclxufVxyXG4jY2hhdC1wYXJlbnR7XHJcbiAgICBzY3JvbGwtYmVoYXZpb3I6IHNtb290aDtcclxufVxyXG4ucmV2ZXJzZS1hcmMtY29sb3ItcmlnaHR7XHJcbiAgICB3aWR0aDogNHZ3O1xyXG4gICAgaGVpZ2h0OiA0dnc7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNHZ3O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogM3Z3O1xyXG59XHJcbi50aW1lci1sZWZ0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IC01MHB4O1xyXG4gICAgY29sb3I6IHJnYmEoMjU1LDI1NSwyNTUsMC43KTs7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB0b3A6IDRweDtcclxufVxyXG4udGltZXItcmlnaHR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAtNTBweDtcclxuICAgIGNvbG9yOiByZ2JhKDI1NSwyNTUsMjU1LDAuNyk7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB0b3A6IDRweDtcclxufVxyXG4udGl0bGUtYmxvY2t7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLnRpdGxlLWljb257XHJcbiAgICBmb250LXNpemU6IDMycHg7XHJcbn1cclxuLnRpdGxlLXRleHR7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcclxufVxyXG4uZmlyc3QtdGlja3tcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IC0yNHB4O1xyXG4gICAgdG9wOiAtNHB4O1xyXG59XHJcbi5zZWNvbmQtdGlja3tcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IC0yOHB4O1xyXG4gICAgdG9wOiAtNHB4O1xyXG59XHJcbi5jdXN0b20tZm9vdGVye1xyXG4gICAgaGVpZ2h0OiA2NXB4O1xyXG59IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1kYXJrKTtcbn1cblxuLmxlZnQge1xuICBmbG9hdDogbGVmdDtcbn1cblxuLmF2YXRhciB7XG4gIHdpZHRoOiBjYWxjKDE2dncgLSA0cHgpO1xuICBoZWlnaHQ6IGNhbGMoMTZ2dyAtIDRweCk7XG4gIGJvcmRlci1yYWRpdXM6IGNhbGMoOHZ3IC0gMnB4KTtcbiAgei1pbmRleDogMjtcbn1cblxuLnJlc3RyaWN0IHtcbiAgLS1oZWlnaHQ6IDMzcHg7XG59XG5cbi5pbnB1dCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIGhlaWdodDogNTZweDtcbn1cblxuLmFuaW1hdGUge1xuICBhbmltYXRpb24tbmFtZTogZmFkZUluO1xuICBhbmltYXRpb24tZHVyYXRpb246IDFzO1xuICB0cmFuc2l0aW9uOiAwLjFzO1xufVxuXG4ucmlnaHQge1xuICBmbG9hdDogcmlnaHQ7XG59XG5cbi50ZXh0IHtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4uY2FyZCB7XG4gIG1hcmdpbi1ib3R0b206IDlweDtcbiAgbWF4LXdpZHRoOiA4MCU7XG4gIHBhZGRpbmc6IDEycHg7XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgYm9yZGVyOiAwLjVweCBzb2xpZCAjZDJkMmQyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLml0ZW0taW5uZXIge1xuICBoZWlnaHQ6IDMzcHg7XG59XG5cbkBrZXlmcmFtZXMgZmFkZUluIHtcbiAgMCUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgxNXB4KTtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIDEwMCUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgwcHgpO1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn1cbi5pbnB1dC1wb3NpdGlvbiB7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItZGFyayksIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSk7XG59XG5cbi5jdXN0b20tZm9vdGVyLXJvdyB7XG4gIGhlaWdodDogNjVweDtcbn1cblxuLmNoYXQtcm93IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbn1cblxuLnJldmVyc2Uge1xuICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XG59XG5cbi5uYW1lLXJvdy1sZWZ0IHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIHBhZGRpbmc6IDVweCAxMHB4IDVweCA5dnc7XG4gIG1hcmdpbi1sZWZ0OiAtOHZ3O1xuICB6LWluZGV4OiAxO1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiBmaXQtY29udGVudDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE2cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDZ2dztcbn1cblxuLm5hbWUtcm93LXJpZ2h0IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDVweCA5dncgNXB4IDEwcHg7XG4gIG1hcmdpbi1yaWdodDogLTh2dztcbiAgei1pbmRleDogMTtcbiAgY29sb3I6IGJsYWNrO1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxNnB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA2dnc7XG59XG5cbi5uYW1lLXJvdy1wYXJlbnQtbGVmdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xufVxuXG4ubmFtZS1yb3ctcGFyZW50LXJpZ2h0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xufVxuXG4ubWVzc2FnZS1yb3ctbGVmdCB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICBwYWRkaW5nOiA3cHggMTVweCA4cHggMTF2dztcbiAgbWFyZ2luLWxlZnQ6IC04dnc7XG4gIG1pbi1oZWlnaHQ6IDEwdnc7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA1dnc7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA1dnc7XG4gIGNvbG9yOiBibGFjaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubWVzc2FnZS1yb3ctcmlnaHQge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIHBhZGRpbmc6IDdweCAxNXZ3IDhweCAxMXB4O1xuICBtYXJnaW4tcmlnaHQ6IC04dnc7XG4gIG1pbi1oZWlnaHQ6IDEwdnc7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDV2dztcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNXZ3O1xuICBjb2xvcjogd2hpdGU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmF2YXRhci1wYXJlbnQge1xuICB6LWluZGV4OiAyO1xuICBib3JkZXItcmFkaXVzOiA4dnc7XG4gIG1pbi13aWR0aDogMTZ2dztcbiAgaGVpZ2h0OiAxNnZ3O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uYXZhdGFyLWJvcmRlciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDE2dnc7XG4gIGhlaWdodDogMTZ2dztcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBib3JkZXI6IDF2dyBzb2xpZCB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3gtc2hhZG93OiBub25lO1xuICBvdXRsaW5lOiBub25lO1xufVxuXG4ucmV2ZXJzZS1hcmMtbGVmdCB7XG4gIHdpZHRoOiA2dnc7XG4gIGhlaWdodDogNHZ3O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBsZWZ0OiA1Ljh2dztcbiAgYm90dG9tOiAtNHZ3O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNHZ3O1xufVxuXG4ucmV2ZXJzZS1hcmMtY29sb3ItbGVmdCB7XG4gIHdpZHRoOiA0dnc7XG4gIGhlaWdodDogNHZ3O1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNHZ3O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMnZ3O1xufVxuXG4ucmV2ZXJzZS1hcmMtcmlnaHQge1xuICB3aWR0aDogNnZ3O1xuICBoZWlnaHQ6IDR2dztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcmlnaHQ6IDUuOHZ3O1xuICBib3R0b206IC00dnc7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNHZ3O1xufVxuXG4jY2hhdC1wYXJlbnQge1xuICBzY3JvbGwtYmVoYXZpb3I6IHNtb290aDtcbn1cblxuLnJldmVyc2UtYXJjLWNvbG9yLXJpZ2h0IHtcbiAgd2lkdGg6IDR2dztcbiAgaGVpZ2h0OiA0dnc7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDR2dztcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogM3Z3O1xufVxuXG4udGltZXItbGVmdCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IC01MHB4O1xuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRvcDogNHB4O1xufVxuXG4udGltZXItcmlnaHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IC01MHB4O1xuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRvcDogNHB4O1xufVxuXG4udGl0bGUtYmxvY2sge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xufVxuXG4udGl0bGUtaWNvbiB7XG4gIGZvbnQtc2l6ZTogMzJweDtcbn1cblxuLnRpdGxlLXRleHQge1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xufVxuXG4uZmlyc3QtdGljayB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAtMjRweDtcbiAgdG9wOiAtNHB4O1xufVxuXG4uc2Vjb25kLXRpY2sge1xuICBmb250LXNpemU6IDI1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogLTI4cHg7XG4gIHRvcDogLTRweDtcbn1cblxuLmN1c3RvbS1mb290ZXIge1xuICBoZWlnaHQ6IDY1cHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/broker-chat/broker-chat.page.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/broker-chat/broker-chat.page.ts ***!
    \*******************************************************/

  /*! exports provided: BrokerChatPage */

  /***/
  function srcAppPagesBrokerChatBrokerChatPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BrokerChatPage", function () {
      return BrokerChatPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _providers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../providers */
    "./src/app/providers/index.ts");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var BrokerChatPage = /*#__PURE__*/function () {
      function BrokerChatPage(brokerService, route, router, service, autservice, _authService, _httpClient) {
        var _this = this;

        _classCallCheck(this, BrokerChatPage);

        this.brokerService = brokerService;
        this.route = route;
        this.router = router;
        this.service = service;
        this.autservice = autservice;
        this._authService = _authService;
        this._httpClient = _httpClient;
        this.cargando = true;
        this.conversation = [];
        this.input = '';
        this.route.queryParams.subscribe(function (params) {
          _this.router.getCurrentNavigation().extras.state.broker;
        });
        this.brokerID = this.router.getCurrentNavigation().extras.state.broker;
      }

      _createClass(BrokerChatPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "traermensajes",
        value: function traermensajes() {
          var _this2 = this;

          this.mensajesEnviados().subscribe(function (res) {
            _this2.scrollToBottom();

            _this2.traermensajes();
          });
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          var _this3 = this;

          this.idUsuario = this.autservice.getIdUsuario();
          this.propierties = this.service.properties;
          var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          };

          this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'usuario-rol', header).subscribe(function (res) {
            _this3.usuariosRol = res;

            var _iterator = _createForOfIteratorHelper(_this3.usuariosRol),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var a = _step.value;
                if (a.usuario.id_usuario == _this3.brokerID) _this3.usuarioreceptorid = a.id_usuario_rol;
                if (a.usuario.id_usuario == _this3.idUsuario) _this3.usuarioemisorid = a.id_usuario_rol;
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            _this3._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'usuario/idrol/' + _this3.usuarioreceptorid).subscribe(function (resusuario) {
              if (resusuario[0].foto_usuario) _this3.fotoreceptor = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + "public/usuario/" + resusuario[0].foto_usuario;else _this3.fotoreceptor = "https://definicion.de/wp-content/uploads/2019/06/perfildeusuario.jpg";
              _this3.broker = {
                name: resusuario[0].nombres_usuario + " " + resusuario[0].apellidos_usuario
              };
            }, function (error) {});

            _this3._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'usuario/idrol/' + _this3.usuarioemisorid).subscribe(function (resusuario) {
              if (resusuario[0].foto_usuario) {
                _this3.fotoemisor = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + "public/usuario/" + resusuario[0].foto_usuario;
              } else {
                _this3.fotoemisor = "https://definicion.de/wp-content/uploads/2019/06/perfildeusuario.jpg";
              }
            }, function (error) {});

            _this3.traermensajes();
          }, function (error) {});

          this.idUsuario = this.autservice.getIdUsuario();
          this.propierties = this.service.properties;

          var _iterator2 = _createForOfIteratorHelper(this.propierties),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var a = _step2.value;

              if (a.id == this.brokerID) {
                this.broker = a;
                this.conversation = [{
                  text: 'Hola como vas?',
                  sender: 0,
                  image: this.broker.picture,
                  fecha: new Date(),
                  id: 0
                }];
                this.conversation.shift();
              }
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }

          setTimeout(function () {
            _this3.scrollToBottom();
          }, 10);
        }
      }, {
        key: "send",
        value: function send() {
          var _this4 = this;

          var fecha = new Date();
          var body;
          var mensaje;

          if (this.input !== '') {
            body = {
              mensaje: this.input,
              hora: fecha.getHours() + ":" + fecha.getMinutes(),
              fecha_mensaje: fecha
            };
            var header = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
              })
            };

            this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'mensaje', JSON.stringify(body), header).subscribe(function (mensaj) {
              mensaje = mensaj;
              body = {
                "id_emisor": _this4.usuarioemisorid,
                "id_receptor": _this4.usuarioreceptorid,
                "id_mensaje": mensaje.identifiers[0].id_mensaje
              };

              _this4._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'emisor_receptor', JSON.stringify(body), header).subscribe(function (emisor_receptor) {
                _this4.conversation.push({
                  text: _this4.input,
                  sender: 1,
                  image: _this4.fotoemisor,
                  fecha: fecha,
                  id: 1
                });

                _this4.input = '';
              }, function (error) {});
            }, function (error) {});

            setTimeout(function () {
              _this4.scrollToBottom();
            }, 100);
          }
        }
      }, {
        key: "scrollToBottom",
        value: function scrollToBottom() {
          var content = document.getElementById("chat-container");
          var parent = document.getElementById("chat-parent");
          var scrollOptions = {
            left: 0,
            top: 1000
          };
          content.scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest"
          });
        }
      }, {
        key: "mensajesEnviados",
        value: function mensajesEnviados() {
          var _this5 = this;

          return new rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"](function (observer) {
            var header = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + _this5._authService.getToken()
              })
            };

            _this5._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'emisor_receptor/emisor/' + _this5.usuarioemisorid + "/" + _this5.usuarioreceptorid, header).subscribe(function (resmen) {
              _this5.mensajes = resmen;
              _this5.conversation = [];

              var _iterator3 = _createForOfIteratorHelper(_this5.mensajes),
                  _step3;

              try {
                for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                  var b = _step3.value;
                  var hora = void 0;
                  hora = new Date(b.fecha_mensaje).toLocaleString();
                  hora = hora.split(" ");
                  hora = hora[1].split(":");
                  hora = hora[0] + ":" + hora[1];
                  if (b.id_emisor == _this5.usuarioemisorid) _this5.conversation.push({
                    text: b.mensaje,
                    sender: 1,
                    image: _this5.fotoemisor,
                    fecha: b.fecha_mensaje,
                    id: b.id_mensaje,
                    hora: hora
                  });else _this5.conversation.push({
                    text: b.mensaje,
                    sender: 0,
                    image: _this5.fotoreceptor,
                    fecha: b.fecha_mensaje,
                    id: b.id_mensaje,
                    hora: hora
                  });
                }
              } catch (err) {
                _iterator3.e(err);
              } finally {
                _iterator3.f();
              }

              setTimeout(function () {
                observer.next(_this5.mensajes);
              }, 1000);
            }, function (error) {});
          });
        }
      }]);

      return BrokerChatPage;
    }();

    BrokerChatPage.ctorParameters = function () {
      return [{
        type: _providers__WEBPACK_IMPORTED_MODULE_6__["BrokerService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers__WEBPACK_IMPORTED_MODULE_6__["PropertyService"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("scrollElement", {
      "static": true
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["IonContent"])], BrokerChatPage.prototype, "content", void 0);
    BrokerChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-broker-chat',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./broker-chat.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-chat/broker-chat.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./broker-chat.page.scss */
      "./src/app/pages/broker-chat/broker-chat.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers__WEBPACK_IMPORTED_MODULE_6__["BrokerService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers__WEBPACK_IMPORTED_MODULE_6__["PropertyService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])], BrokerChatPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-broker-chat-broker-chat-module-es5.js.map