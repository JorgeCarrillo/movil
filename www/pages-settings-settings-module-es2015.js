(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-settings-settings-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/settings/settings.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/settings/settings.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>{{ 'app.pages.settings.title.header' | translate }}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-item class=\"bg-profile\">\r\n    <ion-avatar slot=\"start\">\r\n      <!-- <img src=\"assets/img/avatar.jpeg\" class=\"user-avatar\">-->\r\n      <img style=\"border-radius: 50%;\" [src]=\"picture\" onerror=\"this.src='./assets/img/avatar.png';\">\r\n    </ion-avatar>\r\n    <ion-label>\r\n      <ion-text color=\"light\">\r\n        <h2>\r\n          <strong>{{this._authService.getNombres()}}</strong><strong> {{this._authService.getApellidos()}}</strong>\r\n        </h2>\r\n      </ion-text>\r\n      <ion-text color=\"tertiary\">\r\n        <h3>\r\n          {{email}}\r\n        </h3>\r\n      </ion-text>\r\n      <ion-button icon-left size=\"small\" shape=\"round\" color=\"dark\" (click)=\"editProfile()\">\r\n        <ion-icon name=\"contact\"></ion-icon>\r\n        {{ 'app.pages.menu.editprofileseting' | translate }}\r\n      </ion-button>\r\n      <ion-button icon-left size=\"small\" shape=\"round\" color=\"secondary\" (click)=\"logout()\">\r\n        <ion-icon name=\"log-out\"></ion-icon>\r\n        {{ 'app.pages.menu.logoutseting' | translate }}\r\n      </ion-button>\r\n    </ion-label>\r\n  </ion-item>\r\n\r\n  <ion-list>\r\n    <ion-list-header>\r\n      <ion-text color=\"dark\">\r\n        <h5>Pago</h5>\r\n      </ion-text>\r\n    </ion-list-header>\r\n    <ion-item>\r\n      <ion-icon name=\"card\" slot=\"start\" color=\"primary\"></ion-icon>\r\n      <ion-label color=\"primary\">Método de Pago</ion-label>\r\n      <ion-select [(ngModel)]=\"paymentMethod\">\r\n        <ion-select-option *ngFor=\"let method of paymentMethods\" [value]=\"method\">{{method}}</ion-select-option>\r\n      </ion-select>\r\n    </ion-item>\r\n   <!-- <ion-item>\r\n      <ion-icon name=\"logo-usd\" slot=\"start\" color=\"dark\"></ion-icon>\r\n      <ion-label color=\"primary\">Moneda</ion-label>\r\n      <ion-select [(ngModel)]=\"currency\">\r\n        <ion-select-option *ngFor=\"let currency of currencies\" [value]=\"currency\">{{currency}}</ion-select-option>\r\n      </ion-select>\r\n    </ion-item>-->\r\n  </ion-list>\r\n  <!--<ion-list>\r\n    <ion-list-header>\r\n      <ion-text color=\"dark\">\r\n        <h5>Otras</h5>\r\n      </ion-text>\r\n    </ion-list-header>\r\n    <ion-item>\r\n      <ion-icon name=\"mail\" slot=\"start\" color=\"primary\"></ion-icon>\r\n      <ion-label class=\"label\" color=\"primary\">Habilitar Promociones</ion-label>\r\n      <ion-toggle [(ngModel)]=\"enablePromo\"></ion-toggle>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-icon name=\"mail\" slot=\"start\" color=\"primary\"></ion-icon>\r\n      <ion-label class=\"label\" color=\"primary\">Enable History</ion-label>\r\n      <ion-toggle [(ngModel)]=\"enableHistory\"></ion-toggle>\r\n    </ion-item>\r\n  </ion-list>-->\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/settings/settings.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/settings/settings.module.ts ***!
  \***************************************************/
/*! exports provided: SettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm2015/agm-core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./settings.page */ "./src/app/pages/settings/settings.page.ts");








// Pipes


const routes = [
    {
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_9__["SettingsPage"]
    }
];
let SettingsPageModule = class SettingsPageModule {
};
SettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forChild(),
            _agm_core__WEBPACK_IMPORTED_MODULE_5__["AgmCoreModule"].forRoot({
                apiKey: 'AIzaSyD9BxeSvt3u--Oj-_GD-qG2nPr1uODrR0Y'
            })
        ],
        declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_9__["SettingsPage"]]
    })
], SettingsPageModule);



/***/ }),

/***/ "./src/app/pages/settings/settings.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/settings/settings.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light)) ;\n}\n\nion-avatar {\n  width: 68px;\n  height: 68px;\n}\n\n.bg-profile {\n  background-image: linear-gradient(180deg, #005b85 0%, #005b85 100%);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2V0dGluZ3MvQzpcXHhhbXBwXFxodGRvY3NcXHNvbHV0aXZvXFxhbWF1dGFtb3ZpbC9zcmNcXGFwcFxccGFnZXNcXHNldHRpbmdzXFxzZXR0aW5ncy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3NldHRpbmdzL3NldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLHdGQUFBO0FDQUo7O0FESUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ0RKOztBRElBO0VBQ0UsbUVBQUE7QUNERiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NldHRpbmdzL3NldHRpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICBpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKSwgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSlcclxuICB9XHJcbn1cclxuXHJcbmlvbi1hdmF0YXIge1xyXG4gICAgd2lkdGg6IDY4cHg7XHJcbiAgICBoZWlnaHQ6IDY4cHg7XHJcbn1cclxuXHJcbi5iZy1wcm9maWxlIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCAjMDA1Yjg1IDAlLCAjMDA1Yjg1IDEwMCUpO1xyXG59IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKSwgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSkgO1xufVxuXG5pb24tYXZhdGFyIHtcbiAgd2lkdGg6IDY4cHg7XG4gIGhlaWdodDogNjhweDtcbn1cblxuLmJnLXByb2ZpbGUge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCAjMDA1Yjg1IDAlLCAjMDA1Yjg1IDEwMCUpO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/settings/settings.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/settings/settings.page.ts ***!
  \*************************************************/
/*! exports provided: SettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPage", function() { return SettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _providers_property_property_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/property/property.service */ "./src/app/providers/property/property.service.ts");





let SettingsPage = class SettingsPage {
    constructor(navCtrl, _authService, service) {
        this.navCtrl = navCtrl;
        this._authService = _authService;
        this.service = service;
        this.languages = ['English', 'Portuguese', 'French'];
        this.paymentMethods = ['Paypal', 'Tarjeta de Credito'];
        this.currencies = ['USD', 'BRL', 'EUR'];
        this.fecha = new Date();
    }
    ngOnInit() {
        this.obtenerdatos();
    }
    obtenerdatos() {
        /*obtener nombres de usuario*/
        if (this._authService.getNombres() === null && this._authService.getApellidos() === null) {
            this.nombreusuario = 'BIENVENIDO',
                this.apellidousuario = 'USUARIO';
        }
        else {
            this.nombreusuario = this._authService.getNombres();
            this.apellidousuario = this._authService.getApellidos();
        }
        /*fin nombres de usuario*/
        /*obtener Rol de usuario*/
        if (this._authService.getRol() === null) {
            this.tipousuario = 'CLIENTE';
        }
        else {
            this.tipousuario = this._authService.getRol();
        }
        /*fin rol de usuario*/
        /*obtener correo de usuario*/
        if (this._authService.getNombreLogin() === null) {
            this.email = 'null';
        }
        else {
            this.email = this._authService.getNombreLogin();
        }
        this.service.traerimg().subscribe(res => {
            this.picture = res;
        });
    }
    editProfile() {
        this.navCtrl.navigateForward('edit-profile');
    }
    /*async logout(): Promise<void> {
      try {
        localStorage.getItem('currentUser');
        localStorage.removeItem('currentUser');
        localStorage.clear();
        //window.location.replace('/#/pages/auth/login');
        window.location.replace('/login');
      } catch (error) {
        console.log(error);
      }
    }*/
    logout() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            try {
                this._authService.enLinea(new Date(this.fecha.setMinutes(this.fecha.getMinutes() - 5)).toISOString(), false).subscribe(res => {
                });
                this.navCtrl.navigateForward('/login');
                localStorage.getItem('currentUser');
                localStorage.removeItem('currentUser');
                localStorage.clear();
                //window.location.replace('/login');
            }
            catch (error) {
                console.log(error);
            }
        });
    }
};
SettingsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] },
    { type: _providers_property_property_service__WEBPACK_IMPORTED_MODULE_4__["PropertyService"] }
];
SettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-settings',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./settings.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/settings/settings.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./settings.page.scss */ "./src/app/pages/settings/settings.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
        _providers_property_property_service__WEBPACK_IMPORTED_MODULE_4__["PropertyService"]])
], SettingsPage);



/***/ })

}]);
//# sourceMappingURL=pages-settings-settings-module-es2015.js.map