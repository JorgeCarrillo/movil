(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-availability-page-availability-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/availability-page/availability-page.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/availability-page/availability-page.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Disponibilidad</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div>\n    <ion-grid><br>\n      <div style=\"color: #000000;text-align: center;\">\n        <ion-note class=\"fw700\" style=\"color: #000000;\">Para registrar su disponibilidad lo puede hacer ingresando fecha y hora de inicio, fecha y hora final.</ion-note>\n      </div><br><br>\n    </ion-grid>\n  </div>\n  <ion-item >\n    <ion-label >Fecha inicio</ion-label>\n    <input [(ngModel)]=\"fechainicio\" type=\"date\">\n  </ion-item>\n  <ion-item >\n    <ion-label >Hora Inicio</ion-label>\n    <ion-select value=\"8\" [(ngModel)]=\"horainicio\">\n      <ion-select-option *ngFor=\"let a of horas\" [value]=\"a\" role=\"option\" id=\"ion-selopt-18\" class=\"md hydrated\">{{a}}:00</ion-select-option>\n   </ion-select>\n  </ion-item> \n\n\n  <ion-item >\n    <ion-label >Fecha fin</ion-label>\n    <input [(ngModel)]=\"fechafin\" type=\"date\">\n  </ion-item>\n\n  <ion-item >\n    <ion-label >Hora Fin</ion-label>\n    <ion-select value=\"12\"  [(ngModel)]=\"horafin\">\n      <ion-select-option *ngFor=\"let a of horas\" [value]=\"a\" role=\"option\" id=\"ion-selopt-18\" class=\"md hydrated\">{{a}}:00</ion-select-option>\n   </ion-select>\n  </ion-item>\n  <!--<button (click)=\"generarDisponibilidad()\">sfs</button>-->\n  <div style=\"text-align: center;\">\n    <ion-button icon-left color=\"primary\" (click)=\"generarDisponibilidad()\">\n      <ion-icon name=\"add\"></ion-icon>\n      Agregar Disponibilidad\n    </ion-button>\n  </div>\n  <br>\n  <div>\n    <ion-grid><br>\n      <div style=\"color: #000000;text-align: center;\">\n        <ion-note class=\"fw700\" style=\"color: #000000;\">O puede seleccionar sus horas disponibles para dar clase.</ion-note>\n      </div><br><br>\n      <ion-row>\n        <ion-col style=\"text-align: center;\">\n          <div>\n            <ion-icon style=\"color: #10009c;\" name=\"bookmark\"></ion-icon>\n            <ion-note color=\"primary\">Hora Pendiente </ion-note>\n          </div>\n        </ion-col>\n        <ion-col style=\"text-align: center;\">\n          <div>\n            <ion-icon style=\"color: #eb1414;\" name=\"bookmark\"></ion-icon>\n            <ion-note color=\"danger\">Hora Confirmada</ion-note>\n          </div>\n        </ion-col>\n        <ion-col style=\"text-align: center;\">\n          <div>\n            <ion-icon style=\"color: #009c17;\" name=\"bookmark\"></ion-icon>\n            <ion-note style=\"color: #009c17;\">Hora Disponible</ion-note>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <ng-template #template let-tm=\"tm\" let-hourParts=\"hourParts\" let-eventTemplate=\"eventTemplate\">\n    <div [ngClass]=\"{'calendar-event-wrap': tm.events}\" *ngIf=\"tm.events\">\n      <div *ngFor=\"let displayEvent of tm.events\">\n        <div *ngIf=\"displayEvent.event.reservado==1\">\n          <div [class.active]=\"displayEvent.event.id == activeid && active\" style=\"background-color: rgb(0, 156, 23);\"\n            class=\"calendar-event\" tappable (press)=\"Options(displayEvent.event.id)\"\n            (tap)=\"onEventSelected(displayEvent.event)\"\n            [ngStyle]=\"{top: (37*displayEvent.startOffset/hourParts)+'px',left: 100/displayEvent.overlapNumber*displayEvent.position+'%', width: 100/displayEvent.overlapNumber+'%', height: 37*(displayEvent.endIndex -displayEvent.startIndex - (displayEvent.endOffset + displayEvent.startOffset)/hourParts)+'px'}\">\n          </div>\n        </div>\n        <div *ngIf=\"displayEvent.event.reservado==2\">\n          <div [class.active]=\"displayEvent.event.id == activeid && active\" style=\"background-color: rgb(16, 0, 156);\"\n            class=\"calendar-event\" tappable (press)=\"Options(displayEvent.event.id)\"\n            (tap)=\"onEventSelected(displayEvent.event)\"\n            [ngStyle]=\"{top: (37*displayEvent.startOffset/hourParts)+'px',left: 100/displayEvent.overlapNumber*displayEvent.position+'%', width: 100/displayEvent.overlapNumber+'%', height: 37*(displayEvent.endIndex -displayEvent.startIndex - (displayEvent.endOffset + displayEvent.startOffset)/hourParts)+'px'}\">\n          </div>\n        </div>\n        <div *ngIf=\"displayEvent.event.reservado==3\">\n          <div [class.active]=\"displayEvent.event.id == activeid && active\" style=\"background-color: rgb(235, 20, 20);\"\n            class=\"calendar-event\" tappable (press)=\"Options(displayEvent.event.id)\"\n            (tap)=\"onEventSelected(displayEvent.event)\"\n            [ngStyle]=\"{top: (37*displayEvent.startOffset/hourParts)+'px',left: 100/displayEvent.overlapNumber*displayEvent.position+'%', width: 100/displayEvent.overlapNumber+'%', height: 37*(displayEvent.endIndex -displayEvent.startIndex - (displayEvent.endOffset + displayEvent.startOffset)/hourParts)+'px'}\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </ng-template>\n\n\n  <calendar *ngIf=\"calendario && !cargar\" (click)=\"onTimeSelected($event)\"\n    [weekviewNormalEventSectionTemplate]=\"template\" [dayviewNormalEventSectionTemplate]=\"template\"\n    [eventSource]=\"eventSource\" [calendarMode]=\"calendar.mode\" [currentDate]=\"calendar.currentDate\"\n    (onCurrentDateChanged)=\"onCurrentDateChanged($event)\" (onEventSelected)=\"onEventSelected($event)\"\n    (onTitleChanged)=\"onViewTitleChanged($event)\" (onTimeSelected)=\"onTimeSelected($event)\"\n    [dateFormatter]=\"calendar.dateFormatter\" [startHour]=\"calendar.menor\" [endHour]=\"calendar.mayor\"\n    [lockSwipes]=\"false\" step=\"80\">\n  </calendar>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/availability-page/availability-page-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/availability-page/availability-page-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: AvailabilityPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvailabilityPagePageRoutingModule", function() { return AvailabilityPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _availability_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./availability-page.page */ "./src/app/pages/availability-page/availability-page.page.ts");




const routes = [
    {
        path: '',
        component: _availability_page_page__WEBPACK_IMPORTED_MODULE_3__["AvailabilityPagePage"]
    }
];
let AvailabilityPagePageRoutingModule = class AvailabilityPagePageRoutingModule {
};
AvailabilityPagePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AvailabilityPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/availability-page/availability-page.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/availability-page/availability-page.module.ts ***!
  \*********************************************************************/
/*! exports provided: AvailabilityPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvailabilityPagePageModule", function() { return AvailabilityPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _availability_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./availability-page-routing.module */ "./src/app/pages/availability-page/availability-page-routing.module.ts");
/* harmony import */ var ionic2_calendar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic2-calendar */ "./node_modules/ionic2-calendar/index.js");
/* harmony import */ var _availability_page_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./availability-page.page */ "./src/app/pages/availability-page/availability-page.page.ts");








let AvailabilityPagePageModule = class AvailabilityPagePageModule {
};
AvailabilityPagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            ionic2_calendar__WEBPACK_IMPORTED_MODULE_6__["NgCalendarModule"],
            _availability_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["AvailabilityPagePageRoutingModule"]
        ],
        providers: [
            { provide: _angular_core__WEBPACK_IMPORTED_MODULE_1__["LOCALE_ID"], useValue: 'es-EC' }
        ],
        declarations: [_availability_page_page__WEBPACK_IMPORTED_MODULE_7__["AvailabilityPagePage"]]
    })
], AvailabilityPagePageModule);



/***/ }),

/***/ "./src/app/pages/availability-page/availability-page.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/availability-page/availability-page.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2F2YWlsYWJpbGl0eS1wYWdlL2F2YWlsYWJpbGl0eS1wYWdlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/availability-page/availability-page.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/availability-page/availability-page.page.ts ***!
  \*******************************************************************/
/*! exports provided: AvailabilityPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvailabilityPagePage", function() { return AvailabilityPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_locales_es__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/locales/es */ "./node_modules/@angular/common/locales/es.js");
/* harmony import */ var _angular_common_locales_es__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_es__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");







Object(_angular_common__WEBPACK_IMPORTED_MODULE_4__["registerLocaleData"])(_angular_common_locales_es__WEBPACK_IMPORTED_MODULE_5___default.a);


let AvailabilityPagePage = class AvailabilityPagePage {
    constructor(asCtrl, navCtrl, toastCtrl, modalCtrl, route, alertController, router, propertyService, authservice, loadingCtrl) {
        this.asCtrl = asCtrl;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.route = route;
        this.alertController = alertController;
        this.router = router;
        this.propertyService = propertyService;
        this.authservice = authservice;
        this.loadingCtrl = loadingCtrl;
        this.horas = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
        this.horainicio = null;
        this.horafin = null;
        this.fechainicio = null;
        this.fechafin = null;
        this.calendario = true;
        this.cargar = true;
        this.array = false;
        this.menor = false;
        this.itemHeight = 0;
        this.calendar = {
            mode: 'week',
            menor: "0",
            mayor: "24",
            currentDate: new Date(),
            dateFormatter: {
                formatMonthViewTitle: function (date) {
                    var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
                    ];
                    return monthNames[date.getMonth()] + ' ' + date.getFullYear();
                },
                formatWeekViewTitle: function (date) {
                    var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
                    ];
                    return monthNames[date.getMonth()] + ' ' + date.getFullYear();
                },
                formatDayViewTitle: function (date) {
                    var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
                    ];
                    return monthNames[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
                },
            },
        };
        this.idrol = authservice.getIdUsuarioRol();
        this.presentLoadingDefault();
        //this.generarDisponibilidad()
    }
    presentLoadingDefault() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let loading = yield this.loadingCtrl.create({
                message: "Cargando..."
            });
            loading.present();
            this.propertyService.getDetaHorario(+this.authservice.getIdUsuario()).subscribe(res => {
                this.eventSource = res;
                this.cargar = false;
                loading.dismiss();
                this.calendario = true;
            });
        });
    }
    MOstrarDatos() {
    }
    onTimeSelected(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (ev.selectedTime) {
                if (ev.events.length == 0)
                    this.array = false;
                else
                    this.array = true;
                this.event = ev.events;
                this.obj = new Date(ev.selectedTime);
                if (this.obj < new Date())
                    this.menor = true;
                else
                    this.menor = false;
                this.final = new Date(ev.selectedTime.setHours(ev.selectedTime.getHours() + 1));
                this.newevent = {
                    hora_inicio: this.obj.toISOString(), hora_fin: this.final.toISOString(), estado_reserva: 1, id_usuario_rol: +this.idrol
                };
            }
            else if (ev.isTrusted && this.array && !this.menor) {
                let dia = new Date(this.obj).getDate();
                this.calendario = false;
                let alert = yield this.alertController.create({
                    header: 'Borrar hora',
                    message: '¿Estas seguro de eliminar esta hora?',
                    buttons: [
                        {
                            text: 'No',
                            role: 'No',
                            handler: () => {
                                this.calendario = true;
                            }
                        },
                        {
                            text: 'Si',
                            handler: () => {
                                this.propertyService.eliminarhora(this.event[0].id, +this.idrol);
                                this.presentLoadingDefault();
                            }
                        }
                    ]
                });
                alert.present();
            }
            ;
            if (ev.isTrusted && !this.array && !this.menor) {
                let dia = new Date(this.obj).getDate();
                this.calendario = false;
                let alert = yield this.alertController.create({
                    header: 'Nueva hora',
                    message: '¿Seguro de agregar una nueva hora el dia ' + dia + ' de las ' + new Date(this.obj).getHours() + ' a las ' + (new Date(this.obj).getHours() + 1) + '?',
                    buttons: [
                        {
                            text: 'No',
                            role: 'No',
                            handler: () => {
                                this.calendario = true;
                            }
                        },
                        {
                            text: 'Si',
                            handler: () => {
                                this.propertyService.setreserva(this.newevent, this.authservice.getIdUsuario());
                                this.presentLoadingDefault();
                            }
                        }
                    ]
                });
                alert.present();
            }
            ;
        });
    }
    onEventSelected() {
    }
    ngOnInit() {
    }
    onCurrentDateChanged(event) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    }
    onViewTitleChanged(title) {
    }
    today() {
        this.calendar.currentDate = new Date();
    }
    changeMode(mode) {
        this.calendar.mode = mode;
    }
    generarDisponibilidad() {
        let event = [];
        if (this.fechainicio == null)
            this.crearAlert("Cuiado!!", "Fecha inicial requerida");
        else if (this.horainicio == null)
            this.crearAlert("Cuiado!!", "Hora inicial requerida");
        else if (this.horafin == null)
            this.crearAlert("Cuiado!!", "Hora final requerida");
        else if (this.fechafin == null)
            this.crearAlert("Cuiado!!", "Fecha final requerida");
        else if (this.horafin < this.horainicio)
            this.crearAlert("Cuiado!!", "Para programar nuevas disponibilidades  la hora final debe ser mayor");
        else if (this.horafin == this.horainicio)
            this.crearAlert("Cuiado!!", "Para programar nuevas disponibilidades la hora final debe ser diferente a la inicial");
        else if (new Date(this.fechafin) < new Date(this.fechainicio))
            this.crearAlert("Cuiado!!", "Para programar nuevas disponibilidades la fecha final debe ser mayor a la inicial");
        else {
            let splitfechainicio = this.fechainicio.split("-");
            let splitfechafin = this.fechafin.split("-");
            let fechainicio = new Date(new Date(this.fechainicio).setHours(this.horainicio));
            fechainicio = new Date(fechainicio.setDate(splitfechainicio[2]));
            fechainicio = new Date(fechainicio.setFullYear(splitfechainicio[0]));
            let fechafin = new Date(new Date(this.fechafin).setHours(this.horainicio));
            fechafin = new Date(fechafin.setDate(splitfechafin[2]));
            fechafin = new Date(fechafin.setFullYear(splitfechafin[0]));
            let fechaHoraFin = new Date(new Date(fechainicio).setHours(this.horafin));
            for (fechainicio; fechainicio <= fechafin; fechainicio = new Date(fechainicio.setDate(fechainicio.getDate() + 1))) {
                for (fechainicio; fechainicio <= fechaHoraFin; fechainicio = new Date(fechainicio.setHours(fechainicio.getHours() + 1))) {
                    let horaini = new Date(fechainicio.setSeconds(fechainicio.getSeconds() - fechainicio.getSeconds()));
                    event.push({
                        hora_inicio: horaini.toISOString(), hora_fin: new Date(horaini.setHours(horaini.getHours() + 1)).toISOString(), estado_reserva: 1, id_usuario_rol: +this.idrol
                    });
                    this.propertyService.setreservas(this.newevent);
                }
                fechaHoraFin = new Date(fechaHoraFin.setDate(fechaHoraFin.getDate() + 1));
                fechainicio = new Date(fechainicio.setHours(this.horainicio));
            }
            this.propertyService.setreservas(event).subscribe(res => {
                this.presentLoadingDefault();
            });
        }
    }
    crearAlert(header, mensaje) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let alert = yield this.alertController.create({
                header: header,
                message: mensaje,
                buttons: [
                    {
                        text: 'OK',
                        role: 'No',
                        handler: () => {
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    ;
};
AvailabilityPagePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_6__["PropertyService"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
AvailabilityPagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-availability-page',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./availability-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/availability-page/availability-page.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./availability-page.page.scss */ "./src/app/pages/availability-page/availability-page.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _providers__WEBPACK_IMPORTED_MODULE_6__["PropertyService"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], AvailabilityPagePage);



/***/ })

}]);
//# sourceMappingURL=pages-availability-page-availability-page-module-es2015.js.map