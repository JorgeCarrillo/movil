(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-results-home-results-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button color=\"tertiary\"></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      <ion-text color=\"light\">\r\n        {{ 'app.name' | translate }} <ion-text color=\"tertiary\" class=\"fw700\"> {{ 'app.version' | translate }}\r\n        </ion-text>\r\n      </ion-text>\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <!--<ion-button size=\"small\" shape=\"round\" color=\"medium\" (click)=\"notifications()\">\r\n        <ion-icon name=\"notifications\"></ion-icon>\r\n        <ion-badge color=\"dark\" slot=\"end\">2</ion-badge>\r\n      </ion-button>-->\r\n      <ion-button *ngIf=\"tipoUsuario!='CLIENTE'\" size=\"small\" shape=\"round\" color=\"medium\" tappable routerLink=\"/settings\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"cog\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-searchbar [(ngModel)]=\"searchKey\" (ionChange)=\"onInput($event)\" (ionCaancel)=\"onCancel($event)\"\r\n      placeholder='Buscar'></ion-searchbar>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button size=\"small\" shape=\"round\" color=\"medium\" (click)=\"searchFilter()\">\r\n        <ion-icon name=\"options\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n    \r\n  <ion-grid class=\"ion-no-padding\" fixed>\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col class=\"ion-no-padding\">\r\n        <ion-button class=\"ion-no-margin\" expand=\"full\" size=\"medium\" color=\"secondary\">\r\n          Profesores\r\n        </ion-button>\r\n     \r\n        \r\n    \r\n      </ion-col>\r\n      <!--<ion-col class=\"ion-no-padding\">\r\n        <ion-button class=\"ion-no-margin\" expand=\"full\" size=\"medium\" color=\"primary\"\r\n          (click)=\"openPropertyListPage('rent')\">\r\n          En Linea\r\n        </ion-button>\r\n      </ion-col>-->\r\n    </ion-row>\r\n\r\n    <div *ngIf=\"!cargar\" class=\"ion-no-margin\" [@staggerIn]=\"properties\">\r\n      <!-- # -->\r\n      <div  *ngFor=\"let property of properties  | paginate: { itemsPerPage: 10, currentPage: p }\">\r\n      <ion-item *ngIf=\"property.ensena != (0) && property.ensena != [] && property.price != null && property.price !='' && property.horas\" lines=\"none\" class=\"bg-white\" tappable routerLink=\"/property-detail/{{property.id}}\"\r\n       >\r\n        <ion-thumbnail slot=\"start\">\r\n          <img style=\"border-radius: 50%;\" [src]=\"property.picture\" onerror=\"this.src='./assets/img/avatar.png';\">\r\n        </ion-thumbnail>\r\n        <ion-label>\r\n          <ion-row>\r\n            <ion-col>\r\n              <h2>\r\n                <ion-text color=\"dark\"><strong>{{ property.title }}</strong></ion-text>\r\n              </h2>\r\n              <p class=\"text-12x\">\r\n                <ion-text color=\"primary\" style=\"color: black\">{{property.city}}</ion-text>\r\n                <ion-text color=\"secondary\">{{ property.price }}</ion-text>\r\n              </p>\r\n\r\n            <p class=\"text-12x\">\r\n              <ion-text color=\"black\" *ngFor=\"let ensena of property.ensena\">{{ ensena.nombre_materia }}. </ion-text>\r\n            </p>\r\n\r\n              <ion-badge slot=\"end\" color=\"primary\" *ngIf=\"property.estado_usuario_cuenta === true\">\r\n                <ion-text color=\"light\">En línea</ion-text>\r\n              </ion-badge>\r\n\r\n              <ion-badge slot=\"end\" color=\"secondary\" *ngIf=\"property.label === 'sale'\">\r\n                <ion-text color=\"light\"></ion-text>\r\n              </ion-badge>\r\n            </ion-col>\r\n            <ion-col style=\"text-align: right\">\r\n              <h2>\r\n                <ion-text color=\"black\"><strong>${{ property.price }}/h</strong>\r\n                </ion-text>\r\n              </h2>\r\n              <p class=\"text-12x\">\r\n                <ion-text color=\"black\"><strong></strong></ion-text>\r\n              </p>\r\n              <p class=\"text-12x\">\r\n                <ion-text color=\"primary\" style=\"color: black\">{{property.ciudad_nombre}}</ion-text>\r\n                \r\n              </p>\r\n\r\n            </ion-col>\r\n\r\n          </ion-row>\r\n          <ion-badge slot=\"end\" color=\"primary\" *ngIf=\"property.estado_usuario_linea === true\">\r\n            <ion-text color=\"light\">En línea</ion-text>\r\n          </ion-badge>\r\n\r\n          <ion-badge slot=\"end\" color=\"secondary\" *ngIf=\"property.label === 'sale'\">\r\n            <ion-text color=\"light\"></ion-text>\r\n          </ion-badge>\r\n\r\n          <ionic4-star-rating #rating activeIcon=\"ios-star\" defaultIcon=\"ios-star-outline\" activeColor=\"#ea8d16\"\r\n            defaultColor=\"#e8dede\" readonly=\"true\" [rating]=\"property.estrellas\" fontSize=\"22px\" (ratingChanged)=\"logRatingChange($event)\">\r\n          </ionic4-star-rating>\r\n        </ion-label>\r\n      </ion-item>\r\n      <!-- # -->\r\n     \r\n    </div>\r\n    \r\n    </div>\r\n\r\n  </ion-grid>\r\n\r\n  <!--<ion-button class=\"ion-margin\" expand=\"full\" color=\"secondary\" (click)=\"openPropertyListPage()\">\r\n    {{ 'app.button.moreresults' | translate }}\r\n  </ion-button>-->\r\n\r\n  \r\n</ion-content>\r\n\r\n<ion-footer class=\"animated fadeIn\">\r\n  <pagination-controls  previousLabel=\"Anterior\"nextLabel=\"Siguiente\" (pageChange)=\"p = $event\" style=\"text-align: center;\"></pagination-controls>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-grid class=\"ion-no-padding\">\r\n      <ion-row>\r\n        <!--<ion-col size=\"4\" class=\"ion-no-padding\">\r\n          <ion-button size=\"small\" expand=\"full\" fill=\"clear\" color=\"medium\" routerLink=\"/nearby\">\r\n            <ion-icon slot=\"start\" name=\"compass\"></ion-icon>\r\n            {{ 'app.button.nearby' | translate }}\r\n          </ion-button>\r\n        </ion-col>\r\n        <ion-col size=\"4\" class=\"ion-no-padding\">\r\n          <ion-button size=\"small\" expand=\"full\" fill=\"clear\" color=\"medium\" routerLink=\"/bycategory\">\r\n            <ion-icon slot=\"start\" name=\"apps\"></ion-icon>\r\n            {{ 'app.button.bycategory' | translate }}\r\n          </ion-button>\r\n        </ion-col>-->\r\n        <ion-col class=\"ion-no-padding\" *ngIf=\"tipoUsuario!='CLIENTE'\">\r\n          <ion-button size=\"small\" expand=\"full\" fill=\"clear\" color=\"medium\" routerLink=\"/invoices\">\r\n            <ion-icon slot=\"start\" name=\"list-box\"></ion-icon>\r\n            {{ 'app.button.invoices' | translate }}\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-toolbar>\r\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/home-results/home-results.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.module.ts ***!
  \***********************************************************/
/*! exports provided: HomeResultsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsPageModule", function() { return HomeResultsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _home_results_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home-results.page */ "./src/app/pages/home-results/home-results.page.ts");
/* harmony import */ var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ionic4-star-rating */ "./node_modules/ionic4-star-rating/dist/index.js");








// Pipes



const routes = [
    {
        path: '',
        component: _home_results_page__WEBPACK_IMPORTED_MODULE_9__["HomeResultsPage"]
    }
];
let HomeResultsPageModule = class HomeResultsPageModule {
};
HomeResultsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(),
            ionic4_star_rating__WEBPACK_IMPORTED_MODULE_10__["StarRatingModule"],
        ],
        declarations: [_home_results_page__WEBPACK_IMPORTED_MODULE_9__["HomeResultsPage"]]
    })
], HomeResultsPageModule);



/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: var(--ion-color-light);\n}\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium);\n}\n:host ion-card {\n  border-radius: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL0M6XFx4YW1wcFxcaHRkb2NzXFxzb2x1dGl2b1xcYW1hdXRhbW92aWwvc3JjXFxhcHBcXHBhZ2VzXFxob21lLXJlc3VsdHNcXGhvbWUtcmVzdWx0cy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUtcmVzdWx0cy9ob21lLXJlc3VsdHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBRUksb0NBQUE7QUNEUjtBRElJO0VBQ0ksZ0JBQUE7RUFDQSxpREFBQTtBQ0ZSO0FES0k7RUFDSSxnQkFBQTtBQ0hSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL2hvbWUtcmVzdWx0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLy8gLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpXHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWNhcmQge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4vLyBpb24taXRlbSB7XHJcbi8vICAgICAuaXRlbS1uYXRpdmUge1xyXG4vLyAgICAgICAgIGJvcmRlci1yYWRpdXM6IC41cmVtO1xyXG4vLyAgICAgfVxyXG4vLyB9IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG59XG46aG9zdCBpb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG59XG46aG9zdCBpb24tY2FyZCB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.ts ***!
  \*********************************************************/
/*! exports provided: HomeResultsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsPage", function() { return HomeResultsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _pages_modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../pages/modal/search-filter/search-filter.page */ "./src/app/pages/modal/search-filter/search-filter.page.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _components_notifications_notifications_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../../components/notifications/notifications.component */ "./src/app/components/notifications/notifications.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_usuario_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../services/usuario.service */ "./src/app/services/usuario.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");



















let HomeResultsPage = class HomeResultsPage {
    constructor(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, toastCtrl, loadingCtrl, service, router, _authService, alertController, _httpClient, camara, webview, usuarioservice, transfer, file, actionSheetController, storage, plt, ref, filePath, theInAppBrowser) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.router = router;
        this._authService = _authService;
        this.alertController = alertController;
        this._httpClient = _httpClient;
        this.camara = camara;
        this.webview = webview;
        this.usuarioservice = usuarioservice;
        this.transfer = transfer;
        this.file = file;
        this.actionSheetController = actionSheetController;
        this.storage = storage;
        this.plt = plt;
        this.ref = ref;
        this.filePath = filePath;
        this.theInAppBrowser = theInAppBrowser;
        this.searchKey = '';
        this.label = '';
        this.yourLocation = '463 Beacon Street Guest House';
        this.rate = 3;
        this.cargar = true;
        this.collection = [];
        this.validacion = false;
        this.url = "";
        this.hora = new Date();
        for (let i = 1; i <= 100; i++) {
            this.collection.push(`item ${i}`);
        }
        if (this._authService.getRol() === null) {
            this.tipoUsuario = 'CLIENTE';
        }
        else {
            this.tipoUsuario = this._authService.getRol();
        }
        if (this.tipoUsuario === 'PROFESOR') {
            /*this.service.validarHorasDisponibles(this._authService.getIdUsuario()).subscribe(res => {
              if (res) {
      
                //this.alert("Disponibilidad", "No cuentas con horas disponibles, recuerda agregar nuevas disponabilidades.")
                //this.navCtrl.navigateForward('/availability-page');
              } else {
                this.alert("Disponibilidad", "No cuentas con horas disponibles, recuerda agregar nuevas disponabilidades.")
              }
            })*/
        }
    }
    ionViewDidEnter() {
        this.validacion = false;
        this.hora = new Date();
        this.menuCtrl.enable(true);
        this.tienehora();
        this.verdetalles();
        this.rate = 3;
        this.presentLoadingDefault();
        this.actualizar();
    }
    actualizar() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let usuario;
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            if (this._authService.getIdUsuario() != null) {
                this.service.obtenerusuario(this._authService.getIdUsuario()).subscribe(res => {
                    usuario = res;
                    if (usuario.estado_usuario_cuenta === true && new Date(usuario.ultima_conexion) >= new Date()) {
                        console.log("relax");
                        this._authService.enLinea(new Date(this.hora.setMinutes(this.hora.getMinutes() + 5)).toISOString(), true).subscribe(res => {
                        });
                    }
                    else if (usuario.estado_usuario_cuenta === true && new Date(usuario.ultima_conexion) < new Date()) {
                        this._authService.enLinea(new Date(this.hora.setMinutes(this.hora.getMinutes() - 5)).toISOString(), false).subscribe(res => {
                        });
                    }
                    return usuario;
                });
            }
        });
    }
    presentLoadingDefault() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let loading = yield this.loadingCtrl.create({
                message: "Cargando..."
            });
            loading.present();
            this.service.findAll().subscribe(res => {
                this.properties = res;
                loading.dismiss();
                this.comparaciones();
                this.cargar = false;
            });
        });
    }
    comparaciones() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (!this.validacion) {
                this.validacion = true;
                if (this.tipoUsuario == "PROFESOR") {
                    this.usuario = this.service.getItem(this._authService.getIdUsuario());
                    if (this.usuario.ensena == (0) || this.usuario.ensena == []) {
                        this.alert("Alerta", "Agregue sus materias para continuar.");
                        this.service.faltaMaterias = true;
                        this.navCtrl.navigateForward('/teaches');
                    }
                    else if (this.usuario.estudios.length == 0) {
                        this.alert("Alerta", "Agregue sus titulos para continuar.");
                        this.service.faltaTitulos = true;
                        this.navCtrl.navigateForward('/titles');
                    }
                    else if (this.tienehoras == true) {
                        this.alert("Alerta", "Agregue su disponibilidad para continuar.");
                        this.service.faltaDisponibilidad = true;
                        this.navCtrl.navigateForward('/availability-page');
                    }
                    else if (this.usuario.price == null || this.usuario.price == ""
                        || this.todosdetalles.descri_deta_profe == null
                        || this.todosdetalles.descri_deta_profe == ""
                        || this.todosdetalles.experiencia == null
                        || this.todosdetalles.experiencia == ""
                        || this.todosdetalles.preferencia_ense == null
                        || this.todosdetalles.preferencia_ense == "") {
                        this.alert("Alerta", "Agregue sus detalles en editar perfil para continuar.");
                        this.service.faltaCOnfiguracion = true;
                        this.navCtrl.navigateRoot('/edit-profile');
                    }
                }
            }
        });
    }
    tienehora() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            if (this._authService.getIdUsuario() != null) {
                this.service.validarHorasDisponibles(this._authService.getIdUsuario()).subscribe(res => {
                    this.tienehoras = res;
                });
                return this.tienehoras;
            }
        });
    }
    verdetalles() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            if (this._authService.getIdUsuario() != null) {
                this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].url + 'detalles_profesor/usuario/' + this._authService.getIdUsuarioRol(), header)
                    .subscribe(detallesprof => {
                    this.todosdetalles = detallesprof;
                }, error => {
                });
            }
            return this.todosdetalles;
        });
    }
    alert(header, mensaje) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let alert = yield this.alertController.create({
                header: header,
                message: mensaje,
                buttons: [
                    {
                        text: 'Aceptar', handler: data => {
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    settings() {
        this.navCtrl.navigateForward('settings');
    }
    onInput(event) {
        this.service.findByName(this.searchKey)
            .then(data => {
            this.properties = data;
        })
            .catch(error => alert(JSON.stringify(error)));
    }
    logRatingChange(rating) {
        // do your stuff
    }
    onCancel(event) {
        this.menuCtrl.enable(true);
        this.tienehora();
        this.verdetalles();
        this.rate = 3;
        this.presentLoadingDefault();
    }
    openPropertyListPage(label) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loader = yield this.loadingCtrl.create({
                duration: 2000
            });
            loader.present();
            loader.onWillDismiss().then(() => {
                let navigationExtras = {
                    state: {
                        cat: '',
                        label: label
                    }
                };
                this.router.navigate(['property-list'], navigationExtras);
            });
        });
    }
    alertLocation() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const changeLocation = yield this.alertCtrl.create({
                header: 'Change Location',
                message: 'Type your Address to change list in that area.',
                inputs: [
                    {
                        name: 'location',
                        placeholder: 'Enter your new Location',
                        type: 'text'
                    },
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        handler: data => {
                        }
                    },
                    {
                        text: 'Change',
                        handler: (data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                            this.yourLocation = data.location;
                            const toast = yield this.toastCtrl.create({
                                message: 'Location was change successfully',
                                duration: 3000,
                                position: 'top',
                                closeButtonText: 'OK',
                                showCloseButton: true
                            });
                            toast.present();
                        })
                    }
                ]
            });
            changeLocation.present();
        });
    }
    searchFilter() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let todonull = true;
            const modal = yield this.modalCtrl.create({
                component: _pages_modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_6__["SearchFilterPage"]
            });
            modal.onDidDismiss().then((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.data == "reiniciar") {
                    this.presentLoadingDefault();
                }
                else {
                    this.service.newfilter();
                    for (let a in data.data) {
                        if (data.data[a] != null) {
                            if (a == "estrella")
                                this.properties = this.service.findByEstrella(data.data.estrella);
                            else if (a == "pais")
                                this.properties = this.service.findByPais(data.data.pais);
                            else if (a == "idioma")
                                this.properties = this.service.findByIdioma(data.data.idioma);
                            else if (a == "precmin")
                                this.properties = this.service.findByPrecio(data.data.precmin, data.data.preciomax);
                            else if (a == "categoria")
                                this.properties = this.service.findByCategoria(data.data.categoria);
                            else if (a == "filtrohorario") {
                                this.service.findByHorario(data.data.filtrohorario).subscribe(data => {
                                    let dat;
                                    dat = data;
                                    this.properties = dat;
                                });
                            }
                            todonull = false;
                        }
                    }
                }
            }));
            return yield modal.present();
        });
    }
    notifications() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _components_notifications_notifications_component__WEBPACK_IMPORTED_MODULE_8__["NotificationsComponent"],
                animated: true,
                showBackdrop: true
            });
            return yield popover.present();
        });
    }
};
HomeResultsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"] },
    { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"] },
    { type: _services_usuario_service__WEBPACK_IMPORTED_MODULE_13__["UsuarioService"] },
    { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_17__["FileTransfer"] },
    { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_15__["File"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_14__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__["FilePath"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__["InAppBrowser"] }
];
HomeResultsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-results',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home-results.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["trigger"])('staggerIn', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["transition"])('* => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["style"])({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["stagger"])('200ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["animate"])('400ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["style"])({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
                ])
            ])
        ],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home-results.page.scss */ "./src/app/pages/home-results/home-results.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"],
        _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"],
        _services_usuario_service__WEBPACK_IMPORTED_MODULE_13__["UsuarioService"],
        _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_17__["FileTransfer"],
        _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_15__["File"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_14__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
        _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__["FilePath"],
        _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__["InAppBrowser"]])
], HomeResultsPage);



/***/ })

}]);
//# sourceMappingURL=pages-home-results-home-results-module-es2015.js.map