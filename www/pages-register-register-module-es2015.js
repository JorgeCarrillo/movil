(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-register-register-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"ion-padding animated fadeIn login auth-page\">\r\n  <div class=\"theme-bg\"></div>\r\n  <div class=\"back\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"login\"></ion-back-button>\r\n    </ion-buttons>\r\n  </div>\r\n  <div class=\"auth-content\">\r\n    <!-- Logo -->\r\n    <div class=\"ion-padding-horizontal ion-text-center animated fadeInDown\">\r\n\r\n      <div class=\"logo\"></div>\r\n      <h3 ion-no-margin>\r\n        <ion-text color=\"light\" class=\"fw700\">\r\n         <!-- <ion-text color=\"secondary\">{{ 'app.pages.register.title.header' | translate }}</ion-text>-->\r\n        </ion-text>\r\n      </h3>\r\n    </div>\r\n    <!-- Register form -->\r\n    <form [formGroup]=\"onRegisterForm\" class=\"list-form\">\r\n      <ion-item class=\"ion-no-padding animated fadeInUp\">\r\n        <ion-label position=\"floating\">\r\n          <ion-icon name=\"person\"></ion-icon>\r\n          {{ 'app.label.name' | translate }}\r\n        </ion-label>\r\n        <ion-input color=\"secondary\" type=\"text\" formControlName=\"name\"></ion-input>\r\n      </ion-item>\r\n      <p class=\"text08\" *ngIf=\"onRegisterForm.get('name').touched && onRegisterForm.get('name').hasError('name')\">\r\n        <ion-text color=\"warning\">\r\n          {{ 'app.label.errors.field' | translate }}\r\n        </ion-text>\r\n      </p>\r\n\r\n      <ion-item class=\"ion-no-padding animated fadeInUp\">\r\n        <ion-label position=\"floating\">\r\n          <ion-icon name=\"person\"></ion-icon>\r\n          {{ 'app.label.lastname' | translate }}\r\n        </ion-label>\r\n        <ion-input color=\"secondary\" type=\"text\" formControlName=\"apellido\"></ion-input>\r\n      </ion-item>\r\n      <p class=\"text08\"\r\n        *ngIf=\"onRegisterForm.get('apellido').touched && onRegisterForm.get('apellido').hasError('apellido')\">\r\n        <ion-text color=\"warning\">\r\n          {{ 'app.label.errors.field' | translate }}\r\n        </ion-text>\r\n      </p>\r\n\r\n      <ion-item class=\"ion-no-padding animated fadeInUp\">\r\n        <ion-label position=\"floating\">\r\n          <ion-icon name=\"mail\" item-start></ion-icon>\r\n          {{ 'app.label.email' | translate }}\r\n        </ion-label>\r\n        <ion-input color=\"secondary\" type=\"email\" formControlName=\"email\"></ion-input>\r\n      </ion-item>\r\n      <p class=\"text08\" *ngIf=\"onRegisterForm.get('email').touched && onRegisterForm.get('email').hasError('required')\">\r\n        <ion-text color=\"warning\">\r\n          {{ 'app.label.errors.field' | translate }}\r\n        </ion-text>\r\n      </p>\r\n\r\n      <ion-item class=\"ion-no-padding animated fadeInUp\">\r\n        <ion-label position=\"floating\">\r\n          <ion-icon name=\"lock\" item-start></ion-icon>\r\n          {{ 'app.label.password' | translate }}\r\n        </ion-label>\r\n        <ion-input color=\"secondary\" type=\"password\" formControlName=\"password\"></ion-input>\r\n      </ion-item>\r\n      <p color=\"warning\" class=\"text08\"\r\n        *ngIf=\"onRegisterForm.get('password').touched && onRegisterForm.get('password').hasError('required')\">\r\n        <ion-text color=\"warning\">\r\n          {{ 'app.label.errors.field' | translate }}\r\n        </ion-text>\r\n      </p>\r\n\r\n      <ion-item class=\"ion-no-padding animated fadeInUp\">\r\n        <ion-label position=\"floating\">\r\n          <ion-icon name=\"lock\" item-start></ion-icon>\r\n          {{ 'app.label.passwordConfirm' | translate }}\r\n        </ion-label>\r\n        <ion-input color=\"secondary\" type=\"password\" formControlName=\"passwordConfirm\"></ion-input>\r\n      </ion-item>\r\n      <p color=\"warning\" class=\"text08\"\r\n        *ngIf=\"onRegisterForm.get('passwordConfirm').touched && onRegisterForm.get('passwordConfirm').hasError('required')\">\r\n        <ion-text color=\"warning\">\r\n          {{ 'app.label.errors.field' | translate }}\r\n        </ion-text>\r\n      </p>\r\n      <p color=\"warning\" class=\"text08\" *ngIf=\"!onRegisterForm.get('passwordConfirm').hasError('required') &&\r\n                                       onRegisterForm.get('passwordConfirm').hasError('passwordsNotMatching')\">\r\n        <ion-text color=\"warning\">\r\n          {{ 'app.label.errors.password' | translate }}\r\n        </ion-text>\r\n      </p>\r\n      <ion-item class=\"ion-no-padding animated fadeInUp\">\r\n        <ion-radio-group value=\"rol\" formControlName=\"rol\">\r\n          <ion-label position=\"floating\">\r\n            {{ 'app.label.rol' | translate }}\r\n          </ion-label>\r\n          <br>\r\n          <ion-item *ngFor=\"let rol of rolesRegister\">\r\n            <ion-label> {{rol.clave}}</ion-label>\r\n            <ion-radio slot=\"start\" color=\"success\" [value]=\"rol.valor\"></ion-radio>\r\n          </ion-item>\r\n        </ion-radio-group>\r\n      </ion-item>\r\n\r\n    </form>\r\n\r\n    <div class=\"ion-margin-top\">\r\n      <ion-button icon-left size=\"medium\" expand=\"full\" shape=\"round\" color=\"dark\" (click)=\"signUp()\"\r\n        [disabled]=\"!onRegisterForm.valid\" tappable>\r\n        <ion-icon name=\"log-in\"></ion-icon>\r\n        {{ 'app.button.signup' | translate }}\r\n      </ion-button>\r\n\r\n    </div>\r\n\r\n    <!-- Other links -->\r\n    <div class=\"ion-text-center ion-margin-top\">\r\n      <span (click)=\"goToLogin()\" class=\"paz\" tappable>\r\n        <ion-text color=\"light\">\r\n          <strong>{{ 'app.pages.register.label.ihave' | translate }}</strong>\r\n        </ion-text>\r\n      </span>\r\n    </div>\r\n\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/app.constant.ts":
/*!*********************************!*\
  !*** ./src/app/app.constant.ts ***!
  \*********************************/
/*! exports provided: RolesRegister, RolesSistema */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesRegister", function() { return RolesRegister; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesSistema", function() { return RolesSistema; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

const RolesRegister = [
    {
        clave: 'ESTUDIANTE',
        valor: 'ESTUDIANTE'
    },
    {
        clave: 'PROFESOR',
        valor: 'PROFESOR'
    }
];
const RolesSistema = [
    {
        clave: 'ADMINISTRADOR',
        valor: 'ADMINISTRADOR'
    },
    {
        clave: 'ESTUDIANTE',
        valor: 'ESTUDIANTE'
    },
    {
        clave: 'PROFESOR',
        valor: 'PROFESOR'
    }
];


/***/ }),

/***/ "./src/app/pages/register/register.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/register/register.module.ts ***!
  \***************************************************/
/*! exports provided: RegisterPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./register.page */ "./src/app/pages/register/register.page.ts");








const routes = [
    {
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_7__["RegisterPage"]
    }
];
let RegisterPageModule = class RegisterPageModule {
};
RegisterPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild()
        ],
        declarations: [_register_page__WEBPACK_IMPORTED_MODULE_7__["RegisterPage"]]
    })
], RegisterPageModule);



/***/ }),

/***/ "./src/app/pages/register/register.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/register/register.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: linear-gradient(200deg, #e4e4e4 0%, #005b85 70%);\n}\n:host .back {\n  position: fixed;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  z-index: 0;\n  margin-top: 15px;\n  margin-left: 15px;\n  color: #ffffff;\n}\n:host .theme-bg {\n  position: fixed;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  z-index: 0;\n  opacity: 0.12;\n  background-image: url(\"/assets/img/ionproperty2-bg.jpg\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n:host .paz {\n  position: relative;\n  z-index: 10;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVnaXN0ZXIvQzpcXHhhbXBwXFxodGRvY3NcXHNvbHV0aXZvXFxhbWF1dGFtb3ZpbC9zcmNcXGFwcFxccGFnZXNcXHJlZ2lzdGVyXFxyZWdpc3Rlci5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3JlZ2lzdGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUVFLDhEQUFBO0FDREo7QURJRTtFQUNFLGVBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FDRko7QURNRTtFQUNFLGVBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSx3REFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0pKO0FETUE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7QUNKRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3JlZ2lzdGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICBpb24tY29udGVudCB7XHJcbiAgICAvLy0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpXHJcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyMDBkZWcsICNlNGU0ZTQgMCUsICMwMDViODUgNzAlKTtcclxuICB9XHJcblxyXG4gIC5iYWNrIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuXHJcbn1cclxuXHJcbiAgLnRoZW1lLWJnIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBvcGFjaXR5OiAuMTI7IC8vLjEyXHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltZy9pb25wcm9wZXJ0eTItYmcuanBnXCIpO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcbi5wYXoge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB6LWluZGV4OiAxMDtcclxufVxyXG59XHJcbiIsIjpob3N0IGlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCAjZTRlNGU0IDAlLCAjMDA1Yjg1IDcwJSk7XG59XG46aG9zdCAuYmFjayB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAwO1xuICB6LWluZGV4OiAwO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBtYXJnaW4tbGVmdDogMTVweDtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG46aG9zdCAudGhlbWUtYmcge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogMDtcbiAgei1pbmRleDogMDtcbiAgb3BhY2l0eTogMC4xMjtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWcvaW9ucHJvcGVydHkyLWJnLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbjpob3N0IC5wYXoge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHotaW5kZXg6IDEwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/register/register.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/register/register.page.ts ***!
  \*************************************************/
/*! exports provided: RegisterPage, confirmPasswordValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirmPasswordValidator", function() { return confirmPasswordValidator; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _services_usuario_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/usuario.service */ "./src/app/services/usuario.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_envio_correo_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/envio-correo-service.service */ "./src/app/services/envio-correo-service.service.ts");
/* harmony import */ var js_sha256__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! js-sha256 */ "./node_modules/js-sha256/src/sha256.js");
/* harmony import */ var js_sha256__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(js_sha256__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _app_constant__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../app.constant */ "./src/app/app.constant.ts");











let RegisterPage = class RegisterPage {
    constructor(navCtrl, menuCtrl, loadingCtrl, formBuilder, _usuarioService, router, _correo, toastCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this._usuarioService = _usuarioService;
        this.router = router;
        this._correo = _correo;
        this.toastCtrl = toastCtrl;
        this._unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(false);
    }
    ngOnInit() {
        this.rolesRegister = _app_constant__WEBPACK_IMPORTED_MODULE_10__["RolesRegister"];
        this.onRegisterForm = this.formBuilder.group({
            'name': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ])],
            'apellido': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ])],
            'email': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ])],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ]),
            ],
            'passwordConfirm': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ])],
            'rol': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ])]
        });
        this.onRegisterForm.get('password').valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._unsubscribeAll))
            .subscribe(() => {
            this.onRegisterForm.get('passwordConfirm').updateValueAndValidity();
        });
    }
    signUp() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.createCount();
        });
    }
    // // //
    goToLogin() {
        this.navCtrl.navigateRoot('/login');
    }
    createCount() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loginNew = {
                nombre_login: this.onRegisterForm.value.email,
                password_login: Object(js_sha256__WEBPACK_IMPORTED_MODULE_9__["sha256"])(this.onRegisterForm.value.password),
                fecha_ingre: new Date(),
                id_usuario: 0
            };
            const userNew = {
                nombres_usuario: this.onRegisterForm.value.name,
                apellidos_usuario: this.onRegisterForm.value.apellido,
                correo_usuario: this.onRegisterForm.value.email,
                estado_usuario: false,
                estado_usuario_cuenta: false
            };
            try {
                const res = yield this._usuarioService.createUser({
                    login: loginNew,
                    user: userNew,
                    nombre_rol: this.onRegisterForm.value.rol
                }, {});
                if (res) {
                    try {
                        const resPost = yield this._correo.postEnvioCreacion({
                            nombre_login: loginNew.nombre_login,
                            id_usuario: res.id_usuario,
                            nombre_rol: this.onRegisterForm.value.rol
                        });
                        if (resPost) {
                            //await this.router.navigate(['/pages/auth/mail-confirm', {email: loginNew.nombre_login}]);
                            const loader = yield this.loadingCtrl.create({
                                duration: 2000
                            });
                            loader.present();
                            loader.onWillDismiss().then(() => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                this.navCtrl.navigateRoot('/login');
                                const toast = yield this.toastCtrl.create({
                                    showCloseButton: true,
                                    message: 'Usuario registrado con exito, porfavor active su cuenta.',
                                    duration: 2000,
                                    position: 'bottom'
                                });
                                toast.present();
                            }));
                        }
                    }
                    catch (e) {
                        alert('No se ha enviado ningun correo');
                    }
                }
            }
            catch (e) {
                alert('El usuario no ha podido crearse');
            }
        });
    }
};
RegisterPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _services_envio_correo_service_service__WEBPACK_IMPORTED_MODULE_8__["EnvioCorreoServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
];
RegisterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./register.page.scss */ "./src/app/pages/register/register.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _services_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
        _services_envio_correo_service_service__WEBPACK_IMPORTED_MODULE_8__["EnvioCorreoServiceService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
], RegisterPage);

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
const confirmPasswordValidator = (control) => {
    if (!control.parent || !control) {
        return null;
    }
    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');
    if (!password || !passwordConfirm) {
        return null;
    }
    if (passwordConfirm.value === '') {
        return null;
    }
    if (password.value === passwordConfirm.value) {
        return null;
    }
    return { passwordsNotMatching: true };
};


/***/ })

}]);
//# sourceMappingURL=pages-register-register-module-es2015.js.map