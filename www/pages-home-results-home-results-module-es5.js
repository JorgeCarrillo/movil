function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-results-home-results-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeResultsHomeResultsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button color=\"tertiary\"></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      <ion-text color=\"light\">\r\n        {{ 'app.name' | translate }} <ion-text color=\"tertiary\" class=\"fw700\"> {{ 'app.version' | translate }}\r\n        </ion-text>\r\n      </ion-text>\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <!--<ion-button size=\"small\" shape=\"round\" color=\"medium\" (click)=\"notifications()\">\r\n        <ion-icon name=\"notifications\"></ion-icon>\r\n        <ion-badge color=\"dark\" slot=\"end\">2</ion-badge>\r\n      </ion-button>-->\r\n      <ion-button *ngIf=\"tipoUsuario!='CLIENTE'\" size=\"small\" shape=\"round\" color=\"medium\" tappable routerLink=\"/settings\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"cog\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-searchbar [(ngModel)]=\"searchKey\" (ionChange)=\"onInput($event)\" (ionCaancel)=\"onCancel($event)\"\r\n      placeholder='Buscar'></ion-searchbar>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button size=\"small\" shape=\"round\" color=\"medium\" (click)=\"searchFilter()\">\r\n        <ion-icon name=\"options\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n    \r\n  <ion-grid class=\"ion-no-padding\" fixed>\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col class=\"ion-no-padding\">\r\n        <ion-button class=\"ion-no-margin\" expand=\"full\" size=\"medium\" color=\"secondary\">\r\n          Profesores\r\n        </ion-button>\r\n     \r\n        \r\n    \r\n      </ion-col>\r\n      <!--<ion-col class=\"ion-no-padding\">\r\n        <ion-button class=\"ion-no-margin\" expand=\"full\" size=\"medium\" color=\"primary\"\r\n          (click)=\"openPropertyListPage('rent')\">\r\n          En Linea\r\n        </ion-button>\r\n      </ion-col>-->\r\n    </ion-row>\r\n\r\n    <div *ngIf=\"!cargar\" class=\"ion-no-margin\" [@staggerIn]=\"properties\">\r\n      <!-- # -->\r\n      <div  *ngFor=\"let property of properties  | paginate: { itemsPerPage: 10, currentPage: p }\">\r\n      <ion-item *ngIf=\"property.ensena != (0) && property.ensena != [] && property.price != null && property.price !='' && property.horas\" lines=\"none\" class=\"bg-white\" tappable routerLink=\"/property-detail/{{property.id}}\"\r\n       >\r\n        <ion-thumbnail slot=\"start\">\r\n          <img style=\"border-radius: 50%;\" [src]=\"property.picture\" onerror=\"this.src='./assets/img/avatar.png';\">\r\n        </ion-thumbnail>\r\n        <ion-label>\r\n          <ion-row>\r\n            <ion-col>\r\n              <h2>\r\n                <ion-text color=\"dark\"><strong>{{ property.title }}</strong></ion-text>\r\n              </h2>\r\n              <p class=\"text-12x\">\r\n                <ion-text color=\"primary\" style=\"color: black\">{{property.city}}</ion-text>\r\n                <ion-text color=\"secondary\">{{ property.price }}</ion-text>\r\n              </p>\r\n\r\n            <p class=\"text-12x\">\r\n              <ion-text color=\"black\" *ngFor=\"let ensena of property.ensena\">{{ ensena.nombre_materia }}. </ion-text>\r\n            </p>\r\n\r\n              <ion-badge slot=\"end\" color=\"primary\" *ngIf=\"property.estado_usuario_cuenta === true\">\r\n                <ion-text color=\"light\">En línea</ion-text>\r\n              </ion-badge>\r\n\r\n              <ion-badge slot=\"end\" color=\"secondary\" *ngIf=\"property.label === 'sale'\">\r\n                <ion-text color=\"light\"></ion-text>\r\n              </ion-badge>\r\n            </ion-col>\r\n            <ion-col style=\"text-align: right\">\r\n              <h2>\r\n                <ion-text color=\"black\"><strong>${{ property.price }}/h</strong>\r\n                </ion-text>\r\n              </h2>\r\n              <p class=\"text-12x\">\r\n                <ion-text color=\"black\"><strong></strong></ion-text>\r\n              </p>\r\n              <p class=\"text-12x\">\r\n                <ion-text color=\"primary\" style=\"color: black\">{{property.ciudad_nombre}}</ion-text>\r\n                \r\n              </p>\r\n\r\n            </ion-col>\r\n\r\n          </ion-row>\r\n          <ion-badge slot=\"end\" color=\"primary\" *ngIf=\"property.estado_usuario_linea === true\">\r\n            <ion-text color=\"light\">En línea</ion-text>\r\n          </ion-badge>\r\n\r\n          <ion-badge slot=\"end\" color=\"secondary\" *ngIf=\"property.label === 'sale'\">\r\n            <ion-text color=\"light\"></ion-text>\r\n          </ion-badge>\r\n\r\n          <ionic4-star-rating #rating activeIcon=\"ios-star\" defaultIcon=\"ios-star-outline\" activeColor=\"#ea8d16\"\r\n            defaultColor=\"#e8dede\" readonly=\"true\" [rating]=\"property.estrellas\" fontSize=\"22px\" (ratingChanged)=\"logRatingChange($event)\">\r\n          </ionic4-star-rating>\r\n        </ion-label>\r\n      </ion-item>\r\n      <!-- # -->\r\n     \r\n    </div>\r\n    \r\n    </div>\r\n\r\n  </ion-grid>\r\n\r\n  <!--<ion-button class=\"ion-margin\" expand=\"full\" color=\"secondary\" (click)=\"openPropertyListPage()\">\r\n    {{ 'app.button.moreresults' | translate }}\r\n  </ion-button>-->\r\n\r\n  \r\n</ion-content>\r\n\r\n<ion-footer class=\"animated fadeIn\">\r\n  <pagination-controls  previousLabel=\"Anterior\"nextLabel=\"Siguiente\" (pageChange)=\"p = $event\" style=\"text-align: center;\"></pagination-controls>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-grid class=\"ion-no-padding\">\r\n      <ion-row>\r\n        <!--<ion-col size=\"4\" class=\"ion-no-padding\">\r\n          <ion-button size=\"small\" expand=\"full\" fill=\"clear\" color=\"medium\" routerLink=\"/nearby\">\r\n            <ion-icon slot=\"start\" name=\"compass\"></ion-icon>\r\n            {{ 'app.button.nearby' | translate }}\r\n          </ion-button>\r\n        </ion-col>\r\n        <ion-col size=\"4\" class=\"ion-no-padding\">\r\n          <ion-button size=\"small\" expand=\"full\" fill=\"clear\" color=\"medium\" routerLink=\"/bycategory\">\r\n            <ion-icon slot=\"start\" name=\"apps\"></ion-icon>\r\n            {{ 'app.button.bycategory' | translate }}\r\n          </ion-button>\r\n        </ion-col>-->\r\n        <ion-col class=\"ion-no-padding\" *ngIf=\"tipoUsuario!='CLIENTE'\">\r\n          <ion-button size=\"small\" expand=\"full\" fill=\"clear\" color=\"medium\" routerLink=\"/invoices\">\r\n            <ion-icon slot=\"start\" name=\"list-box\"></ion-icon>\r\n            {{ 'app.button.invoices' | translate }}\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-toolbar>\r\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/home-results/home-results.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/home-results/home-results.module.ts ***!
    \***********************************************************/

  /*! exports provided: HomeResultsPageModule */

  /***/
  function srcAppPagesHomeResultsHomeResultsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeResultsPageModule", function () {
      return HomeResultsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ngx-pagination */
    "./node_modules/ngx-pagination/dist/ngx-pagination.js");
    /* harmony import */


    var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../pipes/pipes.module */
    "./src/app/pipes/pipes.module.ts");
    /* harmony import */


    var _home_results_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./home-results.page */
    "./src/app/pages/home-results/home-results.page.ts");
    /* harmony import */


    var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ionic4-star-rating */
    "./node_modules/ionic4-star-rating/dist/index.js"); // Pipes


    var routes = [{
      path: '',
      component: _home_results_page__WEBPACK_IMPORTED_MODULE_9__["HomeResultsPage"]
    }];

    var HomeResultsPageModule = function HomeResultsPageModule() {
      _classCallCheck(this, HomeResultsPageModule);
    };

    HomeResultsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(), ionic4_star_rating__WEBPACK_IMPORTED_MODULE_10__["StarRatingModule"]],
      declarations: [_home_results_page__WEBPACK_IMPORTED_MODULE_9__["HomeResultsPage"]]
    })], HomeResultsPageModule);
    /***/
  },

  /***/
  "./src/app/pages/home-results/home-results.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/home-results/home-results.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesHomeResultsHomeResultsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host ion-content {\n  --background: var(--ion-color-light);\n}\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium);\n}\n:host ion-card {\n  border-radius: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL0M6XFx4YW1wcFxcaHRkb2NzXFxzb2x1dGl2b1xcYW1hdXRhbW92aWwvc3JjXFxhcHBcXHBhZ2VzXFxob21lLXJlc3VsdHNcXGhvbWUtcmVzdWx0cy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUtcmVzdWx0cy9ob21lLXJlc3VsdHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBRUksb0NBQUE7QUNEUjtBRElJO0VBQ0ksZ0JBQUE7RUFDQSxpREFBQTtBQ0ZSO0FES0k7RUFDSSxnQkFBQTtBQ0hSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL2hvbWUtcmVzdWx0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLy8gLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpXHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWNhcmQge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4vLyBpb24taXRlbSB7XHJcbi8vICAgICAuaXRlbS1uYXRpdmUge1xyXG4vLyAgICAgICAgIGJvcmRlci1yYWRpdXM6IC41cmVtO1xyXG4vLyAgICAgfVxyXG4vLyB9IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG59XG46aG9zdCBpb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG59XG46aG9zdCBpb24tY2FyZCB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/home-results/home-results.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/home-results/home-results.page.ts ***!
    \*********************************************************/

  /*! exports provided: HomeResultsPage */

  /***/
  function srcAppPagesHomeResultsHomeResultsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeResultsPage", function () {
      return HomeResultsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers */
    "./src/app/providers/index.ts");
    /* harmony import */


    var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/ionic-webview/ngx */
    "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
    /* harmony import */


    var _pages_modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../pages/modal/search-filter/search-filter.page */
    "./src/app/pages/modal/search-filter/search-filter.page.ts");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _components_notifications_notifications_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./../../components/notifications/notifications.component */
    "./src/app/components/notifications/notifications.component.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _services_usuario_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../services/usuario.service */
    "./src/app/services/usuario.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @ionic-native/File/ngx */
    "./node_modules/@ionic-native/File/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @ionic-native/file-path/ngx */
    "./node_modules/@ionic-native/file-path/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @ionic-native/file-transfer/ngx */
    "./node_modules/@ionic-native/file-transfer/ngx/index.js");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/ngx/index.js");

    var HomeResultsPage = /*#__PURE__*/function () {
      function HomeResultsPage(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, toastCtrl, loadingCtrl, service, router, _authService, alertController, _httpClient, camara, webview, usuarioservice, transfer, file, actionSheetController, storage, plt, ref, filePath, theInAppBrowser) {
        _classCallCheck(this, HomeResultsPage);

        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.router = router;
        this._authService = _authService;
        this.alertController = alertController;
        this._httpClient = _httpClient;
        this.camara = camara;
        this.webview = webview;
        this.usuarioservice = usuarioservice;
        this.transfer = transfer;
        this.file = file;
        this.actionSheetController = actionSheetController;
        this.storage = storage;
        this.plt = plt;
        this.ref = ref;
        this.filePath = filePath;
        this.theInAppBrowser = theInAppBrowser;
        this.searchKey = '';
        this.label = '';
        this.yourLocation = '463 Beacon Street Guest House';
        this.rate = 3;
        this.cargar = true;
        this.collection = [];
        this.validacion = false;
        this.url = "";
        this.hora = new Date();

        for (var i = 1; i <= 100; i++) {
          this.collection.push("item ".concat(i));
        }

        if (this._authService.getRol() === null) {
          this.tipoUsuario = 'CLIENTE';
        } else {
          this.tipoUsuario = this._authService.getRol();
        }

        if (this.tipoUsuario === 'PROFESOR') {
          /*this.service.validarHorasDisponibles(this._authService.getIdUsuario()).subscribe(res => {
            if (res) {
                      //this.alert("Disponibilidad", "No cuentas con horas disponibles, recuerda agregar nuevas disponabilidades.")
              //this.navCtrl.navigateForward('/availability-page');
            } else {
              this.alert("Disponibilidad", "No cuentas con horas disponibles, recuerda agregar nuevas disponabilidades.")
            }
          })*/
        }
      }

      _createClass(HomeResultsPage, [{
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.validacion = false;
          this.hora = new Date();
          this.menuCtrl.enable(true);
          this.tienehora();
          this.verdetalles();
          this.rate = 3;
          this.presentLoadingDefault();
          this.actualizar();
        }
      }, {
        key: "actualizar",
        value: function actualizar() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this = this;

            var usuario, header;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };

                    if (this._authService.getIdUsuario() != null) {
                      this.service.obtenerusuario(this._authService.getIdUsuario()).subscribe(function (res) {
                        usuario = res;

                        if (usuario.estado_usuario_cuenta === true && new Date(usuario.ultima_conexion) >= new Date()) {
                          console.log("relax");

                          _this._authService.enLinea(new Date(_this.hora.setMinutes(_this.hora.getMinutes() + 5)).toISOString(), true).subscribe(function (res) {});
                        } else if (usuario.estado_usuario_cuenta === true && new Date(usuario.ultima_conexion) < new Date()) {
                          _this._authService.enLinea(new Date(_this.hora.setMinutes(_this.hora.getMinutes() - 5)).toISOString(), false).subscribe(function (res) {});
                        }

                        return usuario;
                      });
                    }

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "presentLoadingDefault",
        value: function presentLoadingDefault() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingCtrl.create({
                      message: "Cargando..."
                    });

                  case 2:
                    loading = _context2.sent;
                    loading.present();
                    this.service.findAll().subscribe(function (res) {
                      _this2.properties = res;
                      loading.dismiss();

                      _this2.comparaciones();

                      _this2.cargar = false;
                    });

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "comparaciones",
        value: function comparaciones() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    if (!this.validacion) {
                      this.validacion = true;

                      if (this.tipoUsuario == "PROFESOR") {
                        this.usuario = this.service.getItem(this._authService.getIdUsuario());

                        if (this.usuario.ensena == 0 || this.usuario.ensena == []) {
                          this.alert("Alerta", "Agregue sus materias para continuar.");
                          this.service.faltaMaterias = true;
                          this.navCtrl.navigateForward('/teaches');
                        } else if (this.usuario.estudios.length == 0) {
                          this.alert("Alerta", "Agregue sus titulos para continuar.");
                          this.service.faltaTitulos = true;
                          this.navCtrl.navigateForward('/titles');
                        } else if (this.tienehoras == true) {
                          this.alert("Alerta", "Agregue su disponibilidad para continuar.");
                          this.service.faltaDisponibilidad = true;
                          this.navCtrl.navigateForward('/availability-page');
                        } else if (this.usuario.price == null || this.usuario.price == "" || this.todosdetalles.descri_deta_profe == null || this.todosdetalles.descri_deta_profe == "" || this.todosdetalles.experiencia == null || this.todosdetalles.experiencia == "" || this.todosdetalles.preferencia_ense == null || this.todosdetalles.preferencia_ense == "") {
                          this.alert("Alerta", "Agregue sus detalles en editar perfil para continuar.");
                          this.service.faltaCOnfiguracion = true;
                          this.navCtrl.navigateRoot('/edit-profile');
                        }
                      }
                    }

                  case 1:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "tienehora",
        value: function tienehora() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this3 = this;

            var header;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };

                    if (!(this._authService.getIdUsuario() != null)) {
                      _context4.next = 4;
                      break;
                    }

                    this.service.validarHorasDisponibles(this._authService.getIdUsuario()).subscribe(function (res) {
                      _this3.tienehoras = res;
                    });
                    return _context4.abrupt("return", this.tienehoras);

                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "verdetalles",
        value: function verdetalles() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this4 = this;

            var header;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };

                    if (this._authService.getIdUsuario() != null) {
                      this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].url + 'detalles_profesor/usuario/' + this._authService.getIdUsuarioRol(), header).subscribe(function (detallesprof) {
                        _this4.todosdetalles = detallesprof;
                      }, function (error) {});
                    }

                    return _context5.abrupt("return", this.todosdetalles);

                  case 3:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "alert",
        value: function alert(header, mensaje) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var alert;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.alertController.create({
                      header: header,
                      message: mensaje,
                      buttons: [{
                        text: 'Aceptar',
                        handler: function handler(data) {}
                      }]
                    });

                  case 2:
                    alert = _context6.sent;
                    alert.present();

                  case 4:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "settings",
        value: function settings() {
          this.navCtrl.navigateForward('settings');
        }
      }, {
        key: "onInput",
        value: function onInput(event) {
          var _this5 = this;

          this.service.findByName(this.searchKey).then(function (data) {
            _this5.properties = data;
          })["catch"](function (error) {
            return alert(JSON.stringify(error));
          });
        }
      }, {
        key: "logRatingChange",
        value: function logRatingChange(rating) {// do your stuff
        }
      }, {
        key: "onCancel",
        value: function onCancel(event) {
          this.menuCtrl.enable(true);
          this.tienehora();
          this.verdetalles();
          this.rate = 3;
          this.presentLoadingDefault();
        }
      }, {
        key: "openPropertyListPage",
        value: function openPropertyListPage(label) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var _this6 = this;

            var loader;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    _context7.next = 2;
                    return this.loadingCtrl.create({
                      duration: 2000
                    });

                  case 2:
                    loader = _context7.sent;
                    loader.present();
                    loader.onWillDismiss().then(function () {
                      var navigationExtras = {
                        state: {
                          cat: '',
                          label: label
                        }
                      };

                      _this6.router.navigate(['property-list'], navigationExtras);
                    });

                  case 5:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));
        }
      }, {
        key: "alertLocation",
        value: function alertLocation() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            var _this7 = this;

            var changeLocation;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    _context9.next = 2;
                    return this.alertCtrl.create({
                      header: 'Change Location',
                      message: 'Type your Address to change list in that area.',
                      inputs: [{
                        name: 'location',
                        placeholder: 'Enter your new Location',
                        type: 'text'
                      }],
                      buttons: [{
                        text: 'Cancel',
                        handler: function handler(data) {}
                      }, {
                        text: 'Change',
                        handler: function handler(data) {
                          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this7, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                            var toast;
                            return regeneratorRuntime.wrap(function _callee8$(_context8) {
                              while (1) {
                                switch (_context8.prev = _context8.next) {
                                  case 0:
                                    this.yourLocation = data.location;
                                    _context8.next = 3;
                                    return this.toastCtrl.create({
                                      message: 'Location was change successfully',
                                      duration: 3000,
                                      position: 'top',
                                      closeButtonText: 'OK',
                                      showCloseButton: true
                                    });

                                  case 3:
                                    toast = _context8.sent;
                                    toast.present();

                                  case 5:
                                  case "end":
                                    return _context8.stop();
                                }
                              }
                            }, _callee8, this);
                          }));
                        }
                      }]
                    });

                  case 2:
                    changeLocation = _context9.sent;
                    changeLocation.present();

                  case 4:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }, {
        key: "searchFilter",
        value: function searchFilter() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
            var _this8 = this;

            var todonull, modal;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    todonull = true;
                    _context11.next = 3;
                    return this.modalCtrl.create({
                      component: _pages_modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_6__["SearchFilterPage"]
                    });

                  case 3:
                    modal = _context11.sent;
                    modal.onDidDismiss().then(function (data) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
                        var _this9 = this;

                        var a;
                        return regeneratorRuntime.wrap(function _callee10$(_context10) {
                          while (1) {
                            switch (_context10.prev = _context10.next) {
                              case 0:
                                if (data.data == "reiniciar") {
                                  this.presentLoadingDefault();
                                } else {
                                  this.service.newfilter();

                                  for (a in data.data) {
                                    if (data.data[a] != null) {
                                      if (a == "estrella") this.properties = this.service.findByEstrella(data.data.estrella);else if (a == "pais") this.properties = this.service.findByPais(data.data.pais);else if (a == "idioma") this.properties = this.service.findByIdioma(data.data.idioma);else if (a == "precmin") this.properties = this.service.findByPrecio(data.data.precmin, data.data.preciomax);else if (a == "categoria") this.properties = this.service.findByCategoria(data.data.categoria);else if (a == "filtrohorario") {
                                        this.service.findByHorario(data.data.filtrohorario).subscribe(function (data) {
                                          var dat;
                                          dat = data;
                                          _this9.properties = dat;
                                        });
                                      }
                                      todonull = false;
                                    }
                                  }
                                }

                              case 1:
                              case "end":
                                return _context10.stop();
                            }
                          }
                        }, _callee10, this);
                      }));
                    });
                    _context11.next = 7;
                    return modal.present();

                  case 7:
                    return _context11.abrupt("return", _context11.sent);

                  case 8:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));
        }
      }, {
        key: "notifications",
        value: function notifications() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var popover;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    _context12.next = 2;
                    return this.popoverCtrl.create({
                      component: _components_notifications_notifications_component__WEBPACK_IMPORTED_MODULE_8__["NotificationsComponent"],
                      animated: true,
                      showBackdrop: true
                    });

                  case 2:
                    popover = _context12.sent;
                    _context12.next = 5;
                    return popover.present();

                  case 5:
                    return _context12.abrupt("return", _context12.sent);

                  case 6:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }]);

      return HomeResultsPage;
    }();

    HomeResultsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"]
      }, {
        type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"]
      }, {
        type: _services_usuario_service__WEBPACK_IMPORTED_MODULE_13__["UsuarioService"]
      }, {
        type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_17__["FileTransfer"]
      }, {
        type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_15__["File"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_14__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
      }, {
        type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__["FilePath"]
      }, {
        type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__["InAppBrowser"]
      }];
    };

    HomeResultsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home-results',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home-results.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html"))["default"],
      animations: [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["trigger"])('staggerIn', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["transition"])('* => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["style"])({
        opacity: 0,
        transform: "translate3d(100px,0,0)"
      }), {
        optional: true
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["stagger"])('200ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["animate"])('400ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_11__["style"])({
        opacity: 1,
        transform: "translate3d(0,0,0)"
      }))]), {
        optional: true
      })])])],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home-results.page.scss */
      "./src/app/pages/home-results/home-results.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _providers__WEBPACK_IMPORTED_MODULE_4__["PropertyService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_auth_service__WEBPACK_IMPORTED_MODULE_12__["AuthService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"], _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"], _services_usuario_service__WEBPACK_IMPORTED_MODULE_13__["UsuarioService"], _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_17__["FileTransfer"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_15__["File"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_14__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__["FilePath"], _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_18__["InAppBrowser"]])], HomeResultsPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-home-results-home-results-module-es5.js.map