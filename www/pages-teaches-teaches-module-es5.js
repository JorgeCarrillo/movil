function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-teaches-teaches-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teaches/teaches.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teaches/teaches.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesTeachesTeachesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Materias</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card class=\"ion-no-margin\">\n    <h1 class=\"ion-text-center fw700\">\n      <ion-text color=\"dark\">REGISTRO MATERIAS</ion-text>\n    </h1>\n  \n    <ion-grid fixed class=\"ion-no-padding\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-padding\">\n\n          <ion-list class=\"ion-margin-bottom\">\n\n            <ion-list-header color=\"light\">\n              <ion-label class=\"fw700\">Materias Registradas</ion-label>\n            </ion-list-header>\n\n            <div *ngFor=\"let clase of property.ensena\">\n            <ion-list>\n              <ion-item >\n                <ion-label class=\"fw700\" ><p style=\"color: black;\">{{clase.categoria}}</p><p style=\"color: black;\">{{clase.niveles}}</p></ion-label>\n\n                <div class=\"item-note\" item-end>\n                  <ion-button (click)=\"editarensena(clase)\" size=\"small\" color=\"primary\">\n                      <ion-icon name=\"create\"></ion-icon>\n                  </ion-button>\n                  <!--<ion-button size=\"small\" color=\"danger\">\n                      <ion-icon name=\"trash\"></ion-icon>\n                  </ion-button>-->\n                  \n              </div>\n              </ion-item>\n              \n            </ion-list>\n            </div><br><br>\n        \n            <ion-list-header color=\"light\">\n              <ion-label class=\"fw700\">Datos Materia</ion-label>\n            </ion-list-header>\n\n            <ion-grid>\n                <ion-col style=\"text-align: center;\">\n                  <ion-item>\n                    <ion-label color=\"dark\">Categoría</ion-label>\n                    <ion-select [(ngModel)]=\"idCategoriaSelec\">\n                      <ion-select-option *ngFor=\"let a of categorias\" [value]=\"a.id_categoria\">{{a.nombre_categoria}}\n                      </ion-select-option>\n                    </ion-select>\n                  </ion-item>\n                </ion-col>\n                <!--<div style=\"text-align: center;\">\n                  <ion-button icon-left color=\"primary\" (click)=\"Agregarcategoria()\">\n                    <ion-icon name=\"add\"></ion-icon>\n                    Agregar Categoria\n                  </ion-button>\n                </div>-->\n            </ion-grid>\n\n            <ion-grid>\n                <ion-col style=\"text-align: center;\">\n                  <ion-item>\n                    <ion-label color=\"dark\">Materia</ion-label>\n                    <ion-select [(ngModel)]=\"idMateriaSelec\">\n                      <div *ngFor=\"let b of materias\">\n                      <ion-select-option *ngIf=\"idCategoriaSelec==b.id_categoria\"  [value]=\"b.id_materia\">{{b.nombre_materia}}\n                      </ion-select-option>\n                    </div>\n                    </ion-select>\n                  </ion-item>\n                </ion-col>\n                <div style=\"text-align: center;\">\n                  <ion-button icon-left color=\"primary\" (click)=\"Agregarmateria(idCategoriaSelec)\">\n                    <ion-icon name=\"add\"></ion-icon>\n                    Agregar Materia\n                  </ion-button>\n                </div>\n            </ion-grid>\n\n            <!-- <ion-item>\n              <ion-icon name=\"eye\" slot=\"start\" color=\"primary\"></ion-icon>\n              <ion-label class=\"label\" color=\"primary\">Activar Demo:</ion-label>\n              <ion-toggle [(ngModel)]=\"enableDemo\"></ion-toggle>\n            </ion-item> -->\n\n            <br><br>\n            <!-- <ion-item>\n              <ion-label color=\"dark\" position=\"stacked\">Duración Demo:</ion-label>\n              <ion-datetime\n              displayFormat=\"DD/MM/YYYY\"\n              pikerFormat=\"YYY MM DD\"\n              cancelText=\"Cancelar\"\n              doneText=\"OK\"\n              #A (ionChange)=\"onChangeano(A.value)\"\n              min={{minFecha}}\n              max={{maxFecha}}>{{dato.fechana}</ion-datetime>\n            </ion-item> -->\n\n           <!-- <br><br>\n            <ion-item>\n              <ion-label color=\"dark\" position=\"stacked\">Precio:</ion-label>\n              <ion-input [(ngModel)]=\"precio\" inputmode=\"text\" placeholder=\"\" >\n              </ion-input>\n            </ion-item>-->\n\n           \n          </ion-list>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-button *ngIf=\"!actualizar\" (click)=\"enviarensena()\" size=\"large\" expand=\"full\" color=\"dark\"  class=\"ion-no-margin\">\n     Guardar Materia</ion-button>\n    <ion-button *ngIf=\"actualizar\" (click)=\"actualizarensena()\" size=\"large\" expand=\"full\" color=\"dark\"  class=\"ion-no-margin\">\n     Actualizar Materia</ion-button>\n  </ion-card>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/teaches/teaches.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/teaches/teaches.module.ts ***!
    \*************************************************/

  /*! exports provided: TeachesPageModule */

  /***/
  function srcAppPagesTeachesTeachesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TeachesPageModule", function () {
      return TeachesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _teaches_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./teaches.page */
    "./src/app/pages/teaches/teaches.page.ts");

    var routes = [{
      path: '',
      component: _teaches_page__WEBPACK_IMPORTED_MODULE_7__["TeachesPage"]
    }];

    var TeachesPageModule = function TeachesPageModule() {
      _classCallCheck(this, TeachesPageModule);
    };

    TeachesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(), _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_teaches_page__WEBPACK_IMPORTED_MODULE_7__["TeachesPage"]]
    })], TeachesPageModule);
    /***/
  },

  /***/
  "./src/app/pages/teaches/teaches.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/pages/teaches/teaches.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesTeachesTeachesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RlYWNoZXMvdGVhY2hlcy5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/pages/teaches/teaches.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/pages/teaches/teaches.page.ts ***!
    \***********************************************/

  /*! exports provided: TeachesPage */

  /***/
  function srcAppPagesTeachesTeachesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TeachesPage", function () {
      return TeachesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");
    /* harmony import */


    var _providers__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../providers */
    "./src/app/providers/index.ts");

    var TeachesPage = /*#__PURE__*/function () {
      function TeachesPage(_authService, _httpClient, route, navCtrl, menuCtrl, loadingCtrl, formBuilder, router, alertController, propertyService) {
        _classCallCheck(this, TeachesPage);

        this._authService = _authService;
        this._httpClient = _httpClient;
        this.route = route;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.router = router;
        this.alertController = alertController;
        this.propertyService = propertyService;
        this.actualizar = false;
        this.property = this.propertyService.getItem(this._authService.getIdUsuario());
        this.traerCategorias();
        this.traerMaterias();
        this.obtenerdatos();
        this.propiedades();
      }

      _createClass(TeachesPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "propiedades",
        value: function propiedades() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee);
          }));
        }
      }, {
        key: "obtenerdatos",
        value: function obtenerdatos() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    if (this._authService.getIdUsuarioRol() === null) {
                      this.properID = 'null';
                    } else {
                      this.properID = this._authService.getIdUsuarioRol();
                    }

                  case 1:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "traerMaterias",
        value: function traerMaterias() {
          var _this = this;

          this.materias = [];
          var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          };

          this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'materia/', header).subscribe(function (resmaterias) {
            _this.materias = resmaterias;
          }, function (error) {});
        }
      }, {
        key: "traerCategorias",
        value: function traerCategorias() {
          var _this2 = this;

          this.categorias = [];
          var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          };

          this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'categoria/', header).subscribe(function (rescategoria) {
            _this2.categorias = rescategoria;
          }, function (error) {});
        }
      }, {
        key: "Agregarcategoria",
        value: function Agregarcategoria() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this3 = this;

            var header, alert;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };
                    _context3.next = 3;
                    return this.alertController.create({
                      header: 'Agregar Categoria',
                      message: 'Ingrese la nueva Categoria.',
                      inputs: [{
                        name: 'nombre_categoria',
                        'placeholder': "Nombre Categoria",
                        'value': name
                      }],
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Agregar',
                        handler: function handler(data) {
                          // data.id_categoria=11;
                          _this3._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'categoria/', data, header).subscribe(function (rescat) {
                            _this3.traerCategorias();
                          }, function (error) {});
                        }
                      }]
                    });

                  case 3:
                    alert = _context3.sent;
                    alert.present();

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "Agregarmateria",
        value: function Agregarmateria(idcategoria) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this4 = this;

            var header, alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };
                    _context4.next = 3;
                    return this.alertController.create({
                      header: 'Agregar Materia',
                      message: 'Ingrese la nueva Materia.',
                      inputs: [{
                        name: 'nombre_materia',
                        'placeholder': "Nombre Materia",
                        'value': name
                      }],
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Agregar',
                        handler: function handler(data) {
                          var body = {
                            nombre_materia: data.nombre_materia,
                            id_categoria: idcategoria
                          };

                          _this4._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'materia/', body, header).subscribe(function (rescat) {
                            _this4.traerMaterias();
                          }, function (error) {});
                        }
                      }]
                    });

                  case 3:
                    alert = _context4.sent;
                    alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "enviarensena",
        value: function enviarensena() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this5 = this;

            var header, alert, _alert;

            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };

                    if (!(this.idMateriaSelec == undefined)) {
                      _context5.next = 8;
                      break;
                    }

                    _context5.next = 4;
                    return this.alertController.create({
                      header: 'Alerta Registro',
                      message: 'Porfavor llene todos los campos.',
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Aceptar',
                        handler: function handler(data) {}
                      }]
                    });

                  case 4:
                    alert = _context5.sent;
                    alert.present();
                    _context5.next = 12;
                    break;

                  case 8:
                    _context5.next = 10;
                    return this.alertController.create({
                      header: 'Agregar Materia',
                      message: 'Desea Agregar esta materia a sus clases.',
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Agregar',
                        handler: function handler(data) {
                          //todo
                          var body = {
                            fecha_creacion: new Date().toISOString(),
                            demo: true,
                            duracion_demo: new Date(new Date().setFullYear(new Date().getFullYear() + 5)).toISOString()
                          }; //registro personalizado

                          _this5._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'personalizado/', body, header).subscribe(function (respersonalizado) {
                            _this5.idpersonalizado = respersonalizado;
                            _this5.ultimopersonalizado = _this5.idpersonalizado.identifiers[0].id_tutoria;
                            var bodydeta = {
                              id_tutoria: _this5.ultimopersonalizado,
                              id_materia: _this5.idMateriaSelec
                            }; //registro deta_dia_hora    

                            _this5._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'detalle_materia', bodydeta, header).subscribe(function (resdetalleMateria) {
                              _this5.iddetallemateria = resdetalleMateria;
                              _this5.ultimodetallemateria = _this5.iddetallemateria.identifiers[0].id_deta_per_mate;
                            }, function (error) {});

                            var bodyperserv = {
                              id_tutoria: _this5.ultimopersonalizado,
                              id_servicio: 1
                            }; //registro serper   

                            _this5._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'asig-servicio-per', bodyperserv, header).subscribe(function (resservicioper) {
                              _this5.idservicioper = resservicioper;
                              _this5.ultimoservicioper = _this5.idservicioper.identifiers[0].id_asig_per_serv;
                              var bodyuserper = {
                                id_usuario_rol: +_this5.properID,
                                id_asig_per_serv: _this5.ultimoservicioper
                              }; //registro serper   

                              _this5._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'asig-user-per', bodyuserper, header).subscribe(function (resuserperr) {
                                _this5.propertyService.faltaMaterias = false;
                              }, function (error) {});
                            }, function (error) {});
                          }, function (error) {});

                          _this5.navCtrl.pop(); // this.navCtrl.navigateRoot('/login');

                        }
                      }]
                    });

                  case 10:
                    _alert = _context5.sent;

                    _alert.present();

                  case 12:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "editarensena",
        value: function editarensena(ensena) {
          var _this6 = this;

          var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          };

          var _iterator = _createForOfIteratorHelper(this.categorias),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var a = _step.value;

              if (a.nombre_categoria == ensena.categoria) {
                this.idCategoriaSelec = a.id_categoria;
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          this.idMateriaSelec = ensena.id_materia;
          this.actualizar = true;

          this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'detalle_materia/idtuto/' + ensena.id_tutoria, header).subscribe(function (resmateria) {
            //location.reload();
            var auxmate;
            auxmate = resmateria; // this./detalle_materia/idtuto/1

            _this6.idpermate = auxmate.id_deta_per_mate;
          }, function (error) {});
        }
      }, {
        key: "actualizarensena",
        value: function actualizarensena(ensena) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var _this7 = this;

            var header, bodydeta, alert;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };
                    bodydeta = {
                      id_materia: this.idMateriaSelec
                    };
                    _context6.next = 4;
                    return this.alertController.create({
                      header: 'Actualizar Materia',
                      message: 'Esta seguro de actualizar esta materia.',
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Actualizar',
                        handler: function handler(data) {
                          _this7._httpClient.put(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'detalle_materia/' + _this7.idpermate, bodydeta, header).subscribe(function (resmat) {
                            //location.reload();
                            _this7.navCtrl.pop();
                          }, function (error) {});
                        }
                      }]
                    });

                  case 4:
                    alert = _context6.sent;
                    alert.present();

                  case 6:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }]);

      return TeachesPage;
    }();

    TeachesPage.ctorParameters = function () {
      return [{
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: _providers__WEBPACK_IMPORTED_MODULE_9__["PropertyService"]
      }];
    };

    TeachesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-teaches',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./teaches.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teaches/teaches.page.html"))["default"],
      animations: [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["trigger"])('staggerIn', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["transition"])('* => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["style"])({
        opacity: 0,
        transform: "translate3d(100px,0,0)"
      }), {
        optional: true
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["animate"])('500ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["style"])({
        opacity: 1,
        transform: "translate3d(0,0,0)"
      }))]), {
        optional: true
      })])])],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./teaches.page.scss */
      "./src/app/pages/teaches/teaches.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _providers__WEBPACK_IMPORTED_MODULE_9__["PropertyService"]])], TeachesPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-teaches-teaches-module-es5.js.map