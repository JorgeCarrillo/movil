(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-teaches-teaches-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teaches/teaches.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teaches/teaches.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Materias</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card class=\"ion-no-margin\">\n    <h1 class=\"ion-text-center fw700\">\n      <ion-text color=\"dark\">REGISTRO MATERIAS</ion-text>\n    </h1>\n  \n    <ion-grid fixed class=\"ion-no-padding\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-padding\">\n\n          <ion-list class=\"ion-margin-bottom\">\n\n            <ion-list-header color=\"light\">\n              <ion-label class=\"fw700\">Materias Registradas</ion-label>\n            </ion-list-header>\n\n            <div *ngFor=\"let clase of property.ensena\">\n            <ion-list>\n              <ion-item >\n                <ion-label class=\"fw700\" ><p style=\"color: black;\">{{clase.categoria}}</p><p style=\"color: black;\">{{clase.niveles}}</p></ion-label>\n\n                <div class=\"item-note\" item-end>\n                  <ion-button (click)=\"editarensena(clase)\" size=\"small\" color=\"primary\">\n                      <ion-icon name=\"create\"></ion-icon>\n                  </ion-button>\n                  <!--<ion-button size=\"small\" color=\"danger\">\n                      <ion-icon name=\"trash\"></ion-icon>\n                  </ion-button>-->\n                  \n              </div>\n              </ion-item>\n              \n            </ion-list>\n            </div><br><br>\n        \n            <ion-list-header color=\"light\">\n              <ion-label class=\"fw700\">Datos Materia</ion-label>\n            </ion-list-header>\n\n            <ion-grid>\n                <ion-col style=\"text-align: center;\">\n                  <ion-item>\n                    <ion-label color=\"dark\">Categoría</ion-label>\n                    <ion-select [(ngModel)]=\"idCategoriaSelec\">\n                      <ion-select-option *ngFor=\"let a of categorias\" [value]=\"a.id_categoria\">{{a.nombre_categoria}}\n                      </ion-select-option>\n                    </ion-select>\n                  </ion-item>\n                </ion-col>\n                <!--<div style=\"text-align: center;\">\n                  <ion-button icon-left color=\"primary\" (click)=\"Agregarcategoria()\">\n                    <ion-icon name=\"add\"></ion-icon>\n                    Agregar Categoria\n                  </ion-button>\n                </div>-->\n            </ion-grid>\n\n            <ion-grid>\n                <ion-col style=\"text-align: center;\">\n                  <ion-item>\n                    <ion-label color=\"dark\">Materia</ion-label>\n                    <ion-select [(ngModel)]=\"idMateriaSelec\">\n                      <div *ngFor=\"let b of materias\">\n                      <ion-select-option *ngIf=\"idCategoriaSelec==b.id_categoria\"  [value]=\"b.id_materia\">{{b.nombre_materia}}\n                      </ion-select-option>\n                    </div>\n                    </ion-select>\n                  </ion-item>\n                </ion-col>\n                <div style=\"text-align: center;\">\n                  <ion-button icon-left color=\"primary\" (click)=\"Agregarmateria(idCategoriaSelec)\">\n                    <ion-icon name=\"add\"></ion-icon>\n                    Agregar Materia\n                  </ion-button>\n                </div>\n            </ion-grid>\n\n            <!-- <ion-item>\n              <ion-icon name=\"eye\" slot=\"start\" color=\"primary\"></ion-icon>\n              <ion-label class=\"label\" color=\"primary\">Activar Demo:</ion-label>\n              <ion-toggle [(ngModel)]=\"enableDemo\"></ion-toggle>\n            </ion-item> -->\n\n            <br><br>\n            <!-- <ion-item>\n              <ion-label color=\"dark\" position=\"stacked\">Duración Demo:</ion-label>\n              <ion-datetime\n              displayFormat=\"DD/MM/YYYY\"\n              pikerFormat=\"YYY MM DD\"\n              cancelText=\"Cancelar\"\n              doneText=\"OK\"\n              #A (ionChange)=\"onChangeano(A.value)\"\n              min={{minFecha}}\n              max={{maxFecha}}>{{dato.fechana}</ion-datetime>\n            </ion-item> -->\n\n           <!-- <br><br>\n            <ion-item>\n              <ion-label color=\"dark\" position=\"stacked\">Precio:</ion-label>\n              <ion-input [(ngModel)]=\"precio\" inputmode=\"text\" placeholder=\"\" >\n              </ion-input>\n            </ion-item>-->\n\n           \n          </ion-list>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-button *ngIf=\"!actualizar\" (click)=\"enviarensena()\" size=\"large\" expand=\"full\" color=\"dark\"  class=\"ion-no-margin\">\n     Guardar Materia</ion-button>\n    <ion-button *ngIf=\"actualizar\" (click)=\"actualizarensena()\" size=\"large\" expand=\"full\" color=\"dark\"  class=\"ion-no-margin\">\n     Actualizar Materia</ion-button>\n  </ion-card>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/teaches/teaches.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/teaches/teaches.module.ts ***!
  \*************************************************/
/*! exports provided: TeachesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachesPageModule", function() { return TeachesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _teaches_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./teaches.page */ "./src/app/pages/teaches/teaches.page.ts");








const routes = [
    {
        path: '',
        component: _teaches_page__WEBPACK_IMPORTED_MODULE_7__["TeachesPage"]
    }
];
let TeachesPageModule = class TeachesPageModule {
};
TeachesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(),
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_teaches_page__WEBPACK_IMPORTED_MODULE_7__["TeachesPage"]]
    })
], TeachesPageModule);



/***/ }),

/***/ "./src/app/pages/teaches/teaches.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/teaches/teaches.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RlYWNoZXMvdGVhY2hlcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/teaches/teaches.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/teaches/teaches.page.ts ***!
  \***********************************************/
/*! exports provided: TeachesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachesPage", function() { return TeachesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");











let TeachesPage = class TeachesPage {
    constructor(_authService, _httpClient, route, navCtrl, menuCtrl, loadingCtrl, formBuilder, router, alertController, propertyService) {
        this._authService = _authService;
        this._httpClient = _httpClient;
        this.route = route;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.router = router;
        this.alertController = alertController;
        this.propertyService = propertyService;
        this.actualizar = false;
        this.property = this.propertyService.getItem(this._authService.getIdUsuario());
        this.traerCategorias();
        this.traerMaterias();
        this.obtenerdatos();
        this.propiedades();
    }
    ngOnInit() {
    }
    propiedades() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
        });
    }
    obtenerdatos() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this._authService.getIdUsuarioRol() === null) {
                this.properID = 'null';
            }
            else {
                this.properID = this._authService.getIdUsuarioRol();
            }
        });
    }
    traerMaterias() {
        this.materias = [];
        let header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
            })
        };
        this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'materia/', header)
            .subscribe(resmaterias => {
            this.materias = resmaterias;
        }, error => {
        });
    }
    traerCategorias() {
        this.categorias = [];
        let header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
            })
        };
        this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'categoria/', header)
            .subscribe(rescategoria => {
            this.categorias = rescategoria;
        }, error => {
        });
    }
    Agregarcategoria() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            let alert = yield this.alertController.create({
                header: 'Agregar Categoria',
                message: 'Ingrese la nueva Categoria.',
                inputs: [
                    { name: 'nombre_categoria', 'placeholder': "Nombre Categoria", 'value': name }
                ],
                buttons: [{ text: 'Cancelar', role: 'cancel' },
                    {
                        text: 'Agregar', handler: data => {
                            // data.id_categoria=11;
                            this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'categoria/', data, header)
                                .subscribe(rescat => {
                                this.traerCategorias();
                            }, error => {
                            });
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    Agregarmateria(idcategoria) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            let alert = yield this.alertController.create({
                header: 'Agregar Materia',
                message: 'Ingrese la nueva Materia.',
                inputs: [
                    { name: 'nombre_materia', 'placeholder': "Nombre Materia", 'value': name }
                ],
                buttons: [{ text: 'Cancelar', role: 'cancel' },
                    {
                        text: 'Agregar', handler: data => {
                            let body = {
                                nombre_materia: data.nombre_materia,
                                id_categoria: idcategoria,
                            };
                            this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'materia/', body, header)
                                .subscribe(rescat => {
                                this.traerMaterias();
                            }, error => {
                            });
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    enviarensena() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            if (this.idMateriaSelec == undefined) {
                let alert = yield this.alertController.create({
                    header: 'Alerta Registro',
                    message: 'Porfavor llene todos los campos.',
                    buttons: [{ text: 'Cancelar', role: 'cancel' },
                        {
                            text: 'Aceptar', handler: data => {
                            }
                        }
                    ]
                });
                alert.present();
            }
            else {
                let alert = yield this.alertController.create({
                    header: 'Agregar Materia',
                    message: 'Desea Agregar esta materia a sus clases.',
                    buttons: [{ text: 'Cancelar', role: 'cancel' },
                        {
                            text: 'Agregar', handler: data => {
                                //todo
                                let body = {
                                    fecha_creacion: new Date().toISOString(),
                                    demo: true,
                                    duracion_demo: new Date(new Date().setFullYear(new Date().getFullYear() + 5)).toISOString(),
                                };
                                //registro personalizado
                                this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'personalizado/', body, header)
                                    .subscribe(respersonalizado => {
                                    this.idpersonalizado = respersonalizado;
                                    this.ultimopersonalizado = this.idpersonalizado.identifiers[0].id_tutoria;
                                    let bodydeta = {
                                        id_tutoria: this.ultimopersonalizado,
                                        id_materia: this.idMateriaSelec
                                    };
                                    //registro deta_dia_hora    
                                    this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'detalle_materia', bodydeta, header)
                                        .subscribe(resdetalleMateria => {
                                        this.iddetallemateria = resdetalleMateria;
                                        this.ultimodetallemateria = this.iddetallemateria.identifiers[0].id_deta_per_mate;
                                    }, error => {
                                    });
                                    let bodyperserv = {
                                        id_tutoria: this.ultimopersonalizado,
                                        id_servicio: 1
                                    };
                                    //registro serper   
                                    this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'asig-servicio-per', bodyperserv, header)
                                        .subscribe(resservicioper => {
                                        this.idservicioper = resservicioper;
                                        this.ultimoservicioper = this.idservicioper.identifiers[0].id_asig_per_serv;
                                        let bodyuserper = {
                                            id_usuario_rol: +this.properID,
                                            id_asig_per_serv: this.ultimoservicioper
                                        };
                                        //registro serper   
                                        this._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'asig-user-per', bodyuserper, header)
                                            .subscribe(resuserperr => {
                                            this.propertyService.faltaMaterias = false;
                                        }, error => {
                                        });
                                    }, error => {
                                    });
                                }, error => {
                                });
                                this.navCtrl.pop();
                                // this.navCtrl.navigateRoot('/login');
                            }
                        }
                    ]
                });
                alert.present();
            }
        });
    }
    editarensena(ensena) {
        let header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this._authService.getToken()
            })
        };
        for (let a of this.categorias) {
            if (a.nombre_categoria == ensena.categoria) {
                this.idCategoriaSelec = a.id_categoria;
            }
        }
        this.idMateriaSelec = ensena.id_materia;
        this.actualizar = true;
        this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'detalle_materia/idtuto/' + ensena.id_tutoria, header)
            .subscribe(resmateria => {
            //location.reload();
            let auxmate;
            auxmate = resmateria;
            // this./detalle_materia/idtuto/1
            this.idpermate = auxmate.id_deta_per_mate;
        }, error => {
        });
    }
    actualizarensena(ensena) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this._authService.getToken()
                })
            };
            let bodydeta = {
                id_materia: this.idMateriaSelec
            };
            let alert = yield this.alertController.create({
                header: 'Actualizar Materia',
                message: 'Esta seguro de actualizar esta materia.',
                buttons: [{ text: 'Cancelar', role: 'cancel' },
                    {
                        text: 'Actualizar', handler: data => {
                            this._httpClient.put(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].url + 'detalle_materia/' + this.idpermate, bodydeta, header)
                                .subscribe(resmat => {
                                //location.reload();
                                this.navCtrl.pop();
                            }, error => {
                            });
                        }
                    }
                ]
            });
            alert.present();
        });
    }
};
TeachesPage.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_9__["PropertyService"] }
];
TeachesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-teaches',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./teaches.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teaches/teaches.page.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["trigger"])('staggerIn', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["transition"])('* => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["style"])({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["animate"])('500ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["style"])({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
                ])
            ])
        ],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./teaches.page.scss */ "./src/app/pages/teaches/teaches.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _providers__WEBPACK_IMPORTED_MODULE_9__["PropertyService"]])
], TeachesPage);



/***/ })

}]);
//# sourceMappingURL=pages-teaches-teaches-module-es2015.js.map