function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-bycategory-bycategory-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bycategory/bycategory.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bycategory/bycategory.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesBycategoryBycategoryPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>{{ 'app.pages.bycategory.title.header' | translate }}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div [@staggerIn]=\"categories\">\r\n    <ion-card class=\"ion-no-margin ion-text-center\" *ngFor=\"let category of categories\" tappable (click)=\"openPropertyListPage(category.value)\">\r\n      <div class=\"shadow\"></div>\r\n      <ion-img [src]=\"category.picture\"></ion-img>\r\n      <h3 class=\"card-title\">{{category.name}}</h3>\r\n      <p class=\"card-subtitle\">{{category.quantity}} Properties</p>\r\n    </ion-card>\r\n  </div>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/pages/bycategory/bycategory.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/bycategory/bycategory.module.ts ***!
    \*******************************************************/

  /*! exports provided: BycategoryPageModule */

  /***/
  function srcAppPagesBycategoryBycategoryModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BycategoryPageModule", function () {
      return BycategoryPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _bycategory_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./bycategory.page */
    "./src/app/pages/bycategory/bycategory.page.ts");

    var routes = [{
      path: '',
      component: _bycategory_page__WEBPACK_IMPORTED_MODULE_7__["BycategoryPage"]
    }];

    var BycategoryPageModule = function BycategoryPageModule() {
      _classCallCheck(this, BycategoryPageModule);
    };

    BycategoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild()],
      declarations: [_bycategory_page__WEBPACK_IMPORTED_MODULE_7__["BycategoryPage"]]
    })], BycategoryPageModule);
    /***/
  },

  /***/
  "./src/app/pages/bycategory/bycategory.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/pages/bycategory/bycategory.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesBycategoryBycategoryPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host ion-content {\n  --background: linear-gradient(135deg, var(--ion-color-dark), var(--ion-color-primary)) ;\n}\n:host ion-card {\n  border-radius: 0;\n  margin-bottom: -3px;\n  -webkit-font-smoothing: subpixel-antialiased;\n}\n.shadow {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  box-shadow: inset 0 0 28rem 0 rgba(var(--ion-color-dark-rgb), 0.95);\n}\n.card-title {\n  position: absolute;\n  top: 36%;\n  width: 100%;\n  font-weight: 700;\n  color: #fff;\n  text-shadow: 1px 2px 2px rgba(var(--ion-color-dark-rgb), 1);\n}\n.card-subtitle {\n  font-weight: 500;\n  position: absolute;\n  top: 52%;\n  width: 100%;\n  color: var(--ion-color-tertiary);\n  text-shadow: 0 0 6px rgba(var(--ion-color-dark-rgb), 1);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnljYXRlZ29yeS9DOlxceGFtcHBcXGh0ZG9jc1xcc29sdXRpdm9cXGFtYXV0YW1vdmlsL3NyY1xcYXBwXFxwYWdlc1xcYnljYXRlZ29yeVxcYnljYXRlZ29yeS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2J5Y2F0ZWdvcnkvYnljYXRlZ29yeS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDRSx1RkFBQTtBQ0FOO0FER0k7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsNENBQUE7QUNEUjtBRE9BO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsbUVBQUE7QUNKRjtBRE9BO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLDJEQUFBO0FDSkY7QURPQTtFQUNFLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtFQUNBLGdDQUFBO0VBQ0EsdURBQUE7QUNKRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2J5Y2F0ZWdvcnkvYnljYXRlZ29yeS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpXHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWNhcmQge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLTNweDtcclxuICAgICAgICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBzdWJwaXhlbC1hbnRpYWxpYXNlZDtcclxuICAgICAgICAvLyBmb250LXNtb290aGluZzogc3VicGl4ZWwtYW50aWFsaWFzZWQ7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4uc2hhZG93IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwO1xyXG4gIGJvdHRvbTogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIGJveC1zaGFkb3c6IGluc2V0IDAgMCAyOHJlbSAwIHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmstcmdiKSwgLjk1KTtcclxufVxyXG5cclxuLmNhcmQtdGl0bGUge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDM2JTtcclxuICB3aWR0aDogMTAwJTtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIHRleHQtc2hhZG93OiAxcHggMnB4IDJweCByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDEpO1xyXG59XHJcblxyXG4uY2FyZC1zdWJ0aXRsZSB7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA1MiU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSk7XHJcbiAgdGV4dC1zaGFkb3c6IDAgMCA2cHggcmdiYSh2YXIoLS1pb24tY29sb3ItZGFyay1yZ2IpLCAxKTtcclxufSIsIjpob3N0IGlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItZGFyayksIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSkgO1xufVxuOmhvc3QgaW9uLWNhcmQge1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBtYXJnaW4tYm90dG9tOiAtM3B4O1xuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBzdWJwaXhlbC1hbnRpYWxpYXNlZDtcbn1cblxuLnNoYWRvdyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBib3gtc2hhZG93OiBpbnNldCAwIDAgMjhyZW0gMCByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDAuOTUpO1xufVxuXG4uY2FyZC10aXRsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAzNiU7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXdlaWdodDogNzAwO1xuICBjb2xvcjogI2ZmZjtcbiAgdGV4dC1zaGFkb3c6IDFweCAycHggMnB4IHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmstcmdiKSwgMSk7XG59XG5cbi5jYXJkLXN1YnRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUyJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xuICB0ZXh0LXNoYWRvdzogMCAwIDZweCByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDEpO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/bycategory/bycategory.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/bycategory/bycategory.page.ts ***!
    \*****************************************************/

  /*! exports provided: BycategoryPage */

  /***/
  function srcAppPagesBycategoryBycategoryPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BycategoryPage", function () {
      return BycategoryPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _providers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers */
    "./src/app/providers/index.ts");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");

    var BycategoryPage = /*#__PURE__*/function () {
      function BycategoryPage(navCtrl, loadingCtrl, service, router) {
        _classCallCheck(this, BycategoryPage);

        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.router = router;
      }

      _createClass(BycategoryPage, [{
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.findAll();
        }
      }, {
        key: "openPropertyListPage",
        value: function openPropertyListPage(cat) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this = this;

            var loader;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.loadingCtrl.create({
                      duration: 500
                    });

                  case 2:
                    loader = _context.sent;
                    loader.present();
                    loader.onWillDismiss().then(function () {
                      var navigationExtras = {
                        state: {
                          cat: cat
                        }
                      };

                      _this.router.navigate(['property-list'], navigationExtras);
                    });

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "findAll",
        value: function findAll() {
          var _this2 = this;

          this.service.findAll().then(function (data) {
            return _this2.categories = data;
          })["catch"](function (error) {
            return alert(error);
          });
        }
      }]);

      return BycategoryPage;
    }();

    BycategoryPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _providers__WEBPACK_IMPORTED_MODULE_4__["CategoryService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }];
    };

    BycategoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-bycategory',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./bycategory.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bycategory/bycategory.page.html"))["default"],
      animations: [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["trigger"])('staggerIn', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["transition"])('* => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({
        opacity: 0,
        transform: "translate3d(0,10px,0)"
      }), {
        optional: true
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["animate"])('600ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({
        opacity: 1,
        transform: "translate3d(0,0,0)"
      }))]), {
        optional: true
      })])])],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./bycategory.page.scss */
      "./src/app/pages/bycategory/bycategory.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _providers__WEBPACK_IMPORTED_MODULE_4__["CategoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])], BycategoryPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-bycategory-bycategory-module-es5.js.map