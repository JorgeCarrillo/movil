(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-nearby-nearby-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/nearby/nearby.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/nearby/nearby.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Tutores cercanos</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n  <!-- # -->\r\n  <div id=\"nearby-map\" class=\"nearby-map\">\r\n\t\t<agm-map *ngIf=\"nearProperties\" [latitude]=\"nearProperties[0]?.lat\" [longitude]=\"nearProperties[0]?.long\" [zoom]=\"12\">\r\n\t\t\t<agm-marker *ngFor=\"let property of nearProperties\"\r\n\t\t\t [latitude]=\"property.lat\" [longitude]=\"property.long\">\r\n\t\t\t\t<agm-info-window>\r\n          <ion-item tappable routerLink=\"/property-detail/{{property.id}}\">\r\n            <ion-thumbnail slot=\"start\">\r\n              <img [src]=\"property.thumbnail\">\r\n            </ion-thumbnail>\r\n            <ion-label>\r\n              <h3>\r\n                <strong>{{property.title}}</strong>\r\n              </h3>\r\n              <p class=\"ion-no-margin\">\r\n                <ion-text color=\"primary\">{{property.city}}, {{property.address}} •\r\n                  <span class=\"fw700\">{{ property.price }}</span>\r\n                </ion-text>\r\n              </p>\r\n            </ion-label>\r\n          </ion-item>\r\n\t\t\t\t</agm-info-window>\r\n\t\t\t</agm-marker>\r\n\t\t</agm-map>\r\n\t</div>\r\n  <!-- # -->\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/nearby/nearby.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/nearby/nearby.module.ts ***!
  \***********************************************/
/*! exports provided: NearbyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NearbyPageModule", function() { return NearbyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/fesm2015/agm-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _nearby_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./nearby.page */ "./src/app/pages/nearby/nearby.page.ts");









const routes = [
    {
        path: '',
        component: _nearby_page__WEBPACK_IMPORTED_MODULE_8__["NearbyPage"]
    }
];
let NearbyPageModule = class NearbyPageModule {
};
NearbyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(),
            _agm_core__WEBPACK_IMPORTED_MODULE_6__["AgmCoreModule"].forRoot({
                apiKey: 'AIzaSyD9BxeSvt3u--Oj-_GD-qG2nPr1uODrR0Y'
            })
        ],
        declarations: [_nearby_page__WEBPACK_IMPORTED_MODULE_8__["NearbyPage"]]
    })
], NearbyPageModule);



/***/ }),

/***/ "./src/app/pages/nearby/nearby.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/nearby/nearby.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: linear-gradient(135deg, var(--ion-color-dark), var(--ion-color-primary)) ;\n}\n\n.nearby-map agm-map {\n  width: 100%;\n  height: 94vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbmVhcmJ5L0M6XFx4YW1wcFxcaHRkb2NzXFxzb2x1dGl2b1xcYW1hdXRhbW92aWwvc3JjXFxhcHBcXHBhZ2VzXFxuZWFyYnlcXG5lYXJieS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL25lYXJieS9uZWFyYnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0UsdUZBQUE7QUNBTjs7QURLQztFQUNDLFdBQUE7RUFDQSxZQUFBO0FDRkYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9uZWFyYnkvbmVhcmJ5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItZGFyayksIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSlcclxuICAgIH1cclxufVxyXG5cclxuLm5lYXJieS1tYXAge1xyXG5cdGFnbS1tYXAge1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRoZWlnaHQ6IDk0dmg7XHJcblx0fVxyXG59XHJcblxyXG4vLyBhZ20tbWFwIHtcclxuLy8gICAgIGhlaWdodDogNzJ2aDtcclxuLy8gICAgIC5nbW5vcHJpbnQge1xyXG4vLyAgICAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbi8vICAgICB9XHJcbi8vIH0iLCI6aG9zdCBpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpIDtcbn1cblxuLm5lYXJieS1tYXAgYWdtLW1hcCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDk0dmg7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/nearby/nearby.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/nearby/nearby.page.ts ***!
  \*********************************************/
/*! exports provided: NearbyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NearbyPage", function() { return NearbyPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");





let NearbyPage = class NearbyPage {
    constructor(navCtrl, service, translate) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.translate = translate;
        this.agmStyles = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].agmStyles;
        this.findAll();
    }
    ionViewWillEnter() {
    }
    findAll() {
        /*this.service.findAll()
            .then(data => {
              this.nearProperties = data;
            })
            .catch(error => alert(error));*/
        this.service.findAll().subscribe(res => {
            this.nearProperties = res;
        });
    }
};
NearbyPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_3__["PropertyService"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_3__["TranslateProvider"] }
];
NearbyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nearby',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./nearby.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/nearby/nearby.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./nearby.page.scss */ "./src/app/pages/nearby/nearby.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _providers__WEBPACK_IMPORTED_MODULE_3__["PropertyService"],
        _providers__WEBPACK_IMPORTED_MODULE_3__["TranslateProvider"]])
], NearbyPage);



/***/ })

}]);
//# sourceMappingURL=pages-nearby-nearby-module-es2015.js.map