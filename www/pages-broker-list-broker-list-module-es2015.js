(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-broker-list-broker-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-list/broker-list.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-list/broker-list.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n    <ion-toolbar color=\"primary\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n        {{ 'app.pages.brokerlist.title.header' | translate }}\r\n      </ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n<ion-content>\r\n    <ion-list class=\"ion-no-padding ion-no-margin\" [@staggerIn]=\"brokers\">\r\n      <ion-item class=\"ion-no-padding\" lines=\"none\" *ngFor=\"let broker of brokers\" tappable routerLink=\"/broker-detail/{{broker.id}}\">\r\n        <ion-thumbnail class=\"ion-margin\" slot=\"start\">\r\n            <img [src]=\"broker.picture\">\r\n          </ion-thumbnail>\r\n        <ion-label>\r\n          <h2 class=\"fw700 text08\">\r\n            <ion-text color=\"dark\">{{broker.name}}</ion-text>\r\n          </h2>\r\n          <p>\r\n            <ion-text color=\"primary\">{{ broker.title }}</ion-text>\r\n          </p>\r\n        </ion-label>\r\n        <ion-badge color=\"primary\">\r\n          <ion-icon name=\"arrow-forward\"></ion-icon>\r\n        </ion-badge>\r\n      </ion-item>\r\n    </ion-list>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/broker-list/broker-list.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/broker-list/broker-list.module.ts ***!
  \*********************************************************/
/*! exports provided: BrokerListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrokerListPageModule", function() { return BrokerListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _broker_list_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./broker-list.page */ "./src/app/pages/broker-list/broker-list.page.ts");








const routes = [
    {
        path: '',
        component: _broker_list_page__WEBPACK_IMPORTED_MODULE_7__["BrokerListPage"]
    }
];
let BrokerListPageModule = class BrokerListPageModule {
};
BrokerListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild()
        ],
        declarations: [_broker_list_page__WEBPACK_IMPORTED_MODULE_7__["BrokerListPage"]]
    })
], BrokerListPageModule);



/***/ }),

/***/ "./src/app/pages/broker-list/broker-list.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/broker-list/broker-list.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: var(--ion-color-light);\n}\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium);\n}\n:host ion-card {\n  border-radius: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYnJva2VyLWxpc3QvQzpcXHhhbXBwXFxodGRvY3NcXHNvbHV0aXZvXFxhbWF1dGFtb3ZpbC9zcmNcXGFwcFxccGFnZXNcXGJyb2tlci1saXN0XFxicm9rZXItbGlzdC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Jyb2tlci1saXN0L2Jyb2tlci1saXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLG9DQUFBO0FDQVI7QURHSTtFQUNJLGdCQUFBO0VBQ0EsaURBQUE7QUNEUjtBRElJO0VBQ0ksZ0JBQUE7QUNGUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Jyb2tlci1saXN0L2Jyb2tlci1saXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgIH1cclxuXHJcbiAgICBpb24tY2FyZCB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgIH1cclxufSIsIjpob3N0IGlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xufVxuOmhvc3QgaW9uLWl0ZW0ge1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBib3JkZXItYm90dG9tOiAxcHggZG90dGVkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xufVxuOmhvc3QgaW9uLWNhcmQge1xuICBib3JkZXItcmFkaXVzOiAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/broker-list/broker-list.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/broker-list/broker-list.page.ts ***!
  \*******************************************************/
/*! exports provided: BrokerListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrokerListPage", function() { return BrokerListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");





let BrokerListPage = class BrokerListPage {
    constructor(navCtrl, brokerService) {
        this.navCtrl = navCtrl;
        this.brokerService = brokerService;
        // this.brokers = this.brokerService.findAll();
    }
};
BrokerListPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_3__["BrokerService"] }
];
BrokerListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-broker-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./broker-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/broker-list/broker-list.page.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('staggerIn', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: `translate3d(-100px,0,0)` }), { optional: true }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('500ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
                ])
            ])
        ],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./broker-list.page.scss */ "./src/app/pages/broker-list/broker-list.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _providers__WEBPACK_IMPORTED_MODULE_3__["BrokerService"]])
], BrokerListPage);



/***/ })

}]);
//# sourceMappingURL=pages-broker-list-broker-list-module-es2015.js.map