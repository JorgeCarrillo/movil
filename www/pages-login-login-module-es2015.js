(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"ion-padding animated fadeIn login auth-page\">\r\n  <div class=\"theme-bg\"></div>\r\n\r\n  <div class=\"back\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\r\n    </ion-buttons>\r\n  </div>\r\n\r\n  <div class=\"auth-content\">\r\n\r\n    <!-- Logo -->\r\n    <!--<div class=\"animated fadeInDown ion-text-center ion-padding-horizontal\">-->\r\n    <div class=\"animated fadeInDown ion-text-center ion-padding-horizontal\">\r\n      <br><br>\r\n      <div class=\"logo\"></div>\r\n      <h4 class=\"ion-no-margin\">\r\n        <ion-text color=\"light\" class=\"fw700\">\r\n          <!--{{ 'app.name' | translate }}-->\r\n          <!--<ion-text color=\"tertiary\">{{ 'app.nameb' | translate }}</ion-text>-->\r\n          {{ 'app.version' | translate }}\r\n        </ion-text>\r\n      </h4>\r\n    </div><br>\r\n\r\n    <!-- Login form -->\r\n    <form [formGroup]=\"onLoginForm\" class=\"list-form\">\r\n      <ion-item class=\"ion-no-padding animated fadeInUp\">\r\n        <ion-label position=\"floating\">\r\n          <ion-icon name=\"mail\" item-start></ion-icon>\r\n          {{ 'app.label.email' | translate }}\r\n        </ion-label>\r\n        <ion-input color=\"secondary\" type=\"email\" formControlName=\"nombre_login\"></ion-input>\r\n      </ion-item>\r\n      <p ion-text class=\"text08\"\r\n        *ngIf=\"onLoginForm.get('nombre_login').touched && onLoginForm.get('nombre_login').hasError('required')\">\r\n        <ion-text color=\"warning\">\r\n          {{ 'app.label.errors.field' | translate }}\r\n        </ion-text>\r\n      </p>\r\n\r\n      <ion-item class=\"ion-no-padding animated fadeInUp\">\r\n        <ion-label position=\"floating\">\r\n          <ion-icon name=\"lock\" item-start></ion-icon>\r\n          {{ 'app.label.password' | translate }}\r\n        </ion-label>\r\n        <ion-input color=\"secondary\" type=\"password\" formControlName=\"password_login\"></ion-input>\r\n      </ion-item>\r\n      <p ion-text color=\"warning\" class=\"text08\"\r\n        *ngIf=\"onLoginForm.get('password_login').touched && onLoginForm.get('password_login').hasError('required')\">\r\n        <ion-text color=\"warning\">\r\n          {{ 'app.label.errors.field' | translate }}\r\n        </ion-text>\r\n      </p>\r\n    </form>\r\n\r\n    <p tappable (click)=\"forgotPass()\" class=\"ion-text-right paz\">\r\n      <ion-text color=\"light\">\r\n        <strong>{{ 'app.pages.login.label.forgot' | translate }}</strong>\r\n      </ion-text>\r\n    </p>\r\n\r\n    <div>\r\n      <ion-button icon-left size=\"medium\" expand=\"full\" shape=\"round\" color=\"dark\" (click)=\"goToHome()\"\r\n        [disabled]=\"!onLoginForm.valid\" tappable>\r\n        <ion-icon name=\"log-in\"></ion-icon>\r\n        {{ 'app.button.signin' | translate }}\r\n      </ion-button>\r\n\r\n      <!--  <p class=\"ion-text-center\">\r\n        <ion-text color=\"light\">\r\n          O {{ 'app.button.signin' | translate }} con:\r\n        </ion-text>\r\n      </p>\r\n\r\n      <ion-grid class=\"btn-group\">\r\n        <ion-row>\r\n          <ion-col size=\"4\">\r\n            <ion-button shape=\"round\" expand=\"full\" color=\"secondary\">\r\n              <ion-icon slot=\"icon-only\" name=\"logo-facebook\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"4\">\r\n            <ion-button shape=\"round\" expand=\"full\" color=\"secondary\">\r\n              <ion-icon slot=\"icon-only\" name=\"logo-twitter\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"4\">\r\n            <ion-button shape=\"round\" expand=\"full\" color=\"secondary\">\r\n              <ion-icon slot=\"icon-only\" name=\"logo-googleplus\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>-->\r\n\r\n    </div>\r\n\r\n    <!-- Other links -->\r\n    <div class=\"ion-text-center ion-margin-top\">\r\n      <span (click)=\"goToRegister()\" class=\"paz\" tappable>\r\n        <ion-text color=\"light\">\r\n          Eres nuevo? <strong>{{ 'app.button.signup' | translate }}</strong>\r\n        </ion-text>\r\n      </span>\r\n    </div>\r\n\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");








const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_7__["LoginPage"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forChild()
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_7__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  /*ion-content {\n      --background: linear-gradient(135deg, #ffffff, #ffffff);\n  }*/\n}\n:host ion-content {\n  --background: linear-gradient(200deg, #e4e4e4 0%, #005b85 70%);\n}\n.back {\n  position: fixed;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  z-index: 0;\n  margin-top: 15px;\n  margin-left: 15px;\n  color: #ffffff;\n}\n.theme-bg {\n  position: fixed;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  z-index: 0;\n  opacity: 0.12;\n  background-image: url(\"/assets/img/ionproperty2-bg.jpg\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.paz {\n  position: relative;\n  z-index: 10;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vQzpcXHhhbXBwXFxodGRvY3NcXHNvbHV0aXZvXFxhbWF1dGFtb3ZpbC9zcmNcXGFwcFxccGFnZXNcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUtJOztJQUFBO0FDREo7QURISTtFQUVHLDhEQUFBO0FDSVA7QURJQTtFQUNJLGVBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FDREo7QURJQTtFQUNJLGVBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSx3REFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0RKO0FESUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7QUNESiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgIC8vIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgIzAwMDAwMCwgIzAwMDAwMCk7XHJcbiAgICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyMDBkZWcsICNlNGU0ZTQgMCUsICMwMDViODUgNzAlKTtcclxuICAgIH1cclxuICAgIC8qaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgI2ZmZmZmZiwgI2ZmZmZmZik7XHJcbiAgICB9Ki9cclxufVxyXG5cclxuXHJcbi5iYWNrIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxufVxyXG5cclxuLnRoZW1lLWJnIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBvcGFjaXR5OiAuMTI7IC8vLjEyXHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltZy9pb25wcm9wZXJ0eTItYmcuanBnXCIpO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcblxyXG4ucGF6IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDEwO1xyXG59XHJcbiIsIjpob3N0IHtcbiAgLyppb24tY29udGVudCB7XG4gICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxMzVkZWcsICNmZmZmZmYsICNmZmZmZmYpO1xuICB9Ki9cbn1cbjpob3N0IGlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCAjZTRlNGU0IDAlLCAjMDA1Yjg1IDcwJSk7XG59XG5cbi5iYWNrIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDA7XG4gIHotaW5kZXg6IDA7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cblxuLnRoZW1lLWJnIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDA7XG4gIHotaW5kZXg6IDA7XG4gIG9wYWNpdHk6IDAuMTI7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaW1nL2lvbnByb3BlcnR5Mi1iZy5qcGdcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5wYXoge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHotaW5kZXg6IDEwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _providers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers */ "./src/app/providers/index.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_usuario_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/usuario.service */ "./src/app/services/usuario.service.ts");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/fesm2015/auth0-angular-jwt.js");
/* harmony import */ var _services_envio_correo_service_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../services/envio-correo-service.service */ "./src/app/services/envio-correo-service.service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../app.component */ "./src/app/app.component.ts");













let LoginPage = class LoginPage {
    constructor(navCtrl, menuCtrl, toastCtrl, alertCtrl, loadingCtrl, translate, formBuilder, authSvc, router, _jwtHelper, _usuarioService, _correo, appcomponet) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.translate = translate;
        this.formBuilder = formBuilder;
        this.authSvc = authSvc;
        this.router = router;
        this._jwtHelper = _jwtHelper;
        this._usuarioService = _usuarioService;
        this._correo = _correo;
        this.appcomponet = appcomponet;
        this.fecha = new Date();
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(false);
    }
    ngOnInit() {
        this.onLoginForm = this.formBuilder.group({
            'nombre_login': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ])],
            'password_login': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ])]
        });
        const snapshot = this.router.routerState.snapshot;
        const root = snapshot.root;
        const queryParams = root.queryParams;
        if (queryParams.id && queryParams.autorizathion) {
            this.activarUsuario(queryParams);
        }
    }
    forgotPass() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: this.translate.get('app.pages.login.label.forgot'),
                message: this.translate.get('app.pages.login.text.forgot'),
                inputs: [
                    {
                        name: 'usuario',
                        type: 'email',
                        placeholder: this.translate.get('app.label.email')
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                        }
                    }, {
                        text: 'Confirm',
                        handler: (data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                            const loader = yield this.loadingCtrl.create({
                                duration: 2000
                            });
                            this.authSvc.usuarioRecuperar = data.usuario;
                            this.authSvc.crearTokenRecuperar(JSON.stringify(data)).subscribe(res => {
                                loader.present();
                                if (res) {
                                    loader.present();
                                    loader.onWillDismiss().then((l) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                        const toast = yield this.toastCtrl.create({
                                            showCloseButton: true,
                                            message: "Correo enviado correctamente",
                                            duration: 3000,
                                            position: 'bottom'
                                        });
                                        toast.present();
                                        this.router.navigate(["/recover-password"]);
                                    }));
                                }
                                else {
                                    loader.present();
                                    loader.onWillDismiss().then((l) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                        const toast = yield this.toastCtrl.create({
                                            showCloseButton: true,
                                            message: "No se encontró el correo",
                                            duration: 3000,
                                            position: 'bottom'
                                        });
                                        toast.present();
                                    }));
                                }
                            });
                        })
                    }
                ]
            });
            yield alert.present();
        });
    }
    // // //
    goToRegister() {
        this.navCtrl.navigateRoot('/register');
    }
    goToHome() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const login = this.onLoginForm.value;
            try {
                const token = yield this.authSvc.login(login.nombre_login, login.password_login);
                if (token && token.accessToken) {
                    const auxToken = Object.values(token)[0];
                    let user;
                    if (Object.values(token)[1]) {
                        user = Object.values(token)[1];
                    }
                    const decodeToken = this._jwtHelper.decodeToken(auxToken);
                    this.authSvc.setCurrentUser(crypto_js__WEBPACK_IMPORTED_MODULE_9__["AES"].encrypt(auxToken, _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].tokenKey).toString(), decodeToken, user);
                    _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].nombreUsuario = decodeToken.data.nombre_login;
                    this.appcomponet.obtenerdatos();
                    this.authSvc.enLinea(new Date(this.fecha.setMinutes(this.fecha.getMinutes() - 5)).toISOString(), false).subscribe(res => {
                    });
                    if (this.authSvc.getRol() === 'ESTUDIANTE') {
                        this.authSvc.getfoto();
                        if (JSON.parse(this.authSvc.getEstadoCuenta())) {
                            this.navCtrl.navigateRoot('/home-results');
                        }
                        else {
                            this.navCtrl.navigateRoot('/home-results');
                        }
                    }
                    if (this.authSvc.getRol() === 'PROFESOR') {
                        this.authSvc.getfoto();
                        if (JSON.parse(this.authSvc.getEstadoCuenta())) {
                            this.navCtrl.navigateRoot('/home-results');
                        }
                        else {
                            this.navCtrl.navigateRoot('/home-results');
                        }
                    }
                }
            }
            catch (error) {
                this.intentos += this.intentos + 1;
                console.log(error);
            }
        });
    }
    activarUsuario(params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const decodeToken = this._jwtHelper.decodeToken(params.autorizathion);
            const res = yield this._usuarioService.activeUser(params.id, params.autorizathion, decodeToken.data.nombre_rol);
            if (res) {
                this.router.navigate(['/login']);
                const toast = yield this.toastCtrl.create({
                    showCloseButton: true,
                    message: 'Inicie sesión para activar su cuenta.',
                    duration: 2000,
                    position: 'bottom'
                });
                toast.present();
            }
            else {
                const resPost = yield this._correo.postEnvioCreacion({
                    nombre_login: decodeToken.data.nombre_login,
                    id_usuario: Number(params.id),
                    nombre_rol: decodeToken.data.nombre_rol
                });
                if (resPost) {
                    alert('El enlace a expirado, se ha enviado nuevamente un correo de activación, recuerde que tiene 5 min para activar su cuenta');
                    yield this.router.navigate(['/pages/auth/mail-confirm', { email: decodeToken.data.nombre_login }]);
                }
            }
        });
    }
    checkUserIsVerified(user) {
        /*if (user && user.emailVerified) {
            this.router.navigate(['/apps/academy/courses']);
        } else if (user) {
            this.router.navigate(['/verification-email']);
        } else {
            this.router.navigate(['/pages/auth/register']);
        }*/
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _providers__WEBPACK_IMPORTED_MODULE_4__["TranslateProvider"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_10__["JwtHelperService"] },
    { type: _services_usuario_service__WEBPACK_IMPORTED_MODULE_8__["UsuarioService"] },
    { type: _services_envio_correo_service_service__WEBPACK_IMPORTED_MODULE_11__["EnvioCorreoServiceService"] },
    { type: _app_component__WEBPACK_IMPORTED_MODULE_12__["AppComponent"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _providers__WEBPACK_IMPORTED_MODULE_4__["TranslateProvider"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
        _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_10__["JwtHelperService"],
        _services_usuario_service__WEBPACK_IMPORTED_MODULE_8__["UsuarioService"],
        _services_envio_correo_service_service__WEBPACK_IMPORTED_MODULE_11__["EnvioCorreoServiceService"],
        _app_component__WEBPACK_IMPORTED_MODULE_12__["AppComponent"]])
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map