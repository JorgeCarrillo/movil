function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-titles-titles-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/titles/titles.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/titles/titles.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesTitlesTitlesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Amautak</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-card class=\"ion-no-margin\">\n    <h1 class=\"ion-text-center fw700\">\n      <ion-text color=\"dark\">TÍTULOS</ion-text>\n    </h1>\n  \n    <ion-grid fixed class=\"ion-no-padding\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-padding\">\n\n          <ion-list class=\"ion-margin-bottom\">\n            <ion-list-header color=\"light\">\n              <ion-label class=\"fw700\">Títulos Registrados</ion-label>\n            </ion-list-header>\n\n            <div *ngFor=\"let a of titulos\">\n            <ion-list>\n              <ion-item >\n                <ion-label class=\"fw700\"><p style=\"color: black;\">{{a.senescyt_titulos_profe}}</p><p style=\"color: black;\">{{a.nivel_profesor}}</p></ion-label>\n\n                <div class=\"item-note\" item-end>\n                  <ion-button size=\"small\" color=\"primary\"  (click)=\"updateTitulos(a)\">\n                      <ion-icon name=\"create\"></ion-icon>\n                  </ion-button>\n                  <ion-button size=\"small\" color=\"danger\" (click)=\"deleteTitulo(a)\">\n                      <ion-icon name=\"trash\"></ion-icon>\n                  </ion-button>\n                  \n              </div>\n              </ion-item>\n              \n            </ion-list>\n            </div><br><br>\n\n\n            \n\n            <ion-list-header color=\"light\">\n              <ion-label class=\"fw700\">Registrar Nuevos Títulos</ion-label>\n            </ion-list-header>\n\n            <br><br>\n            <ion-item>\n              <ion-label color=\"dark\" position=\"stacked\">Nombre Título:</ion-label>\n              <ion-input [(ngModel)]=\"nombre\"  placeholder=\"\" >\n              </ion-input>\n            </ion-item>\n\n            <br><br>\n            <ion-item>\n              <ion-label color=\"dark\" position=\"stacked\">Nivel Título:</ion-label>\n              <ion-input [(ngModel)]=\"nivel\"  placeholder=\"\" >\n              </ion-input>\n            </ion-item>\n           \n          </ion-list>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-button (click)=\"enviartitulos()\" size=\"large\" expand=\"full\" color=\"dark\"  class=\"ion-no-margin\">\n     GUARDAR TÍTULOS</ion-button>\n  </ion-card>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/titles/titles.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/pages/titles/titles.module.ts ***!
    \***********************************************/

  /*! exports provided: TitlesPageModule */

  /***/
  function srcAppPagesTitlesTitlesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TitlesPageModule", function () {
      return TitlesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _titles_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./titles.page */
    "./src/app/pages/titles/titles.page.ts");

    var routes = [{
      path: '',
      component: _titles_page__WEBPACK_IMPORTED_MODULE_7__["TitlesPage"]
    }];

    var TitlesPageModule = function TitlesPageModule() {
      _classCallCheck(this, TitlesPageModule);
    };

    TitlesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(), _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_titles_page__WEBPACK_IMPORTED_MODULE_7__["TitlesPage"]]
    })], TitlesPageModule);
    /***/
  },

  /***/
  "./src/app/pages/titles/titles.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/pages/titles/titles.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesTitlesTitlesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RpdGxlcy90aXRsZXMucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/titles/titles.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/pages/titles/titles.page.ts ***!
    \*********************************************/

  /*! exports provided: TitlesPage */

  /***/
  function srcAppPagesTitlesTitlesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TitlesPage", function () {
      return TitlesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _providers__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../providers */
    "./src/app/providers/index.ts");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");

    var TitlesPage = /*#__PURE__*/function () {
      function TitlesPage(propertyService, route, _authService, _httpClient, navCtrl, alertController) {
        _classCallCheck(this, TitlesPage);

        this.propertyService = propertyService;
        this.route = route;
        this._authService = _authService;
        this._httpClient = _httpClient;
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.obtenerdatos();
        this.traertitulos(this.properID);
      }

      _createClass(TitlesPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "obtenerdatos",
        value: function obtenerdatos() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (this._authService.getIdUsuarioRol() === null) {
                      this.properID = 'null';
                    } else {
                      this.properID = this._authService.getIdUsuarioRol();
                    }

                  case 1:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "traertitulos",
        value: function traertitulos(properID) {
          var _this = this;

          this.titulos = [];
          var header = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this._authService.getToken()
            })
          };

          this._httpClient.get(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'titulos-profesor/usuario/' + properID, header).subscribe(function (restitulos) {
            _this.titulos = restitulos;
          }, function (error) {});
        }
      }, {
        key: "updateTitulos",
        value: function updateTitulos(data1) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this2 = this;

            var header, alert;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };
                    _context2.next = 3;
                    return this.alertController.create({
                      header: 'Editar Titulo',
                      message: 'Escriba su nuevo titulo para actualizar.',
                      inputs: [{
                        name: 'senescyt_titulos_profe',
                        'placeholder': data1.senescyt_titulos_profe,
                        'value': data1.senescyt_titulos_profe
                      }, {
                        name: 'nivel_profesor',
                        'placeholder': data1.nivel_profesor,
                        'value': data1.nivel_profesor
                      }],
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Actualizar',
                        handler: function handler(data) {
                          _this2._httpClient.put(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'titulos-profesor/' + data1.id_titulos_profe, data, header).subscribe(function (restitulos) {
                            _this2.traertitulos(data1.id_usuario_rol);
                          }, function (error) {});
                        }
                      }]
                    });

                  case 3:
                    alert = _context2.sent;
                    alert.present();

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "deleteTitulo",
        value: function deleteTitulo(i) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this3 = this;

            var header, alert;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };
                    _context3.next = 3;
                    return this.alertController.create({
                      header: 'Eliminar Titulo',
                      message: 'Esta seguro de eliminar este titulo:',
                      inputs: [{
                        name: 'senescyt_titulos_profe',
                        'disabled': true,
                        'placeholder': i.senescyt_titulos_profe,
                        'value': i.senescyt_titulos_profe
                      }, {
                        name: 'nivel_profesor',
                        'disabled': true,
                        'placeholder': i.nivel_profesor,
                        'value': i.nivel_profesor
                      }],
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Aceptar',
                        handler: function handler(data) {
                          _this3._httpClient["delete"](_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'titulos-profesor/' + i.id_titulos_profe, header).subscribe(function (restitulos) {
                            _this3.traertitulos(_this3.properID);
                          }, function (error) {});
                        }
                      }]
                    });

                  case 3:
                    alert = _context3.sent;
                    alert.present();

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "enviartitulos",
        value: function enviartitulos() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this4 = this;

            var header, aux, body, alert, _alert;

            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    header = {
                      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                      })
                    };
                    aux = "TITULO ";
                    body = {
                      senescyt_titulos_profe: aux + this.nombre,
                      id_usuario_rol: +this.properID,
                      nivel_profesor: this.nivel
                    };

                    if (!(body.senescyt_titulos_profe == "TITULO undefined" || body.nivel_profesor == "" || body.nivel_profesor == undefined)) {
                      _context4.next = 10;
                      break;
                    }

                    _context4.next = 6;
                    return this.alertController.create({
                      header: 'Alerta Titulo',
                      message: 'Porfavor llena todos los campos.',
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Aceptar',
                        handler: function handler(data) {}
                      }]
                    });

                  case 6:
                    alert = _context4.sent;
                    alert.present();
                    _context4.next = 14;
                    break;

                  case 10:
                    _context4.next = 12;
                    return this.alertController.create({
                      header: 'Agregar Titulo',
                      message: 'Agregar este Titulo a tus estudios.',
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel'
                      }, {
                        text: 'Agregar',
                        handler: function handler(data) {
                          _this4._httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].url + 'titulos-profesor/', body, header).subscribe(function (restitulos) {
                            _this4.traertitulos(body.id_usuario_rol);

                            _this4.nivel = "";
                            _this4.nombre = "";
                            _this4.propertyService.faltaTitulos = false;

                            _this4.navCtrl.pop();
                          }, function (error) {});
                        }
                      }]
                    });

                  case 12:
                    _alert = _context4.sent;

                    _alert.present();

                  case 14:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }]);

      return TitlesPage;
    }();

    TitlesPage.ctorParameters = function () {
      return [{
        type: _providers__WEBPACK_IMPORTED_MODULE_7__["PropertyService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }];
    };

    TitlesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-titles',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./titles.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/titles/titles.page.html"))["default"],
      animations: [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["trigger"])('staggerIn', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["transition"])('* => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["style"])({
        opacity: 0,
        transform: "translate3d(100px,0,0)"
      }), {
        optional: true
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["animate"])('500ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_8__["style"])({
        opacity: 1,
        transform: "translate3d(0,0,0)"
      }))]), {
        optional: true
      })])])],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./titles.page.scss */
      "./src/app/pages/titles/titles.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers__WEBPACK_IMPORTED_MODULE_7__["PropertyService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]])], TitlesPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-titles-titles-module-es5.js.map